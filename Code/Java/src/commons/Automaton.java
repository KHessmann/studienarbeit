package commons;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import generation.AutomatonMatrix;

public class Automaton {
	private UUID id = UUID.randomUUID();
	private Set<Terminal> alphabet = new LinkedHashSet<>();
	private List<State> states = new ArrayList<>();
	private List<Transition> transitions = new ArrayList<>();
	private State initial;
	private List<State> accepting = new ArrayList<>();

	public Automaton() {
	}

	public Automaton(List<State> states, List<Transition> transitions) {
		this.transitions = new ArrayList<>(transitions);
		boolean isInAlphabet = false;
		states.sort(Comparator.comparing(State::getName));
		for (State state : states) {
			if (!this.states.contains(state)) {
				this.states.add(state);
			} else {
				throw new IllegalArgumentException("The list of states should only contain every state once.");
			}
		}
		if (transitions != null)
			for (Transition t : transitions) {
				Set<Terminal> terminals = t.getTerminals();
				for (Terminal terminal : terminals) {
					Terminal alphabetTerminal = new Terminal(terminal);
					for (Terminal a : alphabet) {
						if (a.equals(alphabetTerminal)) {
							isInAlphabet = true;
						}
					}
					if (!isInAlphabet) {
						alphabet.add(terminal);
					}
					isInAlphabet = false;
				}
			}
		if (states != null) {
			for (State s : states) {
				if (s.isInitial()) {
					this.initial = s;
				}
				if (s.isAccepting()) {
					accepting.add(s);
				}
			}
		}
		if (initial != null) {
			states.remove(initial);
			states.add(0, initial);
		}
	}

	public Automaton(Automaton automaton) {
		this(automaton, false);
	}

	public Automaton(Automaton automaton, boolean rename) {
		boolean isInAlphabet = false;
		this.states = new ArrayList<State>();
		this.transitions = new ArrayList<Transition>();
		Map<State, State> oldToNew = new HashMap<>();
		int i = 1;
		for (State state : automaton.getStates()) {
			String newName = null;
			if (state.isInitial()) {
				newName = rename ? "q" + 0 : null;
			} else {
				newName = rename ? "q" + i : null;
				i++;
			}
			State newState = new State(state, newName);
			oldToNew.put(state, newState);
			this.states.add(newState);
			if (newState.isAccepting()) {
				accepting.add(newState);
			}
			if (newState.isInitial()) {
				initial = newState;
			}
		}
		for (Transition transition : automaton.getTransitions()) {
			State start = oldToNew.get(transition.getStartState());
			State end = oldToNew.get(transition.getEndState());
			Set<Terminal> terminals = transition.getTerminals();
			Set<Terminal> newTerminals = new HashSet<>();
			for (Terminal terminal : terminals) {
				newTerminals.add(new Terminal(terminal));
				Terminal alphabetTerminal = new Terminal(terminal);
				for (Terminal a : alphabet) {
					if (a.equals(alphabetTerminal)) {
						isInAlphabet = true;
					}
				}
				if (!isInAlphabet) {
					alphabet.add(terminal);
				}
				isInAlphabet = false;
			}
			Transition newTransition = new Transition(newTerminals, start, end, transition.hasEpsilon());
			this.transitions.add(newTransition);
		}
	}

	public UUID getId() {
		return id;
	}

	public List<State> getStates() {
		return states;
	}

	public List<Transition> getTransitions() {
		return transitions;
	}

	public Set<Terminal> getAlphabet() {
		return alphabet;
	}

	public void removeTerminalType(TerminalType type) {
		List<Terminal> removeTerminals = alphabet.stream().filter(terminal -> terminal.getType() == type)
				.collect(Collectors.toList());
		if (type == TerminalType.SEQUENCE) {
			for (Terminal terminal : removeTerminals) {
				char[] terminalChar = terminal.getTerminal();
				List<Terminal> singles = alphabet.stream().filter(a -> a.getType() == TerminalType.SINGLE)
						.collect(Collectors.toList());
				boolean isInAlphabet = false;
				for (char c : terminalChar) {
					for (Terminal t : singles) {
						if (t.getTerminal()[0] == c) {
							isInAlphabet = true;
						}
					}
					if (!isInAlphabet) {
						alphabet.add(new Terminal(TerminalType.SINGLE, c));
					}
				}
			}
		}
		alphabet.removeAll(removeTerminals);
	}

	public void addTerminal(Terminal terminal) {
		alphabet.add(terminal);
	}

	public boolean addTransition(Transition transition) {
		if (!transitions.contains(transition)) {
			return transitions.add(transition);
		}
		return false;
	}

	public boolean addAllTransitions(List<Transition> transitions) {
		for (Transition transition : transitions) {
			boolean addTransition = addTransition(transition);
			if (!addTransition) {
				return false;
			}
		}
		return true;
	}

	public boolean addState(State state) {
		if (!states.contains(state)) {
			if (state.isInitial()) {
				initial = state;
			}
			if (state.isAccepting()) {
				accepting.add(state);
			}
			return states.add(state);
		}
		return false;
	}

	public void addAccepting(State state) {
		if (states.contains(state) && !accepting.contains(state)) {
			accepting.add(state);
		}
	}

	public boolean removeTransition(Transition transition) {
		return transitions.remove(transition);
	}

	public void removeTransitions(List<Transition> transitions) {
		if (this.transitions.containsAll(transitions)) {
			this.transitions.removeAll(transitions);
		} else {
			throw new IllegalArgumentException("At least one of the transitions is not part of the automaton.");
		}
	}

	public boolean removeState(State state) {
		if (state.isInitial()) {
			initial = null;
		}
		accepting.remove(state);
		return states.remove(state);
	}

	public State getInitial() {
		return initial;
	}

	public List<State> getAccepting() {
		return accepting;
	}

	public AutomatonMatrix getAutomatonMatrix() {
		return new AutomatonMatrix(states, transitions);
	}

	public TransitionTable getTransitionTable() {
		return new TransitionTable(states, transitions, alphabet);
	}

	public int getNumberTerminalOccurences() {
		int i = 0;
		for (Transition t : transitions) {
			i += t.getTerminals().size();
		}
		return i;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		sb.append("#states \n");
		if (!states.isEmpty()) {
			List<State> initialStates = new ArrayList<>();
			List<State> finalStates = new ArrayList<>();
			for (State s : states) {
				sb.append(s.toString() + "; ");
				if (s.isInitial()) {
					initialStates.add(s);
				}
				if (s.isAccepting()) {
					finalStates.add(s);
				}
			}
			sb.deleteCharAt(sb.length() - 1);
			sb.deleteCharAt(sb.length() - 1);
			sb.append("\n");
			sb.append("#initial \n");
			if (!initialStates.isEmpty()) {
				for (State s : initialStates) {
					sb.append(s.toString() + "; ");
				}
				sb.deleteCharAt(sb.length() - 1);
				sb.deleteCharAt(sb.length() - 1);
			}
			sb.append("\n");
			sb.append("#accepting \n");
			if (!finalStates.isEmpty()) {
				for (State s : finalStates) {
					sb.append(s.toString() + "; ");
				}
				sb.deleteCharAt(sb.length() - 1);
				sb.deleteCharAt(sb.length() - 1);
			}
		}
		sb.append("\n");
		sb.append("#alphabet \n");
		if (!alphabet.isEmpty()) {
			List<Terminal> alph = new ArrayList<>(alphabet);
			alph.sort(Comparator.comparing(Terminal::toString));
			for (Terminal t : alph)
				sb.append(t.toString() + "; ");
			sb.deleteCharAt(sb.length() - 1);
			sb.deleteCharAt(sb.length() - 1);
		}
		sb.append("\n");
		sb.append("#transitions ");
		if (!transitions.isEmpty()) {
			transitions.sort(Comparator.comparing(Transition::toString));
			for (Transition t : transitions) {
				sb.append(t.toString());
			}
			sb.deleteCharAt(sb.length() - 1);
		}

		return sb.toString();
	}

	public void setInitial(State state) {
		if (this.initial != null && state != null && !this.initial.equals(state)) {
			throw new IllegalArgumentException("There should not be two initial states at the same time");
		}
		this.initial = state;
	}

}
