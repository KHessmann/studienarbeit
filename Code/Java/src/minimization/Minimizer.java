package minimization;

import commons.Automaton;

public interface Minimizer {
	public Automaton minimize();

	public int getEffort();
}
