package commons;

import java.util.Comparator;
import java.util.List;
import java.util.UUID;

public class State implements Comparable<State> {
	private UUID id;
	private String name;
	private boolean isInitial;
	private boolean isAccepting;
	public static String cummulatedStartChar = "{";
	public static String cummulatedEndChar = "}";
	public static String delimiter = ",";
	public static final String emptyState = "\u00D8";

	public State(String name, boolean isInitial, boolean isAccepting) {
		id = UUID.randomUUID();
		this.name = name;
		this.isInitial = isInitial;
		this.isAccepting = isAccepting;
	}

	public static State createEmptyState() {
		return new State(emptyState, false, false);
	}

	public State(State state) {
		this(state, state.getName());
	}

	public State(State state, String newName) {
		id = UUID.randomUUID();
		if (newName != null) {
			this.name = newName;
		} else {
			this.name = state.getName();
		}
		this.isInitial = state.isInitial();
		this.isAccepting = state.isAccepting();
	}

	/*
	 * This creates a new State out of a list of states if it contains at least one
	 * state. If the list contains more then one state, the property initial is not
	 * considered however if at least one state is accepting the new one is as well
	 */
	public State(List<State> states) {
		this(states, false);
	}

	public State(List<State> states, boolean overwriteInitial) {
		if (states.size() == 1) {
			State state = states.get(0);
			id = UUID.randomUUID();
			this.name = state.getName();
			this.isInitial = state.isInitial();
			this.isAccepting = state.isAccepting();
		} else if (states.size() > 1) {
			states.sort(Comparator.comparing(State::getName));
			boolean accepting = false;
			boolean initial = false;
			StringBuilder sb = new StringBuilder();
			sb.append(cummulatedStartChar);
			for (State s : states) {
				sb.append(s.getName());
				sb.append(delimiter);
				accepting = accepting || s.isAccepting();
				if (overwriteInitial)
					initial = initial || s.isInitial();
			}
			sb.deleteCharAt(sb.length() - 1);
			sb.append(cummulatedEndChar);
			id = UUID.randomUUID();
			this.name = sb.toString();
			this.isInitial = initial;
			this.isAccepting = accepting;
		}
	}

	public UUID getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isInitial() {
		return isInitial;
	}

	public void setInitial(boolean isInitial) {
		this.isInitial = isInitial;
	}

	public boolean isAccepting() {
		return isAccepting;
	}

	public void setAccepting(boolean isAccepting) {
		this.isAccepting = isAccepting;
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public boolean equals(Object anObject) {
		if (this == anObject) {
			return true;
		}
		if (anObject instanceof State) {
			State state = (State) anObject;
			return (name.equals(state.getName()) && isAccepting == state.isAccepting && isInitial == state.isInitial);
		}
		return false;
	}

	@Override
	public int compareTo(State s) {
		return this.getName().compareTo(s.getName());
	}

}
