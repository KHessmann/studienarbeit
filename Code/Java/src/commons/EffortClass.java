package commons;

public enum EffortClass {
	/*
	 * Add own Effort Classes if you need them
	 */
	VERY_SMALL("very easy", 0, 19), SMALL("easy", 20, 39), MEDIUM("medium", 40, 79), BIG("hard", 80, 119),
	VERY_BIG("very hard", 120, Integer.MAX_VALUE), RANDOM("random", 0, Integer.MAX_VALUE);

	private final String level;
	private final int minValue;
	private final int maxValue;

	private EffortClass(String level, int minValue, int maxValue) {
		this.level = level;
		this.minValue = minValue;
		this.maxValue = maxValue;
	}

	public static EffortClass getClassForEffortValue(int effort) {
		EffortClass result = null;
		for (EffortClass e : values()) {
			if (!e.equals(RANDOM) && effort <= e.maxValue && effort >= e.minValue) {
				result = e;
				break;
			}

		}
		if (result == null) {
			result = EffortClass.RANDOM;
		}
		return result;
	}

	public String getName() {
		return level;
	}

	public int getMinValue() {
		return minValue;
	}

	public int getMaxValue() {
		return maxValue;
	}
}
