package commons;

public enum TerminalType {
	SINGLE, SEQUENCE, EPSILON, NONE
};
