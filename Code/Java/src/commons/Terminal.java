package commons;

import java.util.Arrays;

public final class Terminal {

	private TerminalType type;
	private final char[] terminal;
	public static final char epsilon = '\u03B5';

	public Terminal(TerminalType type, char... a) {
		if (type == TerminalType.EPSILON) {
			terminal = new char[] { epsilon };
		} else {
			terminal = a;
		}
		this.type = type;
	}

	public Terminal(Terminal t) {
		terminal = t.getTerminal();
		type = t.getType();
	}

	public TerminalType getType() {
		return type;
	}

	public char[] getTerminal() {
		return terminal;
	}

	@Override
	public String toString() {
		String terminalString = "";
		for (char c : terminal) {
			terminalString += c;
		}
		return terminalString;
	}

	@Override
	public boolean equals(Object anObject) {
		if (this == anObject) {
			return true;
		}
		if (anObject instanceof Terminal) {
			Terminal terminal = (Terminal) anObject;
			return type.equals(terminal.getType()) && Arrays.equals(this.terminal, terminal.getTerminal());
		}
		return false;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 31 * hash + (null == type ? 0 : type.hashCode());
		int terminalHash = 0;
		for (char c : terminal) {
			terminalHash += c;
		}
		hash = 31 * hash + terminalHash;
		return hash;
	}
}
