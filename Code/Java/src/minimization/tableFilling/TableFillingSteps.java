package minimization.tableFilling;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import commons.Automaton;
import commons.State;
import commons.StateTuple;
import helper.TableStringBuilder;

public class TableFillingSteps {

	private Automaton automaton;
	private Automaton finalAutomaton;
	private Map<StateTuple, Set<StateTuple>> predecessors = new HashMap<>();
	private List<StateTuple> startQueue = new ArrayList<>();
	private List<StateTuple> finalQueue = new ArrayList<>();
	private Map<StateTuple, Boolean> startEquivalenceTable = new LinkedHashMap<>();
	private Map<StateTuple, Boolean> finalEquivalenceTable = new LinkedHashMap<>();

	public TableFillingSteps(Automaton a) {
		automaton = a;
	}

	public Automaton getAutomaton() {
		return automaton;
	}

	public boolean isUnset() {
		return predecessors.isEmpty() || startQueue.isEmpty() || finalQueue.isEmpty() || startEquivalenceTable.isEmpty()
				|| finalEquivalenceTable.isEmpty();
	}

	public String getPredecessors() {
		StringBuilder sb = new StringBuilder();
		sb.append("predecessors: \n");
		List<StateTuple> predecessorKeyList = new ArrayList<>(predecessors.keySet());
		predecessorKeyList.sort(Comparator.comparing(StateTuple::toString));
		for (StateTuple tuple : predecessorKeyList) {
			List<StateTuple> predecessorValueList = new ArrayList<>(predecessors.get(tuple));
			if (!predecessorValueList.isEmpty()) {
				predecessorValueList.sort(Comparator.comparing(StateTuple::toString));
				sb.append(tuple.toString() + ": ");
				for (StateTuple t : predecessorValueList) {
					sb.append(t.toString() + ", ");
				}
				sb.deleteCharAt(sb.length() - 2);
				sb.append("\n");
			}
		}
		return sb.toString();
	}

	public void setPredecessors(Map<StateTuple, Set<StateTuple>> predecessors) {
		this.predecessors = predecessors;
	}

	public String getStartQueue() {
		StringBuilder sb = new StringBuilder();
		sb.append("Queue after accepting not accepting check: \n");
		for (StateTuple tuple : startQueue) {
			sb.append(tuple.toString() + ", ");
		}
		if (!startQueue.isEmpty()) {
			sb.deleteCharAt(sb.length() - 2);
		}
		return sb.toString();
	}

	public void setStartQueue(List<StateTuple> startQueue) {
		this.startQueue = startQueue;
	}

	public String getFinalQueue() {
		StringBuilder sb = new StringBuilder();
		sb.append("Final queue: \n");
		for (StateTuple tuple : finalQueue) {
			sb.append(tuple.toString() + ", ");
		}
		if (!finalQueue.isEmpty()) {
			sb.deleteCharAt(sb.length() - 2);
		}
		return sb.toString();
	}

	public void setFinalQueue(List<StateTuple> finalQueue) {
		this.finalQueue = finalQueue;
	}

	public String getStartEquivalenceTable() {
		return convertEquivalenceTableToString(startEquivalenceTable);
	}

	public void setStartEquivalenceTable(Map<StateTuple, Boolean> startEquivalenceTable) {
		this.startEquivalenceTable = startEquivalenceTable;
	}

	public String getFinalEquivalenceTable() {
		return convertEquivalenceTableToString(finalEquivalenceTable);
	}

	public void setFinalEquivalenceTable(Map<StateTuple, Boolean> finalEquivalenceTable) {
		this.finalEquivalenceTable = finalEquivalenceTable;
	}

	private String convertEquivalenceTableToString(Map<StateTuple, Boolean> equivalenceTable) {
		List<State> states = new ArrayList<>();
		if (automaton != null) {
			states = automaton.getStates();
		}
		int size = states.size();
		String[][] table = new String[size][size];
		table[0][0] = "States";
		for (int i = 0; i < size - 1; i++) {
			table[0][i + 1] = states.get(size - 1 - i).getName();
			table[i + 1][0] = states.get(i).getName();
		}
		String input = " ";
		for (int i = 0; i < size - 1; i++) {
			for (int j = 0; j < size - 1; j++) {
				if (i < size - 1 - j) {
					StateTuple tuple = new StateTuple(states.get(i), states.get(size - 1 - j));
					if (!equivalenceTable.get(tuple)) {
						input = "x";
					}
				} else {
					input = "-";
				}
				table[i + 1][j + 1] = input;
				input = " ";
			}
		}
		return TableStringBuilder.getTableString(table);
	}

	public void setFinalAutomaton(Automaton automaton) {
		finalAutomaton = automaton;
	}

	public Automaton getFinalAutomaton() {
		return finalAutomaton;
	}

	public String getInfo(int step) {
		StringBuilder sb = new StringBuilder();
		if (step >= 1) {
			sb.append("\nTABLE FILLING ALGORITHM \n");
			sb.append(getPredecessors());
		}
		if (step >= 2) {
			sb.append(getStartQueue() + "\n");
			sb.append(getStartEquivalenceTable());
		}
		if (step >= 3) {
			sb.append(getFinalQueue() + "\n");
			sb.append(getFinalEquivalenceTable());

		}

		return sb.toString();
	}
}
