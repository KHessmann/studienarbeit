\chapter{Isomorphie}\label{chap:8Isomorphie}
In Kapitel \ref{chap:7Generierung} wurde ein Algorithmus zur Generierung wertvoller Aufgaben vorgestellt, welcher die didaktischen und fachlichen Anforderungen, die in den vorhergehenden Kapiteln gestellt wurden, erfüllt.  Das Verständnis für Vorgehens- und Funktionsweise eines Verfahrens festigt sich durch die häufige Bearbeitung desselben Aufgabentyps angewandt auf unterschiedliche Automaten. Dieses Kapitel dient dem Zweck, die für verschiedene Aufgaben generierten Automaten auf wiederkehrende Strukturen zu überprüfen. So soll sichergestellt werden, dass auch bei wiederholter Bearbeitung der Aufgaben durchweg neue Automaten generiert werden.
Für ein aussagekräftiges Ergebnis soll für den Vergleich nur die Struktur der Übergangsdiagramme berücksichtigt werden.   

Die Problemstellung dieses Kapitels besteht darin, die Übergangsdiagramme der generierten Automaten ohne Berücksichtigung ihrer Gewichte (Eingabesymbole) paarweise auf Isomorphie zu überprüfen. 
Das so genannte Graph-Isomorphismus Problem spielt in verschiedenen Teilgebieten der Informatik wie etwa Machine Learning und Data Mining eine wichtige Rolle \cite[1]{Omos.2005}. Das Problem besteht darin, eine passende Zuordnung zwischen den Mengen an Knoten und Kanten zweier Graphen zu finden (siehe auch Abschnitt \ref{sec:21Graphentheorie}). Der damit verbundene Aufwand steigt mit der Größe und Anzahl paarweise zu vergleichender Graphen. 

Bei dem Vergleich der ungewichteten Graphen der Übergangsdiagramme sollten für jeden Aufgabentyp und jede Aufwandsklasse mindestens 50.000 Automaten generiert und verglichen werden, um ein aussagekräftiges Ergebnis zu erzielen. Auch wenn die generierten DEAs mit bis zu zehn Zuständen vergleichsweise klein sind, kann der Aufwand einer paarweisen Überprüfung durch diese Anzahl sehr groß werden. 
\clearpage
\section{Eine mögliche Lösung}\label{sec:81Problem}
Es wird ein effizienter Algorithmus gesucht, der eine große Anzahl Graphen paarweise auf Isomorphie prüfen kann. 
\subsubsection*{Grundidee}
Den Grundgedanken für diese Lösung liefert der Algorithmus \enquote{GraphComparator} des Softwareprojekts \citetitle{Philippsen.16.03.2020}. ParSeMiS löst das Problem durch einen rekursiven Algorithmus, welcher Knoten unter Beachtung der Kantenbeschriftung paarweise zuordnet. Um Rechenaufwand einzusparen, wird vor Beginn des Algorithmus die Ordnung und Kantenanzahl der Graphen abgeglichen \cite{Philippsen.16.03.2020}.
\subsubsection*{Anpassung des Algorithmus von ParSeMiS}
Die implementierte Lösung unterscheidet sich von ParSeMiS in zwei grundlegenden Eigenschaften. Zum einen sollen die untersuchten Graphen ohne Beachtung ihrer Gewichte verglichen werden. Das erhöht die Anzahl ähnlicher Strukturen, jedoch auch den Aufwand bei der Untersuchung. Aufgrund dessen und wegen der hohen Zahl zu prüfender Automaten soll zudem die Bedingung vor und während der Ausführung des Algorithmus angepasst werden. 

Wenn für die Statistiken 50.000 Automaten erzeugt und verglichen werden sollen, so bedeutet dies für den letzten generierten Automat, dass für den zugehörige Graph 49.999 Überprüfungen durchgeführt werden müssen. Ziel ist es, viele Eigenschaften der Graphen vor der Überprüfung auf Isomorphie bereits abzugleichen, damit möglichst wenige Durchläufe des eigentlichen Algorithmus benötigt werden. Dazu wird die Möglichkeit ausgenutzt, Graphen durch Adjazenzmatrizen zu repräsentieren.

\subsection{Abgleichen der definierenden Eigenschaften zweier Graphen}
Für einen Graphen können neben der Ordnung und Kantenanzahl weitere definierende Eigenschaften bestimmt werden, wie etwa den Eingangs- und Ausgangsgrad seiner Knoten oder die Anzahl Schleifen (siehe auch Abschnitt \ref{sec:21Graphentheorie}). All diese Informationen können aus der zugehörigen Adjazenzmatrix ausgelesen werden. Um den Abgleich zweier Graphen zu erleichtern, wird die zugehörige Adjazenzmatrix mithilfe dieser Werte nach dem Generieren nach folgenden Regeln neu sortiert:
\begin{enumerate}
	\item Die Knoten werden zunächst nach Ausgangsgrad sortiert. Der Knoten mit den meisten ausgehenden Kanten steht an erster Position, die anderen folgen entsprechend.
	\item Knoten mit gleichem Ausgangsgrad werden ihrem Eingangsgrad entsprechend sortiert
	\item Knoten mit identischen Eingangs- und Ausgangsgrad werden danach sortiert, ob sie eine Schleife besitzen oder nicht.
\end{enumerate}

Die Implementierung dieses Regelwerks wird im Folgenden erläutert. Es sei dabei vorausgesetzt, dass die Adjazenzmatrix bereits bekannt ist. Zudem müssen drei Vektoren ermittelt werden, die Ausgangs- und Eingangsgrad sowie die Schleifenanzahl repräsentieren. Diese können aus den Summen der Zeilen-, Spalten- und Diagonalelemente errechnet werden \cite[15\psq]{Paramadevan.2021}. Der Pseudocode dafür ist im Anhang zu finden (siehe \reflst{code:81Vektor}), ein Beispiel in \ref{exmp:81Isomorphie1}. 
\newpassage
\begin{exmp}{81Isomorphie1}
	In \reffig{fig:81Isomorphie1} sind ein Graph A mit fünf Knoten und sieben Kanten sowie die dazu gehörige Adjazenzmatrix zu sehen. Die drei gesuchten Vektoren ergeben sich zu
	\begin{align*}
	&&&\text{Permutation $\pi_0$:} &[0~ 1~ 2~ 3~ 4]&&&&\\
	&&&\text{Ausgangsgrad:} &[1~ 1~ 1~ 1~ 3]&&&&\\
	&&&\text{Eingangsgrad:} &[2~ 2~ 1~ 2~ 0]&&&&\\
	&&&\text{Schleifen:} &[0~ 1~ 0~ 0~ 0]&&&&
	\end{align*} 
\input{./images/81Isomorphie1.tex}
	Der erste Eintrag der drei Vektoren kann für die ursprüngliche Reihenfolge $\pi_0$ jeweils dem Knoten $0$ zugeordnet werden. Da $0$ den Ausgangsgrad 1 und Eingangsgrad 2 besitzt, sowie keine Schleife ergeben sich die Einträge 1, 2 und 0. Dies entspricht den Zeilen- bzw. Spaltensummen und dem Wert des Diagonaleintrags $a_{0,0}$.
\end{exmp}
\newpassage
Diese drei Vektoren können im Anschluss verwendet werden, um eine den Regeln folgende Reihenfolge der Knoten zu bestimmen und die Adjazenzmatrix neu anzuordnen. Dazu muss mit \reflst{code:81Perm} die Permutation $\pi$ bestimmt werdenn. 
\begin{enumerate}
	\item Zeilen 5-8: zunächst wird eine leere Liste erstellt, welche die Knotenreihenfolge repräsentiert. Der Einfachheit halber wird jedem Knoten eine Nummer entsprechend seiner Position in der Adjazenzmatrix zugeordnet. Die Permutations-Liste wird entsprechend anfangs mit [0,1,...n-1] belegt, wobei n der Ordnung des Graphen entspricht.
	\item Zeilen 9-11: Jetzt werden die Knoten entsprechend ihres Ausgangsgrades sortiert. 
	Dafür wird \reflst{code:81AGPerm} im Anhang genutzt, welcher nach dem Insertion Sort Algorithmus vorgeht.
	\item Zeilen 12-20+31: Alle Knoten, welche anhand ihres Ausgangsgrads nicht eindeutig in eine feste Reihenfolge gebracht werden können, werden im Anschluss entsprechend ihres Eingangsgrades sortiert. Auch dieser hier aufgerufene Algorithmus ist im Anhang zu finden (siehe \reflst{code:81EGPerm}). 
	\item Zeilen 21-30: Dieselbe Vorgehensweise wird für die Knoten wiederholt, welche sich nicht durch ihren Ausgangs- und Eingangsgrad in eine feste Reihenfolge bringen lassen. In \reflst{code:81SPerm} im Anhang wird danach sortiert, ob ein Knoten eine Schleife besitzt.
	\item Zeile 32: Nachdem alle Überprüfungen durchgeführt wurden, wird die sortierte Permutations-Liste zurückgegeben, welche die neue Reihenfolge der Knoten vorgibt.
\end{enumerate}
\clearpage

\input{./code/81Perm.tex}

 Mithilfe der Permutation $\pi$ kann die Adjazenzmatrix $AM$ neu angeordnet werden. 
 Dafür wird die zugehörige Permutationsmatrix sowie ihr Inverses benötigt. Da Permutationsmatrizen orthogonal sind, gilt $P^{-1} = P^T$, was die Berechnung etwas vereinfacht \cite[279]{Knabner.2018}. Es gilt 
 \begin{flalign*}
 	&&&& ~~~~~AM_{neu} &= P^{-1} \cdot AM \cdot P &&\\
 	&&&&\overset{P^{-1}=P^T} \Longrightarrow ~~AM_{neu} &= P^T \cdot AM \cdot P &&\text{\cite[16]{Paramadevan.2021}.}
 \end{flalign*}
 Der Algorithmus für die Bestimmung der Permutationsmatrix für eine Permutation $\pi$ ist im Anhang unter \reflst{code:81PermMatrix} aufgeführt. In der Literatur und im Netz finden sich viele Algorithmen zur Matrixmultiplikation und Bestimmung der Transponierten einer Matrix, weshalb darauf in dieser Arbeit nicht weiter eingegangen wird. 
 \newpassage
\begin{exmp}{81Isomorphie2}
	Die Adjazenzmatrix aus Beispiel \ref{exmp:81Isomorphie1} soll den Vorgaben entsprechend sortiert werden. 
	\begin{enumerate}
		\item Dazu wird zunächst der Vektor für den Ausgangsgrad neu sortiert. Der Knoten $4$ wird auf die erste Position gesetzt.
	\begin{align*}
		&&&\text{Permutation $\pi_1$:} &[\textcolor{javaPurple}{\bm{4}}~ 0~ 1~ 2~ 3]&&&&\\
		&&&\text{Ausgangsgrad:} &[\textcolor{javaPurple}{\bm{3}}~ 1~ 1~ 1~ 1]&&&&\\
		&&&\text{Eingangsgrad:} &[{0}~ 2~ 2~ 1~ 2]&&&&\\
		&&&\text{Schleifen:} &[{0}~ 0~ 1~ 0~ 0]&&&&
	\end{align*} 
	\item Die Knoten $0$, $1$, $2$ und $3$ besitzen denselben Ausgangsgrad und müssen weiter unterschieden werden. Da $2$ darunter den kleinsten Eingangsgrad besitzt, erhält dieser Knoten die letzte Position.
	\begin{align*}
		&&&\text{Permutation $\pi_2$:} &[4~ 0~ 1~ 3~ \textcolor{javaPurple}{\bm{2}}]&&&&\\
		&&&\text{Ausgangsgrad:} &[3~ 1~ 1~ 1~ {1}]&&&&\\
		&&&\text{Eingangsgrad:} &[0~ 2~ 2~ 2~ \textcolor{javaPurple}{\bm{1}}]&&&&\\
		&&&\text{Schleifen:} &[0~ 0~ 1~ 0~ {0}]&&&&
	\end{align*} 
	\item Anhand ihrer Eigenschaften können die Knoten $0$, $1$ und $3$ bisher noch nicht in eine feste Reihenfolge gebracht werden und sollen zuletzt danach unterschieden werden, ob sie eine Schleife besitzen oder nicht. Dies ist nur bei $1$ der Fall. 
	\begin{align*}
		&&&\text{Permutation $\pi_3$:} &[4~ \textcolor{javaPurple}{\bm{1}}~ 0~ 3~ 2]&&&&\\
		&&&\text{Ausgangsgrad:} &[3~ {1}~ 1~ 1~ 1]&&&&\\
		&&&\text{Eingangsgrad:} &[0~ {2}~ 2~ 2~ 1]&&&&\\
		&&&\text{Schleifen:} &[0~ \textcolor{javaPurple}{\bm{1}}~ 0~ 0~ 0]&&&&
	\end{align*} 

	\end{enumerate}
	Damit endet das Verfahren. Der sortierte und entsprechend seiner Reihenfolge umbenannte Graph $A'$ sowie die zugehörige Adjazenzmatrix ist in \reffig{fig:81Isomorphie2} zu sehen. Die bestimmte Reihenfolge der Knoten ist nicht eindeutig, da neben $\pi_3 = [4~ 1~ 0~ 3~ 2]$ auch $\pi_3' = [4~ 1~ 3~ 0~ 2]$ eine gültige Lösung wäre. 
	\input{./images/81Isomorphie2.tex}
\end{exmp}
Wenn Graph A$'$ aus Beispiel \ref{exmp:81Isomorphie2} mit einem der anderen 49999 generierten verglichen wird, so kann mithilfe der soeben vorgestellten Methode abgeglichen werden, ob neben der Ordnung auch die drei sortierten Vektoren der beiden Graphen übereinstimmen. In diesem Fall wird der Algorithmus zur Prüfung auf Isomorphie durchgeführt, auf welchen im Folgenden eingegangen werden soll. Sonst wird das Verfahren an dieser Stelle abgebrochen. 
\subsection{Überprüfung zweier Graphen auf Isomorphie}
\reflst{code:81Isomorphie} zeigt den Pseudocode für die Prüfung zweier Graphen auf Isomorphie. Es wird hierbei vorausgesetzt, dass die beiden Adjazenzmatrizen bereits zu einem früheren Zeitpunkt in eine korrekte Reihenfolge gebracht wurden. 
\begin{enumerate}
	\item Zeilen 5-7: Zunächst werden Ordnung und die drei relevanten Vektoren der Matrizen überprüft. Stimmen diese nicht überein, so können auch die Graphen nicht isomorph sein.
	\item Zeilen 8f.: Anschließend werden die Adjazenzmatrizen auf Gleichheit überprüft. Sind beide Matrizen identisch, so ist eine weitere Überprüfung nicht notwendig \cite[16]{Paramadevan.2021}.
	\item Zeile 10: Bei zwei isomorphen Graphen stimmen Ausgangsgrad, Eingangsgrad und Anzahl Schleifen paarweise zugeordneter Knoten überein. Anhand dieser Informationen kann bestimmt werden, welche Knoten für eine Zuordnung in Betracht kommen. Um den Aufwand bei der anschließenden Überprüfung auf Isomorphie gering zu halten, soll damit im Vorhinein eine Liste vertauschbarer Knoten ermittelt werden. Der Pseudocode zur Erzeugung einer solchen Tauschliste ist im Anhang unter \reflst{code:81Tausch} aufgeführt. In Beispiel \ref{exmp:82Tausch} ist die Tauschliste für Graph $A'$ dargestellt.
	\item Zeilen 11-14: Zuletzt werden die Knoten beginnend mit der ersten Position rekursiv abgeglichen. Der Pseudocode dafür findet sich in \reflst{code:81IsomorphieRek}.
\end{enumerate}
\input{./code/81Isomorphie.tex}
\begin{exmp}{82Tausch}
	In \reffig{fig:81Isomorphie2a} ist die Position der Knoten $2$ und $3$ des Graphen $A'$ nicht eindeutig. Die Knoten an dieser Position besitzen dieselben Werte für Eingangs- und Ausgangsgrad sowie keine Schleifen. Wird Graph $A'$ mit einem  ebenfalls sortierten Graph $B'$ abgeglichen, so kann nur diesen beiden Knoten kein eindeutiger Partner zugeordnet werden. Alle anderen Zuordnungen stehen bereits zu Beginn der Durchführung des Algorithmus fest. Die vertauschbaren Knoten können aus der Tauschliste $(0,1,\textcolor{javaPurple}{\bm{2,2}},3)$ ausgelesen werden. Die Knoten $2$ und $3$ gehören derselben Gruppe (2) an.
\end{exmp}
\newpassage

Im Folgenden sollen die Schritte des Algorithmus \ref{code:81IsomorphieRek} im Detail erläutert werden.
\begin{enumerate}
	\item Zeilen 5f.: Die Knoten des ersten Graphen werden aufeinanderfolgend Knoten des zweiten Graphen zugeordnet. Wird Zeile 6 erreicht, so bedeutet dies, dass für alle Knoten $k1$ genau ein zugehöriger Knoten gefunden wurde. Beide Graphen sind damit isomorph.
	\item Zeilen 7-8: Der Knoten $k1$ kann all den Knoten $k2$ des zweiten Graphen zugeordnet werden, die innerhalb der Tauschliste derselben Gruppe angehören. Zusätzlich darf $k2$ in diesem Durchlauf nicht bereits einem anderen Knoten zugeordnet worden sein.  
	\item Zeilen 9-13: Für jeden in Frage kommenden Knoten $k2$ müssen im Anschluss die Einträge in den Adjazenzmatrizen verglichen werden. Der Vergleich der Matrixeinträge entspricht einem Abgleich der eingehenden und ausgehenden Kanten des Tupels. Sind alle bisherigen Zuordnungen inklusive ($k1,k2$) korrekt, so müssen auch die Matrixeinträge bis zur aktuellen Position übereinstimmen. Ist dies nicht der Fall, so ist entweder die Zuordnung von $k1$ zu $k2$ inkorrekt, oder bereits eine vorherige.
	\item Zeilen 14-19: Stimmen alle überprüfbaren Kanten überein, wird $k1$ dem Knoten $k2$ zugeordnet. Anschließend kann der nächste Knoten $k1+1$ untersucht werden. 
	\item Zeile 20: Die Überprüfung scheitert, wenn für keine mögliche Zuordnung alle Einträge der Adjazenzmatrizen übereinstimmen. 
\end{enumerate}
\input{./code/81IsomorphieRek.tex}
\begin{exmp}{82Vergleich}
	Zur Veranschaulichung des Algorithmus ist in \reffig{fig:81Isomorphie3a} ein bereits sortierter Graph $B$ zu sehen. Dieser soll nun mit dem Graph $A'$ abgeglichen werden. Die drei Vektoren für Ausgangsgrad, Eingangsgrad und die Schleifen der Knoten stimmen mit denen des Graphen $A'$ überein.
		\begin{align*}
		&&&\text{Ausgangsgrad:} &[3~ {1}~ 1~ 1~ 1]&&&&\\
		&&&\text{Eingangsgrad:} &[0~ {2}~ 2~ 2~ 1]&&&&\\
		&&&\text{Schleifen:} &[0~ 1~ 0~ 0~ 0]&&&&
	\end{align*} 
	 
	 Mit der Tauschliste $(0,1,2,2,3)$ gilt, dass alle Knoten aus $A'$ gemäß ihrer Reihenfolge den Knoten aus $B$ zugeordnet werden müssen. Die einzige Ausnahme bilden die Knoten $2$ und $3$, für welche die Zuordnungen $(2,2_B), (3,3_B)$ oder $(2,3_B), (3,2_B)$ möglich sind. Die Überprüfung scheitert bereits im zweiten Durchlauf durch die Kante $0_B \rightarrow 1_B$. Es gilt
	\begin{center}
		$AM_{A'}[0][1]=0~~ \neq ~~1=AM_{B}[0][1]$
	\end{center}
	Da laut der Tauschliste keine alternativen Zuordnungen zu dem Knoten $1$ möglich sind, sind Graph $A'$ und $B$ nicht isomorph.
	\input{./images/81Isomorphie3.tex}
\end{exmp}

\subsubsection*{Anmerkung zur Implementierung}
Aus Gründen der Performance bietet es sich an, die Algorithmen \hyperref[code:81Vektor]{erzeugeVektoren} und \hyperref[code:81Tausch]{erzeugeTauschliste} statt wie in \reflst{code:81Isomorphie} bei jedem einzelnen Vergleich nur einmal beim erstmaligen Sortieren der Graphen aufzurufen. Die Ergebnisse der Berechnung können anschließend für alle zukünftigen Vergleiche abgespeichert und wiederverwendet werden. Selbiges gilt auch für die Berechnung der Ordnung. 

\section{Ergebnis}\label{sec:82Ergebnis}
Mit dem in diesem Kapitel eingeführten Algorithmus ist es möglich, eine große Menge Graphen paarweise auf Isomorphie zu prüfen. Das soll in diesem Abschnitt genutzt werden, um zu überprüfen dass genügend unterschiedliche Automaten pro Aufgabentyp und Aufwandsklasse generiert werden. Mit Hinblick auf die erwarteten Einsatzgebiete beschränken sich die untersuchten Aufwandsklassen auf B, C und D, was Aufwänden im Bereich von 20 bis 119 Schritten entspricht. Pro Aufgabentyp und Klasse werden jeweils 50.000 Automaten generiert und die unbeschrifteten Graphen paarweise auf Isomorphie überprüft. 

Sowohl bei der Minimierung als auch bei der Transformation werden die DEAs untersucht, welche sich durch die Potenzmengenkonstruktion ergeben. Das entspricht bei der Minimierung dem Automaten, welchen der Anwender zu Beginn erhält und bei der Transformation dem Ergebnis. Grund hierfür ist unter anderem, dass diese DEAs bei der Generierung durch die Angabe der gewünschten Zustandsanzahl beeinflusst werden können. Die Ergebnisse, aufgeteilt nach Aufgabentyp, werden im Folgenden vorgestellt.

\subsubsection*{Ergebnisse für die Transformation}
Bei den im Rahmen der Transformation untersuchten Automaten handelt es sich um die DEAs, welche durch die Potenzmengenkonstruktion ermittelt werden. Die generierten NEAs werden nicht untersucht, da durch die kleine Zustandsanzahl (hier 5) und die Beschränkung der erlaubten Übergänge (20) die Anzahl strukturell unterschiedlicher Graphen eingeschränkt ist. Durch den Nichtdeterminismus ergeben sich daraus viele unterschiedliche DEAs, welche hier von größerem Interesse sind. Die erlaubte Zustandsanzahl der DEAs wird für diese Datensätze auf 3-10 gesetzt. \reftab{tab:82IsoT} zeigt die Ergebnisse der Untersuchung für die drei Aufwandsklassen.

Die Tabelle wurde für Graphen mit drei bis zehn Knoten untersucht und ist in die drei Aufwandsklassen B, C und D eingeteilt.
Die jeweils erste Spalte gibt die Summe generierter DEAs für eine Zustands (bzw. Knoten) -Zahl an. 
Die zweite Spalte $n_{Struktur}$ zeigt die aufgetretene Anzahl sich in ihrer Struktur unterscheidender Graphen. $s_{max}$ gibt für die Struktur, welche am häufigsten aufgetreten ist, die Anzahl dazu isomorpher Graphen an. 
\input{./tables/82IsoT.tex}

Bei der Interpretation der Datensätze sei darauf hingewiesen, dass die Anzahl unterschiedlicher Graphen in Relation zu der jeweiligen Anzahl generierter Automaten mit passender Knotenanzahl $k$ gesetzt werden muss. In \reffig{fig:82IsoT} ist zu diesem Zweck die durchschnittliche Anzahl isomorpher Graphen pro Struktur inklusive der Standardabweichung aufgetragen. Wie zu erwarten gilt allgemein, dass mit zunehmender Knotenanzahl die mögliche Anzahl Graphen unterschiedlicher Struktur ansteigt. Damit nimmt auch die aufgetretene Anzahl isomorpher Graphen pro Struktur ab. Gegenteiliges gilt für Graphen mit wenigen Knoten. 
\newpassage
Bei der Aufwandsklasse C beispielsweise sind von den 892 Graphen etwa ein Viertel isomorph zu der am häufigsten vorkommenden Struktur $s_{max}$. Insgesamt treten in dem generierten Datensatz nur 13 unterschiedliche Strukturen auf. Die durchschnittliche Anzahl isomorpher Graphen pro Struktur kann mit 70 aus dem Diagramm abgelesen werden und ist ebenfalls relativ hoch. Gerechnet auf die Gesamtanzahl generierter Automaten fällt der Anteil mit 3 Zuständen jedoch relativ klein aus (etwa 1.7\%). Auch ohne Beachtung der Kantenbeschriftung wird es daher voraussichtlich recht lange dauern, bis ein Anwender zwei Mal denselben Automaten mit drei Zuständen generiert. Ähnliches gilt für Automaten mit vier Zuständen. Sollte man jedoch auch diese Fälle ausschließen wollen, so empfiehlt es sich DEAs mit weniger als fünf Zuständen bei der Aufgabengenerierung zu vermeiden. 
\input{./images/82IsoT.tex}
\subsubsection*{Ergebnisse für die Minimierung}
Bei der Minimierung ist der DEA von Interesse, welchen der Anwender zu Beginn erhält, da dieser bei der Generierung durch die erlaubte Zustandszahl beeinflusst werden kann. Die erlaubte Zustandszahl des ausgehenden DEAs wird für die Datensätze in \reftab{tab:82IsoM} auf 5-10 gesetzt. 
\input{./tables/82IsoM.tex}

Die in \reffig{fig:82IsoM} angezeigten Durchschnittswerte für die Anzahl isomorpher Graphen pro auftretender Struktur sind mit Ausnahme von Aufwandsklasse B durchweg im einstelligen Bereich. Auch der Wert $s_{max}$ ist in Relation zu der zugehörigen Automatenanzahl für die meisten Klassen und Knoten klein genug. Die Knotenanzahl 5 und 6 der Aufwandsklasse B weisen sich häufiger wiederholende Strukturen auf. Da der Großteil der generierten Automaten in diesen Bereich fällt, sind diese Werte im Verhältnis jedoch ausreichend klein und stellen kein größeres Problem dar. 
\input{./images/82IsoM.tex}
\newpassage
Insgesamt kann davon ausgegangen werden, dass mit dem in Kapitel \ref{chap:7Generierung} vorgestellten Algorithmus unabhängig von Aufgabentyp und Aufwandsklasse keine merkbare Anzahl an Wiederholungen bei der Automatengenerierung auftritt. Mit der hinzukommenden Beschriftung durch die Ergänzung der Eingabesymbole wird dies zusätzlich gewährleistet. 
