package statistic.structure;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class IsomorphismEquivalenceClass {
	private SimpleMatrix oldSM;
	private int[][] newMatrix;
	private int order;
	private int[] fromTransitions;
	private int[] toTransitions;
	private int[] loopTransitions;
	private int[] switchable;

	public IsomorphismEquivalenceClass(SimpleMatrix oldSM) {
		this.oldSM = oldSM;
		order = oldSM.getOrder();
		fromTransitions = oldSM.getFromTransitions();
		toTransitions = oldSM.getToTransitions();
		loopTransitions = oldSM.getLoopTransitions();
		switchable = getSwitchableStates();
	}

	public boolean contains(SimpleMatrix newSM) {
		newMatrix = newSM.getStructureMatrix();

		if (!preconditionCheck(newSM)) {
			return false;
		}
		return containsWithPreconditionsChecked(newMatrix);

	}

	public boolean containsWithPreconditionsChecked(int[][] newMatrix) {
		this.newMatrix = newMatrix;
		if (isSameMatrix()) {
			return true;
		}

		return isomorphismCheck();
	}

	private boolean isSameMatrix() {
		return SimpleMatrixHelper.equals(oldSM.getStructureMatrix(), newMatrix);
	}

	private boolean isomorphismCheck() {
		boolean isIsomorph = false;
		Map<Integer, Integer> stateMap = new HashMap<>();
		int posA = 0;
		isIsomorph = recursive(stateMap, posA);

		return isIsomorph;
	}

	private boolean recursive(Map<Integer, Integer> stateMap, int stateA) {
		if (stateA == order)
			return true;
		int[][] oldMatrix = oldSM.getStructureMatrix();
		for (int stateB = 0; stateB < order; stateB++) {
			if (switchable[stateA] != switchable[stateB]) {
				continue;
			}
			if (!stateMap.containsValue(stateB)) {
				boolean match = true;
				for (int t = 0; match && t < stateA; ++t) {
					if (oldMatrix[t][stateA] != newMatrix[stateMap.get(t)][stateB]
							|| oldMatrix[stateA][t] != newMatrix[stateB][stateMap.get(t)]) {
						match = false;
					}
				}
				if (match) {
					stateMap.put(stateA, stateB);
					if (recursive(stateMap, stateA + 1)) {
						return true;
					}
					stateMap.remove(stateA);
				}
			}
		}
		return false;
	}

	/*
	 * returns array which includes switchable states e.g.[0,0,0,1,2] means the
	 * first 3 states can be switched, the others have to stay that way
	 */
	private int[] getSwitchableStates() {
		int[] result = new int[order];
		if (order > 0) {
			int state = 0;
			int equiClass = 0;
			result[state] = equiClass;
			while (state < order - 1) {
				state++;
				if (toTransitions[state] != toTransitions[state - 1]
						|| fromTransitions[state] != fromTransitions[state - 1]
						|| loopTransitions[state] != loopTransitions[state - 1]) {
					equiClass++;
				}
				result[state] = equiClass;
			}
		}
		return result;
	}

	private boolean preconditionCheck(SimpleMatrix newSM) {
		boolean check = true;
		check = isSameOrder(newSM.getOrder());
		check = check && isSameToTransitions(newSM.getToTransitions());
		check = check && isSameFromTransitions(newSM.getFromTransitions());
		check = check && isSameLoopTransitions(newSM.getLoopTransitions());
		return check;
	}

	private boolean isSameLoopTransitions(int[] newLoopTransitions) {
		return Arrays.equals(loopTransitions, newLoopTransitions);
	}

	private boolean isSameFromTransitions(int[] newFromTransitions) {
		return Arrays.equals(fromTransitions, newFromTransitions);
	}

	private boolean isSameToTransitions(int[] newToTransitions) {
		return Arrays.equals(toTransitions, newToTransitions);
	}

	private boolean isSameOrder(int newOrder) {
		return (order == newOrder);
	}

}
