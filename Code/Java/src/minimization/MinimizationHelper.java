package minimization;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import commons.Automaton;
import commons.State;
import commons.Terminal;
import commons.Transition;
import commons.TransitionTable;

public class MinimizationHelper {

	public static Automaton createNewAutomaton(Automaton oldAutomaton, Map<State, Set<State>> equivalentStates) {
		Set<EquivalenceClass> ec = new HashSet<>();
		for (Map.Entry<State, Set<State>> entry : equivalentStates.entrySet()) {
			ec.add(new EquivalenceClass(new ArrayList<>(entry.getValue()), entry.getKey()));
		}
		return createNewAutomaton(oldAutomaton, ec);

	}

	public static Automaton createNewAutomaton(Automaton oldAutomaton, Set<EquivalenceClass> equivalenceClasses) {
		TransitionTable table = oldAutomaton.getTransitionTable();
		Map<String, Transition> transitionMap = new HashMap<>();
		Map<State, State> statesMap = new HashMap<>();
		for (EquivalenceClass ec : equivalenceClasses) {
			statesMap.put(ec.getSubstitute(), new State(ec.getStates(), true));
		}
		List<State> states = new ArrayList<>(statesMap.values());
		for (EquivalenceClass ec : equivalenceClasses) {
			for (Terminal a : oldAutomaton.getAlphabet()) {
				State startState = ec.getSubstitute();
				Set<State> toStates = new HashSet<>();
				for (State toState : table.getEndStatesFromTransitionsForState(startState, a)) {
					toStates.add(find(toState, equivalenceClasses));
				}
				for (State endState : toStates) {
					State start = statesMap.get(startState);
					State end = statesMap.get(endState);
					String name = start.getName() + ", " + end.getName();
					Transition transitionForStates = transitionMap.computeIfAbsent(name, key -> new Transition(a, start, end, false));
                    transitionForStates.addTerminal(a);
				}
			}
		}
		return new Automaton(states, new ArrayList<>(transitionMap.values()));
	}

	public static void make(State state, Set<EquivalenceClass> equivalentStates) {
		List<State> singleton = new ArrayList<>();
		singleton.add(state);
		equivalentStates.add(new EquivalenceClass(singleton, state));
	}

	public static State find(State s, Set<EquivalenceClass> equivalentStates) {
		for (EquivalenceClass ec : equivalentStates) {
			if (ec.getStates().contains(s))
				return ec.getSubstitute();
		}
		throw new IllegalArgumentException("There exists no equivalence class for this state.");
	}

	public static void union(State s1, State s2, Set<EquivalenceClass> equivalentStates) {
		if (s1.equals(s2))
			return;
		List<State> set = null;
		List<State> set2 = null;
		EquivalenceClass ec2 = null;
		for (EquivalenceClass ec : equivalentStates) {
			if (ec.containsState(s1)) {
				set = ec.getStates();
			}
			if (ec.containsState(s2)) {
				set2 = ec.getStates();
				ec2 = ec;
			}
			if (set != null && set2 != null)
				break;
		}

		if (set != null && set2 != null) {
			set.addAll(set2);
			equivalentStates.remove(ec2);
		} else {
			throw new IllegalArgumentException("Both states have to be representatives of an equivalence class!");
		}
	}

}
