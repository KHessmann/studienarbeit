package comparison;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import commons.Automaton;
import commons.State;
import commons.Terminal;
import commons.TerminalType;
import commons.TransitionTable;

public class WordGenerationCompareHelper {
	private final static int maxWordLength = 20;

	public static boolean compare(Automaton a, Automaton a2) {
		return getFalseStringsForNEAs(a, a2, true, false, 1).isEmpty();
	}

	public static boolean acceptsSameLanguage(Automaton nea1, Automaton nea2, boolean onlyTrueAccepting,
			int precisionLevel) {
		return getFalseStringsForNEAs(nea1, nea2, onlyTrueAccepting, false, precisionLevel).isEmpty();
	}

	/*
	 * 
	 */
	/**
	 * @param nea1:               a nondeterministic or deterministic automaton
	 * @param nea2:               a second nondeterministic or deterministic
	 *                            automaton
	 * @param onlyTrueAccepting:  should also those witness strings be returned that
	 *                            end in dead states or only those who are accepted
	 *                            by the automaton
	 * @param allWitnessRequired: are all witness strings required or only the first
	 *                            one that fails
	 * @param precisionLevel      the degree to which the automaton is tested. This
	 *                            increases the cost exponentially, so the default
	 *                            value should be 1 at most times.
	 * @return two automaton mapped to those witness strings which should but are
	 *         not accepted by them Automaton nea1 is mapped to these of its strings
	 *         which nea2 doesn't accept and vice versa
	 */
	public static Map<Automaton, Map<Boolean, List<String>>> getFalseStringsForNEAs(Automaton nea1, Automaton nea2,
			boolean onlyTrueAccepting, boolean allWitnessRequired, int precisionLevel) {
		Map<Automaton, Map<Boolean, List<String>>> result = new HashMap<>();
		Map<Boolean, Set<String>> isAcceptingStringMap1 = generateAllStringsFromNea(nea1, onlyTrueAccepting,
				precisionLevel);
		Map<Boolean, List<String>> witnessForNEA1 = getWitness(isAcceptingStringMap1, nea2, onlyTrueAccepting,
				allWitnessRequired);
		Map<Boolean, Set<String>> isAcceptingStringMap2 = generateAllStringsFromNea(nea2, onlyTrueAccepting,
				precisionLevel);
		Map<Boolean, List<String>> witnessForNEA2 = getWitness(isAcceptingStringMap2, nea1, onlyTrueAccepting,
				allWitnessRequired);
		if (!witnessForNEA1.isEmpty())
			result.put(nea1, witnessForNEA1);
		if (!witnessForNEA2.isEmpty())
			result.put(nea2, witnessForNEA2);
		return result;
	}

	private static Map<Boolean, List<String>> getWitness(Map<Boolean, Set<String>> isAcceptingStringMap, Automaton nea,
			boolean onlyTrueAccepting, boolean allWitnessRequired) {
		Map<Boolean, List<String>> witness = new HashMap<>();
		for (String s : isAcceptingStringMap.get(Boolean.TRUE)) {
			if (!allWitnessRequired && !witness.isEmpty()) {
				break;
			}
			if (!doesNEAAcceptString(s, nea, true)) {
				if (!witness.containsKey(Boolean.TRUE))
					witness.put(Boolean.TRUE, new ArrayList<>());
				witness.get(Boolean.TRUE).add(s);
			}
		}
		if (!onlyTrueAccepting) {
			for (String s : isAcceptingStringMap.get(Boolean.FALSE)) {
				if (!allWitnessRequired && !witness.isEmpty()) {
					break;
				}
				if (!doesNEAAcceptString(s, nea, false)) {
					if (!witness.containsKey(Boolean.FALSE))
						witness.put(Boolean.FALSE, new ArrayList<>());
					witness.get(Boolean.FALSE).add(s);
				}
			}
		}
		return witness;
	}

	public static boolean doesNEAAcceptString(String s, Automaton nea, boolean shouldAccept) {
		char[] terminals = s.toCharArray();
		State currentState = nea.getInitial();
		TransitionTable table = nea.getTransitionTable();
		Set<Terminal> alphabet = nea.getAlphabet();
		return recursiveCheckString(currentState, terminals, 0, table, alphabet, shouldAccept, new ArrayList<>());
	}

	private static boolean recursiveCheckString(State currentState, char[] terminals, int currentPosition,
			TransitionTable table, Set<Terminal> alphabet, boolean shouldAccept, List<State> usedEpsilon) {

		if (currentPosition == terminals.length) {
			return lastStepCheckString(currentState, terminals, currentPosition, table, alphabet, shouldAccept,
					new ArrayList<>());
		}
		for (Terminal t : alphabet) {
			int positionShift = getPositionShift(t, terminals, currentPosition);
			if (positionShift > -1) {
				for (State state : table.getEndStatesFromTransitionsForState(currentState, t)) {
					boolean addedEpsilon = false;
					List<State> newUsedEpsilon;
					if (positionShift == 0) {
						if (usedEpsilon.contains(state)) {
							continue;
						}
						newUsedEpsilon = new ArrayList<>(usedEpsilon);
						newUsedEpsilon.add(state);
						addedEpsilon = true;
					} else {
						newUsedEpsilon = new ArrayList<>();
					}
					if (recursiveCheckString(state, terminals, currentPosition + positionShift, table, alphabet,
							shouldAccept, newUsedEpsilon)) {
						return true;
					}
					if (addedEpsilon) {
						newUsedEpsilon.remove(state);
					}
				}
			}
		}
		return false;
	}

	private static int getPositionShift(Terminal t, char[] terminals, int currentPosition) {
		TerminalType type = t.getType();
		int result = -1;
		if (type == TerminalType.SINGLE) {
			if (t.getTerminal()[0] == terminals[currentPosition]) {
				result = 1;
			}
		} else if (type == TerminalType.SEQUENCE) {
			int sequenceLength = t.getTerminal().length;
			if (currentPosition + sequenceLength > terminals.length)
				return -1;
			String s = new String(t.getTerminal());
			String s2 = new String(terminals, currentPosition, sequenceLength);
			if (s.equals(s2)) {
				result = sequenceLength;
			}
		} else if (type == TerminalType.EPSILON) {
			result = 0;
		}
		return result;
	}

	private static boolean lastStepCheckString(State currentState, char[] terminals, int currentPosition,
			TransitionTable table, Set<Terminal> alphabet, boolean shouldAccept, List<State> usedEpsilonStates) {
		if (shouldAccept && currentState.isAccepting() || !shouldAccept && !currentState.isAccepting())
			return true;
		for (Terminal t : alphabet) {
			if (t.getType() == TerminalType.EPSILON) {
				for (State state : table.getEndStatesFromTransitionsForState(currentState, t)) {
					if (!usedEpsilonStates.contains(state)) {
						List<State> newUsedEpsilonList = new ArrayList<>(usedEpsilonStates);
						newUsedEpsilonList.add(state);
						if (lastStepCheckString(state, terminals, currentPosition, table, alphabet, shouldAccept,
								newUsedEpsilonList)) {
							return true;
						}
					}
				}
			}
		}
		return false;
	}

	public static Map<Boolean, Set<String>> generateAllStringsFromNea(Automaton nea, boolean onlyTrueAccepting,
			int precisionLevel) {
		Map<Boolean, Set<String>> result = new HashMap<>();
		result.put(Boolean.TRUE, new HashSet<>());
		if (!onlyTrueAccepting) {
			result.put(Boolean.FALSE, new HashSet<>());
		}
		List<State> stateChain = new ArrayList<>();
		State currentState = nea.getInitial();
		Set<Terminal> alphabet = nea.getAlphabet();
		TransitionTable table = nea.getTransitionTable();
		String currentString = "";

		recursiveStringCreator(currentString, currentState, stateChain, alphabet, table, result, onlyTrueAccepting,
				precisionLevel);
		if (!onlyTrueAccepting) {
			clearDuplicatedWitnesses(result, nea);
		}

		return result;
	}

	private static void clearDuplicatedWitnesses(Map<Boolean, Set<String>> result, Automaton nea) {
		Set<String> newNonAccepting = new HashSet<>();
		for (String s : result.get(Boolean.FALSE)) {
			if (!result.get(Boolean.TRUE).contains(s) && !doesNEAAcceptString(s, nea, true)) {
				newNonAccepting.add(s);
			}
		}
		result.replace(Boolean.FALSE, newNonAccepting);

	}

	private static void recursiveStringCreator(String currentString, State currentState, List<State> stateChain,
			Set<Terminal> alphabet, TransitionTable table, Map<Boolean, Set<String>> result, boolean onlyTrueAccepting,
			int precisionLevel) {

		if (currentString.length() > maxWordLength) {
			return;
		}
		if (Collections.frequency(stateChain, currentState) > precisionLevel)
			return;
		List<State> chain = new ArrayList<>(stateChain);
		chain.add(currentState);

		if (currentState.isAccepting()) {
			result.get(Boolean.TRUE).add(currentString);
		} else if (!onlyTrueAccepting) {
			boolean isDeadState = true;
			if (table.getTransitionTable().containsKey(currentState)) {
				for (List<State> states : table.getTransitionTable().get(currentState).values()) {
					if (!states.isEmpty() && !(states.size() == 1 && states.contains(currentState))) {
						isDeadState = false;
						break;
					}
				}
			}
			if (isDeadState) {
				result.get(Boolean.FALSE).add(currentString);
			}
		}
		for (Terminal t : alphabet) {
			String c;
			switch (t.getType()) {
			case SINGLE:
			case SEQUENCE:
				StringBuilder sb = new StringBuilder(currentString);
				for (int i = 0; i < t.getTerminal().length; i++) {
					sb.append(t.getTerminal()[i]);
				}
				c = sb.toString();
				break;
			case EPSILON:
			case NONE:
			default:
				c = currentString;
				break;

			}
			for (State s : table.getEndStatesFromTransitionsForState(currentState, t)) {
				recursiveStringCreator(c, s, chain, alphabet, table, result, onlyTrueAccepting, precisionLevel);
			}
		}
	}

	public static String getPossibleStrings(Automaton nea, int precisionLevel) {
		Map<Boolean, Set<String>> isAcceptingStringMap1 = generateAllStringsFromNea(nea, false, precisionLevel);
		StringBuilder sb = new StringBuilder();
		sb.append("Accepted Strings: \n");
		sb.append("{");
		if (!isAcceptingStringMap1.get(Boolean.TRUE).isEmpty()) {
			int k = 0;
			for (String s : isAcceptingStringMap1.get(Boolean.TRUE)) {
				sb.append(s + ", ");
				k += s.length() + 2;
				if (k > 400) {
					k = 0;
					sb.append("\n");
				}
			}
			sb.deleteCharAt(sb.length() - 1);
			sb.deleteCharAt(sb.length() - 1);
		}
		sb.append("}\n");
		sb.append("Not Accepted Strings: \n");
		sb.append("{");
		if (!isAcceptingStringMap1.get(Boolean.FALSE).isEmpty()) {
			int k = 0;
			for (String s : isAcceptingStringMap1.get(Boolean.FALSE)) {
				sb.append(s + ", ");
				k += s.length() + 2;
				if (k > 400) {
					k = 0;
					sb.append("\n");
				}
			}

			sb.deleteCharAt(sb.length() - 1);
			sb.deleteCharAt(sb.length() - 1);
		}
		sb.append("}");
		return sb.toString();
	}
}
