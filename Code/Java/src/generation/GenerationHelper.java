package generation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import commons.Automaton;
import commons.EffortClass;
import commons.State;
import commons.Terminal;
import minimization.MooreMinimizer;
import transformation.Transformer;

public class GenerationHelper {

	public static Terminal getRandomTerminal(List<Terminal> terminals, Random random) {
		int randomIndex = random.nextInt(terminals.size());
		return terminals.get(randomIndex);
	}

	public static HashMap<State, HashMap<State, List<Terminal>>> createInitialStateToStateMatrix(List<State> states) {
		HashMap<State, HashMap<State, List<Terminal>>> matrix = new HashMap<State, HashMap<State, List<Terminal>>>();
		for (State start : states) {
			HashMap<State, List<Terminal>> rowStates = new HashMap<>();
			for (State end : states) {
				rowStates.put(end, new ArrayList<>());
			}
			matrix.put(start, new HashMap<>(rowStates));
		}
		return matrix;
	}

	public static Automaton generateForMinimization(AutomatonGenerator generator, int generationLimit,
			EffortClass effort, double probability, int numberOfStates, int minNeaTransitions, int maxNeaTransitions,
			int minDeaCount, int maxDeaCount) {
		boolean resultFound = false;
		int generationCount = 0;
		Automaton resultAutomaton = null;
		int minValue = effort.getMinValue();
		int maxValue = effort.getMaxValue();
		int bestGeneratedEffortGap = Integer.MAX_VALUE;
		int bestGeneratedStateGap = Integer.MAX_VALUE;
		while (!resultFound && generationCount <= generationLimit) {
			Automaton generatedAutomaton = null;
			generatedAutomaton = generator.generateAutomatonWithBitstreams(numberOfStates, probability, 0, 0, 0,
					minNeaTransitions, maxNeaTransitions);
			Transformer transformer = new Transformer(generatedAutomaton);
			Automaton finalDEA = transformer.getRenamedFinalDEA();

			int deaStates = finalDEA.getStates().size();
			if (resultAutomaton == null) {
				resultAutomaton = finalDEA;
			}
			if (finalDEA.getAccepting().size() < deaStates && finalDEA.getAccepting().size() >= 1) {
				if (deaStates >= minDeaCount && deaStates <= maxDeaCount) {
					MooreMinimizer mooreMinimizer = new MooreMinimizer(finalDEA);
					Automaton minimizedDEA = mooreMinimizer.getResultAutomaton();
					int mooreEffort = mooreMinimizer.getEffort();
					EffortClass automatonEffortClass = EffortClass.getClassForEffortValue(mooreEffort);
					bestGeneratedStateGap = 0;
					if (automatonEffortClass.equals(effort)) {
						if (minimizedDEA.getStates().size() < deaStates) {
							resultFound = true;
						}
						resultAutomaton = new Automaton(finalDEA, true);
					} else {
						int newGap = checkIfNewBestGap(minValue, maxValue, bestGeneratedEffortGap, mooreEffort);
						if (newGap < bestGeneratedEffortGap) {
							resultAutomaton = finalDEA;
						}
					}
				} else if (bestGeneratedStateGap > 0) {
					int newGap = checkIfNewBestGap(minDeaCount, maxDeaCount, bestGeneratedStateGap, deaStates);
					if (newGap < bestGeneratedStateGap) {
						resultAutomaton = finalDEA;
					}
				}
			}
			generationCount++;
		}
		return resultAutomaton;
	}

	public static Automaton generateForTransformation(AutomatonGenerator generator, int generationLimit,
			EffortClass effort, int minEpsilon, int maxEpsilon, int sequenceCount, double probability,
			int numberOfStates, int minNeaTransitions, int maxNeaTransitions, int minDeaCount, int maxDeaCount) {
		boolean resultFound = false;
		int generationCount = 0;
		Automaton resultAutomaton = null;
		int minValue = effort.getMinValue();
		int maxValue = effort.getMaxValue();
		int bestGeneratedEffortGap = Integer.MAX_VALUE;
		int bestGeneratedStateGap = Integer.MAX_VALUE;
		while (!resultFound && generationCount < generationLimit) {
			Automaton generatedAutomaton = null;
			generatedAutomaton = generator.generateAutomatonWithBitstreams(numberOfStates, probability, minEpsilon,
					maxEpsilon, sequenceCount, minNeaTransitions, maxNeaTransitions);
			Transformer transformer = new Transformer(generatedAutomaton);
			Automaton finalDEA = transformer.getFinalDEA();
			int deaStates = finalDEA.getStates().size();
			int transformationEffort = transformer.getEffortForTransformation();
			EffortClass automatonEffortClass = EffortClass.getClassForEffortValue(transformationEffort);
			if (deaStates >= minDeaCount && deaStates <= maxDeaCount) {
				bestGeneratedStateGap = 0;
				if (automatonEffortClass.equals(effort)) {
					resultFound = true;
					resultAutomaton = generatedAutomaton;
				} else {
					int newGap = checkIfNewBestGap(minValue, maxValue, bestGeneratedEffortGap, transformationEffort);
					if (newGap < bestGeneratedEffortGap) {
						resultAutomaton = generatedAutomaton;
					}
				}
			} else if (bestGeneratedStateGap > 0) {
				int newGap = checkIfNewBestGap(minDeaCount, maxDeaCount, bestGeneratedStateGap, deaStates);
				if (newGap < bestGeneratedStateGap) {
					resultAutomaton = generatedAutomaton;
				}
			}
			generationCount++;
		}
		return resultAutomaton;
	}

	public static Automaton generateForEpsilonTransformation(boolean withDefault, AutomatonGenerator generator,
			int generationLimit, EffortClass effort, double probability, int numberOfStates) {
		boolean resultFound = false;
		int generationCount = 0;
		Automaton resultAutomaton = null;
		int minValue = effort.getMinValue();
		int maxValue = effort.getMaxValue();
		int bestGeneratedEffortGap = Integer.MAX_VALUE;
		while (!resultFound && generationCount < generationLimit) {
			Automaton generatedAutomaton = null;
			if (withDefault) {
				generatedAutomaton = generator.generateAutomatonWithBitstreamsOnlyWithEpsilon();
			} else {
				generatedAutomaton = generator.generateAutomatonWithBitstreamsOnlyWithEpsilon(numberOfStates,
						probability);
			}
			Transformer transformer = new Transformer(generatedAutomaton);
			int transformationEffort = transformer.getEffortForEpsilonTransformation();
			EffortClass automatonEffortClass = EffortClass.getClassForEffortValue(transformationEffort);
			if (automatonEffortClass.equals(effort)) {
				resultFound = true;
				resultAutomaton = generatedAutomaton;
			} else {
				int newGap = checkIfNewBestGap(minValue, maxValue, bestGeneratedEffortGap, transformationEffort);
				if (newGap < bestGeneratedEffortGap) {
					resultAutomaton = generatedAutomaton;
				}
			}
			generationCount++;
		}
		return resultAutomaton;
	}

	private static int checkIfNewBestGap(int minValue, int maxValue, int bestGeneratedGap, int valueToCheck) {
		int gap = -1;
		if (valueToCheck < minValue) {
			gap = minValue - valueToCheck;
		} else if (valueToCheck > maxValue) {
			gap = valueToCheck - maxValue;
		}
		if (gap < bestGeneratedGap) {
			bestGeneratedGap = gap;
		}
		return bestGeneratedGap;
	}
}
