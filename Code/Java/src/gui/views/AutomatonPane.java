package gui.views;

import java.awt.Dimension;
import java.awt.MediaTracker;
import java.awt.Rectangle;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Scrollable;
import javax.swing.SwingUtilities;

import commons.Automaton;

public class AutomatonPane extends JPanel implements Scrollable {

	private static final long serialVersionUID = 1L;
	private JLabel label;

	public AutomatonPane(Automaton automaton) {
		super();
		setOpaque(true);
		label = new JLabel();
		add(label);
	}

	public boolean showGraph() {
		boolean success = true;
		File f = new File(System.getProperty("user.dir") + "\\graphs\\temp.png");
		ImageIcon icon = null;

		try {
			icon = new ImageIcon(ImageIO.read(f));
		} catch (IOException e) {
			success = false;
		}
		final ImageIcon fIcon = icon;
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				if (fIcon != null && fIcon.getImageLoadStatus() == MediaTracker.COMPLETE) {
					label.setIcon(fIcon);
				}
			}
		});
		return success;
	}

	@Override
	public Dimension getPreferredScrollableViewportSize() {
		return null;
	}

	@Override
	public int getScrollableUnitIncrement(Rectangle visibleRect, int orientation, int direction) {
		return 0;
	}

	@Override
	public int getScrollableBlockIncrement(Rectangle visibleRect, int orientation, int direction) {
		return 0;
	}

	@Override
	public boolean getScrollableTracksViewportWidth() {
		return false;
	}

	@Override
	public boolean getScrollableTracksViewportHeight() {
		return false;
	}

}
