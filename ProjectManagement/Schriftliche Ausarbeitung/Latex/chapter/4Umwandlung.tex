\chapter{Umwandlung in einen DEA}\label{chap:4Umwandlung}
Das folgende Kapitel befasst sich mit der Transformation eines bestehenden \ac{eNEA} oder \ac{NEA} in einen \ac{DEA}, sodass beide Automaten die gleiche Sprache akzeptieren. Die Umwandlung lässt sich dabei in drei inhaltlich getrennte Teilabschnitte gliedern, welche jeweils mithilfe unterschiedlicher Algorithmen einen Teil des Nichtdeterminismus entfernen. 

\noindent Im ersten Teilabschnitt werden Sequenzen von Eigabesymbolen aufgelöst, sodass der eNEA oder NEA einzig Übergänge mit einzelnen Eingabesymbolen beinhaltet. Dies wird im Abschnitt \hyperref[sec:41Sequenzen]{Entfernen von Sequenzen} genauer erläutert. Darauf folgend wird im zweiten Teil die \hyperref[sec:42Epsilon]{Entfernen von Epsilon-Übergängen} thematisiert und somit die Umwandlung eines eNEAs in einen NEA. Im dritten Teilabschnitt wird \hyperref[sec:43Potenzmengenkonstruktion]{die Potenzmengenkonstruktion} behandelt, welche einen NEA in einen DEA umwandelt. \\

\noindent In den einzelnen Abschnitten dieses Kapitels wird zunächst die Problemstellung beschrieben und ein an den (Hoch-)Schulen typisches Verfahren zur Lösung vorgestellt. Im Anschluss wird der Arbeitsaufwand bei der Bearbeitung von Übungsaufgaben mithilfe der beschriebenen Vorgehensweise analysiert und die Implementierung des Verfahrens erläutert. Anschließend werden geeignete Überprüfungsmöglichkeiten für die Ergebnisse der Übungsaufgaben beschrieben. 

%Voraussetzung nea oder eNea 
%Ziel umwandlung des nea oder enea in dea
%Übliche Vorgehensweise (+Aufbau des Kapitels) 
%Inhaltlich getrennte, in sich abgeschlossene Teilabschnitte 
%Voneinander unabhängige Aufgaben und Algorithmen 

\section{Entfernen von Sequenzen}\label{sec:41Sequenzen} 
\subsection{Problemstellung} \label{ssec:411Problemstellung}
Eine beliebige Konkatenation von Eingabesymbolen entspricht einem Wort der Länge $w \geq 2$, wie in der Definition \ref{def:22wort} beschrieben. Ein Wort $|w| \in \Sigma^*$, welches als einzelner Übergang verwendet wird, wird im Folgenden als \definitionCallout{Sequenz} bezeichnet.
Beim Einlesen einer Eingabefolge wird nicht zwischen Sequenzen und Eingabefolgen unterschieden. Da laut der Definition des DEA die Übergangsfunktion $\delta$ nur für Eingabesymbole $a \in \Sigma$ definiert ist(siehe \ref{def:231automat}), darf nach der Umwandlung keine Sequenz vorhanden sein. Die Transformation besteht in diesem Teilschritt aus dem Entfernen der auftretenden Sequenzen aus der Übergangsfunktion des eNEA oder NEA.

Das Entfernen der Übergänge mit Sequenzen kann als Teilaufgabe der gesamten Umwandlung eines Automaten in einen DEA gesehen werden. Alternativ kann dies auch als eigene Aufgabe dienen, um diesen Teilschritt der Umwandlung zu vertiefen und abgesondert zu üben. 

\subsection{Beschreibung des Verfahrens} \label{ssec:412Verfahrensbeschreibung}
Die Grundidee des Algorithmus besteht darin, den Übergang für die Sequenz $s \in \Sigma^*$, $ \delta(q_i, s) = q_j$ durch neue Übergänge für jedes Eingabesymbol zu ersetzen. Dadurch wird jedem Übergang nur ein Eingabesymbol übergeben. Dieses Vorgehen wird in Beispiel \ref{exmp:412Sequenzen} genauer erläutert. Aus Gründen der Lesbarkeit wird im Folgenden beginnend mit diesem Beispiel die Sprache des Automaten durch einen äquivalenten regulären Ausdruck dargestellt.
\input{images/412MitSequenz}
\begin{exmp}{412Sequenzen} 
	Der in \reffig{fig:412MitSequenz} dargestellte Automat akzeptiert die durch den regulären Ausdruck $ab^*$ beschriebene Sprache und enthält eine Sequenz $s=abb$, mit welcher der Zustand $q_0$ in $q_1$ übergeht. Ausgehend von $q_0$ wird für das Eingabesymbol $a$ ein neuer Übergang inklusive eines neuen Zustands $q_2$ erstellt (violett dargestellt).  Für das zweite Eingabesymbol der Sequenz wird ebenfalls ein neuer Übergang und ein neuer Zustand $q_3$ definiert. Der Zustand $q_2$ geht dann mit dem Eingabesymbol $b$ in den  Zustand $q_3$ über (blau). Entsprechend der ursprünglichen Definition der Übergangsfunktion $\delta$ geht $q_3$ mit dem letzten Symbol der Sequenz in $q_1$ über. Die graphische Darstellung des durch die Auflösung der Sequenz entstehenden Automaten wird in \reffig{fig:412AufgelösteSequenz} dargestellt.
	
	\begin{flalign*}
		&&&&\hat{\delta} (q_0, s) =&~~~~\hat{\delta} (q_0, abb) &= q_1 &&&&&&\\ 
		&&&&\overset{\text{ \ref{exmp:231delta2} }} \Longrightarrow &~~~~\delta(\delta(\textcolor{javaPurple}{\delta(q_0,a)},b),b) &= q_1 &&&&&&\\
		&&&&\Longrightarrow 
		&~~~~\delta(\delta(\textcolor{javaPurple}{q_2},b),b) &=q_1 &&&&&& \\
		&&&&
		&~~~~\delta(\textcolor{blue}{\delta(q_2,b)},b) &= q_1 &&&&&&\\
		&&&&\Longrightarrow 
		&~~~~\delta(\textcolor{blue}{q_3},b) &=q_1 &&&&&&
	\end{flalign*} 
\end{exmp}
\input{images/412AufgeloesteSequenz}
\subsubsection{Anmerkungen zur Aufwandsanalyse} \label{ssec:413Aufwandsanalyse}
Um zu bestimmen, wie hoch der Arbeitsaufwand des Anwenders ist, wird sich in Folgenden Aufwandsanalysen auf die in Abschnitt \ref{sec:32Aufwand} definierten Regeln bezogen. Der Aufwand der Auflösung von Sequenzen ist für diese Arbeit nicht weiter relevant und wird im \hyperref[10.4:AufwandSequenz]{Anhang} ausführlich beschrieben.
\subsection{Implementierung} \label{ssec:414Implementierung}
Das in Abschnitt \ref{ssec:412Verfahrensbeschreibung} beschriebene Verfahren wurde mithilfe des in \ref{code:411Sequenzen} beschriebenen Algorithmus umgesetzt.
\clearpage
\input{code/411Sequenzen.tex}
\subsection{Überprüfungsmöglichkeiten} \label{ssec:415Überprüfungsmöglichkeiten}
Bei der Auflösung von Sequenzen wird den neu entstehenden Zuständen eine Benennung zugewiesen, welche bei Übungsaufgaben je nach Anwender verschieden sein kann. Eine Lösungsmöglichkeit für diesen Fall ist das Aufstellen fester Regeln für die Benennung. Bei einer einheitlichen Benennung ist jedoch das Problem der Abarbeitungsreihenfolge nicht gelöst. Das Problem besteht darin, dass kein direkter Abgleich der Lösung stattfinden kann, da der Anwender unter Umständen die Sequenzen in anderer Reihenfolge abgearbeitet hat als der Algorithmus, welcher das Ergebnis zum Vergleich liefert.
\newpassage
\begin{exmp}{415Sequenzen} 
	Der in \reffig{fig:415Doppelsequenz} dargestellte Automat zeigt einen Zustand $q_0$, welcher mit der gleichen Sequenz in zwei verschiedene Zustände $q_1$ und $q_2$ übergeht. Außerdem akzeptiert er die durch den regulären Ausdruck $r = ab$ dargestellte Sprache. Bei der Auflösung kann der Anwender den neu entstehenden Zuständen eine beliebige Benennung, wie \enquote{A} und \enquote{B} oder \enquote{meinNeuerZustand1} und \enquote{meinNeuerZustand2}, zuweisen.
	\input{images/415Doppelsequenz}
	
	\noindent Unter der Annahme, dass für den Anwender eine Regel besagt, Zustände müssen bei der Auflösung mit $q_i$ benannt werden, wobei $i \in \mathbb{N}$ die bisherige Anzahl Zustände angibt, gibt es die in \reffig{fig:415AufgeloesteDoppelsequenz} dargestellten Möglichkeiten zur Auflösung. Das Zusammenfassen der Zustände $q_3$ und $q_4$ ist entsprechend des beschriebenen Verfahrens nicht zulässig, obwohl beide Zustände sich einzig durch den Zustand unterscheiden, in welchen sie mit dem Eingabesymbol $b$ übergehen. Das Problem der Reihenfolge kann gelöst werden, indem dem Anwender zusätzlich zur Benennung vorgeschrieben wird, dass alle Sequenzen in aufsteigender Reihenfolge der Zustandsbenennung aufzulösen sind. Bei dem NEA $N$ müsste also zuerst der Übergang $\delta(q_0, ab)=q_1$ und darauf folgend der Übergang $\delta(q_0, ab)=q_2$ aufgelöst werden, was den NEA $N'$ ergeben würde. Dadurch wäre es nach der Ergänzung dieser Regel im Algorithmus möglich, über einen direkten Vergleich den vom Anwender erstellten neuen Automaten mit dem Ergebnis des Algorithmus zu überprüfen. Im Algorithmus könnte die Regel über eine Sortierung der Übergänge vor ihrer Abarbeitung umgesetzt werden.
	\input{images/415AufgeloesteDoppelsequenz}
\end{exmp}

\noindent Bei der Bearbeitung einer Übungsaufgabe ist innerhalb des beschriebenen Verfahrens in Abschnitt \ref{ssec:412Verfahrensbeschreibung} weder eine einheitliche Benennung der neuen Zustände vorgesehen, noch eine Bearbeitungsreihenfolge, falls der Automat mehr als eine Sequenz beinhaltet. Unter Berücksichtigung der Regel, dass \hyperref[rule:Implementierung3]{durch die Implementierung des Algorithmus keine Verständnisfehler entstehen dürfen}, sollten keine weiteren Regeln eingeführt werden, um die Überprüfung zu erleichtern.

\noindent Da ein Übergang durch die Übergangsfunktion eindeutig definiert ist, ist es möglich, die neuen Zustände unabhängig von ihrer Benennung über die Eingabesymbole und den Ausgangs- und Endzustand der Sequenz eindeutig zuzuordnen. Diese Zuordnung von Übergängen und Zuständen ermöglicht die Identifikation von Fehlern bei der Bearbeitung ohne Vorgabe zusätzlicher Regeln. 

\section{Entfernen von Epsilon-Übergängen}\label{sec:42Epsilon}

\subsection{Problemstellung} \label{ssec:421Problemstellung}
Wie bereits in Abschnitt \ref{ssec:233NEA} beschrieben, sind im \ac{eNEA} zusätzlich spontane Übergänge enthalten. Dies kann dazu führen, dass ein Wort akzeptiert wird, obwohl der Automat sich in keinem akzeptierenden Zustand befindet, da ein spontaner Übergang aus diesem Zustand in einen akzeptierenden Zustand führt. Im Folgenden wird ein Übergang, welcher das leere Wort $\epsilon \notin \Sigma$ als Eingabe akzeptiert, als Epsilon-Übergang bezeichnet. Ziel dieses Abschnitts ist es, durch die Umwandlung eines eNEA in einen NEA diese Epsilon-Übergänge zu entfernen.

\noindent Die Entfernung von Epsilon-Übergängen kann als Teilaufgabe der Transformation eines eNEA in einen DEA oder als eigenständige Aufgabe gestellt werden. 
\subsection{Beschreibung des Verfahrens} \label{ssec:422Verfahrensbeschreibung}
\subsubsection{Grundidee}
Im Folgenden soll das Verhalten eines Epsilon-Übergangs $\delta(q_a,\epsilon)=q_e$ mit $q_a, q_e \in Q$ betrachtet werden.

Der durch das Epsilon hervorgerufene Nichtdeterminismus betrifft den Ausgangszustand $q_a$ des Epsilon-Übergangs sowie alle Zustände $q_i \in Q$, welche mit einem Eingabesymbol $a \in \Sigma$ in diesen Zustand übergehen. Durch den spontanen Übergang kann der Zustand $q_a$ alle Zustände $q_j$ erreichen, welche vom Endzustand $q_e$ über eine einzelnes Eingabesymbol $a$ erreichbar sind.

Für alle Zustände $q_i$ mit $\delta(q_i,a)=q_a$ gilt, dass durch den spontanen Übergang mit dem Eingabesymbol $a$ auch $q_e$ erreicht werden kann (siehe \ref{exmp:233NEA1}). Damit gilt $\delta(q_i, a) = \{q_a,q_e\}$.
\subsubsection{Verfahren}
Für einen gegebenen eNEA $N_\epsilon = (Q_{N_\epsilon}, \Sigma_{N_\epsilon}, \delta_{N_\epsilon}, q_{0,N_\epsilon}, F_{N_\epsilon})$ wird der gesuchte NEA $N = (Q_{N}, \Sigma_{N}, \delta_{N}, q_{0,N}, F_{N})$ wie folgt ermittelt:

\begin{enumerate}
	\item Die Zustandsmenge, das Eingabealphabet und der Startzustand werden übernommen
	\begin{center}$Q_{N} = Q_{N_\epsilon},  \Sigma_{N} = \Sigma_{N_\epsilon},  q_{0,N} = q_{0,N_\epsilon} $\end{center}
	\item Die folgenden Schritte werden für jeden einzelnen Epsilon-Übergang $\delta_{N_\epsilon}(q_a, \epsilon) = q_e$ durchgeführt.
	\begin{enumerate}
		\item  Die Übergangsfunktion aller Zustände $q_i$, welche mit einem Eingabesymbol in $q_a$ übergehen, wird um den Endzustand des Epsilon-Übergangs $q_e$ erweitert.
		\begin{center}$ \delta_{N_\epsilon}(q_i,a) = \{q_a, q_e\}$\end{center}
		\item Für alle Übergänge $ \delta_{N_\epsilon}(q_e,a) = q_j$,  $a \in \Sigma_{N_\epsilon}$ wird ausgehend vom Ausgangszustand $q_a$ des Epsilon-Übergangs ein neuer Übergang erstellt mit $ \delta_{N}(q_a,a) = q_j$
	\end{enumerate}
	\item $F_{N}$ wird um alle $q_a$ ergänzt, für die gilt $\delta_{N_\epsilon}(q_a,\epsilon) = q_e $ mit $q_e \in F_{N_\epsilon}$
	\begin{center}$F_{N}$ = $F_{N_\epsilon} \cup \{q\in Q_{N_\epsilon} | \delta_{N_\epsilon}(q_a,\epsilon) = q_e \in F_{N_\epsilon}\}$ \end{center}
\end{enumerate}

\input{images/422Epsilon}
\noindent In \reffig{fig:422Epsilon} ist eine Verkettung mehrerer Epsilon-Übergänge abgebildet. In solchen Fällen ist es erforderlich, dass für jeden Zustand der Kette die darauf folgenden Zustände $q_k \in Q_{N_\epsilon}$ ebenfalls als erreichbar betrachtet werden. Jeder Zustand $q_k$ wird von den vorherigen Zuständen $q_\epsilon$ der Kette so behandelt, als würde ein direkter Epsilon-Übergang von $q_\epsilon$ zu $q_k$ führen. 

\noindent Die Auflösung verketteter Epsilon-Übergänge wird anhand der Transformation des eNEA \hyperref[fig:422Epsilon]{N} in einen NEA im folgenden Beispiel \ref{exmp:412epsilon}näher erläutert.\\

\begin{exmp}{412epsilon} 
	Der in \reffig{fig:422Epsilon} dargestellte Automat akzeptiert die durch den regulären Ausdruck $r = (ab)^*b^*$ dargestellte Sprache und beinhaltet zwei Epsilon-Übergänge $\delta(q_0,\epsilon)=q_1$ und $\delta(q_1,\epsilon)=q_2$.
	
	\noindent Zunächst wird der Epsilon-Übergang $\delta(q_0,\epsilon)=q_1$ wie in \reffig{fig:422EpsilonSchritt1} dargestellt aufgelöst. Da es sich um eine Verkettung von Epsilon-Übergängen handelt, muss neben $q_1$ auch $q_2$ als möglicher Endzustand betrachtet werden. Der Schritt 2a des zuvor beschriebenen Verfahrens ist hier nicht erforderlich, da in den Zustand $q_0$ keine Übergänge führen. Entsprechend des Schritts 2b werden die ausgehenden Übergänge für $q_1$ und $q_2$ betrachtet. Da $q_1$ mit dem Eingabesymbol $a$ in $q_3$ übergeht und $q_2$ mit dem Eingabesymbol $b$ in sich selbst führt, gilt $\delta(q_0,a)=q_3$ und $\delta(q_0,b)=q_2$.
	
	\input{images/422EpsilonSchritte}
	
	\noindent Anschließend wird der Epsilon-Übergang $\delta(q_1,\epsilon)=q_2$ wie in \reffig{fig:422EpsilonSchritt1} dargestellt aufgelöst. Da $q_3$ mit dem Eingabesymbol $b$ in $q_1$ übergeht, gilt nach Schritt 2a $\delta(q_3,b)=q_2$. Nach Schritt 2b wird der Übergang $\delta(q_2,b)=q_2$ ebenfalls für $q_1$ übernommen. Es folgt $\delta(q_1,b)=q_2$.
	
	\noindent  Da $q_2$ akzeptierend ist, gilt mit Durchführung des 3. Schritts auch $q_0, q_1 \in F$. 
	
	\noindent Nach Auflösen der beiden Übergänge ergibt sich der NEA \hyperref[fig:422EpsilonSchritt2]{N''}. Die Arbeitsschritte 2b und 3 sind in \reffig{fig:422EpsilonSchritte} jeweils in violett hervorgehoben und der Arbeitsschritt 2a in blau.
	
\end{exmp}

\subsubsection{Anmerkungen zur Aufwandsanalyse} \label{ssec:423Aufwandsanalyse}
Der Aufwand der Auflösung von Epsilon-Übergängen ist für diese Arbeit nicht weiter relevant und wird im \hyperref[10.4:AufwandEpsilon]{Anhang} ausführlich beschrieben.

\subsection{Implementierung} \label{ssec:424Implementierung}
Der Algorithmus soll sich an dem in \ref{ssec:422Verfahrensbeschreibung} beschriebenen Verfahren orientieren. Um die durch Epsilon-Ketten erstellten Übergänge zu berücksichtigen, gibt es zwei Möglichkeiten: Als erste Möglichkeit kann man während der Abarbeitung der Übergänge rekursiv durch solche Verkettungen laufen. Dabei können die nötigen Übergänge zusätzlich erstellt werden. Im Gegensatz dazu wird im beschriebenen Verfahren jedes Element der Kette so betrachtet, als würde ein direkter Übergang ausgehend vom derzeit betrachteten Ausgangszustand existieren. Eine solche Vorgehensweise ist daher nicht erwünscht.

\input{code/424Epsilon1.tex}
\noindent Die zweite Möglichkeit besteht darin solche direkten Epsilon-Übergänge vor der Abarbeitung der Übergänge zu erstellen. In \reflst{code:424Epsilon1} ist diese Vorgehensweise aufgeführt. Dabei wird der rekursive Algorithmus \ref{code:424Epsilon2} verwendet, um die von jedem Zustand erreichbaren Zustände der Kette für die noch zu erstellenden Übergänge zu ermitteln. 

\input{code/424Epsilon2.tex}
%\clearpage
\input{code/424Epsilon3.tex}
\noindent Nach der Erzeugung der neuen Zustände kann jeder Übergang einzeln entsprechend des Verfahrens aufgelöst werden. Die gesamte Auflösung der Epsilon-Übergänge ist in \ref{code:424Epsilon3} aufgeführt, wobei der in \ref{code:424Epsilon1} dargestellte Algorithmus anfangs seine Anwendung findet.
%\clearpage


%Schwierigkeit Epsilon-Ketten, Vergleich rekursives Vorgehen 
% Genauer erklären, warum neuen Übergang erstellen auch möglich ist 
\subsection{Überprüfungsmöglichkeiten} \label{ssec:425Überprüfungsmöglichkeiten}
Durch den in \ref{ssec:424Implementierung} beschriebenen Algorithmus entsteht ein neuer Automat, der alle durch die Auflösung entstandenen Übergänge enthält. Da die Vorgehensweise ein eindeutiges Ergebnis liefert, ist die Überprüfung über einen direkten Vergleich von Übergängen und akzeptierenden Zuständen möglich. Bei der Bearbeitung des Anwenders kann also unterschieden werden, ob der gerade erstellte Übergang valide ist oder nicht im endgültigen NEA vorhanden ist. Ebenso können als akzeptierend angesehene Zustände anhand der Menge der akzeptierenden Zustände des NEA geprüft werden. 

\noindent Da so für die Überprüfung des Ergebnisses keine weiteren Regeln abseits des in \ref{ssec:422Verfahrensbeschreibung} beschriebenen Verfahrens benötigt werden, kann die Regel eingehalten werden, dass \hyperref[rule:Implementierung3]{durch die Implementierung des Algorithmus keine Verständnisfehler entstehen dürfen}. Um einen Zeitpunkt für die Überprüfung zu wählen, muss die Regel \hyperref[rule:Implementierung2]{Aufgaben müssen kleinschrittig überprüft werden können} in Betracht gezogen werden. Ein neu erstellter Übergang kann als einzelner Schritt gewählt werden und ist die kleinstmögliche überprüfbare Einheit. 

\noindent Auch das resultierende Übergangsdiagramm kann für eine Überprüfung der Ergebnisse genutzt werden. Ein Beispiel dazu ist in \reffig{fig:425EpsilonFalsch} zu sehen.  Die Übergänge $\delta(q_0,b)$ und $\delta(q_1,a)$ sind als inkorrekt markiert. Der Zustand $q3$ ist ebenfalls rot hervorgehoben, da er fälschlicherweise
als akzeptierend eingetragen ist.
\input{images/425EpsilonFalsch}

\noindent Die Übergänge $\delta(q_0,b)=q_1$ und $\delta(q_1,b)=q_1$ sind nicht korrekt und werden farblich hervorgehoben. Selbiges gilt für den Zustand $q_3$, da er fälschlicherweise als akzeptierend eingetragen ist. Die Abbildung \ref{fig:425EpsilonRichtig} zeigt die korrekte Umwandlung des eNEA $N$. Neu hinzugefügte Übergänge und akzeptierende Zustände sind grün markiert. 
\input{images/425EpsilonRichtig}
\subsection{Die Epsilonhülle als alternative Möglichkeit} \label{ssec:426EpsilonHülle}
\subsubsection{Grundidee}
Das bisher beschriebene Verfahren kann über die Notationsform des Übergangsdiagramms durchgeführt und überprüft werden. Im Gegensatz dazu arbeitet das Verfahren der Epsilonhülle mit der Übergangstabelle eines eNEA $N_\epsilon$ und bietet eine alternative Vorgehensweise für die Umwandlung sowie eine neue Überprüfungsmöglichkeit. 

\noindent Unter der Epsilonhülle eines Zustands werden alle Zustände verstanden, welche über einen einzelnen Epsilon-Übergang oder eine Verkettung mehrerer Epsilon-Übergänge erreichbar sind. Dabei ist in der Hülle eines Zustands immer auch der Zustand selbst enthalten \cite[104]{Hopfcroft.2011}.

Das Verfahren erweitert die Übergangstabelle von $N_\epsilon$ mithilfe der zuvor bestimmten Epsilonhülle und der Übergangsfunktion $\delta_N$. Dazu werden für alle $q \in Q_{N_\epsilon}$ und $a \in \Sigma_{N_\epsilon}$ die erweiterten Übergangsfunktionen $\delta^*(q,a)=\{q_i\}$ bestimmt\cite[107]{Martin.2011}.

%Grundidee (+Motivation, Autor) 
\subsubsection{Vorgehen}
Für einen gegebenen eNEA $N_\epsilon = (Q_{N_\epsilon}, \Sigma_{N_\epsilon}, \delta_{N_\epsilon}, q_{0,N_\epsilon}, F_{N_\epsilon})$ wird der gesuchte NEA $N = (Q_{N}, \Sigma_{N}, \delta_{N}, q_{0,N}, F_{N})$ wie folgt ermittelt:

\begin{enumerate}
	\item Die Zustandsmenge, das Eingabealphabet und der Startzustand werden übernommen
	\begin{center}$Q_{N} = Q_{N_\epsilon},  \Sigma_{N} = \Sigma_{N_\epsilon},  q_{0,N} = q_{0,N_\epsilon} $ \end{center}
	\item Zunächst wird für alle $q \in Q_{N_\epsilon}$ die Epsilonhülle gebildet.
	\begin{center}$ \epsilon-H\textit{Ü}LLE(q) = \{q_i\}$ mit $q_i \in \delta^*(q,\epsilon)$\end{center}
	\item Die Übergangstabelle für $\delta_{N_\epsilon}$ wird gebildet.
	\item Die erweiterte Übergangsfunktion $\delta^*(q,a)=\{q_i\}$ wird für jeden Zustand $q \in Q_{N_\epsilon}$ und jedes Eingabesymbol $a \in \Sigma_{N_\epsilon}$ mit den folgenden Schritten ermittelt. 
	\begin{enumerate}
		\item  Prüfen, ob für alle $q_i \in \delta(q,a)$ einer der Zustände $q_i$ oder der Zustand $q$ selbst eine Epsilon-Hülle $\epsilon-H\textit{Ü}LLE(q_i) = \{q_j\}$ besitzt, welche nicht nur den Zustand selbst enthält.
		\item Erweiterung der Übergangsfunktion $\delta^*(q,a)=\{q_i\}$ um die Zustände $q_k$, die von $q_j$ aus erreicht werden können: $\delta(q_j,a)=\{q_k\}$ 
	\end{enumerate}
	\item $F_{N}$ wird um die Zustände $q_a$ ergänzt, für die gilt $\epsilon-H\textit{Ü}LLE(q_a) = \{q_j\}$ und $ q_e \in \{q_j\}, q_e\in F_{N_\epsilon}$
\end{enumerate}

\begin{exmp}{412epsilon} 
	Der in \reffig{fig:422Epsilon} dargestellten eNEA $N$, welcher die durch den regulären Ausdruck $r = (ab)^*b^*$ dargestellte Sprache akzeptiert, soll mithilfe der Epsilon-Hülle in einen NEA umgewandelt werden. Dazu wird zunächst die Übergangstabelle \ref{tab:426Epsilonhülle} erstellt.
	
	\noindent Bei der Berechnung der Epsilonhülle ergeben sich für die Zustände $q_0$ und $q_1$ Hüllen, welche nicht nur den Zustand selbst enthalten. Die Epsilonhülle für $q_0$ entspricht $\epsilon-H\textit{Ü}LLE(q_0) = \{q_0, q_1, q_2\}$, da $q_0$ über einen Epsilon-Übergang in $q_1$ übergeht und $q_1$ mit $\epsilon$ in $q_2$. Die Hülle von $q_1$ beinhaltet $q_1$ und $q_2$. 
	
	\noindent Für den Zustand $q_0$ werden die Übergänge $\delta(q_1,a) = q_3$  und $\delta(q_2,b)=q_2$ übernommen. Die Übergangsfunktion $\delta(q_1, b)=\emptyset$ wird mithilfe der Epsilonhülle von $q_1$ zu $\delta^*(q_1, b)=q_2$ erweitert.
	Für den Zustand $q_3$ gilt $\delta(q_3,b) = q_1$. Da die Epsilon-Hülle von $q_1$ den Zustand $q_2$ enthält und von $q_2$ aus keine weiteren Zustände erreicht werden, wird der Übergang zu $\delta^*(q_3, b)=\{q_1, q_2\}$ erweitert.  
	\input{tables/426Epsilonhülle.tex}
	
	\noindent Die so erweiterte Übergangsfunktion $\delta^{*}$ definiert den resultierenden NEA $N^{'}$ bis auf die Menge der akzeptierenden Zustände $F_{N'}$. Für diese ergibt sich mit Schritt 5 des Verfahrens $F_{N^{'}} = \{\textcolor{javaPurple}{q_0, q_1}, q_2\}$. Die durch die Erweiterung neu hinzugekommenen Zustände sind in \reftab{tab:426Epsilonhülle} jeweils violett hervorgehoben.
\end{exmp} 
%Vorgehen + Beispiel 
%Epsilon-Hülle: in Introduction to Languages, 107 (120) 

\input{chapter/sections/43Subset.tex}