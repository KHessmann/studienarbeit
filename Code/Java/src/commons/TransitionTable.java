package commons;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import helper.TableStringBuilder;

public final class TransitionTable {

	private HashMap<State, HashMap<Terminal, List<State>>> transitionTable = new LinkedHashMap<>();
	private Set<Terminal> alphabet;

	public TransitionTable(List<State> states, List<Transition> transitions, Set<Terminal> alphabet) {
		this.alphabet = alphabet;
		for (State start : states) {
			HashMap<Terminal, List<State>> rowStates = new HashMap<>();
			for (Terminal terminal : alphabet) {
				List<State> endStates = transitions.stream()
						.filter(transition -> transition.getStartState().equals(start)
								&& isTerminalInTransition(transition, terminal))
						.map(transition -> transition.getEndState()).collect(Collectors.toList());
				Collections.sort(endStates);
				rowStates.put(terminal, new ArrayList<>(endStates));
			}
			transitionTable.put(start, new HashMap<>(rowStates));
		}
	}

	private boolean isTerminalInTransition(Transition transition, Terminal terminal) {
		for (Terminal t : transition.getTerminals()) {
			if (t.equals(terminal)) {
				return true;
			}
		}
		return false;
	}

	public List<State> getEndStatesFromTransitionsForState(State state, Terminal terminal) {
		if (!getTransitionTable().containsKey(state)) {
			return new ArrayList<>();
		}
		return transitionTable.get(state).get(terminal);
	}

	public List<State> getEndStatesFromTransitionsForStates(List<State> states, Terminal terminal) {

		Set<State> set = new HashSet<>();
		for (State state : states) {
			set.addAll(transitionTable.get(state).get(terminal));
		}
		List<State> result = new ArrayList<>(set);
		Collections.sort(result);
		return result;
	}

	public boolean isColumnCorrect(State state, Terminal terminal, List<State> resultStates) {
		List<State> states = transitionTable.get(state).get(terminal);
		if (states.size() == resultStates.size() && states.containsAll(resultStates)) {
			return true;
		}
		return false;
	}

	public boolean isTransitionTableCorrect(HashMap<State, HashMap<Terminal, List<State>>> resultTable) {
		if (transitionTable.keySet().size() != resultTable.keySet().size()
				|| !transitionTable.keySet().containsAll(resultTable.keySet())) {
			return false;
		}
		for (State state : resultTable.keySet()) {
			if (transitionTable.get(state).keySet().size() != resultTable.get(state).keySet().size()
					|| !transitionTable.get(state).keySet().containsAll(resultTable.get(state).keySet())) {
				return false;
			}
			for (Terminal terminal : resultTable.get(state).keySet()) {
				List<State> states = transitionTable.get(state).get(terminal);
				if (states.size() != resultTable.get(state).get(terminal).size()
						|| !states.containsAll(resultTable.get(state).get(terminal))) {
					return false;
				}
			}
		}
		return true;
	}

	public Map<State, HashMap<Terminal, List<State>>> getTransitionTable() {
		return transitionTable;
	}

	@Override
	public String toString() {
		List<Terminal> alph = new ArrayList<>(alphabet);
		alph.sort(Comparator.comparing(Terminal::toString));
		List<State> states = new ArrayList<>(transitionTable.keySet());
		states.sort(Comparator.comparing(State::toString));
		int rowCount = transitionTable.size() + 1;
		int colCount = alphabet.size() + 1;
		String[][] table = new String[rowCount][colCount];
		int i = 0;
		int j = 0;
		table[i][j] = "States";
		for (Terminal t : alph) {
			j++;
			table[i][j] = t.toString();
		}
		for (State from : states) {
			i++;
			j = 0;
			StringBuilder sb = new StringBuilder();
			sb.append(from.toString());
			table[i][j] = sb.toString();
			HashMap<Terminal, List<State>> terminalMap = transitionTable.get(from);
			for (Terminal t : alph) {
				j++;
				String stateEntry = "";
				if (terminalMap.get(t).size() > 1) {
					StringBuilder sb2 = new StringBuilder();
					sb2.append(State.cummulatedStartChar);
					for (State s : terminalMap.get(t)) {
						sb2.append(s.toString());
						sb2.append(State.delimiter);
					}
					sb2.deleteCharAt(sb2.length() - 1);
					sb2.append(State.cummulatedEndChar);
					stateEntry = sb2.toString();
				} else if (terminalMap.get(t).size() == 1) {
					stateEntry = terminalMap.get(t).get(0).toString();
				}
				table[i][j] = stateEntry;
			}
		}
		return TableStringBuilder.getTableString(table);
	}
}
