package helper;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

/*
 *- source:
 * - https://itsallbinary.com/java-printing-to-console-in-table-format-simple-code-with-flexible-width-left-align-header-separator-line/
 */
public class TableStringBuilder {

	public static String getTableString(String[][] table) {

		StringBuilder sb = new StringBuilder();
		Map<Integer, Integer> columnLengths = new HashMap<>();
		Arrays.stream(table).forEach(a -> Stream.iterate(0, (i -> i < a.length), (i -> ++i)).forEach(i -> {
			if (columnLengths.get(i) == null) {
				columnLengths.put(i, 0);
			}
			if (columnLengths.get(i) < a[i].length()) {
				columnLengths.put(i, a[i].length());
			}
		}));

		/*
		 * Prepare format String
		 */
		final StringBuilder formatString = new StringBuilder("");
		columnLengths.entrySet().stream().forEach(e -> formatString.append("| %-" + e.getValue() + "s "));
		formatString.append("|\n");

		/*
		 * Prepare line for top, bottom & below header row.
		 */
		String line = columnLengths.entrySet().stream().reduce("", (ln, b) -> {
			String templn = "+-";
			templn = templn + Stream.iterate(0, (i -> i < b.getValue()), (i -> ++i)).reduce("", (ln1, b1) -> ln1 + "-",
					(a1, b1) -> a1 + b1);
			templn = templn + "-";
			return ln + templn;
		}, (a, b) -> a + b);
		line = line + "+\n";

		/*
		 * Print table
		 */
		sb.append(line);
		Arrays.stream(table).limit(1).forEach(a -> sb.append(String.format(formatString.toString(), a)));
		sb.append(line);

		Stream.iterate(1, (i -> i < table.length), (i -> ++i))
				.forEach(a -> sb.append(String.format(formatString.toString(), table[a])));
		sb.append(line);

//		/*
//		 * Print table
//		 */
//		for (int i = 0; i < table.length; i++) {
//			for (int j = 0; j < table[i].length; j++) {
//				String s = String.format(formatString.toString(), table[i][j]);
//				sb.append(s + "/n");
//			}
//		}
		return sb.toString();
	}
}