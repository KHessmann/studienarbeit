package commons;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public class Transition {
	private UUID id;
	private Set<Terminal> terminals = new HashSet<>();
	private State startState;
	private State endState;
	private boolean hasEpsilon;

	public Transition(Terminal terminal, State startState, State endState, boolean hasEpsilon) {
		id = UUID.randomUUID();
		terminals.add(terminal);
		this.startState = startState;
		this.endState = endState;
		this.hasEpsilon = hasEpsilon;
	}

	public Transition(Set<Terminal> terminals, State startState, State endState, boolean hasEpsilon) {
		id = UUID.randomUUID();
		this.terminals = terminals;
		this.startState = startState;
		this.endState = endState;
		this.hasEpsilon = hasEpsilon;
	}

	public UUID getId() {
		return id;
	}

	public Set<Terminal> getTerminals() {
		return terminals;
	}

	public void addTerminal(Terminal terminal) {
		if (terminal.getType() == TerminalType.EPSILON) {
			hasEpsilon = true;
		}
		terminals.add(terminal);
	}

	public void addTerminals(List<Terminal> terminals) {
		this.terminals.addAll(terminals);
	}

	public void removeTerminal(Terminal terminal) {
		terminals.remove(terminal);
	}

	public void removeAllTerminals() {
		terminals.removeAll(terminals);
	}

	public State getStartState() {
		return startState;
	}

	public void setStartState(State startState) {
		this.startState = startState;
	}

	public State getEndState() {
		return endState;
	}

	public void setEndState(State endState) {
		this.endState = endState;
	}

	public boolean hasEpsilon() {
		return hasEpsilon;
	}

	public void setEpsilon(boolean hasEpsilon) {
		this.hasEpsilon = hasEpsilon;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		for (Terminal t : terminals) {
			sb.append("\n" + startState.getName() + ":" + t.toString() + ">" + endState.getName() + ";");
		}
		return sb.toString();
	}

	@Override
	public boolean equals(Object anObject) {
		if (this == anObject) {
			return true;
		}
		if (anObject instanceof Transition) {
			Transition transition = (Transition) anObject;
			return (startState.equals(transition.getStartState()) && endState.equals(transition.getEndState())
					&& hasEpsilon == transition.hasEpsilon() && terminals.equals(transition.getTerminals()));
		}
		return false;
	}
}
