\section{Edward J. Moore und der Table Filling Algorithmus}\label{sec:52moore1}
\subsubsection*{Eine experimentelle Herangehensweise}
Einer der ersten, der sich mit der Minimierung von Endlichen Automaten auseinandergesetzt hat, ist Edward J. Moore.
In seinem Aufsatz \enquote{gedanken-experiments on sequential machines} beschäftigt sich Moore mit Automaten aus einem experimentellen Gesichtspunkt heraus, um aus seinen Ergebnissen allgemeingültige Schlüsse zu ziehen.
Dort heißt es: \enquote{Ein Zustand $q_i$ einer Maschine S heißt ununterscheidbar von einem Zustand $q_j$ von S genau dann, wenn jedes Experiment, das an S im Zustand $q_i$ beginnend durchgeführt wird, das selbe Ergebnis produziert, wie wenn es im Zustand $q_j$ begänne}\cite[159]{Moore.1974}.\\
\noindent Ein solches Experiment nutzt er dort beispielsweise für den Automaten in \reffig{fig:52moore1}, um dessen Zustände experimentell auf Unterscheidbarkeit zu überprüfen.

\noindent Von unterschiedlichen Zuständen aus beginnend beobachtet er schrittweise die Ausgabe beim Einlesen desselben Worts $w_1=aaabaaab$. Das Experiment, angepasst an die in dieser Arbeit gebräuchliche Notationsform von Automaten, ist in \reftab{tab:52moore1} in Tabellenform dargestellt.  
\clearpage
\input{./images/52moore1.tex} 
\input{./tables/52moore1.tex}

\noindent Durch die Überprüfung, ob das Eingabewort zu jedem Zeitpunkt von zwei betrachteten Zuständen gleichermaßen entweder akzeptiert (violett) oder nicht akzeptiert wird, kann festgestellt werden ob diese beiden Zustände unterscheidbar sind. $q_3$ ist bereits deshalb von jedem anderen Zustand unterscheidbar, da kein anderer  akzeptierend ist. Dies ist gleichbedeutend damit, dass nur von diesem Zustand aus das leere Wort akzeptiert wird. $q_1$ hingegen ist der einzige Zustand, von welchem aus beginnend die Eingabefolge $aa$ akzeptiert wird. Um $q_0$ von $q_2$ zu unterscheiden, wird ein weiteres Experiment benötigt. Das Eingabewort $w_2=b$ wird von $q_0$ ausgehend akzeptiert, von $q_2$ jedoch nicht, was auch diese Zustände als unterscheidbar kennzeichnet.

\noindent Moore folgert insgesamt, dass mehrere einfache Experimente genügen, um für jedes beliebige Zustandspaar feststellen zu können, ob dieses unterscheidbar ist. Das darauf basierende allgemeingültige Verfahren wird von \citeauthor{Hopfcroft.2011} später als \definitionCallout{Table Filling Algorithmus} bezeichnet und soll nun kurz beschrieben werden. 

\subsubsection*{Grundidee}
Der Table-Filling Algorithmus ist ein rekursives Verfahren, welches die Zustandsmenge $Q$ zunächst in die zwei Teilmengen der akzeptierenden und nicht akzeptierenden Zustände aufteilt. Zustände, die unterschiedlichen Teilmengen angehören, sind unterscheidbar.\\
\noindent Zusätzlich dazu sind auch all die Zustände unterscheidbar, die mit demselben Eingabesymbol in zwei unterschiedliche, unterscheidbare Zustände übergehen.
\newpassage 
\begin{exmp}{52idee1}
	In \reffig{fig:52moore1} trifft dies auf die Zustände $q_0$ und $q_1$ zu, welche zwar beide zunächst der Menge der nicht akzeptierenden Zustände angehören, jedoch mit dem Eingabesymbol $a$ in ein Paar unterscheidbarer Zustände übergehen ($q_3$ und $q_2$). Damit werden auch diese beiden Zustände als unterscheidbar vermerkt und müssen bei weiteren Überprüfungen mit bedacht werden.
\end{exmp}
\subsubsection*{Vorgehensweise}
Für einen gegebenen \ac{DEA} $D = (Q_{D}, \Sigma_D, \delta_{D}, q_{0,D}, F_{D})$ wird der gesuchte minimierte DEA $M = (Q_{M}, \Sigma_M, \delta_{M}, q_{0,M}, F_{M})$ wie folgt ermittelt\cite[193\psq]{Hopfcroft.2011}.  
\begin{enumerate}
	\item Zunächst wird eine leere Zustandstabelle $T_Z$ erzeugt, in welche für jedes Zustandspaar $(q_i,q_j)$ eingetragen werden soll, ob dieses unterscheidbar ist.
	\item Daraufhin werden alle Zustandspaare $(q_a,q_{na})$ in $T_Z$ als unterscheidbar markiert, für die gilt: $q_a \in F_D$ und $q_{na} \notin F_D$.
	\item Anschließend werden alle verbleibenden ununterscheidbaren Zustandspaare $(q_i,q_j)$ durchlaufen:
	\begin{enumerate}
		\item $q_{i}$ und $q_{j}$ mit $\delta(q_{i}, a) = q_{ie}$ und $\delta(q_{j}, a) = q_{je}$ werden als unterscheidbar markiert, wenn $(q_{ie}, q_{je})$ unterscheidbar sind.
		\item Das Verfahren endet, wenn alle Zustandspaare in $T_Z$ als unterscheidbar markiert wurden oder in einem vollständigen Durchlauf keine Änderungen mehr aufgetreten sind.
	\end{enumerate}
	\item Alle paarweise nicht unterscheidbaren Zustände werden einer gemeinsamen Äquivalenzklasse zugeordnet. Für zwei in dem Sinne äquivalente Zustände $\{q_i,q_j\}$, welche derselben Äquivalenzklasse $[q_i]$ angehören, kann dann geschrieben werden
	\begin{center}
		 $q_i $ $\equiv$ $q_j$.
	\end{center} Ein Zustand $q_k$, der von allen anderen Zuständen unterschieden werden kann bildet eine eigene Äquivalenzklasse $[q_k]$. Es gilt dann
	\begin{center}
		$\forall i \neq k: q_k \not\equiv q_i$
	\end{center}
	\item Der minimierte DEA $M = (Q_{M}, \Sigma_M, \delta_{M}, q_{0,M}, F_{M})$ ergibt sich damit zu
	\begin{enumerate}
		\item $Q_M$ entspricht der Menge der Äquivalenzklassen $[q]$
		\item $\Sigma_M$ = $\Sigma_D$
		\item $\delta_M$ ergibt sich mit $Q_M$ aus $\delta_D$
		\item $q_{0,M}$ entspricht der Äquivalenzklasse, zu der $q_{0,D}$ gehört
		\item $F_M$ enthält all die Äquivalenzklassen, für deren Elemente gilt $q_i \in F_D$
	\end{enumerate}
\end{enumerate}
\newpassage 
\begin{exmp}{52tfm1}
	Das Verfahren soll zur Minimierung des DEA in \reffig{fig:52moore2} angewandt werden. Um den Vergleich der Verfahren zu erleichtern, wird dieser Automat auch in den kommenden Abschnitten beispielhaft für die Minimierung herangezogen werden.
	\input{./images/52moore2.tex}
	\begin{enumerate}
		\item[1.-2.] Die Zustandstabelle $T_Z$ ist in \reftab{tab:52tfma} zu sehen. Dort sind bereits die durch ihre akzeptierende Eigenschaft unterscheidbaren Zustandspaare markiert.
		\item[3.] In Schritt 2 werden alle verbleibenden Zustandspaare $\{(q_0,q_1), (q_0,q_2),(q_1,q_2), (q_3,q_4)\}$ jeweils einmal untersucht. $(q_1,q_2)$ beispielsweise geht bei Eingabe des Symbols $a$ in das Zustandspaar $(q_2,q_3)$ über. Da dieses in Schritt 1 als unterscheidbar markiert wurde, wird auch für dieses Paar ein Kreuz (violett) in der Tabelle gesetzt. In diesem Durchlauf werden insgesamt zwei Paare als unterscheidbar markiert(siehe \reftab{tab:52tfmb}).\\ 
		\noindent Die in Schritt 2 ermittelten Informationen zur Unterscheidbarkeit von $(q_1,q_2)$ können im folgenden Durchgang genutzt werden, sodass sich für das Paar $(q_0,q_2)$ ergibt:
		\begin{center}
			$\delta(q_0,b) = q_1$ und $\delta(q_2,b) = q_2$ $\Rightarrow (q_0,q_2)$ sind unterscheidbar.
		\end{center} 
		\noindent Weitere Paare werden nicht gefunden, sodass in \reftab{tab:52tfmc} die vollständig ausgefüllte Tabelle zu sehen ist. 
		\item[4.] Insgesamt ergeben sich damit die folgenden vier Äquivalenzklassen
		\begin{align*}
			[q_0] &= \{q_0\}\\
			[q_1] &= \{q_1\}\\
			[q_2] &= \{q_2\}\\
			[q_3] &= \{q_3,q_4\}
		\end{align*}
		\item[5.] Der minimale DEA ist in \reffig{fig:52moore3} abgebildet. Er akzeptiert dieselbe Sprache wie der ursprüngliche Automat, ausgedrückt durch den regulären Ausdruck $r=b[ab]+$.
	\end{enumerate}
\end{exmp}
		\input{./tables/52moore2.tex}
\input{./images/52moore3.tex} 

