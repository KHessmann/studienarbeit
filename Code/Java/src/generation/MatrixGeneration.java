package generation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

import commons.Automaton;
import commons.State;
import commons.Terminal;
import commons.Transition;

public class MatrixGeneration {

	private List<State> states;
	private Automaton automaton;
	private boolean isDea;
	private List<Terminal> terminals = new ArrayList<>();
	private HashMap<State, HashMap<State, List<Terminal>>> matrix = new HashMap<>();
	private Map<String, Transition> transitionMappingToStartAndEnd = new HashMap<>();
	private Map<State, List<Terminal>> terminalsUsedForState = new HashMap<>();
	private int transitionCount;
	private Random rand = new Random();

	public MatrixGeneration(List<State> states, List<Terminal> terminals, int maxNumberOfTransitions,
			boolean useMaxNumberOfTransitions, int numberOfAcceptingStates) {
		this(true, states, terminals, maxNumberOfTransitions, useMaxNumberOfTransitions, numberOfAcceptingStates);
	}

	public MatrixGeneration(boolean dea, List<State> states, List<Terminal> terminals, int maxNumberOfTransitions,
			boolean useMaxNumberOfTransitions, int numberOfAcceptingStates) {
		this.states = states;
		this.terminals = terminals;
		this.isDea = dea;
		createMatrix();
		initiallyConnectStates();
		createNewTransitions(maxNumberOfTransitions, useMaxNumberOfTransitions);
		List<Transition> transitions = transitionMappingToStartAndEnd.values().stream().collect(Collectors.toList());
		addAcceptingAttributeToStates(numberOfAcceptingStates);
		this.automaton = new Automaton(states, transitions);
	}

	private void createMatrix() {
		matrix = GenerationHelper.createInitialStateToStateMatrix(states);
	}

	private void initiallyConnectStates() {
		State initialState = states.get(0);
		initialState.setInitial(true);
		terminalsUsedForState.put(initialState, new ArrayList<>());
		for (int i = 1; i < states.size(); i++) {
			State start = getRandomConnectedState();
			State end = states.get(i);
			Terminal randomTerminal = getRandomTerminal(start);
			Transition transition = new Transition(randomTerminal, start, end, false);
			String name = start.getName() + ", " + end.getName();
			transitionMappingToStartAndEnd.put(name, transition);
			List<Terminal> usedTerminals = terminalsUsedForState.computeIfAbsent(start, key -> new ArrayList<>());
			usedTerminals.add(randomTerminal);
			transitionCount++;
			List<Terminal> terminalList = matrix.get(start).get(end);
			terminalList.add(randomTerminal);
			terminalsUsedForState.put(end, new ArrayList<>());
		}
	}

	private void createNewTransitions(int maxNumberOfTransitions, boolean useMaxNumberOfTransitions) {
		ArrayList<State> possibleEndStates = new ArrayList<>(states);
		for (State start : matrix.keySet()) {
			List<Terminal> usedTerminals = terminalsUsedForState.get(start);
			if (isDea && usedTerminals.size() < terminals.size()) {
				List<Terminal> possibleTerminals = new ArrayList<>(terminals);
				possibleTerminals.removeAll(usedTerminals);
				if (!useMaxNumberOfTransitions
						|| (useMaxNumberOfTransitions && transitionCount <= maxNumberOfTransitions)) {
					for (Terminal t : possibleTerminals) {
						Collections.shuffle(possibleEndStates);
						State end = possibleEndStates.get(0);
						createNewTransition(start, usedTerminals, t, end);
					}
				}
			}
		}
	}

	private void createNewTransition(State start, List<Terminal> usedTerminals, Terminal t, State end) {
		String name = start.getName() + ", " + end.getName();
		Transition transitionForStates = transitionMappingToStartAndEnd.computeIfAbsent(name,
				key -> new Transition(t, start, end, false));
		transitionForStates.addTerminal(t);
		List<Terminal> terminalList = matrix.get(start).get(end);
		terminalList.add(t);
		usedTerminals.add(t);
		transitionCount++;
	}

	private State getRandomConnectedState() {
		List<State> availableConnected = new ArrayList<>();
		if (isDea) {
			for (State state : terminalsUsedForState.keySet()) {
				List<Terminal> terminalList = terminalsUsedForState.get(state);
				if (terminalList != null && terminalList.size() < terminals.size()) {
					availableConnected.add(state);
				}
			}
		} else {
			availableConnected.addAll(terminalsUsedForState.keySet());
		}
		Collections.shuffle(availableConnected);
		State state = availableConnected.get(0);
		return state;
	}

	private Terminal getRandomTerminal(State state) {
		List<Terminal> availableTerminals = new ArrayList<>(terminals);
		if (isDea) {
			availableTerminals.removeAll(terminalsUsedForState.get(state));
		}
		return GenerationHelper.getRandomTerminal(availableTerminals, rand);
	}

	private void addAcceptingAttributeToStates(int numberOfAcceptingStates) {
		List<State> possibleStates = new ArrayList<>(states);
		Collections.shuffle(possibleStates);
		for (int i = 0; i < numberOfAcceptingStates; i++) {
			possibleStates.get(i).setAccepting(true);
		}
	}

	public Automaton getAutomaton() {
		return automaton;
	}
}
