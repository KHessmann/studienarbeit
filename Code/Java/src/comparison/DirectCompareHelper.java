package comparison;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import commons.Automaton;
import commons.State;
import commons.Terminal;
import commons.TransitionTable;
import generation.AutomatonMatrix;
import statistic.structure.IsomorphismEquivalenceClass;
import statistic.structure.SimpleMatrix;

public class DirectCompareHelper {

	/**
	 * @param automaton1 first dea to check
	 * @param automaton2 second dea to check
	 * @return boolean, which shows if the two automaton have exact the same
	 *         structure with possible different naming. The alphabet has to contain
	 *         the same terminals. Doesn't check if both automaton accept the same
	 *         language.
	 */
	public static boolean areSameAutomaton(Automaton automaton1, Automaton automaton2) {
		AutomatonMatrix am1 = automaton1.getAutomatonMatrix();
		AutomatonMatrix am2 = automaton2.getAutomatonMatrix();
		boolean equivalent = true;
		if (am1.isAlmostDea() && am2.isAlmostDea()) {
			if (am1.getSingleCount() != am2.getSingleCount() || am1.getStates().size() != am2.getStates().size()
					|| automaton1.getAccepting().size() != automaton2.getAccepting().size()) {
				equivalent = false;
			} else {
				equivalent = areSameDEA(automaton1, automaton2, am1, am2);
			}
		} else if (!am1.isAlmostDea() && !am2.isAlmostDea()) {
			if (am1.getSequencesCount() == am2.getSequencesCount() && am1.getEpsilonCount() == am2.getEpsilonCount()
					&& automaton1.getAccepting().size() == automaton2.getAccepting().size()) {
				if (am1.getSingleCount() != am2.getSingleCount() || am1.getStates().size() != am2.getStates().size()) {
					equivalent = false;

				} else {
					equivalent = areSameNEA(automaton1, automaton2, am1, am2);
				}
			} else {
				equivalent = false;
			}
		} else {
			equivalent = false;
		}
		return equivalent;
	}

	private static boolean areSameDEA(Automaton automaton1, Automaton automaton2, AutomatonMatrix am1,
			AutomatonMatrix am2) {
		boolean equivalent = false;
		if (isIsomorph(new SimpleMatrix(am1), new SimpleMatrix(am2))) {
			Map<State, State> nameMap = new HashMap<>();
			Map<Terminal, Terminal> terminalMap = setupTerminalMap(automaton1, automaton2);
			if (terminalMap != null) {
				State state1 = automaton1.getInitial();
				State state2 = automaton2.getInitial();
				List<State> stateList = new ArrayList<>();
//				stateList.add(state1);
				nameMap.put(state1, state2);
				equivalent = recursiveCheckEquivalenceForDEA(state1, state2, automaton1.getAlphabet(), terminalMap,
						nameMap, automaton1.getTransitionTable(), automaton2.getTransitionTable(), stateList);
			}
		}
		return equivalent;
	}

	private static boolean checkSameAcceptingProperty(State state1, State state2) {
		return state1.isAccepting() && state2.isAccepting() || !state1.isAccepting() && !state2.isAccepting();
	}

	private static boolean recursiveCheckEquivalenceForDEA(State state1, State state2, Set<Terminal> alphabet,
			Map<Terminal, Terminal> terminalMap, Map<State, State> nameMap, TransitionTable table1,
			TransitionTable table2, List<State> stateList) {
		boolean equivalent = true;
		if (!checkSameAcceptingProperty(state1, state2))
			equivalent = false;
		else {
			stateList.add(state1);
			for (Terminal terminal1 : alphabet) {
				Terminal terminal2 = terminalMap.get(terminal1);
				List<State> states1 = table1.getEndStatesFromTransitionsForState(state1, terminal1);
				List<State> states2 = table2.getEndStatesFromTransitionsForState(state2, terminal2);
				if (states1.size() == states2.size()) {
					if (states1.size() == 1) {
						State endState1 = states1.get(0);
						State endState2 = states2.get(0);
						if (nameMap.get(endState1) != null) {
							if (!nameMap.get(endState1).equals(endState2)) {
								equivalent = false;
								break;
							}
						} else if (nameMap.containsValue(endState2)) {
							equivalent = false;
							break;
						}
						nameMap.put(endState1, endState2);
						if (!stateList.contains(endState1)) {
							List<State> newStateList = new ArrayList<>(stateList);
//							newStateList.add(endState1);
							if (!recursiveCheckEquivalenceForDEA(endState1, endState2, alphabet, terminalMap, nameMap,
									table1, table2, newStateList)) {
								equivalent = false;
								break;
							}
						}

					}

				} else {
					equivalent = false;
					break;
				}
			}
		}
		return equivalent;
	}

	private static Map<Terminal, Terminal> setupTerminalMap(Automaton automaton1, Automaton automaton2) {
		Map<Terminal, Terminal> result = null;
		if (automaton1.getAlphabet().equals(automaton2.getAlphabet())) {
			result = new HashMap<>();
			for (Terminal t1 : automaton1.getAlphabet()) {
				for (Terminal t2 : automaton2.getAlphabet()) {
					if (t1.equals(t2)) {
						result.put(t1, t2);
						break;
					}
				}
			}
		}
		return result;
	}

	private static boolean isIsomorph(SimpleMatrix sm1, SimpleMatrix sm2) {
		IsomorphismEquivalenceClass iso = new IsomorphismEquivalenceClass(sm1);
		return iso.contains(sm2);
	}

	private static boolean areSameNEA(Automaton automaton1, Automaton automaton2, AutomatonMatrix am1,
			AutomatonMatrix am2) {
		if (!isIsomorph(new SimpleMatrix(am1), new SimpleMatrix(am2))) {
			return false;
		}
		Map<Terminal, Terminal> terminalMap = setupTerminalMap(automaton1, automaton2);
		if (terminalMap == null) {
			return false;
		}

		State state1 = automaton1.getInitial();
		State state2 = automaton2.getInitial();
		if (!checkSameAcceptingProperty(state1, state2)) {
			return false;

		}
		throw new IllegalArgumentException("No algorithm implemented for comparison of two nea!");
	}

}
