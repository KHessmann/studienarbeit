package transformation;

import java.util.List;
import java.util.stream.Collectors;

import commons.Automaton;
import commons.State;
import commons.Terminal;
import commons.TerminalType;
import commons.Transition;
import generation.AutomatonMatrix;

public class RemoveInputSequencesTransformer {

	private Automaton automaton;
	private int effort;

	public RemoveInputSequencesTransformer(Automaton automaton, boolean isAlmostDea) {
		this.effort = 1;
		if (isAlmostDea) {
			this.automaton = automaton;
		} else {
			this.automaton = transform(automaton);
		}
	}

	private Automaton transform(Automaton automaton) {
		AutomatonMatrix automatonMatrix = automaton.getAutomatonMatrix();
		Automaton newAutomaton = new Automaton(automaton);
		if (automatonMatrix.getSequencesCount() == 0) {
			return automaton;
		} else {
			return transformSequences(newAutomaton);
		}
	}

	private Automaton transformSequences(Automaton newAutomaton) {
		List<Transition> transitionsWithSequence = newAutomaton.getTransitions().stream()
				.filter(transition -> hasSequenceTerminal(transition)).collect(Collectors.toList());

		for (Transition transition : transitionsWithSequence) {
			List<Terminal> terminals = transition.getTerminals().stream()
					.filter(terminal -> terminal.getType() == TerminalType.SEQUENCE).collect(Collectors.toList());
			for (Terminal t : terminals) {
				char[] sequence = t.getTerminal();
				State start = transition.getStartState();
				effort += sequence.length;
				for (int i = 0; i < sequence.length; i++) {
					Terminal newTerminal = new Terminal(TerminalType.SINGLE, sequence[i]);
					if (i < t.getTerminal().length - 1) {
						String name = "q" + (newAutomaton.getStates().size() + 1);
						State newState = new State(name, false, false);
						newAutomaton.addState(newState);
						newAutomaton.addTransition(new Transition(newTerminal, start, newState, false));
						start = newState;
					} else {
						newAutomaton.addTransition(new Transition(newTerminal, start, transition.getEndState(), false));
					}
				}
				transition.removeTerminal(t);
				if (transition.getTerminals().isEmpty()) {
					newAutomaton.removeTransition(transition);
				}
			}
		}
		newAutomaton.removeTerminalType(TerminalType.SEQUENCE);
		return newAutomaton;
	}

	private boolean hasSequenceTerminal(Transition transition) {
		List<Terminal> terminals = transition.getTerminals().stream()
				.filter(terminal -> terminal.getType() == TerminalType.SEQUENCE).collect(Collectors.toList());
		return !terminals.isEmpty();
	}

	public Automaton getAutomaton() {
		return automaton;
	}

	public int getEffort() {
		return effort;
	}

	public String getInfo() {
		StringBuilder sb = new StringBuilder();
		sb.append("\nREMOVE SEQUENCES \n");
		sb.append("Effort: " + effort + "\n");
		return sb.toString();
	}
}
