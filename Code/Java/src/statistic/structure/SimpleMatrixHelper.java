package statistic.structure;

public class SimpleMatrixHelper {
	/**
	 * Used for conversion of string to vector/matrix and vice versa Note: These two
	 * have to be different!
	 */
	private static String colDelimiter = ",";
	private static String rowDelimiter = ";";

	protected static int getSumForRow(int[][] matrix, int rowNumber) {
		int result = 0;
		int[] row = matrix[rowNumber];
		for (int i = 0; i < row.length; i++) {
			if (row[i] != 0) {
				result += row[i];
			}
		}
		return result;
	}

	protected static int getSumForColumn(int[][] matrix, int colNumber) {
		int result = 0;
		for (int i = 0; i < matrix.length; i++) {
			if (matrix[i][colNumber] != 0) {
				result += matrix[i][colNumber];
			}
		}
		return result;
	}

	protected static boolean isLoopsOnly(int[][] matrix) {
		for (int i = 0; i < matrix.length; i++) {
			if (getSumForRow(matrix, i) != matrix[i][i])
				return false;
		}
		return true;
	}

	protected static boolean isZero(int[][] matrix) {
		for (int i = 0; i < matrix.length; i++) {
			for (int k = 0; k < matrix.length; k++) {
				if (matrix[i][k] != 0) {
					return false;
				}
			}
		}
		return true;
	}

	protected static int[][] multiply(int[][] left, int[][] right) {
		int order = left.length;
		if (order != right.length)
			return null;
		int[][] result = new int[order][order];
		for (int i = 0; i < order; i++) {
			for (int j = 0; j < order; j++) {
				result[i][j] = 0;
				for (int k = 0; k < order; k++) {
					result[i][j] += left[i][k] * right[k][j];
				}
			}
		}
		return result;
	}

	protected static boolean hasEqualEntries(int[] array, int from, int to) {
		boolean equal = true;
		if (array.length > 1) {
			int first = array[from];
			for (int i = 1; i < to + 1; i++) {
				if (first != array[i]) {
					equal = false;
					break;
				}
			}
		}
		return equal;
	}

	protected static int[][] getPermutationMatrixForSortedArray(int[] array) {
		int order = array.length;
		int[][] matrix = new int[order][order];
		for (int col = 0; col < order; col++) {
			int row = array[col];
			matrix[row][col] = 1;
		}
		return matrix;
	}

	protected static int[][] getTransposedMatrix(int[][] matrix) {
		int order = matrix.length;
		int[][] transposed = new int[order][order];
		for (int i = 0; i < order; i++) {
			for (int j = 0; j < order; j++) {
				int k = matrix[i][j];
				if (k != 0) {
					transposed[j][i] = k;
				}
			}
		}
		return transposed;
	}

	protected static boolean equals(int[][] sm1, int[][] sm2) {
		int order = sm1.length;
		if (order != sm2.length) {
			return false;
		}
		boolean result = true;
		for (int i = 0; i < order; i++) {
			for (int j = 0; j < order; j++) {
				if (sm1[i][j] != sm2[i][j]) {
					result = false;
					break;
				}
			}
		}
		return result;
	}

	public static String matrixToString(int[][] matrix) {
		StringBuilder sb = new StringBuilder();
		int order = matrix.length;
		sb.append("[");
		for (int i = 0; i < order; i++) {
			for (int j = 0; j < order; j++) {
				sb.append(matrix[i][j]);
				sb.append(colDelimiter);
			}
			sb.deleteCharAt(sb.length() - 1);
			sb.append("\n");
			sb.append(rowDelimiter);
		}
		sb.deleteCharAt(sb.length() - 1);
		sb.append("]");
		return sb.toString();
	}

	public static String vectorToString(int[] vec) {
		StringBuilder sb = new StringBuilder();
		sb.append("(");
		for (int j = 0; j < vec.length; j++) {
			sb.append(vec[j]);
			sb.append(colDelimiter);
		}
		sb.deleteCharAt(sb.length() - 1);
		sb.append(")");
		return sb.toString();
	}

	/**
	 * @param String matrixString
	 * @return int[][] matrix only works for quadratic (nxn) matrices with
	 *         delimiters as in this class defined
	 */
	public static int[][] stringToMatrix(String matrixString) {
		matrixString = matrixString.replaceAll("[\\s\\[()\\]]", "");
		String[] rowStrings = matrixString.split(rowDelimiter);
		int order = rowStrings.length;
		int[][] matrix = new int[order][order];
		for (int i = 0; i < order; i++) {
			String rowString = rowStrings[i];
			String[] colStrings = rowString.split(colDelimiter);
			for (int j = 0; j < order; j++) {
				matrix[i][j] = Integer.parseInt(colStrings[j]);
			}
		}
		return matrix;
	}

	/**
	 * @param String vectorString
	 * @return int[] vector only works with delimiters as in this class defined
	 */
	public static int[] stringToVector(String vectorString) {
		vectorString = vectorString.replaceAll("[\\s[()\\]]", "");
		String[] colStrings = vectorString.split(colDelimiter);
		int order = colStrings.length;
		int[] vector = new int[order];
		for (int i = 0; i < order; i++) {
			vector[i] = Integer.parseInt(colStrings[i]);
		}
		return vector;
	}

	public static void removeLoops(int[][] matrix) {
		for (int i = 0; i < matrix.length; i++) {
			matrix[i][i] = 0;
		}
	}
}
