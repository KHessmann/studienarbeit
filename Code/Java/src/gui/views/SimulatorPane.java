package gui.views;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import commons.Automaton;
import commons.EffortClass;
import generation.ExerciseGenerator;
import minimization.IncrementalMinimizer;
import minimization.Minimizer;
import transformation.Transformer;

public class SimulatorPane extends JFrame {
	private static final long serialVersionUID = 1L;
	private Automaton automaton;
	private AutomatonPane automatonPane;
	private Transformer transformer;
	private Minimizer minimizer;
	private ConfigurationPane configurationPane;

	public SimulatorPane(Automaton automaton) {
		super("Automaton Simulator");
		this.automaton = automaton;
		setTransformer(new Transformer(automaton));
		setMinimizer(new IncrementalMinimizer(transformer.getRenamedFinalDEA()));
		initView();
		configurationPane.setCurrentAutomaton(automaton);
		configurationPane.showCurrentAutomaton();
	}

	private void initView() {
		JPanel main = new JPanel(new GridBagLayout());
		main.setLayout(new BorderLayout(3, 3));

		configurationPane = new ConfigurationPane(this);
		automatonPane = new AutomatonPane(automaton);

		JScrollPane scroller2 = new JScrollPane(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		scroller2.getViewport().setView(automatonPane);

		JSplitPane split = new JSplitPane(JSplitPane.VERTICAL_SPLIT, configurationPane, scroller2);
		double width = Toolkit.getDefaultToolkit().getScreenSize().getWidth();
		double heigth = Toolkit.getDefaultToolkit().getScreenSize().getHeight();
		split.setPreferredSize(new Dimension((int) width - 40, (int) heigth - 70));

		main.add(split, BorderLayout.CENTER);
		add(main);
		pack();
		setExtendedState(JFrame.MAXIMIZED_BOTH);
		split.setDividerLocation(0.55);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

	public void setAutomaton(Automaton automaton) {
		setTransformer(new Transformer(automaton));
		setMinimizer(new IncrementalMinimizer(getTransformer().getRenamedFinalDEA()));
		this.automaton = automaton;
	}

	public Automaton getAutomaton() {
		return automaton;
	}

	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		Automaton a = generateTransformationTask();
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				new SimulatorPane(a).setVisible(true);
			}
		});
	}

	public static Automaton generateTransformationTask() {
		return ExerciseGenerator.generateAutomatonForTransformationWithDefault(EffortClass.MEDIUM, 1, 2, 1);
	}

	public static Automaton generateMinimizationTask() {
		return ExerciseGenerator.generateAutomatonForMinimizationWithDefault(EffortClass.MEDIUM);
	}

	public void showAutomaton() {
		automatonPane.showGraph();
	}

	public Transformer getTransformer() {
		return transformer;
	}

	public void setTransformer(Transformer transformer) {
		this.transformer = transformer;
	}

	public void setMinimizer(Minimizer minimizer) {
		this.minimizer = minimizer;
	}

	public Minimizer getMinimizer() {
		return minimizer;
	}

}
