%\clearpage 
\section{Watson und Almeidas Inkrementeller Algorithmus}\label{sec:56incremental}
Die in den vorherigen Abschnitten beschriebenen Verfahren, ermöglichen es Anwendern, mithilfe einer klar definierten Vorgehensweise für jeden beliebigen DEA D verlässlich den minimierten DEA M zu ermitteln. Ein Nachteil beider Verfahren liegt darin, dass es Laien schwer fallen kann, nachzuvollziehen warum sie funktionieren. Ein Anwender würde, wenn er das erste Mal mit der Problemstellung konfrontiert wäre, vermutlich intuitiv versuchen, Zustände zusammenzufassen oder aus dem Automaten zu entfernen, ohne die akzeptierte Sprache zu verändern. Der Table Filling Algorithmus arbeitet stattdessen zunächst mit zwei oder sogar nur einer einzigen Äquivalenzklasse, die solange aufgeteilt werden, bis erst im letzten Schritt ein gültiger, minimierter Automat erstellt wird. 

\noindent Das im Folgenden vorgestellte Verfahren orientiert sich an der intuitiven Herangehensweise des Anwenders. Der DEA soll dazu schrittweise minimiert werden. Dabei gilt, dass zu jedem Zeitpunkt ein gültiger Automat ausgegeben werden kann, dessen Zustandszahl stetig abnimmt und der dieselbe Sprache akzeptiert wie der DEA.
Das Verfahren wurde von B.W.Watson nebst anderen entwickelt und von \citeauthor{Almeida.2014} erweitert.
\subsection{Beschreibung des Verfahrens}\label{ssec:561verfahren}

\subsubsection*{Grundidee}
Das Verfahren basiert erneut auf der Verwendung von Äquivalenzklassen. Im Gegensatz zu den vorherigen Verfahren sollen Zustände hier schrittweise auf Äquivalenz überprüft und zusammengefasst werden. Die Vorgehensweise orientiert sich dabei an der in Abschnitt \ref{ssec:234Äquivalenz} aufgeführten Definition der Unterscheidbarkeit. Zwei Zustände werden so nicht unterscheidbar genannt, wenn von diesen Zuständen ausgehend alle Eingabefolgen ebenfalls in nicht unterscheidbare Zustandspaare führen. In dem Verfahren werden alle Zustandspaare, über die noch keine Aussage bezüglich ihrer Unterscheidbarkeit gemacht werden kann, durch das Überprüfen der möglichen Eingabefolgen untersucht. 
\subsubsection*{Vorgehensweise}
Für einen gegebenen \ac{DEA} $D = (Q_{D}, \Sigma_D, \delta_{D}, q_{0,D}, F_{D})$ wird der gesuchte minimierte DEA $M = (Q_{M}, \Sigma_M, \delta_{M}, q_{0,M}, F_{M})$ wie folgt ermittelt\cite[3\psqq]{Almeida.2014}.  
\begin{enumerate}
	\item Zunächst wird für jeden Zustand $q \in Q_D$ eine Äquivalenzklasse $[q]$ erstellt
	\item Außerdem werden alle Zustandspaare $(q_a,q_{na})$ einer Liste unterscheidbarer Zustandspaare $L_U$ hinzugefügt, für die gilt $q_a \in F_D$ und $q_{na} \not\in F_D$ 
	\item Anschließend werden alle Zustandspaare $(q_i,q_j)$ durchlaufen, die nicht bereits in $L_U$ enthalten sind oder in derselben Äquivalenzklasse liegen
	\begin{enumerate}
		\item Ausgehend von $(q_i,q_j)$ wird systematisch für alle Eingabefolgen $w \in \Sigma^k$ das Ergebnis der erweiterten Übergangsfunktion $(\hat{\delta}(q_i,w),\hat{\delta}(q_j,w)) = (q_{ie},q_{j_e})$ ermittelt. Eine Eingabefolge wird nicht mehr um weitere Eingabesymbole erweitert, wenn
		\begin{enumerate}
			\item $(q_{ie},q_{j_e})$ bereits in dem Pfad der durchlaufenen Zustandspaare vorkommt
			\item $[q_{ie}]=[q_{j_e}]$, das End-Zustandspaar also entweder identisch ist oder zu derselben Äquivalenzklasse gehört
			\item $(q_{ie},q_{j_e})\in L_U$, das End-Zustandspaar also bereits als unterscheidbar bekannt ist 
		\end{enumerate}
		\item tritt für alle Eingabefolgen der Fall i) oder ii) ein, sind $(q_i,q_j)$ und alle anderen Zustandspfade auf den durchlaufenen Pfaden jeweils äquivalent.
		\item sobald eine der Eingabefolgen fehlschlägt (Fall iii)) ist das Zustandspaar $(q_i,q_j)$ und alle anderen Paare auf dem zugehörigen Pfad unterscheidbar und wird $L_U$ hinzugefügt. 
	\end{enumerate}
	\item Der minimierte DEA $M = (Q_{M}, \Sigma_M, \delta_{M}, q_{0,M}, F_{M})$ ergibt sich damit zu
	\begin{enumerate}
		\item $Q_M$ entspricht der Menge der Äquivalenzklassen $[q]$
		\item $\Sigma_M$ = $\Sigma_D$
		\item $\delta_M$ ergibt sich mit $Q_M$ aus $\delta_D$
		\item $q_{0,M}$ entspricht der Äquivalenzklasse, zu der $q_{0,D}$ gehört
		\item $F_M$ enthält all die Äquivalenzklassen, für deren Elemente gilt $q_i \in F_D$
	\end{enumerate}
	Zusätzlich kann zu jedem beliebigen Zeitpunkt ein DEA D' auf dieselbe Weise erstellt werden, für den gilt
	\begin{center}
		$|Q_D'| \leq |Q_D|$ und L(D) = L(D')
	\end{center}
\end{enumerate}

\input{./images/562inkr1.tex}

\subsubsection*{Beispiel}
Das Verfahren der schrittweisen Minimierung soll nun beispielhaft für den bereits zuvor betrachteten DEA in \reffig{fig:562inkr1} angewandt werden.
\begin{enumerate}
	\item Erstellen der Äquivalenzklassen $\{[q_0],[q_1],[q_2],[q_3],[q_4]\}$
	\item Unterscheidbare Zustandspaare zu Beginn
	\begin{center}
		$L_U = \{(q_0,q_3),(q_0,q_4),(q_1,q_3),(q_1,q_4),(q_2,q_3),(q_2,q_4)\}$
	\end{center} 
	\item Überprüfung aller verbleibenden Zustandspaare. Die graphische Abbildung dazu findet sich in \reffig{graph:562inkrementell1}. 
	\begin{enumerate}
		\item $(q_0,q_1):$  Das Paar geht durch Einlesen des Worts $w=a$ in ein unterscheidbares Paar über und ist damit selbst unterscheidbar (rot markiert). Weitere Eingabefolgen müssen nicht mehr untersucht werden (gestrichelte Linie).
		\begin{center}
			$(\hat{\delta}(q_0,a),\hat{\delta}(q_1,a)) = (q_2,q_3) \in L_U$ $\Rightarrow (q_0,q_1) \in L_U$
		\end{center}
		\item $(q_0,q_2):$  Das Paar geht durch Einlesen des Worts $w=ba$ zunächst in $(q_1,q_2)$ und anschließend in das unterscheidbare Paar $(q_2,q_3)$ über. Damit sind $(q_0,q_2)$ und $(q_1,q_2)$ unterscheidbar.
		\begin{center}
			$(\hat{\delta}(q_0,ba),\hat{\delta}(q_2,ba)) = ({\delta}(q_1,a),{\delta}(q_2,a)) = (q_2,q_3) \in L_U$ $\Rightarrow \{(q_0,q_2), (q_1,q_2)\} \in L_U$
		\end{center}
			\item $(q_3,q_4):$  Das Paar geht durch Einlesen der Worte $w_1=a, w_2=b$ in $(q_4,q_4)$ über. Da $q_4$ und $q_4$ identisch sind und damit trivialerweise in derselben Äquivalenzklasse liegen, gilt $q_3 \equiv q_4$ (grün markiert).
		\begin{center}
			$(\hat{\delta}(q_3,a),\hat{\delta}(q_4,a)) = (q_4,q_4)_{\equiv}$ $\wedge$ $(\hat{\delta}(q_3,b),\hat{\delta}(q_4,b)) = (q_4,q_4)_{\equiv}$\\ $\Rightarrow q_3 \equiv q_4$ mit $[q_3]
 = \{q_3,q_4\}$
		\end{center}
	\end{enumerate}
	\item Da für alle Zustandspaare eindeutig festgelegt werden konnte, ob sie unterscheidbar sind oder in derselben Äquivalenzklasse liegen, ist das Verfahren beendet. Der resultierende minimale DEA ist wie auch zuvor in \reffig{fig:52moore3} abgebildet.
\end{enumerate}
\input{./tikz/562tree.tex}
\subsection{Implementierung}\label{ssec:562implementierung}
Im Folgenden soll eine mögliche Umsetzung des Verfahrens vorgestellt werden. Der in \reflst{code:562inkr} und \reflst{code:562requiv} aufgeführte Pseudocode orientiert sich im Kern an der Lösung von \cite[3\psqq]{Almeida.2014} und wurde nur in Teilen abgeändert.

\noindent Die Implementierung folgt strukturell dem in Abschnitt \ref{ssec:561verfahren} vorgestellten Verfahren. 
\begin{enumerate}
	\item Die Erstellung der Äquivalenzklassen wird in den Zeilen 9f. im \reflst{code:562inkr} durchgeführt. Eine Äquivalenzklasse entspricht hier einem Tupel aus einem Schlüsselzustand und der Liste zugehöriger Zustände. Zu Beginn enthält diese Liste jeweils ein Element, welches gleichzeitig auch dem Schlüssel entspricht. Der Schlüsselzustand dient dazu, eine Äquivalenzklasse leicht identifizieren zu können. Die Zuordnung der zu Beginn unterscheidbaren Zustandspaare zu einem Set erfolgt in Zeile 11f.
	\item In den Zeilen 13-27 folgt die Überprüfung der verbleibenden Zustandspaare, indem jedes Paar rekursiv durch das Durchlaufen aller benötigten Eingabefolgen auf Unterscheidbarkeit untersucht wird.
	\item Der rekursive Algorithmus ist in \reflst{code:562requiv} aufgeführt und wird für jede Eingabefolge (Pfad) solange durchgeführt, bis eine der drei möglichen Abbruchbedingungen eintritt:
	\begin{enumerate}
		\item In Zeile 6f. wird für das aktuell untersuchte Zustandspaar überprüft, ob es bereits als unterscheidbar aufgelistet wurde. In diesem Fall ist der vollständige Pfad gescheitert und es wird der boolesche Wert $falsch$ zurückgegeben.
		\item Die Zeile 8f. prüft, ob das Paar bereits in dem durchlaufenen Pfad enthalten ist und melden in diesem Fall für den Pfad einen Erfolg zurück.
		\item Zeile 18: Wenn beide Zustände derselben Äquivalenzklasse angehören, wird ebenfalls $wahr$ zurückgegeben. 
	\end{enumerate}
\item Zeilen 26f.: Scheitert keine der durchlaufenen Eingabefolgen, so werden die Äquivalenzklassen aller überprüften Paare jeweils zusammengefasst (\reflst{code:562inkr}, Z.19-21). Sobald mindestens eine Folge zu einem unterscheidbaren Paar führt, werden die Paare aller fehlgeschlagenen Pfade dem Set unterscheidbarer Zustandspaare hinzugefügt.
\item Zeilen 22-24: Jedes mal, wenn die Äquivalenzklassen eines Zustandspaars zusammengefasst werden, wird mit diesem Algorithmus ein Automat erstellt, der dieses Zwischenergebnis repräsentiert und dieselbe Sprache akzeptiert wie der eingegebene DEA D. 
\end{enumerate}
\input{./code/562Inkrementell.tex}
\input{./code/562PrüfeEquivalenzRekursiv.tex}
\clearpage
\subsection{Überprüfungsmöglichkeiten}\label{ssec:563test}
Das vorgestellte Verfahren zur schrittweisen Minimierung liefert eine intuitive Vorgehensweise, die zuverlässig für jeden beliebigen DEA D den zugehörigen minimierten DEA M erzeugt und dabei in jedem Schritt ein nachvollziehbares Zwischenergebnis liefert. Ein weiteres Beispiel für eine solche schrittweise Minimierung ist in \reffig{fig:564Inkr1} zu sehen. 
\input{./images/564Inkr1.tex}
 
\noindent Ein Problem des Verfahrens ist die zuverlässige Überprüfung der Anwender-Ergebnisse. Zwar wäre es problemlos möglich, die in \reffig{graph:562inkrementell1} gezeigten Baumstrukturen zu überprüfen. Jedoch sind diese Lösungen nicht eindeutig. In Abhängigkeit von der gewählten Abarbeitungsreihenfolge der Zustandspaare kann der Anwender unterschiedliche Bäume erzeugen. Der Vergleich der Abbildungen \reffig{graph:562inkrementell1} und \reffig{graph:564inkrementell2} verdeutlicht diese Problematik.

Die Abbildung \reffig{graph:564inkrementell2} stellt wie auch \reffig{graph:562inkrementell1} den dritten Schritt der Minimierung des DEA aus \reffig{fig:562inkr1} dar. Der Unterschied besteht darin, dass hier das Zustandspaar $(q_1,q_2)$ vor dem Paar $(q_0,q_2)$ untersucht wird. Die vertauschte Reihenfolge bewirkt, dass ein weiterer Baum benötigt wird (vier statt wie zuvor drei). Das liegt daran, dass in Schritt 2 die Informationen für das Paar $(q_1,q_2)$ separat ermittelt werden müssen. In \reffig{graph:562inkrementell1} ist bereits durch die Überprüfung von $(q_0,q_2)$ in Schritt 2 bekannt, dass dieses Paar unterscheidbar ist. Eine zusätzliche Überprüfung in einem weiteren Baum ist nicht notwendig.

Des Weiteren werden in Schritt 3 bei der Überprüfung des Paars $(q_0,q_2)$ durch die Vertauschung der Reihenfolge weniger Überprüfungen benötigt als zuvor in Schritt 2 bei \reffig{graph:562inkrementell1}. Dies liegt daran, dass $(q_1,q_2)$ bereits in Schritt 2 als unterscheidbar markiert wurde.

Unter Berücksichtigung von Regel \ref{rule:Implementierung3} soll es dem Anwender erlaubt sein, beliebige Abarbeitungsreihenfolgen auszuwählen. Es wird eine flexible Methode zur Überprüfung der Anwenderlösung benötigt.

\noindent Ein solcher Algorithmus müsste alle vom Anwender angegebenen Bäume der Reihe nach durchlaufen. Es müsste festgehalten werden, für welche Zustandspaare bereits Informationen ermittelt wurden und ob unter Berücksichtigung dieser Informationen die Lösung des Anwenders korrekt ist. Hätte der Anwender in \reffig{graph:564inkrementell2} beispielsweise die Schritte 2 und 3 erneut getauscht ohne den Baum des Paars $(q_0,q_2)$ anzupassen, so wäre das Ergebnis inkorrekt, da zu diesem Zeitpunkt nicht bekannt wäre, dass $(q_1,q_2)$ unterscheidbar sind. Eine solche Methode wird im Rahmen dieser Arbeit nicht vorgestellt.
\input{./tikz/564tree.tex}

\noindent 
Eine weitere Möglichkeit, die Zwischenergebnisse des Minimierungsverfahrens festzuhalten, ist die Darstellung des Übergangsdiagramms. Auch hier sind in Abhängigkeit von der Abarbeitungsreihenfolge der Zustandspaare verschiedene Zwischenergebnisse möglich. 

Die Abbildungen in \reffig{fig:564Inkr1b} und \ref{fig:564Inkr2} zeigen drei verschiedene Zwischenergebnisse. Ein solches Zwischenergebnis ist in \reffig{fig:564Inkr1b} für den DEA D zu sehen, welcher die Sprache akzeptiert, die durch den regulären Ausdruck $r=b^*$ beschrieben wird.
Die drei akzeptierenden Zustände des Automaten gehören im zugehörigen DEA M derselben Äquivalenzklasse  an ($[q_0]=\{q_0,q_1,q_2\})$. $D'_{(1)}$ repräsentiert den DEA, der nach Untersuchung des Zustandspaars $(q_1,q_2)$ erzeugt werden kann. In \ref{fig:564Inkr2} sind zwei alternative Übergangsdiagramme abgebildet, welche ebenfalls eine gültige Zwischenlösung repräsentieren. Hier wurde zunächst das Zustandspaar $(q_0,q_2)$ untersucht. Die beiden Automaten unterscheiden sich zusätzlich darin, dass nicht klar definiert ist, zu welchem Zustand die Übergangsfunktion $\delta([q_0],b)$ führt. Je nachdem, ob als Endzustand die Klasse $[q2]=[q_0]=\{q_0,q_2\}$ oder $[q_1]$ gewählt wird, ergeben sich zwei unterschiedliche Automaten, die dieselbe Sprache akzeptieren. Das Problem des DEA in \reffig{fig:564Inkr2a} besteht dabei zusätzlich darin, dass nach Regel \ref{rule:Automat2} Zustände, die nicht vom Startzustand aus erreicht werden können, nicht gewünscht sind. Da es sich hierbei lediglich um ein Zwischenergebnis handelt, wird dies an dieser Stelle in Kauf genommen. 

\noindent Die Überprüfung solcher Zustandsdiagramme gestaltet sich schwierig, da auch hier keine eindeutige Lösung ohne die Einführung von Zusatzregeln bestimmt werden kann. Von den in Kapitel \ref{chap:6Testen} vorgestellten Verfahren zur Überprüfung zweier Automaten würde sich daher der direkte Vergleich nicht eignen. Mit den anderen Verfahren könnte lediglich überprüft werden, ob es sich bei dem Zwischenergebnis um einen DEA handelt, der dieselbe Sprache akzeptiert wie die korrekte Lösung.  
\input{./images/564Inkr2.tex}
