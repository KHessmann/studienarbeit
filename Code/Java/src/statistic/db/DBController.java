package statistic.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DBController {
	private final static DBController DB_CONTROLLER = new DBController();
//	private final String dbPath = System.getProperty("user.home") + "\\" + "studienarbeitStatistiken.sqlite";
	private final String dbPath = "studienarbeitStatistiken.sqlite";

	private Connection connection;

	private DBController() {
		initDBConnection();
	}

	public static DBController getInstance() {
		return DB_CONTROLLER;
	}

	/**
	 * inits the connection to the database. it checks for existence of the database
	 * driver, establishes a connection and installs a shutdown hook for safe
	 * database connection.
	 */
	private void initDBConnection() {
		try {
			Class.forName("org.sqlite.JDBC");
		} catch (ClassNotFoundException e) {
			System.out.println("Fehler beim Laden des JDBC-Treibers");
			e.printStackTrace();
		}
		try {
			if (connection != null) {
				return;
			}
			connection = DriverManager.getConnection("jdbc:sqlite:" + dbPath);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		Runtime.getRuntime().addShutdownHook(new Thread(() -> {
			try {
				if (!connection.isClosed() && connection != null) {
					connection.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}));
		initDB();
	}

	/**
	 * init the database with all needed table, if they not exist.
	 */
	private void initDB() {
		executeCreateOrDeleteSQL(
				"CREATE TABLE IF NOT EXISTS TFMMooreMinimizationTest (id INTEGER NOT NULL, dea VARCHAR(250), quickTF VARCHAR(250), slowTF VARCHAR(250), quickMoore VARCHAR(250), slowMoore VARCHAR(250), tfEquivalent boolean, mooreEquivalent boolean, tfMooreEquivalent boolean, was_Minimal boolean, PRIMARY KEY (id));");
		executeCreateOrDeleteSQL(
				"CREATE TABLE IF NOT EXISTS Effort(id INTEGER NOT NULL, dea_id INTEGER NOT NULL, minimized_dea_id INTEGER NOT NULL, tf_effort INTEGER, tf_precalculate_effort INTEGER, moore_effort INTEGER, moore_redundancy_effort INTEGER, PRIMARY KEY(id), FOREIGN KEY (minimized_dea_id) REFERENCES DEAInfo(id) ON DELETE CASCADE, FOREIGN KEY (dea_id) REFERENCES DEAInfo(id) ON DELETE CASCADE);");
		executeCreateOrDeleteSQL(
				"CREATE TABLE IF NOT EXISTS MinimizationAlgorithmTest(id INTEGER NOT NULL, dea VARCHAR(250), table_filling VARCHAR(250), moore VARCHAR(250), incremental VARCHAR(250), directTfmMoore boolean, directTfmIncr boolean, directMooreIncr boolean, wordTfm boolean, wordMoore boolean, wordIncr boolean, tfmTFM boolean, tfmMoore boolean, tfmIncr boolean, PRIMARY KEY (id));");
		executeCreateOrDeleteSQL(
				"CREATE TABLE IF NOT EXISTS TransformationTest (id INTEGER NOT NULL, nea VARCHAR(250), dea VARCHAR(250), is_identical boolean, witness VARCHAR(100), PRIMARY KEY (id));");
		executeCreateOrDeleteSQL(
				"CREATE TABLE IF NOT EXISTS StructureMatrix (id INTEGER NOT NULL, matrix VARCHAR(250), to_vector VARCHAR(35), from_vector VARCHAR(35), loop_vector VARCHAR(35), PRIMARY KEY (id));");
		executeCreateOrDeleteSQL(
				"CREATE TABLE IF NOT EXISTS Equivalence (id INTEGER NOT NULL, matrix_id INTEGER NOT NULL, PRIMARY KEY (id), FOREIGN KEY (matrix_id) REFERENCES StructureMatrix(id) ON DELETE CASCADE);");
		executeCreateOrDeleteSQL(
				"CREATE TABLE IF NOT EXISTS NEAInfo (id INTEGER NOT NULL, automaton VARCHAR(2000), state_count INTEGER, true_transition_count INTEGER, structure_transition_count INTEGER, accepting_count INTEGER, single_count INTEGER, sequence_count INTEGER, epsilon_count INTEGER, loop_count INTEGER, max_distance INTEGER, is_almost_dea boolean, PRIMARY KEY (id));");
		executeCreateOrDeleteSQL(
				"CREATE TABLE IF NOT EXISTS DEAInfo (id INTEGER NOT NULL, automaton VARCHAR(250), state_count INTEGER, true_transition_count INTEGER, structure_transition_count INTEGER, accepting_count INTEGER, loop_count INTEGER, max_distance INTEGER, is_minimal boolean, PRIMARY KEY (id));");
		executeCreateOrDeleteSQL(
				"CREATE TABLE IF NOT EXISTS NEAEquivalenceClass(id INTEGER NOT NULL, matrix_id INTEGER NOT NULL, equivalence_id INTEGER NOT NULL, nea_id INTEGER NOT NULL,PRIMARY KEY(id), FOREIGN KEY (matrix_id ) REFERENCES StructureMatrix(id) ON DELETE CASCADE, FOREIGN KEY(equivalence_id) REFERENCES Equivalence(id), FOREIGN KEY(nea_id) REFERENCES NEAInfo(id) ON DELETE CASCADE);");
		executeCreateOrDeleteSQL(
				"CREATE TABLE IF NOT EXISTS DEAEquivalenceClass(id INTEGER NOT NULL, matrix_id INTEGER NOT NULL, equivalence_id INTEGER NOT NULL, dea_id INTEGER NOT NULL,PRIMARY KEY(id), FOREIGN KEY (matrix_id ) REFERENCES StructureMatrix(id) ON DELETE CASCADE, FOREIGN KEY(equivalence_id) REFERENCES Equivalence(id), FOREIGN KEY(dea_id) REFERENCES DEAInfo(id) ON DELETE CASCADE);");
		executeCreateOrDeleteSQL(
				"CREATE TABLE IF NOT EXISTS NEAGeneration(id INTEGER NOT NULL, e_nea_id INTEGER NOT NULL,nea_id INTEGER NOT NULL, dea_id INTEGER NOT NULL, effort_sequence INTEGER, effort_epsilon INTEGER, effort_subset INTEGER, PRIMARY KEY(id), FOREIGN KEY (e_nea_id) REFERENCES NEAInfo(id) ON DELETE CASCADE, FOREIGN KEY (nea_id) REFERENCES NEAInfo(id) ON DELETE CASCADE, FOREIGN KEY (dea_id) REFERENCES DEAInfo(id) ON DELETE CASCADE);");
		executeCreateOrDeleteSQL(
				"CREATE TABLE IF NOT EXISTS DEAGeneration(id INTEGER NOT NULL, nea_id INTEGER NOT NULL, dea_id INTEGER NOT NULL, minimized_dea_id INTEGER NOT NULL, effort INTEGER, PRIMARY KEY(id), FOREIGN KEY (nea_id) REFERENCES NEAInfo(id) ON DELETE CASCADE, FOREIGN KEY (dea_id) REFERENCES DEAInfo(id) ON DELETE CASCADE, FOREIGN KEY(minimized_dea_id) REFERENCES DEAInfo(id) ON DELETE CASCADE);");

	}

	/**
	 * executes the give SQL statement and returns the result. only for basic
	 * statements, like SELECT and DELETE.
	 * 
	 * @param sql SQL statement as String
	 * @return ResultSet
	 */
	protected ResultSet executeSQL(String sql) {
		try {
			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery(sql);
			return rs;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	protected void executeCreateOrDeleteSQL(String sql) {
		try {
			Statement statement = connection.createStatement();
			statement.executeUpdate(sql);
			statement.close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * executes the give SQL statement. only for table and database updates
	 * 
	 * Returns the generated id as an int
	 * 
	 * @param sql SQL statement as String
	 */
	protected int executeUpdateSQL(String sql) {
		int generatedKey = -1;
		try {
			PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			statement.executeUpdate();
			ResultSet rs = statement.getGeneratedKeys();
			if (rs.next()) {
				generatedKey = rs.getInt(1);
			} else {
				throw (new SQLException("No row has been created."));
			}
			statement.close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return generatedKey;
	}
}
