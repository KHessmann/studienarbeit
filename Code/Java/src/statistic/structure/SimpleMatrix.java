package statistic.structure;

import java.util.HashMap;
import java.util.List;

import commons.State;
import commons.Terminal;
import generation.AutomatonMatrix;

public class SimpleMatrix {
	private int[][] structureMatrix;
	private int[] fromTransitions;
	private int[] toTransitions;
	private int[] loopTransitions;
	private int order;

	public SimpleMatrix(AutomatonMatrix automaton) {
		order = automaton.getStates().size();
		fromTransitions = new int[order];
		toTransitions = new int[order];
		loopTransitions = new int[order];
		processAutomaton(automaton);

		sortMatrix();
	}

	public SimpleMatrix(int[][] matrix) {
		order = matrix.length;
		structureMatrix = matrix;
		fromTransitions = new int[order];
		toTransitions = new int[order];
		loopTransitions = new int[order];

		sortMatrix();
	}

	private void processAutomaton(AutomatonMatrix automaton) {
		List<State> states = automaton.getStates();
		HashMap<State, Integer> stateNumberMap = new HashMap<>();
		structureMatrix = new int[order][order];

		for (int i = 0; i < order; i++) {
			stateNumberMap.put(states.get(i), i);
		}

		for (int from = 0; from < order; from++) {
			State fromState = states.get(from);
			HashMap<State, List<Terminal>> stateTerminalMap = automaton.getTransitionsForState(fromState);
			for (State toState : stateTerminalMap.keySet()) {
				if (!stateTerminalMap.get(toState).isEmpty()) {
					int to = stateNumberMap.get(toState);
					structureMatrix[from][to] = 1;
				}
			}
		}
	}

	private void computeTransitionArrays() {
		for (int i = 0; i < structureMatrix.length; i++) {
			fromTransitions[i] = SimpleMatrixHelper.getSumForRow(structureMatrix, i);
			toTransitions[i] = SimpleMatrixHelper.getSumForColumn(structureMatrix, i);
			loopTransitions[i] = structureMatrix[i][i];
		}
	}

	/*
	 * Rules for sorting: 1. Number of transitions to another state 2. Number of
	 * transitions from another state 3. occurrence of loop (transition from state
	 * to itself)
	 */
	/*
	 * Not implemented: 4. sort according to order of higher ranking states that
	 * this state connects to 5. sort according to order of higher ranking states
	 * that this state connects from 6. sort according to number of states of the
	 * same equivalence class that this state connects to
	 */
	private void sortMatrix() {
		computeTransitionArrays();

		int[] permArray = getPermutationArray();

		switchStateOrder(permArray);

	}

	/*
	 * Use insertion sort and sort the permutation array with every permutation Sort
	 * first according to number of transitions from state Sort then according to
	 * number of transitions to states Last, sort according to number of transitions
	 * to self Return the permutation array
	 */
	private int[] getPermutationArray() {
		int[] perm = new int[order];
		for (int i = 0; i < order; i++) {
			perm[i] = i;
		}
		if (!SimpleMatrixHelper.hasEqualEntries(fromTransitions, 0, order - 1)) {
			for (int i = 1; i < order; i++) {
				int maxFrom = fromTransitions[i];
				int maxTo = toTransitions[i];
				int maxLoop = loopTransitions[i];
				int maxState = perm[i];
				int j = i - 1;
				while ((j > -1) && (fromTransitions[j] < maxFrom)) {
					fromTransitions[j + 1] = fromTransitions[j];
					toTransitions[j + 1] = toTransitions[j];
					loopTransitions[j + 1] = loopTransitions[j];
					perm[j + 1] = perm[j];
					j--;
				}
				fromTransitions[j + 1] = maxFrom;
				toTransitions[j + 1] = maxTo;
				loopTransitions[j + 1] = maxLoop;
				perm[j + 1] = maxState;
			}
		}
		int from = 0;
		while (from < order) {
			int maxFrom = fromTransitions[from];
			int to = from;
			while (to + 1 < order && fromTransitions[to + 1] == maxFrom) {
				to++;
			}

			if (to - from > 0) {
				getPermutationArrayWithToTransitions(perm, from, to);
			}
			from = to + 1;
		}

		return perm;
	}

	/*
	 * Use insertion sort and sort the permutation array with every permutation\\
	 * Sort first according to number of transitions to states Then, sort according
	 * to number of transitions to self Return the permutation array
	 */
	private int[] getPermutationArrayWithToTransitions(int[] perm, int from, int to) {
		if (!SimpleMatrixHelper.hasEqualEntries(toTransitions, from, to)) {
			for (int i = from + 1; i < to + 1; i++) {
				int maxTo = toTransitions[i];
				int maxLoop = loopTransitions[i];
				int maxState = perm[i];
				int j = i - 1;
				while ((j > from - 1) && (toTransitions[j] < maxTo)) {
					toTransitions[j + 1] = toTransitions[j];
					loopTransitions[j + 1] = loopTransitions[j];
					perm[j + 1] = perm[j];
					j--;
				}
				toTransitions[j + 1] = maxTo;
				loopTransitions[j + 1] = maxLoop;
				perm[j + 1] = maxState;

			}

			int k = from;
			while (k < to + 1) {
				int maxTo = toTransitions[k];
				int l = k;
				while (l + 1 < order && toTransitions[l + 1] == maxTo) {
					l++;
				}
				if (l - k > 0) {
					getPermutationArrayWithLoopTransitions(perm, k, l);
				}
				k = l + 1;
			}

		}

		return perm;
	}

	/*
	 * Use insertion sort and sort the permutation array with every permutation Sort
	 * according to number of transitions to self Return the permutation array
	 */
	private int[] getPermutationArrayWithLoopTransitions(int[] perm, int from, int to) {
		if (!SimpleMatrixHelper.hasEqualEntries(loopTransitions, from, to)) {
			for (int k = from + 1; k < to + 1; k++) {
				int maxLoop = loopTransitions[k];
				int maxState = perm[k];
				int l = k - 1;
				while ((l > from - 1) && (loopTransitions[l] < maxLoop)) {
					loopTransitions[l + 1] = loopTransitions[l];
					perm[l + 1] = perm[l];
					l--;
				}
				loopTransitions[l + 1] = maxLoop;
				perm[l + 1] = maxState;

			}
		}
		return perm;
	}

	private void switchStateOrder(int[] array) {
		int[][] permutation = SimpleMatrixHelper.getPermutationMatrixForSortedArray(array);
		int[][] transposedPermutation = SimpleMatrixHelper.getTransposedMatrix(permutation);
		int[][] sortedMatrix = SimpleMatrixHelper.multiply(transposedPermutation, structureMatrix);
		structureMatrix = SimpleMatrixHelper.multiply(sortedMatrix, permutation);
	}

	public int[][] getStructureMatrix() {
		return structureMatrix;
	}

	public int[] getFromTransitions() {
		return fromTransitions;
	}

	public int[] getToTransitions() {
		return toTransitions;
	}

	public int[] getLoopTransitions() {
		return loopTransitions;
	}

	public int getOrder() {
		return order;
	}

	/**
	 * @return longest distance between any two states not including loops. If the
	 *         longest possible distance is smaller then the order, there are no
	 *         ring structures. Else the return value equals Integer.MAX_VALUE.
	 */
	public int getLongestDistanceWithoutLoops() {
		int longestDistance = 1;
		int[][] currentMatrix = structureMatrix.clone();
		SimpleMatrixHelper.removeLoops(currentMatrix);
		for (int i = 1; i < order; i++) {
			currentMatrix = SimpleMatrixHelper.multiply(structureMatrix, currentMatrix);
			if (SimpleMatrixHelper.isZero(currentMatrix))
				break;
			longestDistance++;
		}
		if (longestDistance == order) {
			longestDistance = Integer.MAX_VALUE;
		}
		return longestDistance;
	}

	public int getLoopNumber() {
		int loopCount = 0;
		for (int i = 0; i < order; i++) {
			if (loopTransitions[i] != 0)
				loopCount++;
		}
		return loopCount;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Matrix:\n");
		sb.append(SimpleMatrixHelper.matrixToString(structureMatrix));
		sb.append("\n");

		sb.append("Transitions from State:\n");
		sb.append(SimpleMatrixHelper.vectorToString(fromTransitions));
		sb.append("\n");

		sb.append("Transitions to State:\n(");
		sb.append(SimpleMatrixHelper.vectorToString(toTransitions));
		sb.append("\n");

		sb.append("Loops:\n(");
		sb.append(SimpleMatrixHelper.vectorToString(loopTransitions));
		sb.append("\n");

		return sb.toString();
	}

}
