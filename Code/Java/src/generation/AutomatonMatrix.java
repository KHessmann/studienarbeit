package generation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import commons.State;
import commons.Terminal;
import commons.TerminalType;
import commons.Transition;

public class AutomatonMatrix {

	private HashMap<State, HashMap<State, List<Terminal>>> matrix = new HashMap<>();
	private List<State> states;
	private int epsilonCount = 0;
	private int sequencesCount = 0;
	private int singleCount = 0;
	private boolean isAlmostDea;

	public AutomatonMatrix(List<State> states, List<Transition> transitions) {
		this.states = new ArrayList<>(states);
		matrix = GenerationHelper.createInitialStateToStateMatrix(states);
		for (Transition transition : transitions) {
			State startState = transition.getStartState();
			State endState = transition.getEndState();
			List<Terminal> transitionTerminals = matrix.get(startState).get(endState);
			Set<Terminal> terminals = transition.getTerminals();
			transitionTerminals.addAll(terminals);
		}
		isAlmostDea = checkDEAProperty(true);
	}

	private boolean checkDEAProperty(boolean countTerminalTypes) {
		boolean isAlmostDea = true;
		for (State from : matrix.keySet()) {
			Set<Character> terminals = new HashSet<>();
			HashMap<State, List<Terminal>> transitions = matrix.get(from);
			for (State to : transitions.keySet()) {
				for (Terminal t : transitions.get(to)) {
					if (t.getType() == TerminalType.SINGLE) {
						if (!terminals.add(t.getTerminal()[0])) {
							if (!countTerminalTypes) {
								return false;
							}
							isAlmostDea = false;
						}
						this.singleCount++;
					} else if (t.getType() == TerminalType.SEQUENCE) {
						if (!countTerminalTypes) {
							return false;
						}
						this.sequencesCount++;
						isAlmostDea = false;
					} else if (t.getType() == TerminalType.EPSILON) {
						if (!countTerminalTypes) {
							return false;
						}
						this.epsilonCount++;
						isAlmostDea = false;
					}
				}
			}
		}
		return isAlmostDea;
	}

	public HashMap<State, List<Terminal>> getTransitionsForState(State state) {
		return matrix.get(state);
	}

	public List<State> getStates() {
		return states;
	}

	public boolean isAlmostDea() {
		return isAlmostDea;
	}

	public int getSingleCount() {
		return singleCount;
	}

	public int getSequencesCount() {
		return sequencesCount;
	}

	public int getEpsilonCount() {
		return epsilonCount;
	}
}
