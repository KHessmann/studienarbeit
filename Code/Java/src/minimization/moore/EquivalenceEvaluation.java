package minimization.moore;

import java.util.Map;

import commons.State;
import commons.Terminal;
import helper.TableStringBuilder;

public final class EquivalenceEvaluation {
	private Map<Terminal, EquivalenceInfo> evaluation;
	private boolean equivalent;
	private State state2;
	private State state1;
	private boolean redundantInformation;

	public EquivalenceEvaluation(State s1, State s2, Map<Terminal, EquivalenceInfo> evaluation, boolean equivalent,
			boolean redundantInformation) {
		this.state1 = s1;
		this.state2 = s2;
		this.evaluation = evaluation;
		this.equivalent = equivalent;
		this.redundantInformation = redundantInformation;
	}

	public State getFirstState() {
		return state1;
	}

	public State getSecondState() {
		return state2;
	}

	public Map<Terminal, EquivalenceInfo> getEvaluation() {
		return evaluation;
	}

	public EquivalenceInfo getEvaluationForTerminal(Terminal t) {
		return evaluation.get(t);
	}

	public boolean isEquivalent() {
		return equivalent;
	}

	public boolean isRedundantInformation() {
		return redundantInformation;
	}

	@Override
	public String toString() {
		String[][] table = new String[evaluation.size() + 1][4];
		table[0][0] = "Terminal";
		table[0][1] = state1.getName();
		table[0][2] = state2.getName();
		table[0][3] = "Result";
		int i = 1;
		for (Terminal t : evaluation.keySet()) {
			EquivalenceInfo info = evaluation.get(t);
			State firstEnd = info.getFirstEndState();
			State secondEnd = info.getSecondEndState();
			table[i][0] = t.toString();
			table[i][1] = firstEnd == null ? " " : firstEnd.getName();
			table[i][2] = secondEnd == null ? "  " : secondEnd.getName();
			table[i][3] = info.isEquivalent() ? "\u2713" : "x";
			i++;
		}
		return TableStringBuilder.getTableString(table);
	}
}
