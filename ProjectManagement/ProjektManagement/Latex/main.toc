\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {ngerman}{}\relax 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1}Thema: Graphentheorie}{2}{section.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.1}Datum: 21. Oktober 2021}{2}{subsection.1.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2}Mealy/Moore Automat}{4}{section.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1}Datum: 21. Oktober 2021}{4}{subsection.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3}Nichtdeterministische endliche Automaten (NEA)}{4}{section.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1}Datum: 24. Oktober 2021}{4}{subsection.3.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4}Generierung eines NEA}{6}{section.4}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1}Datum: 23./24. Oktober 2021}{6}{subsection.4.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5}Isomorphie}{9}{section.5}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1}Grundproblem}{9}{subsection.5.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2}Adjazenzmatrix}{9}{subsection.5.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.3}Algorithmus}{10}{subsection.5.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6}Statistik}{12}{section.6}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.1}Datenbank}{12}{subsection.6.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.2}Diagramme}{12}{subsection.6.2}%
