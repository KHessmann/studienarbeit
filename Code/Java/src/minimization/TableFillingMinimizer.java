package minimization;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import commons.Automaton;
import commons.State;
import commons.StateTuple;
import commons.Terminal;
import commons.Transition;
import minimization.tableFilling.TableFillingSteps;

public class TableFillingMinimizer implements Minimizer {

	private Map<StateTuple, Boolean> equivalenceTable = new LinkedHashMap<>();
	private List<StateTuple> queue = new ArrayList<>();

	private Automaton automaton;

	private List<State> initialStatesWithoutInComingTransitions = new ArrayList<>();

	private TableFillingSteps steps;
	private int effort;
	private int preCalculatePredecessorsEffort;

	public TableFillingMinimizer(Automaton a) {
		steps = new TableFillingSteps(a);
		automaton = a;
	}

	@Override
	public Automaton minimize() {
		clearOldValuesAndCreateInitialValues();

		Map<StateTuple, Set<StateTuple>> predecessors = calculatePredecessors(automaton.getTransitions());
		createEquivalenceTable(automaton.getStates());
		identifyDistinguishableStateTuples(predecessors);
		Set<EquivalenceClass> equivalentStates = createEquivalenzRelations(automaton.getStates());
		Automaton result = MinimizationHelper.createNewAutomaton(automaton, equivalentStates);
		steps.setFinalAutomaton(result);
		return result;
	}

	public Map<StateTuple, Boolean> createFinalEquivalenceTable() {
		clearOldValuesAndCreateInitialValues();
		Map<StateTuple, Set<StateTuple>> predecessors = calculatePredecessors(automaton.getTransitions());
		createEquivalenceTable(automaton.getStates());
		identifyDistinguishableStateTuples(predecessors);
		return equivalenceTable;
	}

	private void clearOldValuesAndCreateInitialValues() {
		equivalenceTable.clear();
		queue.clear();
		initialStatesWithoutInComingTransitions.clear();
		initialStatesWithoutInComingTransitions = new ArrayList<>(automaton.getStates());
	}

	private void identifyDistinguishableStateTuples(Map<StateTuple, Set<StateTuple>> predecessors) {
		boolean initialWithoutInComingTransitions = !initialStatesWithoutInComingTransitions.isEmpty();
		for (int i = 0; i < queue.size(); i++) {
			StateTuple next = queue.get(i);
			Set<StateTuple> predecessorSet = predecessors.get(next);
			if (initialWithoutInComingTransitions
					&& (next.getFirstState().isInitial() || next.getSecondState().isInitial())) {
				effort += 2;
			} else {
				effort += 4;
			}
			if (predecessorSet != null) {
				preCalculatePredecessorsEffort += predecessorSet.size();
				effort += predecessorSet.size() * 3;
				for (StateTuple predecessor : predecessorSet) {
					equivalenceTable.put(predecessor, false);
					if (!queue.contains(predecessor)) {
						queue.add(predecessor);
					}
				}
			}
		}
		steps.setFinalQueue(queue);
		steps.setFinalEquivalenceTable(equivalenceTable);

		if (initialWithoutInComingTransitions) {
			effort++;
		}
		preCalculatePredecessorsEffort += queue.size() * 2;
	}

	private void createEquivalenceTable(List<State> states) {
		for (int i = 0; i < states.size(); i++) {
			for (int j = states.size() - 1; j > i; j--) {
				State s1 = states.get(i);
				State s2 = states.get(j);
				StateTuple stateTuple = new StateTuple(s1, s2);
				if ((s1.isAccepting() && s2.isAccepting()) || (!s1.isAccepting() && !s2.isAccepting())) {
					equivalenceTable.put(stateTuple, true);
				} else {
					equivalenceTable.put(stateTuple, false);
					queue.add(stateTuple);
				}
			}
		}
		List<StateTuple> queueCopy = copyQueue();
		steps.setStartQueue(queueCopy);
		Map<StateTuple, Boolean> equivalenceTableCopy = copyEquivalenceTable();
		steps.setStartEquivalenceTable(equivalenceTableCopy);

		int notEquivalentTuple = (int) equivalenceTable.values().stream().filter(isEquivalent -> !isEquivalent).count();
		effort += 1 + notEquivalentTuple;
		preCalculatePredecessorsEffort += 1 + notEquivalentTuple;
	}

	private Map<StateTuple, Boolean> copyEquivalenceTable() {
		Map<StateTuple, Boolean> copy = new HashMap<>();
		for (StateTuple tuple : equivalenceTable.keySet()) {
			StateTuple newTuple = new StateTuple(tuple.getFirstState(), tuple.getSecondState());
			boolean isEquivalent = equivalenceTable.get(tuple).booleanValue();
			copy.put(newTuple, isEquivalent);
		}
		return copy;
	}

	private List<StateTuple> copyQueue() {
		List<StateTuple> copy = new ArrayList<>();
		for (StateTuple tuple : queue) {
			StateTuple newTuple = new StateTuple(tuple.getFirstState(), tuple.getSecondState());
			copy.add(newTuple);
		}
		return copy;
	}

	private Map<StateTuple, Set<StateTuple>> calculatePredecessors(List<Transition> transitions) {
		Map<StateTuple, Set<StateTuple>> predecessors = new HashMap<>();
		for (Transition transition : transitions) {
			initialStatesWithoutInComingTransitions.remove(transition.getEndState());
			for (Transition t : transitions) {
				if (!transition.getEndState().equals(t.getEndState())
						&& !transition.getStartState().equals(t.getStartState())) {
					Set<StateTuple> startStates = new HashSet<>();
					StateTuple endStates = new StateTuple(transition.getEndState(), t.getEndState());
					Set<StateTuple> startTuples = predecessors.computeIfAbsent(endStates, key -> startStates);
					boolean hasSameTerminal = false;
					for (Terminal terminal : transition.getTerminals()) {
						if (!hasSameTerminal) {
							hasSameTerminal = t.getTerminals().contains(terminal);
						}
					}
					if (hasSameTerminal) {
						StateTuple startStateTuple = new StateTuple(transition.getStartState(), t.getStartState());
						startTuples.add(startStateTuple);
					}
				}
			}
		}
		predecessors.values().stream()
				.forEach(predecessorSet -> preCalculatePredecessorsEffort += predecessorSet.size() * 3);
		steps.setPredecessors(predecessors);
		return predecessors;
	}

	private Set<EquivalenceClass> createEquivalenzRelations(List<State> states) {
		Set<EquivalenceClass> equivalentStates = new HashSet<>();
		ArrayList<StateTuple> stateTuples = new ArrayList<>(equivalenceTable.keySet());
		List<State> currentList = new ArrayList<>();
		int stateCounter = 0;
		State currentState = new ArrayList<>(states).get(stateCounter);
		currentList.add(currentState);
		equivalentStates.add(new EquivalenceClass(currentList, currentState));
		boolean isInExsistingRelation = false;
		State lastState = states.get(states.size() - 1);
		boolean isLastStateInEquivalenceRelation = false;
		for (StateTuple tuple : stateTuples) {
			isInExsistingRelation = false;
			if (!tuple.isFirstOrSecondState(currentState)) {
				stateCounter++;
				currentState = states.get(stateCounter);
			}
			if (!currentList.contains(currentState)) {
				for (List<State> relation : getAllEquivalentStateLists(equivalentStates)) {
					if (relation.contains(currentState)) {
						isInExsistingRelation = true;
						currentList = relation;
					}
				}
				if (!isInExsistingRelation) {
					currentList = new ArrayList<>();
					currentList.add(currentState);
					equivalentStates.add(new EquivalenceClass(currentList, currentState));
				}
			}
			if (equivalenceTable.get(tuple)) {
				State otherState = tuple.getOtherState(currentState);
				if (otherState.equals(lastState)) {
					isLastStateInEquivalenceRelation = true;
				}
				if (!currentList.contains(otherState)) {
					currentList.add(otherState);
				}
			}
		}
		if (!isLastStateInEquivalenceRelation) {
			currentList = new ArrayList<>();
			currentList.add(lastState);
			equivalentStates.add(new EquivalenceClass(currentList, lastState));
		}
		int equivalenceClasses = equivalentStates.size();
		int equivalenceClassesEffort = Math.min(equivalenceClasses,
				automaton.getStates().size() + 1 - equivalenceClasses);
		effort += equivalenceClassesEffort;
		preCalculatePredecessorsEffort += equivalenceClassesEffort;
		return equivalentStates;
	}

	private List<List<State>> getAllEquivalentStateLists(Set<EquivalenceClass> equivalentStates) {
		List<List<State>> listsOfEquivalenceClasses = new ArrayList<>();
		for (EquivalenceClass eq : equivalentStates) {
			listsOfEquivalenceClasses.add(eq.getStates());
		}
		return listsOfEquivalenceClasses;
	}

	public TableFillingSteps getStepwiseResults() {
		if (steps == null || steps.isUnset()) {
			minimize();
		}
		return steps;
	}

	@Override
	public int getEffort() {
		if (effort == 0) {
			minimize();
		}
		return effort;
	}

	public int getPreCalculatePredecessorsEffort() {
		if (preCalculatePredecessorsEffort == 0) {
			minimize();
		}
		return preCalculatePredecessorsEffort;
	}

	public String getInfo(int step) {
		StringBuilder sb = new StringBuilder();
		sb.append(steps.getInfo(step));
		if (step >= 3) {
			sb.append("Effort with predecessors calculated when needed: " + effort + "\n");
			sb.append("Effort for precalculated predecessors: " + preCalculatePredecessorsEffort + "\n");
		}
		return sb.toString();
	}

	public Automaton getAutomaton() {
		return automaton;
	}
}
