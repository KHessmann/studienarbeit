package transformation;

import commons.Automaton;

public class Transformer {

	private RemoveInputSequencesTransformer removeSequencesTransformer;
	private RemoveEpsilonTransformer removeEpsilonTransformer;
	private SubsetConstructionTransformer subsetConstructionTransformer;
	private Automaton originalNEA;
	private Automaton renamedFinalDEA;

	public Transformer(Automaton a) {
		originalNEA = a;
		boolean isAlmostDea = a.getAutomatonMatrix().isAlmostDea();
		removeSequencesTransformer = new RemoveInputSequencesTransformer(a, isAlmostDea);
		removeEpsilonTransformer = new RemoveEpsilonTransformer(removeSequencesTransformer.getAutomaton(), isAlmostDea);
		subsetConstructionTransformer = new SubsetConstructionTransformer(removeEpsilonTransformer.getAutomaton(),
				isAlmostDea);
		renamedFinalDEA = new Automaton(subsetConstructionTransformer.getAutomaton(), true);

	}

	public Automaton getOriginalNEA() {
		return originalNEA;
	}

	public Automaton getRemovedSequenceNEA() {
		return removeSequencesTransformer.getAutomaton();
	}

	public Automaton getRemovedEpsilonNEA() {
		return removeEpsilonTransformer.getAutomaton();
	}

	public Automaton getFinalDEA() {
		return subsetConstructionTransformer.getAutomaton();
	}

	public Automaton getRenamedFinalDEA() {
		return renamedFinalDEA;
	}

	public int getEffortForSequenceTransformation() {
		return removeSequencesTransformer.getEffort();
	}

	public int getEffortForEpsilonTransformation() {
		return removeEpsilonTransformer.getEffort();
	}

	public int getEffortForSubsetConstructionTransformation() {
		return subsetConstructionTransformer.getEffort();
	}

	public int getEffortForTransformation() {
		return removeSequencesTransformer.getEffort() + removeEpsilonTransformer.getEffort()
				+ subsetConstructionTransformer.getEffort();
	}

	public String getInfoForSubsetConstructionTransformation() {
		return subsetConstructionTransformer.getInfo();
	}

	public String getInfoForEpsilonTransformation() {
		return removeEpsilonTransformer.getInfo();
	}

	public String getInfoForSequenceTransformation() {
		return removeSequencesTransformer.getInfo();
	}

	public String getInfoForTransformation() {
		return "Resulting Effort: " + getEffortForSequenceTransformation() + " + " + getEffortForEpsilonTransformation()
				+ " + " + getEffortForSubsetConstructionTransformation() + " = " + getEffortForTransformation() + "\n";
	}
}
