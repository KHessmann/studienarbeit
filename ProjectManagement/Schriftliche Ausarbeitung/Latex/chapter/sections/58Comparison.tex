\section{Beurteilung der Algorithmen}\label{sec:58Beurteilung}
Im Gegensatz zu der Umwandlung eines \ac{NEA} in einen \ac{DEA} existieren für die Minimierung in der Literatur viele verschiedene Verfahren, die alle ihre Vor- und Nachteile mit sich bringen. Mit Blick auf die Aufgabenstellung soll nun eine Entscheidung getroffen werden, welches der Verfahren aus didaktischer Sicht das Beste ist. Dazu werden im Folgenden der \hyperref[sec:53moore2]{anschauliche \acl{TFA}(\ac{TFA})}, der \hyperref[sec:54PredecessorTableFilling]{ \ac{TFA} mit Vorgängerbestimmung}, das \hyperref[sec:56incremental]{Inkrementelle} Verfahren und das von \hyperref[sec:57other]{Brzozowski} verglichen.  
\subsection{Vergleich der Verfahren}\label{ssec:581vergleich}
Für den Vergleich werden zunächst vier verschiedene Kriterien herangezogen: 
\begin{description}
	\item[Anschaulichkeit der Funktionsweise:] Nach Regel \ref{rule:Implementierung1} soll ein didaktisch geeignetes Verfahren so gestaltet sein, dass dem Anwender leicht verständlich gemacht werden kann warum es funktioniert.
	\item[Anschaulichkeit der Vorgehensweise:] Die Vorgehensweise des Verfahrens darf nicht zu aufwändig oder komplex sein (Regel \ref{rule:Implementierung1}).
	\item[Möglichkeit zur Überprüfung:] Das Verfahren sollte so gestaltet sein, dass eine schrittweise Überprüfung durch den Aufgabensteller leicht durchführbar ist. Dies bezieht sich hier auf die Implementierung eines geeigneten Algorithmus, kann aber auch auf die manuelle Überprüfung \enquote{auf dem Papier} ausgeweitet werden (Regel \ref{rule:Implementierung2}).
	\item[Fehlersuche durch den Anwender:] Anwender sollen nicht nur angezeigt bekommen, ob ihre Lösung korrekt ist, sondern auch aus ihren Fehlern lernen. Dem Anwender sollte es daher möglich sein, Fehler im Nachhinein zu identifizieren und nachvollziehen zu können (Regel \ref{rule:Implementierung2}).
\end{description}
\subsubsection*{Anmerkung zur Objektivität der Bewertung}
Es sei angemerkt, dass die Bewertung der Verfahren nach diesen Kriterien im Folgenden zum Großteil auf persönlichen Einschätzungen, Erfahrungen und Präferenzen beruht. Die in \reftab{tab:58compare} aufgeführten Werte können daher von der Meinung des Lesers abweichen.
\subsubsection*{Anschaulichkeit der Funktionsweise}
Bei intuitiver Herangehensweise erschließt sich das Inkrementelle Verfahren durch die schrittweise Minimierung des Automaten für den Anwender am besten. Darauf folgen die beiden betrachteten Table Filling Algorithmen und die geringste Transparenz weist der Algorithmus von Brzozowski auf (siehe auch \cite[232]{Wendlandt.2021}). 
\subsubsection*{Anschaulichkeit der Vorgehensweise}
Keines der Verfahren ist außergewöhnlich leicht durchführbar. Für den anschaulichen \ac{TFA} und das Inkrementelle Verfahren müssen viele Schritte durchgeführt werden, welche jedoch für sich genommen alle nicht besonders anspruchsvoll sind. Brzozowskis Algorithmus lässt sich in wenigen Worten beschreiben, enthält aber Schritte, die etwas komplexer sind als eine reine Übergangsanalyse. Dazu zählt insbesondere das \enquote{Umkehren} von Übergängen, welches erfahrungsgemäß fehleranfällig ist. Der TFA mit Vorgängerüberprüfung enthält gleichermaßen viele wie auch anspruchsvolle Schritte und wird in dieser Bewertung als am wenigsten anschaulich eingestuft. 
\subsubsection*{Überprüfungsmöglichkeiten}
In den vorhergehenden Abschnitten wurde bereits darauf eingegangen, wie die Anwenderlösungen durch den Aufgabensteller überprüft werden können. Das wichtigste Kriterium dafür lautet, ob alle Zwischenergebnisse des Anwenders abgesehen von der Reihenfolge und Benennung eindeutig sind. Das trifft auf alle Verfahren bis auf das Inkrementelle zu. Es soll nochmals angemerkt werden, dass sich die Zwischenergebnisse bei Brzozowskis Algorithmus theoretisch gut überprüfen lassen, in dieser Arbeit jedocch keine Methode dazu vorgestellt wurde.
\subsubsection*{Fehlersuche durch den Anwender}
Durch die Dokumentation aller Zwischenergebnisse gestaltet sich die nachträgliche Fehlersuche bei dem anschaulichen TFA, dem Inkrementellen und Brzozowskis Verfahren einfach. Bei dem TFA mit Vorgängerüberprüfung ist die Dokumentation der untersuchten Übergänge nicht vorgesehen. Es werden lediglich schrittweise die Warteschlange und Vorgängerliste um Zustandstupel ergänzt. Im Anschluss kann nicht mehr nachvollzogen werden, durch welches Eingabesymbol ein Zustandstupel ergänzt wurde. Daher kann es sich als schwierig herausstellen, im Nachhinein einen Fehler zu finden und zu beheben.
\input{./tables/58compare.tex}
\subsection{Vergleich des Aufwands ausgesuchter Verfahren}\label{ssec:582aufwand}
Die Einschätzungen in \reftab{tab:58compare} lassen bereits vermuten, dass der anschauliche TFA das geeignetste Verfahren für die Minimierung eines DEA durch einen Anwender ist. Nach Regel \ref{rule:Implementierung4} muss bei der Auswahl eines Verfahrens auch der mit der Durchführung verbundene Aufwand berücksichtigt werden. 

Im Folgenden sollen daher die beiden untersuchten Varianten des TFA mit Blick auf ihren Aufwand empirisch verglichen werden. Dazu wird für eine Grundmenge von 10.000 generierten DEAs bei beiden Verfahren der jeweilige Minimierungs-Aufwand berechnet. Die Vorgaben für die jeweilige Zusammensetzung des Aufwands können in Abschnitten \ref{sec:532aufwand} und \ref{sec:552aufwand} nachgelesen werden. 

In \reffig{fig:85MinGeeignetOpacity} und \ref{fig:85MinGeeignet} ist das Ergebnis dieses Vergleichs in zwei Diagrammen aufgetragen. Der x-Wert eines Punktes gibt für einen spezifischen Automaten den errechneten Aufwand bei der Durchführung des anschaulichen TFA an. Der y-Wert entspricht dem zugehörigen Aufwand bei der Minimierung desselben Automaten mithilfe des TFA mit Vorgängerbestimmung. Die \reffig{fig:85MinGeeignetOpacity} kennzeichnet durch die Opazität die Häufigkeit der aufgetretenen Werte. Die graue Linie gibt den Grenzwert an, für welchen beide Aufwände gleich sind. Liegt ein Punkt unterhalb dieser Linie, so ist der anschauliche TFA aufwändiger und umgekehrt. Es ist deutlich erkennbar, dass der anschauliche TFA in den meisten Fällen weniger Arbeitsschritte erfordert als der TFA mit Vorgängerbestimmung.
\input{./images/58MinGeeignetOpacity.tex}

Auch wenn es das Ziel aller Verfahren ist, einen nicht minimalen DEA in einen minimalen umzuwandeln, so können diese auch auf minimale DEAs angewandt werden, deren Zustandsanzahl sich nicht mehr verkleinern lässt. Die Verfahren liefern in diesem Fall den DEA zurück, mit welchem begonnen wurde. Solche DEAs werden im Folgenden vereinfacht als \enquote{nicht minimierbar} bezeichnet.
Aus \reffig{fig:85MinGeeignet} ergibt sich der Zusammenhang, dass der Aufwand, der mit der Prüfung solcher nicht minimierbaren DEAs verbunden ist, bei der Durchführung des TFA mit Vorgängerbestimmung immer deutlich höher ist. Von den 10.000 untersuchten DEAs sind die rund 1500 nicht minimierbaren als orange Punkte aufgetragen.


\input{./images/58MinGeeignet.tex} 

\subsubsection*{Schlussfolgerung}
Sowohl beim Vergleich mittels der in \reftab{tab:58compare} aufgeführten Kriterien als auch bei der Untersuchung des Arbeitsaufwands der ausgewählten Varianten des TFA ergibt sich, dass der anschauliche \acl{TFA} den anderen Verfahren aus didaktischer Sicht überlegen ist. 

Daher wird in den folgenden Kapiteln dieses Verfahren angewandt und zum weiteren Vergleich genutzt. Es sei jedoch darauf hingewiesen, dass insbesondere das Inkrementelle Verfahren gut für einen intuitiven Einstieg in das Thema genutzt werden kann und Brzozowskis Algorithmus eine interessante Alternative zu den sonst recht ähnlichen Verfahren darstellt. 
