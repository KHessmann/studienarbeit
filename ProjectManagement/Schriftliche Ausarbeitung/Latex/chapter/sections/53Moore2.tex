
\section{Eine anschauliche Version des Table Filling Algorithmus}\label{sec:53moore2}
Der Table Filling Algorithmus ist gerade wegen der unkomplizierten Vorgehensweise, die aus wenigen gut verständlichen Schritten besteht, an (Hoch-)Schulen sehr beliebt. Mit diesem Verfahren können auch Automaten mit mehr Zuständen verlässlich minimiert werden. Ein entscheidender Nachteil besteht jedoch darin, dass durch das simple Setzen von Kreuzen Informationen über die untersuchten Übergänge verloren gehen. Im Nachhinein ist es nicht nachvollziehbar, welcher Übergang dazu führte, dass zwei Zustände als unterscheidbar markiert wurden. In der Tabelle ist lediglich ein Kreuz zu sehen (siehe beispielsweise $q_0$ und $q_2$ in \reftab{tab:52moore1}). Gerade für die Fehlersuche kann dies zu einem Problem werden. Wurde bereits am Anfang ein Kreuz zu viel gesetzt, so muss im schlimmsten Fall das komplette Verfahren erneut durchgeführt werden. Die folgende Version des Table Filling Algorithmus soll dieses Problem beheben.
\subsection{Beschreibung des Verfahrens}\label{sec:531verfahren}
\subsubsection*{Grundidee}
Das Verfahren bleibt im Kern bestehen und die grundsätzliche Vorgehens- und Funktionsweise ändert sich nicht. Anstatt die Ergebnisse der Übergangsevaluation eines Zustandspaars als Kreuz in einer Tabelle festzuhalten, werden diese einzeln festgehalten.
Es handelt sich um eine reine Anpassung der Darstellungsform.
\subsubsection*{Vorgehensweise}
Für einen gegebenen \ac{DEA} $D = (Q_{D}, \Sigma_D, \delta_{D}, q_{0,D}, F_{D})$ wird der gesuchte minimierte DEA $M = (Q_{M}, \Sigma_M, \delta_{M}, q_{0,M}, F_{M})$ wie folgt ermittelt.  
\begin{enumerate}
	\item Zunächst werden alle Zustände $q\in Q_D$ in zwei Äquivalenzklassen eingeteilt: die Klasse der akzeptierenden und die der nicht akzeptierenden Zustände.
	\item Anschließend werden alle Zustände innerhalb einer Äquivalenzklasse paarweise untersucht:
	\begin{enumerate}
		\item Für ein Zustandspaar $(q_i,q_j)$ derselben Äquivalenzklasse und alle Eingabesymbole $a \in \Sigma_D$ werden $\delta(q_{i},a)=q_{ie,a}$ und $\delta(q_{j},a)=q_{je,a}$ ermittelt und tabellarisch gegenübergestellt.
		\item Das Paar ist unterscheidbar, wenn mindestens eines der End-Zustandspaare unterschiedlichen Äquivalenzklassen angehört.
		\begin{center}
			$\exists a \in \Sigma: \delta(q_{i},a) \not \equiv \delta(q_{j},a) \Rightarrow q_i \not \equiv q_j$
		\end{center}
		\item Nachdem alle Zustandspaare $(q_i,q_j)$ einer Äquivalenzklasse $[q_i]$ so untersucht wurden, wird $[q_i]$ falls nötig in mehrere neue Äquivalenzklassen unterteilt.
		\item Das Verfahren wird solange wiederholt, bis in einem Durchlauf keine der Äquivalenzklassen weiter unterteilt wird oder alle Zustandspaare als unterscheidbar markiert wurden. 
	\end{enumerate}
	\item Der minimierte DEA $M = (Q_{M}, \Sigma_M, \delta_{M}, q_{0,M}, F_{M})$ ergibt sich damit zu
	\begin{enumerate}
		\item $Q_M$ entspricht der Menge der Äquivalenzklassen $[q]$
		\item $\Sigma_M$ = $\Sigma_D$
		\item $\delta_M$ ergibt sich mit $Q_M$ aus $\delta_D$
		\item $q_{0,M}$ entspricht der Äquivalenzklasse, zu der $q_{0,D}$ gehört
		\item $F_M$ enthält all die Äquivalenzklassen, für deren Elemente gilt $q_i \in F_D$
	\end{enumerate}
\end{enumerate}
\newpassage
\begin{exmp}{53moore1}
	Das vereinfachte Verfahren soll nun beispielhaft zur Minimierung des bereits zuvor betrachteten DEA in \reffig{fig:52moore2} angewandt werden.
	\begin{enumerate}
		\item $Q_D$ wird in zwei Äquivalenzklassen aufgeteilt.
		\begin{align*}
			[q_0] &= \{q_0,q_1,q_2\}\\
			[q_3] &= \{q_3,q_4\}
		\end{align*}
	\item
	\begin{enumerate}
		\item Die Zustände in $\{\{q_0,q_1,q_2\},\{q_3,q_4\}\}$ werden paarweise untersucht (siehe \reftab{tab:53moore1a} bis \ref{tab:53moore1d}). Neue aus den Äquivalenzklassen gewonnene Informationen sind violett markiert. Die neuen Äquivalenzklassen ergeben sich zu
		\begin{center}
			$\{\{q_0,q_2\},\{q_1\},\{q_3,q_4\}\}$
		\end{center} 
		\item Die Zustände in $\{\{q_0,q_2\},\{q_1\},\{q_3,q_4\}\}$  werden paarweise untersucht(siehe \reftab{tab:53moore2a} und \ref{tab:53moore2b}). Die neuen Äquivalenzklassen ergeben sich zu
		\begin{center}
			 $\{\{q_0\},\{q_2\},\{q_1\},\{q_3,q_4\}\}$
		\end{center} 
		\item Da eine weitere Überprüfung des Zustandspaars $(q_3,q_4)$ im letzten Durchlauf keine weiteren Änderungen zur Folge hat (siehe \reftab{tab:53moore3a}), ergeben sich insgesamt durch diesen Weg erneut die vier Äquivalenzklassen
		\begin{align*}
			[q_0] &= \{q_0\}\\
			[q_1] &= \{q_1\}\\
			[q_2] &= \{q_2\}\\
			[q_3] &= \{q_3,q_4\}
		\end{align*}
	\end{enumerate}
	\item Der resultierende minimale DEA ist wie auch im Abschnitt zuvor in \reffig{fig:52moore3} abgebildet.
	\end{enumerate}
\end{exmp}
	\input{./tables/52moore3.tex}

\subsubsection*{Anmerkung zur Optimierung der Vorgehensweise}
Das Beispiel zeigt die Vorteile des Verfahrens auf. Zwar ist der Aufwand durch das Zeichnen mehrerer Einzeltabellen im Vergleich zu einer einzigen wie zuvor in \ref{exmp:52tfm1} größer, jedoch können hier alle Arbeitsschritte auch im Nachhinein nachvollzogen werden. In der Praxis könnte ein Anwender zudem einige Schritte einsparen:
\begin{enumerate}
	\item Häufig können aus vorhergehenden Überprüfungen Rückschlüsse auf die Unterscheidbarkeit anderer Zustandspaare gezogen werden. Da in Beispiel \ref{exmp:53moore1} ermittelt wurde, dass $q_0 \not\equiv q_1$ und $q_0 \equiv q_2$ folgt direkt ohne weitere Überprüfung, dass auch $q_1 \not\equiv q_2$ gilt.
	\item Für jeden Zustand müssen alle Übergänge nur ein einziges Mal analysiert werden. Wurden die Ergebnisse für $\delta(q_0,a)$ und $\delta(q_0,b)$ in \reftab{tab:53moore1a} bereits ermittelt, so kann diese Spalte in \reftab{tab:53moore1b} übernommen werden.
	\item Nicht alle Übergänge eines Zustandspaars müssen ausgewertet werden. Da es ausreicht, wenn ein End-Zustandspaar unterscheidbar ist, kann an dieser Stelle abgebrochen werden. In \reftab{tab:53moore1a} ist die Übergangsanalyse für das Eingabesymbol $b$ beispielsweise nicht zwingend notwendig.
	\item Gilt für alle Zustände einer Äquivalenzklasse, dass alle zugehörigen Übergänge wieder in Zustände derselben Äquivalenzklasse führen, so kann diese Äquivalenzklasse nicht weiter unterteilt werden. Dies trifft auf das Zustandspaar $\{q_3,q_4\}$ in \reftab{tab:53moore1d} zu, welches für jeden Ausgangszustand und jedes Eingabesymbol immer zurück in den Zustand $q_4$ der Äquivalenzklasse übergeht.
\end{enumerate} 	
Für die nachfolgende Aufwandsanalyse wird davon ausgegangen, dass zumindest die 1. und 2. Anpassung von den meisten Anwendern bereits nach wenigen Durchläufen genutzt wird.

\subsection{Aufwandsanalyse}\label{sec:532aufwand}
Der Aufwand für die Durchführung des  Verfahrens durch einen Anwender kann auf folgende Teiloperationen aufgeteilt werden:
\begin{itemize}
	\item Aufteilung der Zustände in zwei Äquivalenzklassen
	\newline \textit{\hyperref[rule:Aufwand1]{(Suchoperation)}} 		\hspace*{\fill} \arrowCallout{1 Schritt}%
	\item Einmalige Überprüfung aller benötigten Übergänge $\delta(q,a)$
	\newline
	\textit{\hyperref[rule:Aufwand3]{(Übergangsanalyse)}} 		\hspace*{\fill} \arrowCallout{1 Schritt pro Analyse}%
	\item Überprüfung, ob zwei verschiedene Zustände derselben Äquivalenzklasse angehören
	\newline
	\textit{\hyperref[rule:Aufwand1]{(Suchoperation)}} 	\hspace*{\fill} \arrowCallout{1 Schritt pro Überprüfung}%
	\item Ermittlung der neuen Äquivalenzklassen nach einem vollständigen Durchgang
	\newline
	\textit{\hyperref[rule:Aufwand5]{(Äquivalenzklassen aufteilen)}} 	\hspace*{\fill} \arrowCallout{min(k,n+1-k) Schritte pro Klasse}%
	\item Überprüfung, ob mit den durchgeführten Evaluationen Rückschlüsse auf weitere gezogen werden können\\ \noindent Dies ist bei geordnetem Durchlauf gegebenenfalls mehrmals pro Klasse notwendig. So etwa im Beispiel \ref{exmp:53moore1} bei der Äquivalenzklasse ${q_0,q_1,q_2}$ nachdem $(q_0,q_1)$ und $(q_0,q_2)$ bearbeitet wurden und das Paar $(q_1,q_2)$ betrachtet werden müsste. 
	\newline
	\textit{\hyperref[rule:Aufwand1]{(Suchoperation)}} 	\hspace*{\fill} \arrowCallout{1 Schritt}%
\end{itemize}

\subsection{Implementierung}\label{sec:533implementierung}
Der im Folgenden vorgestellte Algorithmus orientiert sich im Kern an der Interpretation des Table Filling Algorithmus, wie sie in \cite[126]{Bassino.2009} nachgelesen werden kann. In dieser Version wird der Fokus jedoch weniger auf die Optimierung der Performance gelegt. Vielmehr sollen Strukturen eingebaut werden, die es ermöglichen, Zwischenergebnisse abzufragen und zum Abgleich mit der Anwenderlösung zu verwenden.

\noindent Der \reflst{code:533moore} entspricht in der Vorgehensweise dem in Abschnitt \ref{sec:531verfahren} beschriebenen Verfahren. Die Zeilen 9-14 entsprechen Schritt 1 der Vorgehensweise und die Zeilen 15-25 Schritt 2 mit der dort beschriebenen Abbruchbedingung, dass der Vorgang solange durchlaufen werden soll, bis keine Änderungen mehr auftreten (Z.15).
Um auch später noch die in den einzelnen Durchläufen durchgeführten Untersuchungen auf Äquivalenz (Äquivalenzevaluationen) abrufen zu können, werden diese in der Map $schritte$ abgespeichert.
Eine Äquivalenzevaluation ist ein Quadrupel, welches für ein Zustandspaar abspeichert, ob dieses zu dem Zeitpunkt der Untersuchung unterscheidbar ist und welche Ergebnisse der Vergleich der einzelnen Übergangsfunktionen liefert. Ein solches Objekt enthält damit alle Informationen einer im Verfahren erklärten Tabelle wie sie in \reftab{tab:53moore1} zu sehen ist. Der Pseudocode zur Erzeugen einer Äquivalenzevaluation ist im \reflst{code:533equiv} beschrieben.

\noindent Beim Zusammenfügen der Äquivalenzklassen in \reflst{code:533class}wird das Ergebnis der Evaluation jedes einzelnen Zustandspaars untersucht. Dabei wird geprüft, ob der zweite Zustand $q_2$ der Äquivalenzklasse des ersten Zustands $q_1$ hinzugefügt werden soll. Für $q_2$ wird dabei nie eine eigene Äquivalenzklasse erstellt, da durch den Schleifendurchlauf in \reflst{code:533moore} (Z.19) jeder Zustand $q_2$ selbst in einem späteren Schleifendurchlauf als erster Zustand genutzt wird. Ein Ausnahmefall bildet der letzte Durchgang. Wenn auch im letzten Durchgang der verbleibende Zustand keiner Äquivalenzklasse angehört, so erhält $q_2$ eine eigene (\reflst{code:533class}, Z.24).   
\input{./code/533Moore.tex}
\input{./code/533EquivalenzEvaluation.tex}
\input{./code/533EquivalenzClass.tex}
\subsection{Überprüfungsmöglichkeiten}\label{sec:534test}
Die einzelnen Arbeitsschritte des Anwenders sollen auf Korrektheit überprüft werden können. Das beinhaltet die Prüfung der in jedem Schritt und Durchgang ermittelten Äquivalenzklassen inklusive der benötigten Äquivalenzevaluationen. Nach Implementierung des im vorherigen Abschnitt vorgestellten Algorithmus \ref{code:533moore} ist dies mit der Map $schritte$ möglich. Der Schlüssel, die Liste an Äquivalenzklassen zu Beginn des Durchlaufs, entspricht einem Minimierungs-Schritt. Jedem Schritt ist eine Liste an Äquivalenzevaluationen zugeordnet. Die Überprüfung der Zwischenergebnisse lässt sich damit gut umsetzen.
\input{./tables/534moore.tex}

\noindent Die neuen Zustände des minimierten DEA werden in \reffig{fig:534test2a} in Mengenschreibweise dargestellt. Da diese Art der Benennung mit wachsender Zustandsanzahl die Lesbarkeit verringert, könnte der Anwender sich dazu entscheiden, die Zustände ähnlich wie in \reffig{fig:534test2b} umzubenennen. Nach Regel \ref{rule:Automat5} ist eine Umbenennung der Zustände durch den Algorithmus nicht vorgesehen. Da für die Korrektheit eines Ergebnisses die Benennung keine Rolle spielt, soll eine Anwenderlösung mit dem korrekten Automat unabhängig von der Zustandsbenennung abgeglichen werden können.Dazu wird in Kapitel \ref{chap:6Testen} ein Algorithmus für den direkten Vergleich zweier DEAs vorgestellt.
\input{./images/534moore.tex}