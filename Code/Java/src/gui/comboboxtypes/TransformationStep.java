package gui.comboboxtypes;

public enum TransformationStep {

	ORIGINAL("Original NEA"), NO_SEQUENCE("Without Sequences"), NO_EPSILON("Without Epsilon"), DEA("DEA"),
	CLEAN_DEA("Renamed DEA"), MINIMIZED_DEA("Minimized DEA");

	private final String name;

	TransformationStep(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

}
