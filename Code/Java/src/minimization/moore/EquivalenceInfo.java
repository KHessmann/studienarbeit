package minimization.moore;

import commons.State;
import commons.Terminal;

public final class EquivalenceInfo {

	private Terminal terminal;
	private boolean equivalent;
	private State start1;
	private State start2;
	private State end1;
	private State end2;

	public EquivalenceInfo(Terminal t, State start1, State start2, State end1, State end2, boolean equivalent) {
		this.terminal = t;
		this.start1 = start1;
		this.start2 = start2;
		this.end1 = end1;
		this.end2 = end2;
		this.equivalent = equivalent;
	}

	public Terminal getTerminal() {
		return terminal;
	}

	public boolean isEquivalent() {
		return equivalent;
	}

	public State getFirstStartState() {
		return start1;
	}

	public State getSecondStartState() {
		return start2;
	}

	public State getFirstEndState() {
		return end1;
	}

	public State getSecondEndState() {
		return end2;
	}

}
