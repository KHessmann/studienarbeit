package gui.drawing;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import commons.Automaton;
import commons.State;
import commons.Terminal;
import commons.TerminalType;
import commons.Transition;

public class DotFile {

	public static String createDotFile(Automaton a) {
		StringBuilder sb = new StringBuilder();

		State initial = a.getInitial();

		sb.append("node [shape=circle, color=black, " + "fontcolor=black, fixedsize= \"true\", width=\"1\"];");
		sb.append("rankdir=LR");
		sb.append(System.getProperty("line.separator"));

		sb.append("{");// rank=same;");
		String nameInitial = stateLabel(initial);
		String shape = "circle";
		if (initial.isAccepting())
			shape = "doublecircle";
		sb.append(nameInitial + " [shape=" + shape + ", style=\"filled\", fillcolor=white, fontcolor=black, label = "
				+ getLabelForCummulatedStates(initial) + "];");
		sb.append(System.getProperty("line.separator"));

		List<State> accepting = new ArrayList<>(a.getAccepting());
		accepting.remove(initial);
		for (Iterator<State> i = accepting.iterator(); i.hasNext();) {
			Object s = i.next();
			sb.append(
					stateLabel(s) + " [shape=doublecircle,style=\"filled\", fillcolor=white, fontcolor=black, label = "
							+ getLabelForCummulatedStates(s) + "];");
		}

		List<State> states = new ArrayList<>(a.getStates());
		states.remove(initial);
		states.removeAll(accepting);
		for (Iterator<State> i = states.iterator(); i.hasNext();) {
			sb.append(stateLabelFirst(i.next()) + ";");
		}
		sb.append("}");

		sb.append("init [label=\"\", shape=none, width=0.1];");
		sb.append(System.getProperty("line.separator"));
		sb.append("init -> " + nameInitial + " [style=\"bold\"]");
		sb.append(System.getProperty("line.separator"));

		/* edges */
		for (Iterator<Transition> i = a.getTransitions().iterator(); i.hasNext();) {
			Transition tr = i.next();
			sb.append(stateLabel(tr.getStartState()) + " -> " + stateLabel(tr.getEndState()) + " [ label=\""
					+ transitionLabel(tr) + "\" ];");
			sb.append(System.getProperty("line.separator"));
		}
		return sb.toString();
	}

	private static String getLabelForCummulatedStates(Object st) {
		String rawLabel = st.toString();
		if (rawLabel.startsWith(State.cummulatedStartChar)) {
			rawLabel = rawLabel.replace(State.cummulatedStartChar, "");
			rawLabel = rawLabel.replace(State.cummulatedEndChar, "");
			String[] states = rawLabel.split(State.delimiter);
			StringBuilder sb = new StringBuilder();
			sb.append("\"&#123;");
			for (String s : states) {
				sb.append(s);
				sb.append("&#44;");
			}
			sb.replace(sb.length() - 5, sb.length(), "");
			sb.append("&#125;\"");
			return sb.toString();
		}
		return rawLabel;
	}

	private static String stateLabelFirst(Object st) {
		String rawLabel = st.toString();
		if (rawLabel.startsWith(State.cummulatedStartChar)) {
			rawLabel = rawLabel.replace(State.cummulatedStartChar, "");
			rawLabel = rawLabel.replace(State.cummulatedEndChar, "");
			rawLabel = rawLabel.replace(State.delimiter, "");
			rawLabel = rawLabel + "[label = " + getLabelForCummulatedStates(st) + "]";
		} else if (rawLabel.equals(State.emptyState)) {
			int emptyHex = Character.codePointAt(State.emptyState, 0);
			rawLabel = "emptySet[label=\"&#" + emptyHex + ";\"]";
		}
		return rawLabel;
	}

	private static String stateLabel(Object st) {
		String rawLabel = st.toString();
		if (rawLabel.startsWith(State.cummulatedStartChar)) {
			rawLabel = rawLabel.replace(State.cummulatedStartChar, "");
			rawLabel = rawLabel.replace(State.cummulatedEndChar, "");
			rawLabel = rawLabel.replace(State.delimiter, "");
		} else if (rawLabel.equals(State.emptyState)) {
			rawLabel = "emptySet";
		}
		return rawLabel;
	}

	public static String transitionLabel(Transition trans) {
		StringBuilder sb = new StringBuilder();
		for (Terminal t : trans.getTerminals()) {
			if (t.getType() == TerminalType.EPSILON) {
				int epsilonHex = Character.codePointAt(Character.toString(Terminal.epsilon), 0);
				sb.append("&#" + epsilonHex + ";" + ",");
			} else {
				sb.append(t + ",");
			}
		}
		if (sb.length() > 0) {
			sb.deleteCharAt(sb.length() - 1);
		}
		return sb.toString();
	}

	public static void createDotGraph(String dotFormat, String fileName, boolean test) {
		GraphViz gv = new GraphViz();
		gv.addln(gv.start_graph());
		gv.add(dotFormat);
		gv.addln(gv.end_graph());
		String type = "png";
		if (!test) {
			gv.decreaseDpi();
			gv.decreaseDpi();
		}
		byte[] graph = gv.getGraph(gv.getDotSource(), type);
		File tempOut;
		if (!test) {
			tempOut = new File("graphs//temp." + type);
		} else {
			tempOut = new File("test//" + fileName + "." + type);
		}
		gv.writeGraphToFile(graph, tempOut);
	}

	public static void createDotGraph(String dotFormat, String fileName) {
		createDotGraph(dotFormat, fileName, false);
	}
}
