package minimization;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import commons.Automaton;
import commons.State;
import commons.Terminal;
import commons.TransitionTable;
import minimization.moore.EquivalenceEvaluation;
import minimization.moore.EquivalenceInfo;
import minimization.moore.MinimizationStep;

public class MooreMinimizer implements Minimizer {

	private List<MinimizationStep> minimizationSteps = new ArrayList<>();
	private Set<EquivalenceClass> equivalentClasses = new HashSet<>();
	private Automaton initialAutomaton;
	private TransitionTable table;
	private Automaton finalAutomaton;
	private int effort = 0;
	private int redundantEffort = 0;
	private boolean quickCompute = false;

	public MooreMinimizer(Automaton a, boolean shouldQuickCompute) {
		this(a);
		quickCompute = shouldQuickCompute;
	}

	public MooreMinimizer(Automaton a) {
		initialAutomaton = a;
		table = a.getTransitionTable();
	}

	@Override
	public Automaton minimize() {
		minimizationSteps.clear();
		equivalentClasses.clear();
		effort = 1;
		redundantEffort = 0;
		List<State> accepting = new ArrayList<>();
		List<State> notAccepting = new ArrayList<>();
		for (State s : initialAutomaton.getStates()) {
			if (s.isAccepting()) {
				accepting.add(s);
			} else {
				notAccepting.add(s);
			}
		}

		if (!accepting.isEmpty()) {
			equivalentClasses.add(new EquivalenceClass(accepting));
		}
		if (!notAccepting.isEmpty()) {
			equivalentClasses.add(new EquivalenceClass(notAccepting));
		}

		int equivalenceClassNumber;
		int step = 0;
		while (true) {
			equivalenceClassNumber = equivalentClasses.size();
			MinimizationStep ms = nextMinimizationStep(step);
			minimizationSteps.add(ms);
			if (equivalenceClassNumber == equivalentClasses.size() || ms.isFinalStep()) {
				ms.setFinalStep();
				break;
			}
			step++;
		}

		finalAutomaton = MinimizationHelper.createNewAutomaton(initialAutomaton, equivalentClasses);
		return finalAutomaton;
	}

	private MinimizationStep nextMinimizationStep(int step) {

		Map<EquivalenceClass, List<EquivalenceEvaluation>> oldECEvaluationMap = new HashMap<>();
		Set<EquivalenceClass> allEquiClasses = new HashSet<>();
		boolean finalStep = true;
		for (EquivalenceClass equiClass : equivalentClasses) {
			int statesNumber = equiClass.getStates().size();
			if (step == 0 && statesNumber > 1) {
				effort += statesNumber * (initialAutomaton.getAlphabet().size() - 1);
			}
			List<EquivalenceEvaluation> evaluations = new ArrayList<>();
			List<EquivalenceClass> newClasses = new ArrayList<>();
			if (statesNumber == 1) {
				newClasses.add(equiClass);
			} else {
				evaluations = computeEquivalenceEvaluations(equiClass, newClasses, step);
			}
			for (EquivalenceClass ec : newClasses) {
				allEquiClasses.add(ec);
				if (finalStep && ec.getStates().size() != 1) {
					finalStep = false;
				}
			}
			oldECEvaluationMap.put(equiClass, evaluations);
			if (statesNumber > 1) {
				effort += Math.min(newClasses.size(), statesNumber + 1 - newClasses.size());
			}
		}
		MinimizationStep ms = new MinimizationStep(oldECEvaluationMap, allEquiClasses, step);
		equivalentClasses = allEquiClasses;

		if (finalStep) {
			ms.setFinalStep();
		}
		return ms;
	}

	private List<EquivalenceEvaluation> computeEquivalenceEvaluations(EquivalenceClass equiClass,
			List<EquivalenceClass> newClasses, int step) {
		List<EquivalenceEvaluation> result = new ArrayList<>();
		List<State> states = new ArrayList<>(equiClass.getStates());
		State first, second;
		boolean lastTuple = false;
		for (int i = 0; i < states.size(); i++) {
			first = states.get(i);
			if (quickCompute) {
				boolean breakLoop = false;
				int stateCount = 0;
				boolean hasEquivalenceClass = false;
				for (EquivalenceClass ec : newClasses) {
					stateCount += ec.getStates().size();
					if (ec.containsState(first)) {
						hasEquivalenceClass = true;
						if (i == states.size() - 1 || ec.getStates().size() > 1) {
							breakLoop = true;
							break;
						}
					}
				}
				if (breakLoop) {
					continue;
				}
				if ((i > 0 && newClasses.size() == 1 && stateCount == equiClass.getStates().size() - 1
						&& !hasEquivalenceClass) || (i == states.size() - 1)) {
					newClasses.add(new EquivalenceClass(Stream.of(first).collect(Collectors.toList())));
					break;
				}
			}
			for (int j = i + 1; j < states.size(); j++) {
				second = states.get(j);
				if (i == states.size() - 2) {
					lastTuple = true;
				}
				Boolean redundantEquivalence = checkRedundantEquivalence(newClasses, first, second, equiClass, i);

				EquivalenceEvaluation evaluation = getEquivalenceRelation(first, second, step, redundantEquivalence);
				result.add(evaluation);
				updateEquivalentClasses(evaluation, newClasses, lastTuple);
				// search of transitions in foregoing results
				if (redundantEquivalence != null) {
					redundantEffort += 2;
				} else {
					effort += 2;
				}

			}
			// check if you need any more evaluations
			if (i != 0 && i != states.size() - 1 && !quickCompute) {
				effort += 1;
				redundantEffort -= 1;
			}
		}
		return result;
	}

	private Boolean checkRedundantEquivalence(List<EquivalenceClass> newClasses, State first, State second,
			EquivalenceClass equiClass, int step) {
		Boolean result = null;
		if (!quickCompute) {
			EquivalenceClass firstClass = null, secondClass = null;
			int stateCount = 0;
			for (EquivalenceClass ec : newClasses) {
				stateCount += ec.getStates().size();
				if (ec.containsState(first))
					firstClass = ec;
				if (ec.containsState(second))
					secondClass = ec;
				if (firstClass != null && secondClass != null)
					break;
			}

			if (firstClass != null && secondClass != null) {
				if (firstClass.equals(secondClass)) {
					result = Boolean.TRUE;
				} else {
					result = Boolean.FALSE;
				}
			} else if (step > 0 && newClasses.size() == 1 && stateCount == equiClass.getStates().size() - 1) {
				result = Boolean.FALSE;
			}
		}
		return result;
	}

	private EquivalenceEvaluation getEquivalenceRelation(State state, State state2, int step,
			Boolean redundantEquivalence) {
		Map<Terminal, EquivalenceInfo> equivalenceInfoMap = new HashMap<>();
		boolean equivalent = true;
		for (Terminal t : initialAutomaton.getAlphabet()) {
			boolean equivalentStates = true;
			List<State> endStates1 = table.getEndStatesFromTransitionsForState(state, t);
			List<State> endStates2 = table.getEndStatesFromTransitionsForState(state2, t);
			State endState1 = endStates1.isEmpty() ? null : endStates1.get(0);
			State endState2 = endStates2.isEmpty() ? null : endStates2.get(0);
			if (endState1 != null && endState2 != null && !endState1.equals(endState2)) {
				for (EquivalenceClass ec : equivalentClasses) {
					if (ec.containsState(endState1)) {
						if (!ec.containsState(endState2)) {
							equivalentStates = false;
						}
						break;
					}
				}
				// check for same equivalenceClass
				if (redundantEquivalence != null) {
					redundantEffort += 1;
				} else {
					effort += 1;
				}
			}
			equivalenceInfoMap.put(t, new EquivalenceInfo(t, state, state2, endState1, endState2, equivalentStates));

			equivalent = equivalent && equivalentStates;
		}
		if (redundantEquivalence != null && (equivalent != redundantEquivalence.booleanValue()))

		{
			throw new IllegalArgumentException(
					"All equivalence evaluations for a state should render the same result!");
		}
		return new EquivalenceEvaluation(state, state2, equivalenceInfoMap, equivalent, (redundantEquivalence != null));
	}

	private void updateEquivalentClasses(EquivalenceEvaluation evaluation, List<EquivalenceClass> newClasses,
			boolean lastTuple) {
		State s1 = evaluation.getFirstState();
		State s2 = evaluation.getSecondState();
		if (evaluation.isEquivalent()) {
			boolean classExists = false;
			for (EquivalenceClass ec : newClasses) {
				if (ec.containsState(s1)) {
					ec.addStateIfNotExistent(s2);
					classExists = true;
					break;
				}
			}
			if (!classExists) {
				newClasses.add(new EquivalenceClass(Stream.of(s1, s2).collect(Collectors.toList())));
			}
		} else {
			boolean s1ClassExists = false;
			boolean s2ClassExists = false;
			for (EquivalenceClass ec : newClasses) {
				if (ec.containsState(s1)) {
					s1ClassExists = true;
				}
				if (ec.containsState(s2)) {
					s2ClassExists = true;
				}
			}
			if (!s1ClassExists) {
				newClasses.add(new EquivalenceClass(Stream.of(s1).collect(Collectors.toList())));
			}
			if (lastTuple && !s2ClassExists) {
				newClasses.add(new EquivalenceClass(Stream.of(s2).collect(Collectors.toList())));
			}
		}
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (MinimizationStep ms : minimizationSteps) {
			sb.append(ms.toString());
		}
		return sb.toString();

	}

	public List<MinimizationStep> getStepwiseResults() {
		if (minimizationSteps.isEmpty()) {
			minimize();
		}
		return minimizationSteps;
	}

	public Automaton getResultAutomaton() {
		if (finalAutomaton == null) {
			minimize();
		}
		return finalAutomaton;
	}

	public Automaton getInitialAutomaton() {
		return initialAutomaton;
	}

	public String getInfo(int step, boolean withRedundantInformation) {
		if (minimizationSteps.isEmpty()) {
			minimize();
		}
		StringBuilder sb = new StringBuilder();
		if (step > -1) {
			sb.append("\nMOORE ALGORITHM \n");
			for (int i = 0; i <= step; i++) {
				sb.append("\n" + minimizationSteps.get(i).toString(withRedundantInformation) + "\n");
			}
		}
		if (step == minimizationSteps.size() - 1) {
			int effortInt = effort;
			sb.append("Effort: " + effortInt + "\n");
//			if (withRedundantInformation) {
			effortInt += redundantEffort;
			sb.append("Effort with redundant Computations: " + effortInt + "\n");
//			}
		}
		return sb.toString();
	}

	@Override
	public int getEffort() {
		if (effort == 0) {
			minimize();
		}
		return effort;
	}

	public int getEffortWithRedundancy() {
		if (effort == 0) {
			minimize();
		}
		return effort + redundantEffort;
	}
}
