package statistic.db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import statistic.structure.SimpleMatrixHelper;

public class DBHelper {

	public static int addStructureMatrix(DBController controller, int[][] matrix, int[] toVector, int[] fromVector,
			int[] loopVector) {
		String matrixString = SimpleMatrixHelper.matrixToString(matrix);
		String tVS = SimpleMatrixHelper.vectorToString(toVector);
		String fVS = SimpleMatrixHelper.vectorToString(fromVector);
		String lVS = SimpleMatrixHelper.vectorToString(loopVector);

		String sql = "INSERT INTO StructureMatrix(matrix, to_vector, from_vector, loop_vector) VALUES('" + matrixString
				+ "','" + tVS + "','" + fVS + "','" + lVS + "');";
		return controller.executeUpdateSQL(sql);
	}

	public static int addEquivalence(DBController controller, int matrixID) {
		String sql = "INSERT INTO Equivalence(matrix_id) VALUES(" + matrixID + ");";
		return controller.executeUpdateSQL(sql);
	}

	/**
	 * @param toVector
	 * @param fromVector
	 * @param loopVector
	 * @return Map of Equivalence and Matrix IDs for possible equivalence classes
	 */
	public static Map<Integer, Integer> getPossibleEquivalenceClasses(DBController controller, int[] toVector,
			int[] fromVector, int[] loopVector) {
		Map<Integer, Integer> equivalenceToMatrixMap = new HashMap<>();
		String tVS = SimpleMatrixHelper.vectorToString(toVector);
		String fVS = SimpleMatrixHelper.vectorToString(fromVector);
		String lVS = SimpleMatrixHelper.vectorToString(loopVector);

		String sql = "SELECT e.id, e.matrix_id FROM Equivalence e, StructureMatrix m WHERE e.matrix_id = m.id AND m.to_vector = '"
				+ tVS + "' AND m.from_vector = '" + fVS + "' AND m.loop_vector = '" + lVS + "';";
		ResultSet rs = controller.executeSQL(sql);
		try {
			while (rs.next()) {
				equivalenceToMatrixMap.put(rs.getInt("id"), rs.getInt("matrix_id"));
			}
		} catch (SQLException e) {
			throw new RuntimeException("No equivalence class found - create a new one!");
		}

		return equivalenceToMatrixMap;
	}

	public static int[][] getMatrix(DBController controller, int matrixID) {
		String sql = "SELECT matrix FROM StructureMatrix WHERE id = " + matrixID + ";";
		ResultSet rs = controller.executeSQL(sql);
		try {
			if (rs.next()) {
				String matrixString = rs.getString("matrix");
				return SimpleMatrixHelper.stringToMatrix(matrixString);
			}
		} catch (SQLException e) {
			throw new RuntimeException("MatrixID " + matrixID + "not found in table StructureMatrix");
		}
		return null;
	}

	public static void createDEAGenerationWithNEAView(DBController controller) {
		String sql = "CREATE VIEW IF NOT EXISTS DEAGenerateWithNEAView AS SELECT nea.automaton AS nea, nea.state_count AS nea_states, nea.true_transition_count AS nea_true_transitions,  nea.accepting_count AS nea_accepting, nea.epsilon_count AS nea_epsilon, nea.sequence_count AS nea_sequence, dea.state_count AS dea_states, dea.true_transition_count AS dea_true_transitions, dea.accepting_count AS dea_accepting, dea.is_minimal AS was_Minimal, mindea.state_count AS min_dea_states, case when dea.structure_transition_count<21 and dea.true_transition_count < 21 and dea.state_count <11 then 'true' else 'false' end as 'validDEA', case when g.effort between 0 and 19 then 'A' when g.effort between 20 and 39 then 'B' when g.effort between 40 and 79 then 'C' when g.effort between 80 and 119 then 'D' when g.effort >= 120 then 'E' end AS effort FROM NEAInfo nea, DEAInfo dea, DEAInfo mindea, DEAGeneration g Where nea.id = g.nea_id AND dea.id = g.dea_id AND mindea.id = g.minimized_dea_id;";
		controller.executeCreateOrDeleteSQL(sql);
	}

	public static void createDEAGenerationWithoutNEAView(DBController controller) {
		String sql = "CREATE VIEW IF NOT EXISTS DEAGenerateWithoutNEAView AS SELECT dea.automaton AS dea, dea.state_count AS states, dea.true_transition_count AS true_transitions, dea.accepting_count AS accepting, dea.is_minimal AS was_Minimal, case when dea.structure_transition_count<21 and dea.true_transition_count < 21 and dea.state_count <11 then 'true' else 'false' end as 'validDEA', mindea.state_count AS min_dea_states, case when g.effort between 0 and 19 then 'A' when g.effort between 20 and 39 then 'B' when g.effort between 40 and 79 then 'C' when g.effort between 80 and 119 then 'D' when g.effort >= 120 then 'E' end AS effort FROM  DEAInfo dea, DEAInfo mindea, DEAGeneration g Where dea.id = g.dea_id AND mindea.id = g.minimized_dea_id;";
		controller.executeCreateOrDeleteSQL(sql);
	}

	public static void createNEAGenerationView(DBController controller) {
		String sql = "CREATE VIEW NEAGenerateView AS SELECT enea.automaton AS enea, enea.true_transition_count AS enea_true_transitions,  enea.epsilon_count AS enea_epsilon, nea.state_count AS nea_states, nea.true_transition_count AS nea_true_transitions,  nea.is_almost_dea AS nea_almost_dea, dea.state_count AS dea_states, dea.true_transition_count AS dea_true_transitions, case when dea.structure_transition_count<21 and dea.true_transition_count < 21 and dea.state_count <11 then 'true' else 'false' end as 'validDEA', case when g.effort_sequence between 0 and 19 then 'A' when g.effort_sequence between 20 and 39 then 'B' when g.effort_sequence between 40 and 79 then 'C' when g.effort_sequence between 80 and 119 then 'D' when g.effort_sequence >= 120 then 'E' end AS effort_sequence, case when g.effort_epsilon between 0 and 19 then 'A' when g.effort_epsilon between 20 and 39 then 'B' when g.effort_epsilon between 40 and 79 then 'C' when g.effort_epsilon between 80 and 119 then 'D' when g.effort_epsilon >= 120 then 'E' end AS effort_epsilon, case when g.effort_subset between 0 and 19 then 'A' when g.effort_subset between 20 and 39 then 'B' when g.effort_subset between 40 and 79 then 'C' when g.effort_subset between 80 and 119 then 'D' when g.effort_subset >= 120 then 'E' end AS effort_subset, case when g.effort_epsilon+g.effort_sequence+g.effort_subset between 0 and 19 then 'A' when g.effort_epsilon+g.effort_sequence+g.effort_subset between 20 and 39 then 'B' when g.effort_epsilon+g.effort_sequence+g.effort_subset between 40 and 79 then 'C' when g.effort_epsilon+g.effort_sequence+g.effort_subset between 80 and 119 then 'D' when g.effort_epsilon+g.effort_sequence+g.effort_subset >= 120 then 'E' end AS effort FROM NEAInfo enea, NEAINFO nea, DEAInfo dea, NEAGeneration g Where enea.id = g.e_nea_id AND nea.id = g.nea_id AND dea.id = g.dea_id;";
		controller.executeCreateOrDeleteSQL(sql);
	}

	public static void createNEAIsomorphismView(DBController controller) {
		String sql = " CREATE VIEW IF NOT EXISTS NEAIsmorphismView AS SELECT m.matrix, m.to_vector, m.from_vector, m.loop_vector, ec.equivalence_id, a.true_transition_count,a.state_count, count(*) AS count FROM StructureMatrix m, NEAInfo a, NEAEquivalenceClass ec Where a.id = ec.nea_id AND ec.matrix_id = m.id  GROUP BY ec.equivalence_id ORDER BY count DESC; ";
		controller.executeCreateOrDeleteSQL(sql);
	}

	public static void createDEAIsomorphismView(DBController controller) {
		String sql = " CREATE VIEW IF NOT EXISTS DEAIsmorphismView AS SELECT m.matrix, m.to_vector, m.from_vector, m.loop_vector, ec.equivalence_id, a.true_transition_count,a.state_count, count(*) AS count FROM StructureMatrix m, DEAInfo a, DEAEquivalenceClass ec Where a.id = ec.dea_id AND ec.matrix_id = m.id  GROUP BY ec.equivalence_id ORDER BY count DESC; ";
		controller.executeCreateOrDeleteSQL(sql);
	}

	public static void createMinimizationEffortView(DBController controller) {
		String sql = "CREATE VIEW IF NOT EXISTS MinimizationEffortView AS SELECT dea.automaton AS dea, dea.state_count AS deaStates, dea.true_transition_count AS deaTrueTransitions, dea.accepting_count AS deaAccepting, dea.is_minimal AS was_Minimal, mindea.state_count AS minDEAStates, mindea.true_transition_count AS minDEATrueTransitions, mindea.accepting_count AS minDEAAccepting, case when dea.structure_transition_count<21 and dea.true_transition_count < 21 and dea.state_count <11 then 'true' else 'false' end as 'validDEA', e.tf_effort, e.tf_precalculate_effort, e.moore_effort, e.moore_redundancy_effort, case when e.tf_effort between 0 and 19 then 'A' when e.tf_effort between 20 and 39 then 'B' when e.tf_effort between 40 and 79 then 'C' when e.tf_effort between 80 and 119 then 'D' when e.tf_effort >= 120 then 'E' end AS tfmEffort, case when e.tf_precalculate_effort between 0 and 19 then 'A' when e.tf_precalculate_effort between 20 and 39 then 'B' when e.tf_precalculate_effort between 40 and 79 then 'C' when e.tf_precalculate_effort between 80 and 119 then 'D' when e.tf_precalculate_effort >= 120 then 'E' end AS tfmPrecalculated, case when e.moore_effort between 0 and 19 then 'A' when e.moore_effort between 20 and 39 then 'B' when e.moore_effort between 40 and 79 then 'C' when e.moore_effort between 80 and 119 then 'D' when e.moore_effort >= 120 then 'E' end AS mooreEffort, case when e.moore_redundancy_effort between 0 and 19 then 'A' when e.moore_redundancy_effort between 20 and 39 then 'B' when e.moore_redundancy_effort between 40 and 79 then 'C' when e.moore_redundancy_effort between 80 and 119 then 'D' when e.moore_redundancy_effort >= 120 then 'E' end AS mooreRedundancyEffort FROM  DEAInfo dea, DEAInfo mindea, Effort e Where dea.id = e.dea_id AND mindea.id = e.minimized_dea_id;";
		controller.executeCreateOrDeleteSQL(sql);
	}

	public static void resetTableViews(DBController controller) {
		String sql = "DROP VIEW IF EXISTS MinimizationEffortView; DROP VIEW  IF EXISTS NEAIsmorphismView; DROP VIEW  IF EXISTS DEAIsmorphismView; DROP VIEW  IF EXISTS NEAGenerateView; DROP VIEW  IF EXISTS DEAGenerateWithNEAView; DROP VIEW  IF EXISTS DEAGenerateWithoutNEAView; ";
		controller.executeCreateOrDeleteSQL(sql);
	}

	public static void deleteAllTableEntries(DBController controller) {
		String sql = "DELETE FROM TFMMooreMinimizationTest;DELETE FROM Effort;DELETE FROM MinimizationAlgorithmTest;DELETE FROM TransformationTest;DELETE FROM StructureMatrix;DELETE FROM Equivalence;DELETE FROM NEAInfo;DELETE FROM DEAInfo;DELETE FROM NEAEquivalenceClass;DELETE FROM DEAEquivalenceClass;DELETE FROM NEAGeneration;DELETE FROM DEAGeneration;";
		controller.executeCreateOrDeleteSQL(sql);
	}

	public static int addTFMMooreMinimizationTest(DBController controller, String dea, String quickTF, String slowTF,
			String quickMoore, String slowMoore, boolean tfEquivalent, boolean mooreEquivalent,
			boolean tfMooreEquivalent, boolean wasMinimal) {
		String sql = "INSERT INTO TFMMooreMinimizationTest( dea, quickTF, slowTF, quickMoore, slowMoore, tfEquivalent, mooreEquivalent, tfMooreEquivalent, was_Minimal) VALUES ('"
				+ dea + "','" + quickTF + "','" + slowTF + "','" + quickMoore + "','" + slowMoore + "','" + tfEquivalent
				+ "','" + mooreEquivalent + "','" + tfMooreEquivalent + "','" + wasMinimal + "');";
		return controller.executeUpdateSQL(sql);
	}

	public static int addEffortEntry(DBController controller, int deaID, int minimizedDeaID, int tfmCC,
			int tfmPrecalculatedCC, int mooreCC, int mooreRedundancyCC) {
		String sql = "INSERT INTO Effort(dea_id, minimized_dea_id, tf_effort, tf_precalculate_effort, moore_effort, moore_redundancy_effort) VALUES ("
				+ deaID + "," + minimizedDeaID + "," + tfmCC + "," + tfmPrecalculatedCC + "," + mooreCC + ","
				+ mooreRedundancyCC + ");";
		return controller.executeUpdateSQL(sql);
	}

	public static int addMinimizationAlgorithmTest(DBController controller, String dea, String tfm, String moore,
			String incremental, boolean tfmMooreDirectEquivalent, boolean tfmIncrementalDirectEquivalent,
			boolean incrementalMooreDirectEquivalent, boolean tfmWordGenerationEquivalent,
			boolean mooreWordGenerationEquivalent, boolean incrementalWordGenerationEquivalent,
			boolean tfmTFMEquivalent, boolean mooreTFMEquivalent, boolean incrementalTFMEquivalent) {
		String sql = "INSERT INTO MinimizationAlgorithmTest(dea, table_filling, moore, incremental, directTfmMoore, directTfmIncr, directMooreIncr, wordTfm, wordMoore, wordIncr, tfmTFM, tfmMoore, tfmIncr) VALUES ('"
				+ dea + "','" + tfm + "','" + moore + "','" + incremental + "','" + tfmMooreDirectEquivalent + "','"
				+ tfmIncrementalDirectEquivalent + "','" + incrementalMooreDirectEquivalent + "','"
				+ tfmWordGenerationEquivalent + "','" + mooreWordGenerationEquivalent + "','"
				+ incrementalWordGenerationEquivalent + "','" + tfmTFMEquivalent + "','" + mooreTFMEquivalent + "','"
				+ incrementalTFMEquivalent + "');";
		return controller.executeUpdateSQL(sql);
	}

	public static int addTransformationTest(DBController controller, String nea, String dea, boolean isIdentical,
			String witness) {
		String sql = "INSERT INTO TransformationTest(nea, dea, is_identical, witness) VALUES ('" + nea + "','" + dea
				+ "','" + isIdentical + "','" + witness + "');";
		return controller.executeUpdateSQL(sql);
	}

	public static int addDEAGeneration(DBController controller, int neaID, int deaID, int minDeaID, int effort) {
		String sql = "INSERT INTO DEAGeneration(nea_id, dea_id, minimized_dea_id, effort) VALUES (" + neaID + ","
				+ deaID + "," + minDeaID + "," + effort + ");";
		return controller.executeUpdateSQL(sql);

	}

	public static int addNEAGeneration(DBController controller, int eNeaID, int neaID, int deaID, int sequenceEffort,
			int epsilonComlexity, int subsetEffort) {
		String sql = "INSERT INTO NEAGeneration(e_nea_id, nea_id, dea_id, effort_sequence,effort_epsilon,effort_subset) VALUES ("
				+ eNeaID + "," + neaID + "," + deaID + "," + sequenceEffort + "," + epsilonComlexity + ","
				+ subsetEffort + ");";
		return controller.executeUpdateSQL(sql);
	}

	public static int addDEAEquivalenceClass(DBController controller, int matrixID, int eqID, int deaID) {
		String sql = "INSERT INTO DEAEquivalenceClass(matrix_id, equivalence_id, dea_id) VALUES (" + matrixID + ","
				+ eqID + "," + deaID + ");";
		return controller.executeUpdateSQL(sql);
	}

	public static int addNEAEquivalenceClass(DBController controller, int matrixID, int eqID, int neaID) {
		String sql = "INSERT INTO NEAEquivalenceClass(matrix_id, equivalence_id, nea_id) VALUES (" + matrixID + ","
				+ eqID + "," + neaID + ");";
		return controller.executeUpdateSQL(sql);
	}

	public static int addNEA(DBController controller, String automaton, int stateCount, int terminalOccurenceCount,
			int transitionCount, int acceptingCount, int singleCount, int sequenceCount, int epsilonCount,
			int loopCount, int maxDistance, boolean isAlmostDea) {
		String sql = "INSERT INTO NEAInfo(automaton, state_count, true_transition_count, structure_transition_count, accepting_count, single_count, sequence_count, epsilon_count, loop_count, max_distance, is_almost_dea) VALUES ('"
				+ automaton + "'," + stateCount + "," + terminalOccurenceCount + "," + transitionCount + ","
				+ acceptingCount + "," + singleCount + "," + sequenceCount + "," + epsilonCount + "," + loopCount + ","
				+ maxDistance + ",'" + isAlmostDea + "');";
		return controller.executeUpdateSQL(sql);
	}

	public static int addDEA(DBController controller, String automaton, int stateCount, int terminalOccurenceCount,
			int transitionCount, int acceptingCount, int loopCount, int maxDistance, boolean isMinimal) {
		String sql = "INSERT INTO DEAInfo(automaton, state_count, true_transition_count, structure_transition_count, accepting_count, loop_count, max_distance, is_minimal) VALUES ('"
				+ automaton + "'," + stateCount + "," + terminalOccurenceCount + "," + transitionCount + ","
				+ acceptingCount + "," + loopCount + "," + maxDistance + ",'" + isMinimal + "');";
		return controller.executeUpdateSQL(sql);

	}

}
