package minimization;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import commons.Automaton;
import commons.State;
import commons.StateTuple;
import commons.Terminal;
import commons.TerminalType;
import commons.TransitionTable;

public class IncrementalMinimizer implements Minimizer {

	Set<EquivalenceClass> equivalentStates = new HashSet<>();
	Set<StateTuple> notEquivalentStates = new HashSet<>();
	Set<StateTuple> rekursiveEquiStates = new HashSet<>();
	Set<StateTuple> rekursivePath = new HashSet<>();
	List<Automaton> stepwiseResults = new ArrayList<>();
	Automaton automaton;

	public IncrementalMinimizer(Automaton a) {
		automaton = a;
	}

	@Override
	public Automaton minimize() {
		return minimize(true);
	}

	public Automaton minimize(boolean stepwiseMinimization) {
		equivalentStates.clear();
		notEquivalentStates.clear();
		stepwiseResults.clear();
		int statenumber = automaton.getStates().size();
		if (stepwiseMinimization)
			stepwiseResults.add(automaton);
		List<State> states = automaton.getStates();
		TransitionTable table = automaton.getTransitionTable();
		int setSize = (int) Math.pow(states.size(), 2);
		for (State s : states) {
			make(s);
		}
		for (State accState : automaton.getAccepting()) {
			for (State naccState : states) {
				if (!naccState.isAccepting()) {
					notEquivalentStates.add(new StateTuple(accState, naccState));
				}
			}
		}
		for (State s1 : states) {
			for (State s2 : states) {
				if (s1.getId().compareTo(s2.getId()) <= 0)
					continue;
				StateTuple tuple = new StateTuple(s1, s2);
				if (notEquivalentStates.contains(tuple) || find(s1).equals(find(s2)))
					continue;
				// TODO: maybe linkedHashset?
				rekursiveEquiStates = new HashSet<>(setSize);
				rekursivePath = new HashSet<>(setSize);
				if (isEquivalent(tuple, table)) {
					for (StateTuple t : rekursiveEquiStates) {
						union(find(t.getFirstState()), find(t.getSecondState()));
						if (stepwiseMinimization && equivalentStates.size() < statenumber) {
							stepwiseResults.add(MinimizationHelper.createNewAutomaton(automaton, equivalentStates));
							statenumber = equivalentStates.size();
						}
					}
				} else {
					for (StateTuple t : rekursivePath) {
						notEquivalentStates.add(t);
					}

				}
			}
		}

		return MinimizationHelper.createNewAutomaton(automaton, equivalentStates);

	}

	private boolean isEquivalent(StateTuple tuple, TransitionTable table) {
		if (notEquivalentStates.contains(tuple))
			return false;
		if (rekursivePath.contains(tuple))
			return true;
		rekursivePath.add(tuple);
		for (Terminal a : automaton.getAlphabet()) {
			if (a.getType() != TerminalType.SINGLE)
				throw new IllegalArgumentException(this.getClass().getName() + " does only allow DEAs.");
			List<State> s1 = table.getEndStatesFromTransitionsForState(tuple.getFirstState(), a);
			List<State> s2 = table.getEndStatesFromTransitionsForState(tuple.getSecondState(), a);
			if (s1.size() != s2.size()) {
				return false;
			} else if (s1.size() == 1) {
				State state1 = find(s1.get(0));
				State state2 = find(s2.get(0));
				if (!state1.equals(state2)) {
					StateTuple nextTuple = new StateTuple(state1, state2);
					if (!rekursiveEquiStates.contains(nextTuple)) {
						rekursiveEquiStates.add(nextTuple);
						if (!isEquivalent(nextTuple, table))
							return false;
						else
							rekursivePath.remove(nextTuple);
					}
				}
			}
		}
		rekursiveEquiStates.add(tuple);
		return true;
	}

	private void make(State state) {
		MinimizationHelper.make(state, equivalentStates);
	}

	private State find(State s) {
		return MinimizationHelper.find(s, equivalentStates);
	}

	private void union(State s1, State s2) {
		MinimizationHelper.union(s1, s2, equivalentStates);
	}

	public List<Automaton> getStepwiseResults() {
		if (stepwiseResults.isEmpty())
			minimize();

		return stepwiseResults;
	}

	public int getEffort() {
		return -1;
	}
}
