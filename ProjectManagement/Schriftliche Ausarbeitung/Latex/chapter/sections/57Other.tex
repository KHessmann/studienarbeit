\section{Brzozowskis Algorithmus}\label{sec:57other}
In den vorhergehenden Abschnitten wurden verschiedene Verfahren beschrieben, welche die Eigenschaften von Äquivalenzrelation für die Minimierung ausnutzen. Neben diesen im Kern ähnlichen Verfahren gibt es noch einige weitere Varianten, auf welche hier nicht weiter eingegangen werden soll. Eine Lösung, die weit von den bisherigen vorgestellten abweicht, ist der Minimierungsalgorithmus von Brzozowski, welcher im Folgenden beschrieben werden soll.
\subsubsection*{Grundidee}
Brzozowskis Algorithmus basiert auf zwei Mechanismen, die nacheinander durchgeführt werden. Das Verfahren beginnt damit, den gegebenen DEA \enquote{umzudrehen} (im Folgenden: invertieren), führt anschließend die bereits in Abschnitt \ref{sec:43Subset} vorgestellte Potenzmengenkonstruktion durch und wiederholt dieses Vorgehen ein zweites Mal. Der resultierende Automat entspricht dann dem gesuchten minimierten DEA \cite[6]{Almeida.2007}.
\subsubsection*{Vorgehensweise}
Für einen gegebenen \ac{DEA} $D = (Q_{D}, \Sigma_D, \delta_{D}, q_{0,D}, F_{D})$ wird der gesuchte minimierte DEA $M = (Q_{M}, \Sigma_M, \delta_{M}, q_{0,M}, F_{M})$ wie folgt ermittelt\cite[24\psqq]{VanderVeen.2017}.  

\noindent Der Algorithmus wird in der Literatur häufig durch folgende Kurzschreibweise repräsentiert
\begin{center}
	M =	$det(rev(det(rev(D))))$ \cite[6]{Almeida.2007},
\end{center}
wobei $rev$ (reverse) einen Algorithmus für das Invertieren eines DEA repräsentiert und $det$ (determinization) eine Form der Potenzmengenkonstruktion, die NEAs mit mehr als einen Startzustand akzeptiert.
Die einzelnen Schritte sollen nun kurz erläutert werden.
\begin{enumerate}
	\item Zunächst wird der Automat invertiert. Dabei wird ein NEA $D^{-1}$ ermittelt, für den gilt: alle akzeptierenden Zustände aus $D$ werden zu Startzuständen in $D^{-1}$ und der Startzustand aus $D$ wird einziger akzeptierender Zustand in $D^{-1}$. 	\begin{center}
		$Q_{0,D^{-1}} = F_D$, ~~~$F_{D^{-1}} = \{q_{0,D}\}$
	\end{center} 
	\noindent Das Alphabet $\Sigma_{D^{-1}}$ bleibt dabei bestehen, die Übergangsfunktion $\delta_{D^{-1}}$ wird so verändert, dass die Positionen der Ausgangs- und Endzustände vertauscht werden. Die Übergangsfunktionen laufen somit in die \enquote{entgegengesetzte} Richtung. 
	
	\noindent Nicht von den Startzuständen aus erreichbare Zustände werden aus $Q_{D^{-1}}$ entfernt.
	\item Anschließend wird die erste Potenzmengenkonstruktion durchgeführt, wobei das bisher bekannte Vorgehen so abgeändert wird, dass im ersten Schritt die Menge der Startzustände zusammengefasst wird.
	\item Der resultierende DEA wird erneut invertiert und alle nicht erreichbaren Zustände entfernt.
	\item Zuletzt wird eine zweite Potenzmengenkonstruktion durchgeführt. Der resultierende DEA ist der gesuchte minimale DEA M.	 
\end{enumerate}
Der Beweis für die Korrektheit des Verfahrens ist in \cite{Bonchi.2012} beschrieben.
\subsubsection*{Beispiel}
Das Verfahren soll nun beispielhaft zur Minimierung des bereits zuvor betrachteten DEA in \reffig{fig:57Brzo1a} angewandt werden.
\begin{enumerate}
	\item Der invertierte Automat $D^{-1}$ ist in \reffig{fig:57Brzo1b} zu sehen. Die zuvor noch akzeptierenden Zustände $q_3, q_4$ werden als Startzustände markiert und der alte Startzustand $q_0$ wird zum neuen, einzigen akzeptierenden Zustand. $q_2$ ist nicht mehr von den Startzuständen aus erreichbar und wird daher aus der Zustandsmenge des Automaten entfernt. 
	\item Ausgehend von den zusammengefassten Zuständen $\{q_3,q_4\}$ wird die erste Potenzmengenkonstruktion durchgeführt. Der resultierende DEA ist in \reffig{fig:57Brzo1c} zu sehen.
	\item Erneutes Invertieren liefert den in \reffig{fig:57Brzo1d} dargestellten NEA. Da alle Zustände erreichbar sind, muss keiner aus der Zustandsmenge entfernt werden. Um die Lesbarkeit zu erleichtern, ist in \reffig{fig:57Brzo1e} der umbenannte Automat zu sehen. 
	\item Die letzte Potenzmengenkonstruktion liefert den gesuchten minimalen Automaten M, der bis auf die Benennung identisch zu den Ergebnissen der Verfahren aus den vorherigen Abschnitten ist.
\end{enumerate}
\input{./images/57Brzozowski.tex}
\subsubsection*{Anmerkung zur Implementierung}
	Da das Verfahren im Wesentlichen auf der bereits in der Umwandlung behandelten Potenzmengenkonstruktion basiert, wird hier nicht näher auf die Implementierung eingegangen. In dieser Arbeit sollen unterschiedliche Verfahren untersucht werden, weshalb der Fokus auf die in den vorherigen Abschnitten beschriebenen Methoden gelegt wurde. Es sei jedoch darauf hingewiesen, dass die Algorithmen für die verwendete Variante der Potenzmengenkonstruktion und die Invertierung eines DEA im Detail in \cite[7]{VanderVeen.2017} aufgeführt sind. 
	\clearpage
\subsubsection*{Anmerkung zu Überprüfungsmöglichkeiten}
	Die drei Zwischenergebnisse des Algorithmus sind abgesehen von der Benennung eindeutig und lassen sich daher grundsätzlich gut überprüfen. Für die Ergebnisse der Potenzmengenkonstruktion kann die in Abschnitt \ref{sec:43Subset} beschriebene Überprüfungsmethode verwendet werden. Für einen Abgleich der invertierten Automaten wurde bisher keine Überprüfungsmethode implementiert, da es sich hierbei um NEAs mit ggf. mehreren Startzuständen handelt. Der Algorithmus für den direkten Vergleich zweier DEAs aus Kapitel \ref{chap:6Testen} kann daher nicht verwendet werden. Eine Ergänzung des Algorithmus für nichtdeterministische Automaten ist denkbar, wird hier aber nicht weiter verfolgt.
