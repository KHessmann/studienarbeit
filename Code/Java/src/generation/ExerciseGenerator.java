package generation;

import commons.Automaton;
import commons.EffortClass;

public class ExerciseGenerator {

	private static AutomatonGenerator generator = new AutomatonGenerator();
	private static final int GENERATION_COUNT = 25;

	public static Automaton generateAutomatonForMinimizationWithDefault(EffortClass effort) {
		return GenerationHelper.generateForMinimization(generator, GENERATION_COUNT, effort,
				GenerationDefaultSetting.PROBABILITY_TRANSITION_EXISTS_MINIMIZATION,
				GenerationDefaultSetting.NUMBER_OF_STATES_BITSTREAM_MINIMIZATION,
				GenerationDefaultSetting.MINIMUM_TRANSITIONS_MINIMIZATION,
				GenerationDefaultSetting.MAXIMUM_TRANSITIONS_MINIMIZATION,
				GenerationDefaultSetting.MIN_GENERATED_DEA_STATES_MINIMIZATION,
				GenerationDefaultSetting.MAX_GENERATED_DEA_STATES_MINIMIZATION);
	}

	public static Automaton generateAutomatonForMinimization(EffortClass effort, double probabilityForTransitions,
			int numberOfStates, int minNeaTransitions, int maxNeaTransitions, int minDeaStateCount,
			int maxDeaStateCount) {
		return GenerationHelper.generateForMinimization(generator, GENERATION_COUNT, effort, probabilityForTransitions,
				numberOfStates, minNeaTransitions, maxNeaTransitions, minDeaStateCount, maxDeaStateCount);
	}

	public static Automaton generateAutomatonForTransformationWithDefault(EffortClass effort, int minEpsilonCountCount,
			int maxEpsilonCount, int sequenceCount) {
		return GenerationHelper.generateForTransformation(generator, GENERATION_COUNT, effort, minEpsilonCountCount,
				maxEpsilonCount, sequenceCount, GenerationDefaultSetting.PROBABILITY_TRANSITION_EXISTS_TRANSFORMATION,
				GenerationDefaultSetting.NUMBER_OF_STATES_TRANSFORMATION,
				GenerationDefaultSetting.MINIMUM_TRANSITIONS_TRANSFORMATION,
				GenerationDefaultSetting.MAXIMUM_TRANSITIONS_TRANSFORMATION,
				GenerationDefaultSetting.MIN_GENERATED_DEA_STATES_TRANSFORMATION,
				GenerationDefaultSetting.MAX_GENERATED_DEA_STATES_TRANSFORMATION);
	}

	public static Automaton generateAutomatonForTransformation(EffortClass effort, int minEpsilonCountCount,
			int maxEpsilonCount, int sequenceCount, double probabilityForTransitions, int numberOfStates,
			int minNeaTransitions, int maxNeaTransitions, int minDeaStateCount, int maxDeaStateCount) {
		return GenerationHelper.generateForTransformation(generator, GENERATION_COUNT, effort, minEpsilonCountCount,
				maxEpsilonCount, sequenceCount, probabilityForTransitions, numberOfStates, minNeaTransitions,
				maxNeaTransitions, minDeaStateCount, maxDeaStateCount);
	}

	public static Automaton generateAutomatonForSubsetConstructionWithDefault(EffortClass effort) {
		return GenerationHelper.generateForTransformation(generator, GENERATION_COUNT, effort, 0, 0, 0,
				GenerationDefaultSetting.PROBABILITY_TRANSITION_EXISTS_TRANSFORMATION,
				GenerationDefaultSetting.NUMBER_OF_STATES_TRANSFORMATION,
				GenerationDefaultSetting.MINIMUM_TRANSITIONS_TRANSFORMATION,
				GenerationDefaultSetting.MAXIMUM_TRANSITIONS_TRANSFORMATION,
				GenerationDefaultSetting.MIN_GENERATED_DEA_STATES_TRANSFORMATION,
				GenerationDefaultSetting.MAX_GENERATED_DEA_STATES_TRANSFORMATION);
	}

	public static Automaton generateAutomatonForSubsetConstruction(EffortClass effort, double probabilityForTransitions,
			int numberOfStates, int minNeaTransitions, int maxNeaTransitions, int minDeaStateCount,
			int maxDeaStateCount) {
		return GenerationHelper.generateForTransformation(generator, GENERATION_COUNT, effort, 0, 0, 0,
				probabilityForTransitions, numberOfStates, minNeaTransitions, maxNeaTransitions, minDeaStateCount,
				maxDeaStateCount);
	}

	public static Automaton generateAutomatonForEpsilonWithDefault(EffortClass effort) {
		return GenerationHelper.generateForEpsilonTransformation(true, generator, GENERATION_COUNT, effort,
				GenerationDefaultSetting.PROBABILITY_TRANSITION_EXISTS_TRANSFORMATION,
				GenerationDefaultSetting.NUMBER_OF_STATES_EPSILON);
	}

	public static Automaton generateAutomatonForEpsilon(EffortClass effort, double probabilityForTransitions,
			int numberOfStates) {
		return GenerationHelper.generateForEpsilonTransformation(false, generator, GENERATION_COUNT, effort,
				probabilityForTransitions, numberOfStates);
	}

	public static Automaton generateAutomatonForEpsilonWithDefault(int maxEpsilonCount, int minEpsilonCountCount) {
		return generator.generateAutomatonWithBitstreamsOnlyWithEpsilon(maxEpsilonCount, minEpsilonCountCount);
	}

	public static Automaton generateAutomatonForEpsilon(int maxEpsilonCount, int minEpsilonCountCount,
			double probabilityForTransitions, int numberOfStates) {
		return generator.generateAutomatonWithBitstreamsOnlyWithEpsilon(numberOfStates, probabilityForTransitions,
				minEpsilonCountCount, maxEpsilonCount);
	}

	public static Automaton generateAutomatonForEpsilonWithDefault(int epsilonCount) {
		return generateAutomatonForEpsilonWithDefault(epsilonCount, epsilonCount);
	}

	public static Automaton generateAutomatonForEpsilon(int epsilonCount, double probabilityForTransitions,
			int numberOfStates) {
		return generateAutomatonForEpsilon(epsilonCount, epsilonCount, probabilityForTransitions, numberOfStates);
	}

	public static Automaton generateAutomatonForSequenceWithDefault(int sequenceCount) {
		return generator.generateAutomatonWithBitstreamsOnlyWithSequence(sequenceCount);
	}

	public static Automaton generateAutomatonForSequence(int sequenceCount, double probabilityForTransitions,
			int numberOfStates) {
		return generator.generateAutomatonWithBitstreamsOnlyWithSequence(numberOfStates, probabilityForTransitions,
				sequenceCount);
	}

}
