package commons;

public final class StateTuple {

	private State state1;
	private State state2;

	public StateTuple(State s1, State s2) {
		int comparison = s1.getId().compareTo(s2.getId());
		if (comparison == 0) {
			throw new IllegalArgumentException("Can't create a tuple for the same state.");
		}
		if (s1.isInitial() && s2.isInitial() || !s1.isInitial() && !s2.isInitial()) {
			if (comparison < 0) {
				state1 = s1;
				state2 = s2;
			} else {
				state1 = s2;
				state2 = s1;
			}
		} else if (s1.isInitial()) {
			state1 = s1;
			state2 = s2;
		} else {
			state1 = s2;
			state2 = s1;
		}
	}

	public State getFirstState() {
		return state1;
	}

	public State getSecondState() {
		return state2;
	}

	public boolean isFirstOrSecondState(State state) {
		if (state1.equals(state) || state2.equals(state)) {
			return true;
		}
		return false;
	}

	public State getOtherState(State state) {
		if (state1.equals(state)) {
			return state2;
		} else {
			return state1;
		}
	}

	@Override
	public boolean equals(Object anObject) {
		if (this == anObject) {
			return true;
		}
		if (anObject instanceof StateTuple) {
			StateTuple tuple = (StateTuple) anObject;
			return (state1.equals(tuple.getFirstState()) && state2.equals(tuple.getSecondState()));
		}
		return false;
	}

	@Override
	public int hashCode() {
		int hash = state1.getId().hashCode() + state2.getId().hashCode();
		return hash;
	}

	@Override
	public String toString() {
		if (state1.getName().compareTo(state2.getName()) < 0) {
			return "[" + state1.getName() + "," + state2.getName() + "]";
		} else
			return "[" + state2.getName() + "," + state1.getName() + "]";

	}

}
