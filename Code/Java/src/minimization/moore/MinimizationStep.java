package minimization.moore;

import java.util.List;
import java.util.Map;
import java.util.Set;

import minimization.EquivalenceClass;

public class MinimizationStep {
	private boolean isFinalStep;
	private int stepNumber;
	private Map<EquivalenceClass, List<EquivalenceEvaluation>> oldClassEvaluationMap;
	private Set<EquivalenceClass> newClasses;

	public MinimizationStep(Map<EquivalenceClass, List<EquivalenceEvaluation>> oldECEvaluationMap,
			Set<EquivalenceClass> newClasses, int stepNumber) {
		this.oldClassEvaluationMap = oldECEvaluationMap;
		this.newClasses = newClasses;
		this.isFinalStep = false;
		this.stepNumber = stepNumber;
	}

	public boolean isFinalStep() {
		return isFinalStep;
	}

	public void setFinalStep() {
		isFinalStep = true;
	}

	public int getStepNumber() {
		return stepNumber;
	}

	public Set<EquivalenceClass> getOldEquivalenceClasses() {
		return oldClassEvaluationMap.keySet();
	}

	public Set<EquivalenceClass> getNewEquivalenceClasses() {
		return newClasses;
	}

	public Map<EquivalenceClass, List<EquivalenceEvaluation>> getOldClassEvaluationMap() {
		return oldClassEvaluationMap;
	}

	public List<EquivalenceEvaluation> getEvaluationForClass(EquivalenceClass ec) {
		return oldClassEvaluationMap.get(ec);
	}

	public String toStringWithoutRedundantInformation() {
		return toString(false);
	}

	@Override
	public String toString() {
		return toString(true);
	}

	public String toString(boolean withRedundantInformation) {
		StringBuilder sb = new StringBuilder();
		sb.append("=========================================================\n");
		sb.append("Step Number: " + stepNumber + "\n");
		sb.append("Old Equivalence Classes: {");
		for (EquivalenceClass ec : getOldEquivalenceClasses()) {
			sb.append(ec.toString() + ", ");
		}
		sb.deleteCharAt(sb.length() - 1);
		sb.deleteCharAt(sb.length() - 1);
		sb.append("}\n");

		sb.append("New Equivalence Classes: {");
		for (EquivalenceClass ec : getNewEquivalenceClasses()) {
			sb.append(ec.toString() + ", ");
		}
		sb.deleteCharAt(sb.length() - 1);
		sb.deleteCharAt(sb.length() - 1);
		sb.append("}\n");
		for (EquivalenceClass ec : getOldEquivalenceClasses()) {
			for (EquivalenceEvaluation evaluation : oldClassEvaluationMap.get(ec)) {
				if (withRedundantInformation || !evaluation.isRedundantInformation()) {
					sb.append(evaluation.toString());
				}
			}
		}
		sb.append("Letzter Schritt: " + isFinalStep + "\n");
		sb.append("=========================================================");
		return sb.toString();

	}

}
