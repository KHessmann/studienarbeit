package comparison;

import java.util.ArrayList;
import java.util.Map;

import commons.Automaton;
import commons.State;
import commons.StateTuple;
import commons.Transition;
import minimization.TableFillingMinimizer;

public class TableFillingAutomatonCompareHelper {

	public static boolean compareDEA(Automaton a1, Automaton a2) {
		Automaton combinedAutomaton = combineAutomata(a1, a2);
		TableFillingMinimizer tableFillingMinimizer = new TableFillingMinimizer(combinedAutomaton);
		Map<StateTuple, Boolean> equivalenceTable = tableFillingMinimizer.createFinalEquivalenceTable();
		return testEquivalence(a1.getInitial(), a2.getInitial(), equivalenceTable);
	}

	private static boolean testEquivalence(State initial1, State initial2, Map<StateTuple, Boolean> equivalenceTable) {
		StateTuple initialStateTuple = new StateTuple(initial1, initial2);
		if (equivalenceTable.get(initialStateTuple)) {
			return true;
		}
		return false;
	}

	private static Automaton combineAutomata(Automaton a1, Automaton a2) {
		ArrayList<State> states = new ArrayList<>(a1.getStates());
		states.addAll(a2.getStates());
		ArrayList<Transition> transitions = new ArrayList<>(a1.getTransitions());
		int stateCounter = 0;
		for (State state : states) {
			state.setName("q" + stateCounter);
			stateCounter++;
		}
		transitions.addAll(a2.getTransitions());
		return new Automaton(states, transitions);
	}

}
