package generation;

public class GenerationDefaultSetting {

	protected static final int NUMBER_OF_TERMINALS = 2;

	protected static final int SEQUENCE_COUNT = 1;
	protected static final int NUMBER_OF_STATES_SEQUENCE = 4;
	protected static final double PROBABILITY_TRANSITION_EXISTS_SEQUENCE = 0.2;
	protected static final int MAXIMUM_TRANSITIONS_SEQUENCE = 15;
	protected static final int MINIMUM_TRANSITIONS_SEQUENCE = 7;

	protected static final int NUMBER_OF_STATES_EPSILON = 6;
	protected static final double PROBABILITY_TRANSITION_EXISTS_EPSILON = 0.2;
	protected static final int MAXIMUM_TRANSITIONS_EPSILON = 15;
	protected static final int MINIMUM_TRANSITIONS_EPSILON = 7;
	/*
	 * These values are recommended defaults for the generation with bitstreams
	 */
	protected static final int NUMBER_OF_STATES_BITSTREAM = 4;
	protected static final double PROBABILITY_TRANSITION_EXISTS_BITSTREAM = 0.2;
	protected static final int MAXIMUM_OF_ACCEPTING_STATES = Math.min(NUMBER_OF_STATES_BITSTREAM / 2, 3);
	protected static final int MINIMUM_EPSILON = 1;
	protected static final int MAXIMUM_EPSILON = NUMBER_OF_STATES_BITSTREAM / 2;
	protected static final int MAXIMUM_TRANSITIONS = NUMBER_OF_STATES_BITSTREAM * NUMBER_OF_STATES_BITSTREAM
			* NUMBER_OF_TERMINALS;
	protected static final int MINIMUM_TRANSITIONS = NUMBER_OF_STATES_BITSTREAM - 1;
	protected static final double PROBABILITY_EPSILON_TRANSITION = 1.0 / NUMBER_OF_STATES_BITSTREAM;
	protected static final boolean END_STATE_OF_EPSILON_IS_ACCEPTING = true;
	protected static final boolean USE_MIN_MAX_VALUES = true;

	protected static final int NUMBER_OF_STATES_BITSTREAM_MINIMIZATION = 4;
	protected static final int MIN_GENERATED_DEA_STATES_MINIMIZATION = 5;
	protected static final int MAX_GENERATED_DEA_STATES_MINIMIZATION = 10;
	protected static final int MINIMUM_TRANSITIONS_MINIMIZATION = 9;
	protected static final int MAXIMUM_TRANSITIONS_MINIMIZATION = 12;
	protected static final double PROBABILITY_TRANSITION_EXISTS_MINIMIZATION = (MAXIMUM_TRANSITIONS_MINIMIZATION
			- MINIMUM_TRANSITIONS_MINIMIZATION)
			/ (2 * (NUMBER_OF_STATES_BITSTREAM_MINIMIZATION * NUMBER_OF_STATES_BITSTREAM_MINIMIZATION
					* NUMBER_OF_TERMINALS - MINIMUM_TRANSITIONS_MINIMIZATION));

	protected static final int NUMBER_OF_STATES_TRANSFORMATION = 5;
	protected static final int MIN_GENERATED_DEA_STATES_TRANSFORMATION = 3;
	protected static final int MAX_GENERATED_DEA_STATES_TRANSFORMATION = 10;
	protected static final int MINIMUM_TRANSITIONS_TRANSFORMATION = 9;
	protected static final int MAXIMUM_TRANSITIONS_TRANSFORMATION = 14;
	protected static final double PROBABILITY_TRANSITION_EXISTS_TRANSFORMATION = (MAXIMUM_TRANSITIONS_TRANSFORMATION
			- MAXIMUM_TRANSITIONS_TRANSFORMATION)
			/ (2 * (NUMBER_OF_STATES_TRANSFORMATION * NUMBER_OF_STATES_TRANSFORMATION * NUMBER_OF_TERMINALS
					- MINIMUM_TRANSITIONS_TRANSFORMATION));

	/*
	 * These values are defaults for the generation with matrices
	 */
	protected static final int NUMBER_OF_STATES_MATRIX = 5;
	protected static final boolean USE_MAX_NUMBER_OF_TRANSITIONS = false;
	protected static final int MAX_NUMBER_OF_TRANSITIONS = NUMBER_OF_STATES_MATRIX * 2;
	protected static final int NUMBER_OF_ACCEPTING_STATES = Math.min(NUMBER_OF_STATES_MATRIX / 2, 3);
}
