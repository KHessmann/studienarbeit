\section{Potenzmengenkonstruktion}\label{sec:43Subset}
\subsection{Problemstellung}\label{ssec:431SubsetProblem}
Die vorherigen Abschnitte dieses Kapitels behandeln, wie ein \acl{NEA} so umgewandelt werden kann, dass die Übergangsfunktion $\delta$ des resultierenden Automaten weder $\epsilon$ noch Sequenzen $s$ übernimmt. 
Der so ermittelte NEA hat gegenüber einem DEA den Nachteil, dass beim Einlesen eines Eingabesymbols nicht eindeutig definiert ist, in welchen Zustand der zugehörige Automat übergeht. Ziel dieses Abschnitts ist es daher, für einen beliebigen NEA den zugehörigen DEA zu konstruieren. Dafür soll im Folgenden die Potenzmengenkonstruktion (auch: Teilmengenkonstruktion) eingeführt werden.   

\subsection{Beschreibung des Verfahrens}\label{ssec:432SubsetAlg}
\subsubsection*{Grundidee}
Der Nichtdeterminismus des NEA besteht darin, dass für einen Ausgangszustand $q_i$ und ein Eingabesymbol $a \in \Sigma$ nicht zwingend genau ein Endzustand $q_j$ durch die Übergangsfunktion definiert ist. Dies kann in zwei Problemfälle unterteilt werden. \\
\noindent \textbf{1. $|\delta(q_i,a)|>1$}: Falls die Übergangsfunktion $\delta$ für ein Eingabetupel mehrfach definiert ist, werden alle so erreichbaren Endzustände zu einem neuen Zustand zusammengefasst.\\
\noindent \textbf{2. $|\delta(q_i,a)|=0$}: In dem Fall, dass die Übergangsfunktion $\delta$ für ein Eingabetupel undefiniert ist, wird $\delta(q_i,a)=\emptyset$ neu erstellt. Dabei steht $\emptyset$ für den \definitionCallout{Zustand der leeren Menge}.\\ 
\subsubsection*{Vorgehensweise}
Für einen gegebenen NEA $N = (Q_{N}, \Sigma_{N}, \delta_{N}, q_{0,N}, F_{N})$ wird der gesuchte DEA $D = (Q_{D}, \Sigma_{D}, \delta_{D}, q_{0,D}, F_{D})$ wie folgt ermittelt\cite[106]{Martin.2011}. 
\begin{enumerate}
	\item Die Zustandsmenge $Q_D$ entspricht der Potenzmenge der Zustandsmenge $Q_N$. 
	\begin{center}$Q_D=P(Q_{N})$\end{center}
	\item Das Eingabealphabet wird übernommen. 
	\begin{center}$\Sigma_{D} = \Sigma_{N}$\end{center}
	\item Das Ergebnis der Übergangsfunktion für ein Eingabesymbol $a$ und einen Zustand $q_D=\{q_{N,1}, q_{N,2},...\}$ entspricht der Vereinigung aller Zustände, die für $\delta_{NEA}$ mit den in $q_D$ enthaltenen Zuständen und $a$ erreicht werden können. 
	\begin{center}$\forall q_D \in Q_{D}, a \in \Sigma_{N}: \delta_{D}(q_D,a) = \bigcup \{\delta_{N}(q_N,a) | q_N \in q_D\}$\end{center}
	\item Der Startzustand entspricht der Menge, die den Startzustand $q_{0,N}$ enthält. 
	\begin{center} $q_{0,D} = \{q_{0,N}\}$\end{center}
	\item Ein Zustand $q_D=\{q_{N,1}, q_{N,2},...\}$ ist genau dann akzeptierend, wenn mindestens einer der Zustände $q_{N,i}$ ebenfalls akzeptierend ist.
	\begin{center}$F_D =\{ q_D \in Q_D | q_D \cap F_N \neq \emptyset\}$ \end{center}
\end{enumerate}
\begin{exmp}{432subset1}
	Mithilfe des beschriebenen Verfahrens soll der DEA für den in \reffig{fig:432subset1} gezeigten NEA konstruiert werden. Die genaue Vorgehensweise zur Bestimmung der Übergangsfunktion $\delta_D$ wird in \reffig{fig:432subset1a} erläutert. Ausgehend vom Startzustand $q_{0,D} = \{q_{0}\}$ soll $\delta(q_{0,D},a)$ ermittelt werden. Die in dem Zustandsdiagramm violett markierten Kanten führen zu den Zuständen $q_0$ und $q_1$. In der Übergangstabelle wird \reftab{tab:432Subset1} $\delta(\{q_0\},a) = \{q_0,q_1\}$ eingetragen. Weder $q_0$, noch $q_1$ sind akzeptierende Zustände und damit ist auch der neue Zustand $\{q_0,q_1\}$ nicht akzeptierend. Von diesem ausgehend soll das Ergebnis für $\delta(\{q_0,q_1\},b)$ bestimmt werden. Die in \reffig{fig:432subset1b} markierten Kanten führen zu den Zuständen $q_2$ und $q_3$. Da $q_2$ ein akzeptierender Zustand ist, ist auch der neue Zustand $\{q_2,q_3\}$ akzeptierend. 
\end{exmp}
\input{./images/432subset12.tex}
\input{tables/432Subset1.tex}

Wird für jedes Element der Potenzmenge einer Menge mit $n$ Zuständen die Übergangstabelle befüllt, so müssen insgesamt $2^n$ Zustände untersucht und Zeilen ausgefüllt werden. Im Fall des in \reffig{fig:432subset1} gezeigten NEA würde das bereits 16 Einträgen entsprechen, wo\-rauf jedoch verzichtet wird. Das hat den Grund, dass im Normalfall nicht alle $2^n$ Zustände vom Startzustand aus erreicht werden. Von $\{q_0\}$ ausgehend werden hier etwa die Zustände $\{q_0,q_1\}$ und $\{q_3\}$ erreicht. Von dort gelangt man erneut zu $\{q_0,q_1\}$, $\{q_2,q_3\}$ und dem Zustand der leeren Menge $\emptyset$. Alle fünf so erreichbaren Zustände sind in \reftab{tab:432Subset1} violett markiert und entsprechen eben den Zuständen des in \reffig{fig:432subset2} dargestellten resultierenden DEA. Um den Anwendern zusätzlichen Arbeitsaufwand zu ersparen, soll daher auch im implementierten Algorithmus die zusätzliche Einschränkung gelten, dass die Übergangsfunktion $\delta$ nur für die Zustände bestimmt werden soll, die von $q_{0,D}$ beginnend auch tatsächlich erreicht werden. 
\input{./images/432subset3.tex}

\subsubsection*{Korrektheit des Verfahrens}
\noindent Das Verfahren ist für jeden beliebigen $NEA$ einsetzbar. Die reguläre Sprache eines mithilfe der Potenzmengenkonstruktion ermittelten DEA $D$ ist stets äquivalent zu der des ursprünglichen NEA $N$: $L(N)$ = $L(D)$. Der Beweis dazu findet sich in \cite[91\psq]{Hopfcroft.2011}. 
\subsection{Aufwandsanalyse}\label{ssec:433SubsetAufwand}
Der Aufwand für die Durchführung des  Verfahrens durch einen Anwender kann auf folgende Teiloperationen aufgeteilt werden:
\begin{itemize}
	\item Überprüfung zu Anfang, ob der NEA nichtdeterministisch ist $\Rightarrow$ Suche eines Zustands $q_{nd}$ mit nichtdeterministischen Eigenschaften 
	\newline \textit{\hyperref[rule:Aufwand1]{(Suchoperation)}} 		\hspace*{\fill} \arrowCallout{min($n_{ges}, n_{ges}+1-n_{nd})$ Schritte}%
	\item Ermittlung der Ergebnismenge für $\delta(q,a)$ 
	\newline
	\textit{\hyperref[rule:Aufwand3]{(Übergangsanalyse)}} 		\hspace*{\fill} \arrowCallout{1 Schritt pro Analyse}%
	\item Prüfen, ob eine resultierende nichtleere Zustandsmenge bereits in der Tabelle als neuer Zustand aufgeführt ist
	\newline
	\textit{\hyperref[rule:Aufwand1]{(Suchoperation)}} 	\hspace*{\fill} \arrowCallout{1 Schritt}%
	\item Prüfen, ob eine Zustandsmenge mit mehr als einem Element mindestens einen akzeptierenden Zustand enthält
	\newline
	\textit{\hyperref[rule:Aufwand1]{(Suchoperation)}} 	\hspace*{\fill} \arrowCallout{1 Schritt}%
	\item Hinzufügen des Eintrags für den Zustand der leeren Menge $\emptyset$
		\newline
	\textit{\hyperref[rule:Aufwand4]{(Erstellen eines Zustands)}} 	\hspace*{\fill} \arrowCallout{1 Schritt}%
\end{itemize}
\clearpage
\subsection{Implementierung}\label{ssec:434SubsetImpl}
Für die Erstellung eines zum Verfahren zugehörigen Algorithmus gibt es in der Literatur viele verschiedenen Lösungsmöglichkeiten. Die meisten dieser Lösungen sind deutlich performanter als die im Folgenden vorgestellte (vergleiche beispielsweise \citeNoParen[3\psqq]{Johnson.1997}). Da das Anwendungsgebiet dieser Arbeit in der Lehre liegt und dort keine so großen Automaten gewünscht sind, dass ein Fokus auf die Performance erforderlich wäre, wird diese hier vernachlässigt. Vielmehr soll bereits bei der Implementierung darauf geachtet werden, Möglichkeiten zur Überprüfung in den Algorithmus mit einzubauen. \reflst{code:434transitiontable} ermöglicht es, für einen beliebigen Automaten eine Übergangstabelle zu ermitteln. Diese entspricht schematisch der in Kapitel \ref{chap:2Grundlagen} vorgestellten gleichnamigen Notationsform und wird im eigentlichen Algorithmus \ref{code:434subset} verwendet, um für eine Liste an Ausgangszuständen und ein Eingangssymbol alle zugehörigen Endzustände zu ermitteln.
\newpassage
\input{code/434Übergangstabelle.tex}

\input{code/434Subset.tex}
\newpage
%\clearpage



\subsection{Überprüfungsmöglichkeiten}\label{ssec:435SubsetÜberprüfung}
Mit der in \reflst{code:434transitiontable} beschriebenen Übergangstabelle ist eine Überprüfung der Ergebnisse des Anwenders möglich, vorausgesetzt er benennt die Zustände nicht um. In \reftab{tab:435Test1} sind diese beiden Fälle aufgeführt. Beim Vergleich des korrekten Automaten mit der ersten Lösung können auftretende Fehler durch zeilenweise Überprüfung identifiziert werden. Auch wenn der in \reffig{fig:435subset4} dargestellte Automaten bis auf die Benennung identisch zum korrekten Automat in \reffig{fig:432subset2} ist und damit eine richtige Lösung für die Aufgabenstellung darstellt, lässt sich dies aus der Tabelle nicht ohne weiteres herauslesen. Möchte man eine solche Lösung ebenfalls als richtig werten, ist es denkbar, stattdessen eine der in Kapitel \ref{chap:6Testen} vorgestellten Möglichkeiten zur Überprüfung zweier Automaten auf Äquivalenz zu nutzen.  
\input{./tables/435Überprüfung.tex}
\input{./images/435Subset4.tex}
