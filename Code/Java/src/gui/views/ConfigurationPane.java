package gui.views;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.Scrollable;

import commons.Automaton;
import gui.comboboxtypes.MinimizationType;
import gui.comboboxtypes.TransformationStep;
import gui.drawing.DotFile;
import minimization.IncrementalMinimizer;
import minimization.Minimizer;
import minimization.MooreMinimizer;
import minimization.TableFillingMinimizer;
import minimization.moore.MinimizationStep;
import minimization.tableFilling.TableFillingSteps;
import transformation.Transformer;

public class ConfigurationPane extends JPanel implements Scrollable {
	private static final long serialVersionUID = 1L;
	private JTextArea generateTextarea = new JTextArea();
	private JButton generateTransformButton = new JButton("Transformation Task");
	private JButton generateMinimizationButton = new JButton("Minimization Task");
	private SimulatorPane simulatorPane;
	private Automaton currentAutomaton;
	private JComboBox<String> transformationComboBox;
	private JComboBox<String> minimizationComboBox;
	private JComboBox<String> minimizerStepComboBox;

	public ConfigurationPane(SimulatorPane simulatorPane) {
		super();
		this.simulatorPane = simulatorPane;
		initView();
	}

	private void initView() {
		setPreferredSize(new Dimension(400, 400));
		setLayout(new BorderLayout());

		generateTextarea.setEditable(false);
		JScrollPane scroll = new JScrollPane(generateTextarea);
		scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scroll.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 30));

		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new BoxLayout(buttonPane, BoxLayout.LINE_AXIS));
		buttonPane.setBorder(BorderFactory.createEmptyBorder(10, 10, 0, 10));
		buttonPane.add(new JLabel("Automat: "));

		buttonPane.add(Box.createRigidArea(new Dimension(10, 0)));
		buttonPane.add(generateTransformButton);
		generateTransformButton.addActionListener(new GenerateTransformationListener());
		buttonPane.add(Box.createRigidArea(new Dimension(10, 0)));
		buttonPane.add(generateMinimizationButton);
		generateMinimizationButton.addActionListener(new GenerateMinimizationListener());

		transformationComboBox = new JComboBox<>(
				new String[] { TransformationStep.ORIGINAL.getName(), TransformationStep.NO_SEQUENCE.getName(),
						TransformationStep.NO_EPSILON.getName(), TransformationStep.DEA.getName(),
						TransformationStep.CLEAN_DEA.getName(), TransformationStep.MINIMIZED_DEA.getName() });
		transformationComboBox.addActionListener(new TransformationListener());
		buttonPane.add(Box.createRigidArea(new Dimension(25, 0)));
		buttonPane.add(transformationComboBox);

		minimizationComboBox = new JComboBox<>(new String[] { MinimizationType.INCREMENTAL.getName(),
				MinimizationType.MOORE.getName(), MinimizationType.TABLE_FILLING.getName() });
		minimizationComboBox.addActionListener(new ChangeMinimizationAlgorithmListener());
		minimizationComboBox.setVisible(false);
		buttonPane.add(Box.createRigidArea(new Dimension(60, 0)));
		buttonPane.add(minimizationComboBox);

		minimizerStepComboBox = new JComboBox<>();
		minimizerStepComboBox.addActionListener(new MinimizationStepListener());
		minimizerStepComboBox.setVisible(false);
		buttonPane.add(Box.createRigidArea(new Dimension(60, 0)));
		buttonPane.add(minimizerStepComboBox);

		buttonPane.add(Box.createRigidArea(new Dimension(70, 0)));

		add(buttonPane, BorderLayout.NORTH);
		add(scroll, BorderLayout.CENTER);
	}

	private class TransformationListener implements ActionListener {
		@Override
		@SuppressWarnings("unchecked")
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() instanceof JComboBox<?>) {
				JComboBox<String> choice = (JComboBox<String>) e.getSource();
				String s = ((String) choice.getSelectedItem());
				minimizationComboBox.setVisible(false);
				minimizerStepComboBox.setVisible(false);
				String text = "";
				if (s.equals(TransformationStep.ORIGINAL.getName())) {
					currentAutomaton = simulatorPane.getTransformer().getOriginalNEA();
				} else if (s.equals(TransformationStep.NO_SEQUENCE.getName())) {
					currentAutomaton = simulatorPane.getTransformer().getRemovedSequenceNEA();
					text = simulatorPane.getTransformer().getInfoForSequenceTransformation();
				} else if (s.equals(TransformationStep.NO_EPSILON.getName())) {
					currentAutomaton = simulatorPane.getTransformer().getRemovedEpsilonNEA();
					text = simulatorPane.getTransformer().getInfoForEpsilonTransformation();
				} else if (s.equals(TransformationStep.DEA.getName())) {
					currentAutomaton = simulatorPane.getTransformer().getFinalDEA();
					text = simulatorPane.getTransformer().getInfoForSubsetConstructionTransformation();
				} else if (s.equals(TransformationStep.CLEAN_DEA.getName())) {
					currentAutomaton = simulatorPane.getTransformer().getRenamedFinalDEA();
					text = simulatorPane.getTransformer().getInfoForTransformation();
				} else if (s.equals(TransformationStep.MINIMIZED_DEA.getName())) {
					minimizationComboBox.setVisible(true);
					currentAutomaton = simulatorPane.getMinimizer().minimize();
					{
						setupMinimizerStepComboBox();
						minimizerStepComboBox.setVisible(true);
						minimizerStepComboBox.setSelectedItem(null);
					}
				}
				showCurrentAutomaton();
				if (!text.isBlank())
					alterTextField(text);
			}
		}
	}

	private class GenerateTransformationListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			Automaton automaton = SimulatorPane.generateTransformationTask();

			simulatorPane.setAutomaton(automaton);
			currentAutomaton = automaton;

			transformationComboBox.setSelectedItem(TransformationStep.ORIGINAL.getName());
			minimizationComboBox.setSelectedItem(MinimizationType.INCREMENTAL.getName());
			minimizerStepComboBox.setSelectedItem(null);
			minimizationComboBox.setVisible(false);
			minimizerStepComboBox.setVisible(false);
		}

	}

	private class GenerateMinimizationListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			Automaton automaton = SimulatorPane.generateMinimizationTask();

			simulatorPane.setAutomaton(automaton);
			currentAutomaton = automaton;

			transformationComboBox.setSelectedItem(TransformationStep.CLEAN_DEA.getName());
			minimizationComboBox.setSelectedItem(MinimizationType.INCREMENTAL.getName());
			minimizerStepComboBox.setSelectedItem(null);
			minimizationComboBox.setVisible(false);
			minimizerStepComboBox.setVisible(false);
		}

	}

	private class ChangeMinimizationAlgorithmListener implements ActionListener {
		@Override
		@SuppressWarnings("unchecked")
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() instanceof JComboBox<?>) {
				minimizerStepComboBox.setVisible(false);
				JComboBox<String> choice = (JComboBox<String>) e.getSource();
				Transformer transformer = simulatorPane.getTransformer();
				String s = ((String) choice.getSelectedItem());
				if (s.equals(MinimizationType.INCREMENTAL.getName())) {
					simulatorPane.setMinimizer(new IncrementalMinimizer(transformer.getRenamedFinalDEA()));
				} else if (s.equals(MinimizationType.MOORE.getName())) {
					simulatorPane.setMinimizer(new MooreMinimizer(transformer.getRenamedFinalDEA()));
				} else if (s.equals(MinimizationType.TABLE_FILLING.getName())) {
					simulatorPane.setMinimizer(new TableFillingMinimizer(transformer.getRenamedFinalDEA()));
				}
				if (transformationComboBox.getSelectedItem().equals(TransformationStep.MINIMIZED_DEA.getName())) {
					Minimizer minimizer = simulatorPane.getMinimizer();
					currentAutomaton = minimizer.minimize();
					if (minimizer instanceof IncrementalMinimizer || minimizer instanceof MooreMinimizer
							|| minimizer instanceof TableFillingMinimizer) {
						setupMinimizerStepComboBox();
						minimizerStepComboBox.setVisible(true);
						minimizerStepComboBox.setSelectedItem(null);
					}
					showCurrentAutomaton();
				}
			}
		}

	}

	private void setupMinimizerStepComboBox() {
		int stepCount = 0;
		if (simulatorPane.getMinimizer() instanceof IncrementalMinimizer) {
			IncrementalMinimizer im = (IncrementalMinimizer) simulatorPane.getMinimizer();
			stepCount = im.getStepwiseResults().size();
		} else if (simulatorPane.getMinimizer() instanceof TableFillingMinimizer) {
			stepCount = 4;
		} else if (simulatorPane.getMinimizer() instanceof MooreMinimizer) {
			MooreMinimizer mm = (MooreMinimizer) simulatorPane.getMinimizer();
			stepCount = mm.getStepwiseResults().size() + 1;
		}
		if (stepCount != minimizerStepComboBox.getItemCount()) {
			minimizerStepComboBox.removeAllItems();
			minimizerStepComboBox.setActionCommand("ADD");
			for (int i = 0; i < stepCount; i++) {
				minimizerStepComboBox.addItem("Step " + i);
			}
			minimizerStepComboBox.setActionCommand("SELECT");
		}
	}

	private class MinimizationStepListener implements ActionListener {
		@Override
		@SuppressWarnings("unchecked")
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() instanceof JComboBox<?> && e.getActionCommand().equals("SELECT")) {

				JComboBox<String> choice = (JComboBox<String>) e.getSource();

				if (transformationComboBox.getSelectedItem().equals(TransformationStep.MINIMIZED_DEA.getName())) {
					if (minimizationComboBox.getSelectedItem().equals(MinimizationType.INCREMENTAL.getName())
							&& simulatorPane.getMinimizer() instanceof IncrementalMinimizer) {
						IncrementalMinimizer im = (IncrementalMinimizer) simulatorPane.getMinimizer();
						List<Automaton> stepWiseAutomatons = im.getStepwiseResults();
						int index = choice.getSelectedIndex();
						if (index >= 0 && stepWiseAutomatons.size() > index) {
							currentAutomaton = stepWiseAutomatons.get(choice.getSelectedIndex());
							showCurrentAutomaton();
						}
					} else if (minimizationComboBox.getSelectedItem().equals(MinimizationType.TABLE_FILLING.getName())
							&& simulatorPane.getMinimizer() instanceof TableFillingMinimizer) {
						TableFillingMinimizer tfm = ((TableFillingMinimizer) simulatorPane.getMinimizer());
						TableFillingSteps steps = tfm.getStepwiseResults();
						int index = choice.getSelectedIndex();
						if (index >= 0) {
							currentAutomaton = steps.getFinalAutomaton();
							showCurrentAutomaton();
							String tableInfo = tfm.getInfo(index);
							alterTextField(tableInfo);
						}
					} else if (minimizationComboBox.getSelectedItem().equals(MinimizationType.MOORE.getName())
							&& simulatorPane.getMinimizer() instanceof MooreMinimizer) {
						MooreMinimizer mm = (MooreMinimizer) simulatorPane.getMinimizer();
						List<MinimizationStep> mooreMinimizationSteps = mm.getStepwiseResults();
						int index = choice.getSelectedIndex();
						if (index >= 0) {
							String mooreInfo;
							if (mooreMinimizationSteps.size() > index) {
								currentAutomaton = mm.getInitialAutomaton();
								mooreInfo = mm.getInfo(index, false);
							} else {
								currentAutomaton = mm.getResultAutomaton();
								mooreInfo = mm.getInfo(index - 1, false);
							}
							showCurrentAutomaton();
							alterTextField(mooreInfo);
						}
					}
				}
			}
		}

	}

	public void alterTextField(String text) {
		if (!text.isBlank()) {
			StringBuilder sb = new StringBuilder();
			sb.append(generateTextarea.getText());
			sb.append(text);
			generateTextarea.setText(sb.toString());
		}
	}

	public void showCurrentAutomaton() {
		Thread thread = new Thread() {
			@Override
			public void run() {
				Automaton dotAuto = new Automaton(currentAutomaton);
				String dotfile = DotFile.createDotFile(dotAuto);
				DotFile.createDotGraph(dotfile, currentAutomaton.getId().toString());
				simulatorPane.showAutomaton();
			}
		};
		String automatonString = currentAutomaton.toString();
		generateTextarea.setText(automatonString + "\n" + currentAutomaton.getTransitionTable().toString());
		thread.start();

	}

	@Override
	public Dimension getPreferredScrollableViewportSize() {
		return null;
	}

	@Override
	public int getScrollableUnitIncrement(Rectangle visibleRect, int orientation, int direction) {
		return 0;
	}

	@Override
	public int getScrollableBlockIncrement(Rectangle visibleRect, int orientation, int direction) {
		return 0;
	}

	@Override
	public boolean getScrollableTracksViewportWidth() {
		return false;
	}

	@Override
	public boolean getScrollableTracksViewportHeight() {
		return false;
	}

	public void setCurrentAutomaton(Automaton automaton) {
		currentAutomaton = automaton;

	}

}
