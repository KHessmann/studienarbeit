package gui.comboboxtypes;

public enum MinimizationType {
	INCREMENTAL("Incremental Minimization"), MOORE("Moore Minimization"), TABLE_FILLING("Table Filling Minimization");

	private final String name;

	MinimizationType(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
}
