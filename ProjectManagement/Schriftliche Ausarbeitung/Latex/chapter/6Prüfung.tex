\chapter{Testen von Automaten}\label{chap:6Testen}
In den Kapiteln \ref{chap:4Umwandlung} und \ref{chap:5Minimierung} wurden verschiedene Verfahren vorgestellt, bei denen neben der Eignung aus Sicht des Anwenders auch stets die verschiedenen Möglichkeiten zur Überprüfung der Ergebnisse durch den Aufgabensteller betrachtet wurden. Im Folgenden sollen drei verschiedene Methoden vorgestellt werden, wie Endliche Automaten miteinander verglichen werden können.
%Motivation: 1. Überprüfung der Ergebnisse während der Implementierung -> empirischer beweis, dass alle minimierungsalgorithmen in allen fällen richtig sind
%2. Überprüfung von anwenderergebnissen


\section{Direkter Vergleich zweier DEAs}\label{sec:61Direkt}
Sowohl bei der Überprüfung von Anwenderlösungen als auch der Implementierung neuer Algorithmen kann es sinnvoll sein, für zwei DEAs zu ermitteln, ob sie mit Ausnahme der Benennung identisch sind. Dies ist insbesondere mit Blick auf Regel \ref{rule:Automat5} wichtig, um es Anwendern zu ermöglichen, Zustände im Rahmen der Aufgabenbearbeitung umzubenennen. Dies ist etwa in Abschnitt \ref{sec:534test} bei der Überprüfung der graphischen Anwenderlösung der Fall. Das entsprechende Beispiel ist in \reffig{fig:61exmp1} noch einmal zu sehen.
\input{./images/61exmp.tex}
\subsubsection*{Grundidee}
Die Grundidee des Algorithmus besteht darin, ausgehend von dem eindeutigen Startzustand die Übergänge des DEA zu durchlaufen und dabei die jeweiligen Endzustände paarweise einander zuzuordnen. Unterscheiden sich die Automaten, so wird es zwangsläufig zu dem Fall kommen, dass ein Eingabesymbol zu einem Zustandspaar führt, welches bereits anderen Zuständen zugeordnet ist.
\newpassage
\begin{exmp}{61direkt}
	In \reffig{fig:61exmp1} ist dies bei dem Paar $(\delta_1(q_1,b), \delta_2(q_1,b))=(\{q_3,q_4\},q_0)$ der Fall. Der Zustand $\{q_3,q_4\}$ des linken Automaten müsste damit dem Startzustand $q_0$  des rechten Automaten zugeordnet werden. Da $q_0$ im ersten Schritt des Verfahrens jedoch bereits dem Startzustand des linken Automaten zugeordnet wurde, kommt es hier zu einem Konflikt. In diesem Schritt kann das Verfahren bereits beendet werden.
\end{exmp} 
%\newpassage
\subsubsection*{Algorithmus}
\reflst{code:61direkt} zeigt den ersten Schritt des Verfahrens. Als erstes Zustandspaar werden die Startzustände beider DEAs einander zugeordnet (Z.9). Anschließend werden von diesem Starttupel ausgehend rekursiv alle anderen Übergänge durchlaufen. Dies ist in \reflst{code:61rekursiv} realisiert. Um Aufwand zu vermeiden wird zu Beginn des Algorithmus geprüft, dass Alphabet sowie Anzahl Übergänge, Zustände und akzeptierende Zustände beider Automaten identisch sind. Ist diese Voraussetzung nicht erfüllt, so können die Automaten nicht gleich sein.
\input{./code/61Direkt.tex}
\clearpage
\noindent Der rekursive Teil des Algorithmus beginnt damit, dass für alle eingehenden Paare überprüft wird, ob diese entweder beide akzeptierenden oder nicht akzeptierenden Zuständen entsprechen. Anschließend werden für alle Übergänge des Eingabealphabets folgende Schritte durchgeführt.
\begin{enumerate}
	\item Zeile 10-16: Zunächst wird aus der Übergangstabelle (siehe \reflst{code:434transitiontable}) das End-Zustandspaar für das aktuelle Eingabesymbol ermittelt. Sollte für ein Element des Paars bereits eine Zuordnung zu einem anderen Zustand existieren, so bricht der Algorithmus an dieser Stelle ab. Sonst werden die Zustände des Paars einander zugeordnet, falls dies nicht bereits geschehen ist. 
	\item Zeile 17-21: Beim Durchlaufen des Algorithmus wird eine Liste aller in einem Pfad bereits überprüften Zustände des ersten Automaten geführt. Wurde in einem Pfad (also einer Reihe aufeinanderfolgender Übergänge) ein Zustand bereits überprüft, so handelt es sich um eine Ringstruktur und weitere Überprüfungen sind in diesem Pfad nicht notwendig. Sonst wird das Verfahren beginnend von dem Endzustand aus wiederholt (Z.19f.).
	Um sicher zu stellen, dass diese Liste nicht durch Überprüfungen in parallel durchlaufenen Pfaden verändert wird, muss für den nächsten rekursiven Aufruf der Methode eine Kopie mitgegeben werden (Z.19).  
	\item Zeile 22: Das Verfahren endet dann erfolgreich, wenn alle Zustände jedes Pfades überprüft wurden, ohne einen Konflikt zu erzeugen.
\end{enumerate}
\clearpage
\input{./code/61Rekursiv.tex}

\section{Vergleich der Sprache durch Wortgenerierung}\label{sec:62Wortgenerierung}
Zwei Automaten S und T werden ununterscheidbar genannt, wenn es \enquote{zu jedem Zustand $q_i$ von S und zu jedem Experiment [...] einen Zustand $q_j$ von T derart [gibt], dass das Experiment beginnend mit S im Zustand $q_i$ das gleiche Resultat liefert, wie wenn es mit T im Zustand $q_j$ begonnen wird} \cite[160]{Moore.1974}.
Entsprechendes muss zusätzlich für $q_j$ und T gelten. 

\noindent Dieser Satz, welchen Edward Moore in seinem Beitrag \citetitle{Moore.1974} formulierte, dient im Folgenden als Inspiration für einen Vergleich beliebiger Endlicher Automaten unabhängig von ihrer deterministischen Eigenschaft. 
\subsubsection*{Grundidee}
Die Idee hinter dem folgenden Algorithmus besteht darin, nicht die Struktur oder Übergänge zweier Automaten zu vergleichen, sondern die akzeptierte Sprache, welche für eNEAs, NEAs, DEAs und minimierte DEAs übergreifend übereinstimmen kann. 
Um eine systematische Überprüfung der Sprache zweier Endlicher Automaten zu ermöglichen, müssen zunächst alle zu überprüfenden Wörter, die in einer Sprache enthalten sind, ermittelt werden. Dies geschieht, wie auch Moore fordert, für beide Automaten. Anschließend wird überprüft, ob beide Automaten alle so generierten Worte der Sprache des jeweils anderen Automaten akzeptieren. Ein Wort, welches nur von einem der beiden Automaten akzeptiert wird, wird als \definitionCallout{Zeuge} (engl. witness) bezeichnet. Werden alle Worte akzeptiert, bleiben also keine Zeugen übrig, so sind beide Sprachen äquivalent.
\subsubsection*{Vergleich der Sprache zweier Endlicher Automaten}
\reflst{code:62SprachPrüfen} stellt diesen Ablauf dar. Bei Bedarf können die Zeugen zu didaktischen Zwecken ausgelesen werden. Einem Anwender könnte dies helfen, um exemplarisch nachzuvollziehen, warum zwei Automaten nicht dieselbe Sprache akzeptieren. 
\input{./code/62Sprachprüfung.tex}

\noindent Im Folgenden soll auf die Umsetzung der Wortgenerierung und -Überprüfung eingegangen werden.
\subsubsection*{Generierung von Eingabefolgen für einen Endlichen Automaten}
In \reflst{code:62WortErzeugen} werden all die Variablen initialisiert, die im Anschluss im eigentlichen rekursiven Algorithmus verwendet und stetig erweitert werden. Die Liste $folge$ repräsentiert die in einem Pfad an durchlaufenen Übergängen verwendeten Eingabesymbole. Um sicher zu gehen, dass diese Liste nicht durch Überprüfungen in parallel durchlaufenen Pfaden verändert wird, muss diese für jede Verzweigung eines Pfads kopiert und neu mitgegeben werden. In dem Set $worte$ werden alle dabei durch den Endlichen Automaten akzeptierten Eingabefolgen gespeichert und in der Liste $zustands-kette$ alle besuchten Zustände eines Pfades festgehalten. Auch hier muss darauf geachtet werden, dass für jeden Pfad eine eigene Version dieser Liste geführt wird. 
\input{./code/62Wortgenerierung.tex}

\noindent \reflst{code:62WorteErzeugenRek} zeigt den Pseudocode für die rekursive Erweiterung der betrachteten Eingabefolge. Der Algorithmus unterteilt sich in folgende Teilschritte 
\begin{enumerate}
	\item Zeile 5-8: Um den Aufwand gerade bei großen Automaten einzugrenzen, ist es empfehlenswert, die Länge der Eingabefolge einzuschränken. Gleiches gilt für die Anzahl der Male, die ein Zustand innerhalb eines Pfades besucht werden darf. Auch wenn es sinnvoll sein kann, Kreisstrukturen oder Schleifen innerhalb eines Pfades mehrmals zu durchlaufen, sollte dies eingeschränkt werden, um Endlosschleifen zu vermeiden. Die dazu benötigten Werte können in $maximal-l\ddot ange$ und $maximal-wiederholungen$ festgelegt werden. Diese Einschränkungen sorgen zwangsläufig dafür, dass das Verfahren nicht immer zuverlässig funktioniert. Ein Beispiel dafür wird in \ref{exmp:62wortgenerierung} erläutert.
	\item Zeilen 9f.: Hier erfolgt die bereits angekündigte Duplizierung der $zustands-kette$. Der aktuelle Zustand $q_i$ wird ans Ende angehängt.
	\item Zeilen 11-13: Falls $q_i$ ein akzeptierender Zustand ist, wird auch das Wort, welches sich aus den aktuellen Elementen in der Eingabefolge $folge$ ergibt, von dem Automaten akzeptiert. Das Set $worte$ wird um dieses $wort$ ergänzt.
	\item Zeilen 14-19: Für alle Eingabesymbole des Alphabets und den aktuellen Zustand $q_i$ wird aus der Übergangstabelle die Liste von möglichen Endzuständen ermittelt. Ausgehend von diesen Zuständen wird der Algorithmus im Anschluss ein weiteres Mal durchlaufen. Die aktuelle Eingabefolge $folge$ wird dafür dupliziert und um das Eingabesymbol erweitert. An dieser Stelle sei darauf hingewiesen, dass das Alphabet hier auch Sequenzen und $\epsilon$ enthalten kann.  
	\item Der Algorithmus bricht für einen Pfad ab, wenn eine der Abbruchbedingungen in Zeile 5-8 erfüllt ist oder für einen Ausgangszustand und ein Eingabesymbol keine Endzustände definiert sind (Z.16).
\end{enumerate}
\input{./code/62WortgenerierungRek.tex}
\subsubsection*{Prüfung der Übernahme von Eingabefolgen durch einen Endlichen Automaten}
Der zweite große Teil des Algorithmus zum Vergleich der Sprache zweier Automaten besteht in der Prüfung der zuvor generierten Worte. Der Pseudocode dafür ist in \reflst{code:62WortPrüfen} zu sehen. In dem Set $zeugen$ sollen all die Eingabefolgen des ersten Automaten festgehalten werden, die von dem zweiten Automaten nicht akzeptiert werden (Z.11). Der rekursive Überprüfungsalgorithmus wird vom Startzustand aus beginnend für jedes Wort einzeln durchgeführt. 
\clearpage
\input{./code/62Wortprüfung.tex}

\noindent Das Vorgehen für die rekursive Wortprüfung in \reflst{code:62WortPrüfenRek} untergliedert sich in folgende Schritte:
\begin{enumerate}
	\item \label{62rekwort1} Zeile 5-8: Beim Durchlaufen der Eingabefolge wird das aktuell betrachtete Eingabesymbol durch den Integer $position$ angegeben. Entspricht dieser Wert der Wortlänge, so muss überprüft werden, ob der aktuelle Zustand $q_i$ das Wort akzeptiert. Diese letzte Prüfung wird in \reflst{code:62WortPrüfenZuletzt} durchgeführt.
	\item Zeile 9-13: In jedem Durchlauf werden alle Elemente des Alphabets $A$ mit der aktuellen Position im Wort abgeglichen. Stimmt das einzelne Symbol oder die Sequenz $a$ mit den nächsten Elementen aus $wort$ überein oder gilt $\epsilon \in A$ , so werden alle zugehörigen Endzustände aus der Übergangstabelle ermittelt. Jeder Endzustand $q_{e,a}$ wird im Anschluss einzeln untersucht. 
	\item \label{62rekwort3} Zeile 14-19: Für den Fall, dass es sich bei $a$ um $\epsilon$ handelt, wird der Endzustand $q_{e,a}$ erreicht, ohne dass sich $position$ ändert. Das bedeutet, dass zufällig in einen anderen Zustand gewechselt wird, ohne das nächste Eingabesymbol in $wort$ zu überprüfen. Dies ist zwar prinzipiell erlaubt, kann jedoch bei aus $\epsilon$-Übergängen bestehenden Kreisstrukturen zu einer Endlosschleife führen. Um das zu verhindern werden in der Liste $epsilon-kette$ alle Zustände vermerkt, die aufeinanderfolgend mit $epsilon$ erreicht wurden. Sollte diese Liste den Zustand $q_{e,a}$ bereits enthalten, so handelt es sich um eine solche Kreisstruktur und die Überprüfung wird für das nächste Element aus $A$ fortgesetzt (Z.16f.).
	\item Zeilen 10-22: Ist dies nicht der Fall, so wird die Überprüfung von dem Endzustand aus beginnend für das nächste Element in $wort$ durchgeführt.
	\item Der Algorithmus hält an, wenn $wort$ am Ende eines Pfades akzeptiert wurde. Gilt für jeden Pfad, dass er entweder nicht in einem akzeptierenden Zustand endet (Z.8) oder für einen Ausgangszustand $q_i$ keine passenden Übergänge definiert sind (Z.23), so wird das $wort$ nicht von dem Automaten akzeptiert.
\end{enumerate}
\input{./code/62WortprüfungRek.tex}

\noindent In \hyperref[62rekwort1]{Schritt 1} der Beschreibung für \reflst{code:62WortPrüfenRek} wird bereits darauf hingewiesen, dass der letzte Zustand eines durchlaufenen Pfades akzeptierend sein muss, damit das untersuchte Wort Teil der Sprache des Automaten ist. Dieser letzte Überprüfungsschritt ist in \reflst{code:62WortPrüfenZuletzt} dargestellt. Ähnlich wie in \hyperref[62rekwort3]{Schritt 3} kann es auch bei dieser letzten Überprüfung dazu kommen, dass der akzeptierende Zustand über einen oder mehrere Epsilon-Übergänge erreicht wird. Neben der Prüfung, ob der aktuelle Zustand $q_i$ akzeptierend ist (Z.6f.), werden daher auch alle Zustände überprüft, die durch solche Übergänge erreicht werden können (Z.11-16). Auch hier sollten durch Kreisstrukturen erzeugte Endlosschleifen vermieden werden (Z.12-14).
\input{./code/62WortprüfungZuletzt.tex}

\noindent Die in \reflst{code:62WortPrüfenRek} beschriebene Wortprüfung kann auch als separates Verfahren vom Anwender verwendet werden um exemplarisch zu prüfen, ob ein beliebiges Wort von einem betrachteten Automaten akzeptiert wird.
\newpassage
\begin{exmp}{62wortprüfen}
	\reffig{fig:62NEA1} zeigt eine solche Überprüfung beispielhaft für das Wort $w=ab$. In \reffig{fig:62NEA1a} ist der erste Schritt des Verfahrens für $w[0]=a$ zu sehen. Da für $\delta(q_1,b)$ keine Endzustände definiert sind scheitert der Pfad $\{q_0,q_1\}$. Ausgehend von dem Zustand $q_2$ können durch das Einlesen von $b$ zwei mögliche Endzustände erreicht werden. Der Pfad $\{q_0,q_2,q_2\}$ scheitert, da $q_2$ kein akzeptierender Zustand ist. Mit dem Pfad $\{q_0,q_2,q_3\}$ wird das Wort schließlich akzeptiert und es gilt $ab \in L(N)$.
	\input{./images/62Wortprüfung.tex}
\end{exmp}
 
\subsubsection{Anmerkungen zum Scheitern des Verfahrens}
Die Überprüfung der Sprache zweier Automaten ist ein Verfahren, welches Endliche Automaten unabhängig davon überprüfen kann, ob sie deterministisch oder nichtdeterministisch sind, epsilon-Übergänge besitzen oder Sequenzen. Darin unterscheidet es sich von den anderen beiden in diesem Kapitel beschriebenen Verfahren. Der große Nachteil liegt jedoch darin, dass mit diesem Verfahren nicht für alle beliebigen Automaten zuverlässig  eine Aussage darüber getroffen werden kann, ob sie dieselbe Sprache akzeptieren. Durch die in \reflst{code:62WorteErzeugenRek} eingebauten Einschränkungen der Wortlänge und Zustandswiederholungen können nicht beliebig viele Worte generiert werden, was für fehlerhafte Ergebnisse bei der Überprüfung sorgen kann.
\newpassage
\begin{exmp}{62wortgenerierung}
	In \reffig{fig:62GenerationProblem1} ist ein Beispiel für zwei Automaten mit wenigen Zuständen zu sehen, für welche eine Überprüfung der Sprache mit einer kleinen Anzahl erlaubter Zustandswiederholungen fehlschlägt. Für den NEA in \reffig{fig:62GenerationProblem1a} können insgesamt 15 Worte
	\begin{align*}
		w=\{\epsilon,a,b,aa,ab,ba,bb,aaa,aab,aba,abb,baa,bab,bba,bbb\}
	\end{align*} generiert werden, welche alle Teil der Sprache des Automaten sind. Alle werden von dem DEA in \reffig{fig:62GenerationProblem1b} akzeptiert. Bei der umgekehrten Prüfung der Worte des DEAs kann in Abhängigkeit von dem Parameter $maximal-wiederholungen$ aufgrund der Schleife $\delta(q_0,a)=\delta(q_0,b)=q_0$ eine unterschiedliche Anzahl Worte generiert werden. Erst bei fünfmaligem Aufruf des Zustands $q_0$ wird ein Wort der Länge 4 generiert, welches von dem NEA nicht mehr akzeptiert wird.  
	\input{./images/62GenerationProblem1.tex}
\end{exmp}
\newpassage
\noindent Ein weiteres Problem ist die Menge generierter Worte bei großen Automaten. In dieser Arbeit werden lediglich Automaten mit maximal 10 Zuständen betrachtet, bei denen sich die Anzahl der Übergänge, Kreisstrukturen und Schleifen in Grenzen hält. Bei größeren Automaten muss jedoch mit einem deutlich höheren Aufwand gerechnet werden, der mit der erlaubten Wortlänge und Anzahl Zustandswiederholungen in einem Pfad noch steigt. Bei der Wahl der Parameter $maximal-wiederholungen$ und $wortl\ddot ange$ muss daher ein Kompromiss zwischen Performance und Korrektheit des Verfahrens eingegangen werden. In dieser Arbeit wurden die Werte $wortl\ddot ange=25$ und $maximal-wiederholungen=2$ gewählt, wodurch zumindest zweifache Schleifendurchläufe erlaubt sind. 

\noindent Die Methoden der Wortgenerierung und Wortüberprüfung für beliebige Automaten sind für sich genommen nützlich, um Anwendern den Zusammenhang zwischen einem Automaten und der Sprache, welche er akzeptiert, verständlich zu machen. Insgesamt ist das vorgestellte Verfahren jedoch nicht zuverlässig nutzbar. 

\noindent Unabhängig davon gilt, dass für zwei beliebige Endliche Automaten bestimmt werden kann, ob sie dieselbe Sprache akzeptieren. \citeauthor{RaywardSmith.1995} nennt dies das \enquote{Äquivalenzproblem} regulärer Sprachen und liefert dazu folgenden Beweis \cite[99]{RaywardSmith.1995}:\\
\noindent Für zwei beliebige reguläre Grammatiken $G_1, G_2$ gilt:
\begin{center}
	$L(G_1) = L(G_2)$\\ 
	$\Leftrightarrow$ \\
	Die zugehörigen minimalen DEAs sind bis auf ihre Benennung identisch.
\end{center} 
Die Umwandlung eines beliebige NEA in einen zugehörigen DEA wurde bereits in Kapitel \ref{chap:4Umwandlung} erklärt. Für den Vergleich zweier beliebiger DEAs mittels Minimierung wird im folgenden Abschnitt ein Verfahren eingeführt.

\section{Vergleich zweier DEAs durch Minimierung}\label{sec:63Minimierung}
\subsubsection{Grundidee}
Der in Abschnitt \ref{sec:54PredecessorTableFilling} vorgestellte Algorithmus kann nicht nur zur Minimierung eines einzelnen Automaten, sondern auch zum Vergleich von zwei beliebigen DEAs verwendet werden. Dazu werden die Zustandsmengen beider Automaten und die jeweiligen Übergangsfunktionen zusammengefügt. Dabei wird vorausgesetzt, dass die Zustände beider Automaten in ihrer Benennung unterscheidbar sind. Zudem muss das Alphabet beider Automaten identisch sein. Nach der Minimierung wird geprüft, ob das Zustandspaar der beiden Startzustände als unterscheidbar markiert worden ist. Falls die Startzustände nicht unterscheidbar sind, akzeptieren die beiden Automaten dieselbe Sprache und sind somit äquivalent \cite[19]{Norton.2009}.
\subsubsection{Implementierung}
Bei der Implementierung des Algorithmus \ref{code:63tfmv} werden zunächst die Mengen der Zustände und die Übergangsfunktionen vereinigt (Z.6f.). Darauffolgend wird die Minimierung mithilfe der bereits beschriebenen Algorithmen \ref{code:543predecessors}, \ref{code:543equivTab} und \ref{code:543equivTabResult} durchgeführt, um die Äquivalenztabelle der zusammengefügten Zustandsmenge der beiden Automaten zu berechnen (Z.8ff.). In den Zeilen 11-15 wird geprüft, ob das Zustandspaar der Startzustände unterscheidbar ist.
\input{./code/63tfm.tex}