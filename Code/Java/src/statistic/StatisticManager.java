package statistic;

import java.util.List;
import java.util.Map;

import commons.Automaton;
import commons.EffortClass;
import comparison.DirectCompareHelper;
import comparison.TableFillingAutomatonCompareHelper;
import comparison.WordGenerationCompareHelper;
import generation.AutomatonGenerator;
import generation.AutomatonMatrix;
import generation.ExerciseGenerator;
import minimization.IncrementalMinimizer;
import minimization.MooreMinimizer;
import minimization.TableFillingMinimizer;
import statistic.db.DBController;
import statistic.db.DBHelper;
import statistic.structure.IsomorphismEquivalenceClass;
import statistic.structure.SimpleMatrix;
import transformation.Transformer;

public class StatisticManager {
	private int generationCount = 500;
	private DBController controller = DBController.getInstance();

	public void generateEntries() {
		AutomatonGenerator generator = new AutomatonGenerator();
		for (int i = 0; i < generationCount; i++) {
			generateTableFillingMooreMinimizationAlgorithmTestEntry(generator);
			generateTransformationTestEntry(generator);
			generateAllMinimizationAlgorithmsTestEntry(generator);
			generateDEAGenerationEntry(generator, false);
			generateNEAGenerationEntry(generator);
			generateDEAIsomorphismEntry(generator, false);
			generateNEAIsomorphismEntry(generator);
			generateEffortEntry(generator);
			if (i % 100 == 0) {
				System.out.println("Generierte Nummer: " + i);
			}
		}
		System.out.println("======================DONE======================");
	}

	private void generateTransformationTestEntry(AutomatonGenerator generator) {
		Automaton nea = generator.generateAutomatonWithBitstreams();
		Transformer transformer = new Transformer(nea);
		Automaton dea = transformer.getFinalDEA();

		AutomatonMatrix am = dea.getAutomatonMatrix();

		String witness = "";

		boolean isIdentical = false;
		if (am.isAlmostDea() && am.getStates().size() * dea.getAlphabet().size() == dea.getNumberTerminalOccurences()) {
			Map<Automaton, Map<Boolean, List<String>>> witnessMap = WordGenerationCompareHelper
					.getFalseStringsForNEAs(nea, dea, true, true, 1);
			isIdentical = witnessMap.isEmpty();
			if (witnessMap.containsKey(nea)) {
				witness = witnessMap.get(nea).get(Boolean.TRUE).get(0);
			}
			if (witnessMap.containsKey(dea)) {
				witness = witness + " " + witnessMap.get(dea).get(Boolean.TRUE).get(0);
			}
		}

		DBHelper.addTransformationTest(controller, nea.toString(), dea.toString(), isIdentical, witness);
	}

	private void generateDEAGenerationEntry(AutomatonGenerator generator, boolean withMatrix) {
		Automaton dea;
		int neaID = -1;
		if (withMatrix) {
			dea = generator.generateAutomatonWithMatrix();
		} else {
			Automaton nea = generator.generateAutomatonWithBitstreams(0, 0, 0);
			dea = new Transformer(nea).getRenamedFinalDEA();

			AutomatonMatrix am = nea.getAutomatonMatrix();
			SimpleMatrix sm = new SimpleMatrix(am);
			neaID = DBHelper.addNEA(controller, nea.toString(), nea.getStates().size(),
					nea.getNumberTerminalOccurences(), nea.getTransitions().size(), nea.getAccepting().size(),
					am.getSingleCount(), am.getSequencesCount(), am.getEpsilonCount(), sm.getLoopNumber(),
					sm.getLongestDistanceWithoutLoops(), am.isAlmostDea());
		}
		AutomatonMatrix am = dea.getAutomatonMatrix();
		SimpleMatrix sm = new SimpleMatrix(am);
		Automaton tfm = new TableFillingMinimizer(dea).minimize();
		boolean isMinimal = tfm.getStates().size() == dea.getStates().size();
		int deaID = DBHelper.addDEA(controller, dea.toString(), dea.getStates().size(),
				dea.getNumberTerminalOccurences(), dea.getTransitions().size(), dea.getAccepting().size(),
				sm.getLoopNumber(), sm.getLongestDistanceWithoutLoops(), isMinimal);

		MooreMinimizer moore = new MooreMinimizer(dea);
		Automaton minimizedDea = moore.minimize();
		AutomatonMatrix am2 = minimizedDea.getAutomatonMatrix();
		SimpleMatrix sm2 = new SimpleMatrix(am2);
		int minDeaID = DBHelper.addDEA(controller, minimizedDea.toString(), minimizedDea.getStates().size(),
				minimizedDea.getNumberTerminalOccurences(), minimizedDea.getTransitions().size(),
				minimizedDea.getAccepting().size(), sm2.getLoopNumber(), sm2.getLongestDistanceWithoutLoops(), true);

		int effort = moore.getEffort();

		DBHelper.addDEAGeneration(controller, neaID, deaID, minDeaID, effort);
	}

	private void generateNEAGenerationEntry(AutomatonGenerator generator) {
		Automaton eNea = generator.generateAutomatonWithBitstreams(0, 0, 0);
		Transformer t = new Transformer(eNea);
		Automaton nea = t.getRemovedEpsilonNEA();
		Automaton dea = t.getRenamedFinalDEA();

		AutomatonMatrix am = eNea.getAutomatonMatrix();
		SimpleMatrix sm = new SimpleMatrix(am);
		int eNeaID = DBHelper.addNEA(controller, eNea.toString(), eNea.getStates().size(),
				eNea.getNumberTerminalOccurences(), eNea.getTransitions().size(), eNea.getAccepting().size(),
				am.getSingleCount(), am.getSequencesCount(), am.getEpsilonCount(), sm.getLoopNumber(),
				sm.getLongestDistanceWithoutLoops(), am.isAlmostDea());
		AutomatonMatrix am2 = nea.getAutomatonMatrix();
		SimpleMatrix sm2 = new SimpleMatrix(am2);
		int neaID = DBHelper.addNEA(controller, nea.toString(), nea.getStates().size(),
				nea.getNumberTerminalOccurences(), nea.getTransitions().size(), nea.getAccepting().size(),
				am2.getSingleCount(), am2.getSequencesCount(), am2.getEpsilonCount(), sm2.getLoopNumber(),
				sm2.getLongestDistanceWithoutLoops(), am2.isAlmostDea());

		AutomatonMatrix am3 = dea.getAutomatonMatrix();
		SimpleMatrix sm3 = new SimpleMatrix(am3);
		Automaton tfm = new TableFillingMinimizer(dea).minimize();
		boolean isMinimal = tfm.getStates().size() == dea.getStates().size();
		int deaID = DBHelper.addDEA(controller, dea.toString(), dea.getStates().size(),
				dea.getNumberTerminalOccurences(), dea.getTransitions().size(), dea.getAccepting().size(),
				sm3.getLoopNumber(), sm3.getLongestDistanceWithoutLoops(), isMinimal);

		int sequenceEffort = t.getEffortForSequenceTransformation();
		int epsilonEffort = t.getEffortForEpsilonTransformation();
		int subsetEffort = t.getEffortForSubsetConstructionTransformation();
		DBHelper.addNEAGeneration(controller, eNeaID, neaID, deaID, sequenceEffort, epsilonEffort, subsetEffort);

	}

	private void generateDEAIsomorphismEntry(AutomatonGenerator generator, boolean withMatrix) {
		Automaton dea;
		if (withMatrix) {
			dea = generator.generateAutomatonWithMatrix();
		} else {
			Transformer t = new Transformer(generator.generateAutomatonWithBitstreams(0, 0, 0));
			dea = t.getRenamedFinalDEA();
		}
		AutomatonMatrix am = dea.getAutomatonMatrix();
		SimpleMatrix sm = new SimpleMatrix(am);
		Automaton tfm = new TableFillingMinimizer(dea).minimize();
		boolean isMinimal = tfm.getStates().size() == dea.getStates().size();
		int deaID = DBHelper.addDEA(controller, dea.toString(), dea.getStates().size(),
				dea.getNumberTerminalOccurences(), dea.getTransitions().size(), dea.getAccepting().size(),
				sm.getLoopNumber(), sm.getLongestDistanceWithoutLoops(), isMinimal);

		IsomorphismEquivalenceClass iso = new IsomorphismEquivalenceClass(sm);

		int matrixID = DBHelper.addStructureMatrix(controller, sm.getStructureMatrix(), sm.getToTransitions(),
				sm.getFromTransitions(), sm.getLoopTransitions());
		Map<Integer, Integer> eqMap = DBHelper.getPossibleEquivalenceClasses(controller, sm.getToTransitions(),
				sm.getFromTransitions(), sm.getLoopTransitions());
		boolean foundIso = false;
		if (!eqMap.isEmpty()) {
			for (int eqID : eqMap.keySet()) {
				int emID = eqMap.get(eqID);
				int[][] matrix = DBHelper.getMatrix(controller, emID);
				if (matrix != null && iso.containsWithPreconditionsChecked(matrix)) {
					DBHelper.addDEAEquivalenceClass(controller, matrixID, eqID, deaID);
					foundIso = true;
					break;
				}
			}
		}
		if (!foundIso) {
			int eqID = DBHelper.addEquivalence(controller, matrixID);
			DBHelper.addDEAEquivalenceClass(controller, matrixID, eqID, deaID);

		}
	}

	private void generateNEAIsomorphismEntry(AutomatonGenerator generator) {
		Automaton nea = generator.generateAutomatonWithBitstreams();
		AutomatonMatrix am = nea.getAutomatonMatrix();
		SimpleMatrix sm = new SimpleMatrix(am);
		int neaID = DBHelper.addNEA(controller, nea.toString(), nea.getStates().size(),
				nea.getNumberTerminalOccurences(), nea.getTransitions().size(), nea.getAccepting().size(),
				am.getSingleCount(), am.getSequencesCount(), am.getEpsilonCount(), sm.getLoopNumber(),
				sm.getLongestDistanceWithoutLoops(), am.isAlmostDea());

		IsomorphismEquivalenceClass iso = new IsomorphismEquivalenceClass(sm);

		int matrixID = DBHelper.addStructureMatrix(controller, sm.getStructureMatrix(), sm.getToTransitions(),
				sm.getFromTransitions(), sm.getLoopTransitions());
		Map<Integer, Integer> eqMap = DBHelper.getPossibleEquivalenceClasses(controller, sm.getToTransitions(),
				sm.getFromTransitions(), sm.getLoopTransitions());
		boolean foundIso = false;
		if (!eqMap.isEmpty()) {
			for (int eqID : eqMap.keySet()) {
				int emID = eqMap.get(eqID);
				int[][] matrix = DBHelper.getMatrix(controller, emID);
				if (matrix != null && iso.containsWithPreconditionsChecked(matrix)) {
					DBHelper.addNEAEquivalenceClass(controller, matrixID, eqID, neaID);
					foundIso = true;
					break;
				}
			}
		}
		if (!foundIso) {
			int eqID = DBHelper.addEquivalence(controller, matrixID);
			DBHelper.addNEAEquivalenceClass(controller, matrixID, eqID, neaID);
		}
	}

	private void generateAllMinimizationAlgorithmsTestEntry(AutomatonGenerator generator) {
		Automaton dea = generator.generateAutomatonWithMatrix();

		Automaton tfm = new TableFillingMinimizer(dea).minimize();
		Automaton moore = new MooreMinimizer(dea).minimize();
		Automaton incremental = new IncrementalMinimizer(dea).minimize();

		boolean tfmMooreDirectEquivalent = DirectCompareHelper.areSameAutomaton(tfm, moore);
		boolean tfmIncrementalDirectEquivalent = DirectCompareHelper.areSameAutomaton(tfm, incremental);
		boolean incrementalMooreDirectEquivalent = DirectCompareHelper.areSameAutomaton(incremental, moore);

		boolean tfmWordGenerationEquivalent = WordGenerationCompareHelper.acceptsSameLanguage(dea, tfm, false, 1);
		boolean mooreWordGenerationEquivalent = WordGenerationCompareHelper.acceptsSameLanguage(dea, moore, false, 1);
		boolean incrementalWordGenerationEquivalent = WordGenerationCompareHelper.acceptsSameLanguage(dea, incremental,
				false, 1);

		boolean tfmTFMEquivalent = TableFillingAutomatonCompareHelper.compareDEA(dea, tfm);
		boolean mooreTFMEquivalent = TableFillingAutomatonCompareHelper.compareDEA(dea, moore);
		boolean incrementalTFMEquivalent = TableFillingAutomatonCompareHelper.compareDEA(dea, incremental);

		DBHelper.addMinimizationAlgorithmTest(controller, dea.toString(), tfm.toString(), moore.toString(),
				incremental.toString(), tfmMooreDirectEquivalent, tfmIncrementalDirectEquivalent,
				incrementalMooreDirectEquivalent, tfmWordGenerationEquivalent, mooreWordGenerationEquivalent,
				incrementalWordGenerationEquivalent, tfmTFMEquivalent, mooreTFMEquivalent, incrementalTFMEquivalent);
	}

	private void generateEffortEntry(AutomatonGenerator generator) {
		ExerciseGenerator.generateAutomatonForMinimizationWithDefault(EffortClass.MEDIUM);
		Automaton dea = generator.generateAutomatonWithBitstreams(0, 0, 0);
		TableFillingMinimizer tfm = new TableFillingMinimizer(dea);
		Automaton minimized = tfm.minimize();

		AutomatonMatrix am = dea.getAutomatonMatrix();
		SimpleMatrix sm = new SimpleMatrix(am);
		int deaID = DBHelper.addDEA(controller, dea.toString(), dea.getStates().size(),
				dea.getNumberTerminalOccurences(), dea.getTransitions().size(), dea.getAccepting().size(),
				sm.getLoopNumber(), sm.getLongestDistanceWithoutLoops(),
				minimized.getStates().size() == dea.getStates().size());

		AutomatonMatrix am2 = minimized.getAutomatonMatrix();
		SimpleMatrix sm2 = new SimpleMatrix(am2);
		int minimizedDeaID = DBHelper.addDEA(controller, minimized.toString(), minimized.getStates().size(),
				minimized.getNumberTerminalOccurences(), minimized.getTransitions().size(),
				minimized.getAccepting().size(), sm2.getLoopNumber(), sm2.getLongestDistanceWithoutLoops(), true);

		int tfmCC = tfm.getEffort();
		int tfmPrecalculatedCC = tfm.getPreCalculatePredecessorsEffort();

		MooreMinimizer moore = new MooreMinimizer(dea);
		int mooreCC = moore.getEffort();
		int mooreRedundancyCC = moore.getEffortWithRedundancy();

		DBHelper.addEffortEntry(controller, deaID, minimizedDeaID, tfmCC, tfmPrecalculatedCC, mooreCC,
				mooreRedundancyCC);
	}

	private void generateTableFillingMooreMinimizationAlgorithmTestEntry(AutomatonGenerator generator) {
		boolean wasMinimal = true;
		while (wasMinimal) {
			Automaton dea = generator.generateAutomatonWithMatrix();

			Automaton tfm1 = new TableFillingMinimizer(dea).minimize();
			wasMinimal = dea.getStates().size() == tfm1.getStates().size();
			if (!wasMinimal) {
				Automaton tfm2 = new TableFillingMinimizer(dea).minimize();
				boolean tfmEquivalent = DirectCompareHelper.areSameAutomaton(tfm1, tfm2);

				Automaton moore1 = new MooreMinimizer(dea, true).minimize();
				Automaton moore2 = new MooreMinimizer(dea, false).minimize();
				boolean mooreEquivalent = DirectCompareHelper.areSameAutomaton(moore1, moore2);

				boolean tfmMooreEquivalent = DirectCompareHelper.areSameAutomaton(moore1, tfm1);

				DBHelper.addTFMMooreMinimizationTest(controller, dea.toString(), tfm1.toString(), tfm2.toString(),
						moore1.toString(), moore2.toString(), tfmEquivalent, mooreEquivalent, tfmMooreEquivalent,
						wasMinimal);
			}
		}
	}

	private void deleteAllEntries() {
		DBHelper.deleteAllTableEntries(controller);
	}

	private void createNEAIsomorphismView() {
		DBHelper.createNEAIsomorphismView(controller);
	}

	private void createDEAIsomorphismView() {
		DBHelper.createDEAIsomorphismView(controller);
	}

	private void createNEAGenerationView() {
		DBHelper.createNEAGenerationView(controller);
	}

	private void createDEAGenerationWithNEAView() {
		DBHelper.createDEAGenerationWithNEAView(controller);
	}

	private void createDEAGenerationWithoutNEAView() {
		DBHelper.createDEAGenerationWithoutNEAView(controller);
	}

	private void createMinimizationEffortView() {
		DBHelper.createMinimizationEffortView(controller);
	}

	private void resetViews() {
		DBHelper.resetTableViews(controller);
		DBHelper.createNEAIsomorphismView(controller);
		DBHelper.createDEAIsomorphismView(controller);
		DBHelper.createNEAGenerationView(controller);
		DBHelper.createDEAGenerationWithNEAView(controller);
		DBHelper.createDEAGenerationWithoutNEAView(controller);
		DBHelper.createMinimizationEffortView(controller);
	}

	public static void main(String[] args) {
		StatisticManager statisticManager = new StatisticManager();

//		statisticManager.deleteAllEntries();
		statisticManager.resetViews();
//		statisticManager.generateEntries();
	}
}
