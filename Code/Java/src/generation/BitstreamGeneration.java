package generation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

import commons.Automaton;
import commons.State;
import commons.Terminal;
import commons.TerminalType;
import commons.Transition;

public class BitstreamGeneration {
	private List<State> states;
	private Set<State> statesWithOutGoingTransitions = new HashSet<State>();
	private Automaton automaton;
	private List<Terminal> terminals = new ArrayList<>();
	private Map<String, Transition> transitionMappingToStartAndEnd = new HashMap<>();
	private List<Integer> bitstream = new ArrayList<>();
	private Random rand = new Random();
	private boolean initialHasInGoingTransition = false;

	public BitstreamGeneration(List<State> states, List<Terminal> terminals, double probabilityTransitionExists,
			double probabilityEpsilonExists, int inputSequenceCount, int numberOfAcceptingStates,
			boolean endStateOfEpsilonIsAccepting, int maximumTransitions, int minimumTransitions, int maximumEpsilon,
			int minimumEpsilon, boolean useMinMaxTransitions) {
		if (minimumTransitions > maximumTransitions || minimumEpsilon > maximumEpsilon) {
			throw new IllegalArgumentException("The maximum number of transitions should be greater than the minimum!");
		}
		if (maximumEpsilon > states.size()) {
			throw new IllegalArgumentException(
					"The number of states should be greater than the maximum number of epsilon!");
		}
		this.states = states;
		this.terminals = terminals;
		initiallyConnectStates();
		generateBitstreamAndCreateTransitions(probabilityTransitionExists, maximumTransitions, minimumTransitions,
				useMinMaxTransitions);
		boolean hasEpsilon = false;
		if (maximumEpsilon >= minimumEpsilon && maximumEpsilon > 0 && minimumEpsilon > 0) {
			generateBitstreamForEpsilonAndCreateTransitions(probabilityEpsilonExists, maximumEpsilon, minimumEpsilon,
					useMinMaxTransitions);
			hasEpsilon = true;
		}
		if (inputSequenceCount > 0) {
			createInputSequenceForTransitions(inputSequenceCount);
		}
		List<Transition> transitions = transitionMappingToStartAndEnd.values().stream().collect(Collectors.toList());
		addAcceptingAttributeToStates(numberOfAcceptingStates, hasEpsilon, endStateOfEpsilonIsAccepting, transitions);
		this.automaton = new Automaton(states, transitions);
	}

	private void initiallyConnectStates() {
		State initialState = states.get(0);
		initialState.setInitial(true);
		List<State> connected = new ArrayList<>();
		connected.add(initialState);
		for (int i = 1; i < states.size(); i++) {
			Collections.shuffle(connected);
			State start = connected.get(0);
			State end = states.get(i);
			Transition transition = new Transition(getRandomTerminal(false), start, end, false);
			String name = start.getName() + ", " + end.getName();
			transitionMappingToStartAndEnd.put(name, transition);
			statesWithOutGoingTransitions.add(start);
			connected.add(end);
		}
	}

	private void generateBitstreamAndCreateTransitions(double probabilityTransitionExists, int maximumTransitions,
			int minimumTransitions, boolean useMinMaxTransitions) {
		int transitionCount = states.size() - 1;
		int probabilityTransitionInt = (int) (probabilityTransitionExists * 100.0);
		int length = states.size() * states.size() * terminals.size();
		int l = length - transitionCount;
		int min = minimumTransitions - transitionCount;
		int max = maximumTransitions - transitionCount;
		int nextInt = rand.nextInt((max - min + 1));
		int count1 = nextInt + min;

		if (useMinMaxTransitions) {
			for (int i = 0; i < l; i++) {
				if (i < count1) {
					bitstream.add(1);
				} else {
					bitstream.add(0);
				}
			}
			Collections.shuffle(bitstream);
		} else {
			for (int i = 0; i < length; i++) {
				if (rand.nextInt(100) + 1 < probabilityTransitionInt) {
					bitstream.add(1);
				} else {
					bitstream.add(0);
				}
			}
		}

		boolean transitionExsists = false;
		int counter = 0;
		for (int i = 0; i < terminals.size(); i++) {
			for (int j = 0; j < states.size(); j++) {
				for (int k = 0; k < states.size(); k++) {
					transitionExsists = false;
					Terminal terminal = terminals.get(i);
					State start = states.get(j);
					State end = states.get(k);
					String name = start.getName() + ", " + end.getName();
					Transition transition = transitionMappingToStartAndEnd.get(name);
					if (transition != null) {
						if (transition.getTerminals().contains(terminal)) {
							transitionExsists = true;
						}
					}
					if (bitstream.get(counter) == 1 && !transitionExsists) {
						if (end.isInitial()) {
							initialHasInGoingTransition = true;
						}
						Transition transitionForStates = transitionMappingToStartAndEnd.computeIfAbsent(name,
								key -> new Transition(getRandomTerminal(false), start, end, false));
						transitionForStates.addTerminal(terminal);
						if (!name.equals(start.getName() + ", " + start.getName())) {
							statesWithOutGoingTransitions.add(start);
						}
						counter++;
					} else {
						if (!transitionExsists) {
							counter++;
						}
					}
				}
			}
		}
	}

	private void generateBitstreamForEpsilonAndCreateTransitions(double probabilityEpsilonExists, int maximumEpsilon,
			int minimumEpsilon, boolean useMinMax) {
		Terminal epsilon = new Terminal(TerminalType.EPSILON, Terminal.epsilon);
		terminals.add(epsilon);

		if (maximumEpsilon > states.size()) {
			throw new IllegalArgumentException(
					"The number of epsilon transitions should not exceed the number of states!");
		}

		int probabiltiyEpsilonInt = (int) (probabilityEpsilonExists * 100.0);
		int count1 = rand.nextInt(maximumEpsilon - minimumEpsilon + 1) + minimumEpsilon;

		List<Integer> bitstreamForEpsilon = new ArrayList<>();
		if (useMinMax) {
			for (int i = 0; i < states.size(); i++) {
				if (i < count1) {
					bitstreamForEpsilon.add(1);
				} else {
					bitstreamForEpsilon.add(0);
				}
			}
			Collections.shuffle(bitstreamForEpsilon);
		} else {
			for (int i = 0; i < states.size(); i++) {
				if (rand.nextInt(100) + 1 < probabiltiyEpsilonInt) {
					bitstreamForEpsilon.add(1);
				} else {
					bitstreamForEpsilon.add(0);
				}
			}
		}

		boolean hasEpsilon = false;
		for (int i = 0; i < states.size(); i++) {
			State start = states.get(i);
			if (bitstreamForEpsilon.get(i) == 1) {
				if (statesWithOutGoingTransitions.contains(start)) {
					replaceTerminalsOfExistingTransitionWithEpsilon(epsilon, start);
				} else {
					addTransitionAndIfStartIsInitialCheckIfAnAdditionalTerminalIsNeeded(epsilon, start);
				}
				hasEpsilon = true;
			}
		}

		if (!hasEpsilon) {
			Collections.shuffle(states);
			State start = states.get(0);
			if (statesWithOutGoingTransitions.contains(start)) {
				replaceTerminalsOfExistingTransitionWithEpsilon(epsilon, start);
			} else {
				addTransitionAndIfStartIsInitialCheckIfAnAdditionalTerminalIsNeeded(epsilon, start);
			}
		}
	}

	private void addTransitionAndIfStartIsInitialCheckIfAnAdditionalTerminalIsNeeded(Terminal epsilon, State start) {
		State end = getRandomStateOfAllStatesWithoutState(start);
		String name = start.getName() + ", " + end.getName();
		if (!start.isInitial() || (start.isInitial() && initialHasInGoingTransition)) {
			Transition transitionForStates = transitionMappingToStartAndEnd.computeIfAbsent(name,
					key -> new Transition(epsilon, start, end, true));
			transitionForStates.addTerminal(epsilon);
		} else {
			Transition transitionForStates = transitionMappingToStartAndEnd.computeIfAbsent(name,
					key -> new Transition(epsilon, start, end, true));
			transitionForStates.addTerminal(epsilon);
			transitionForStates.addTerminal(getRandomTerminal(false));
		}
	}

	// method should only be called if start has transitions to other states, loop
	// transitions do not count
	private void replaceTerminalsOfExistingTransitionWithEpsilon(Terminal epsilon, State start) {
		boolean existingTransitionFound = false;
		State end;
		String name;
		int stateCounter = 0;
		Collections.shuffle(states);
		while (!existingTransitionFound && stateCounter < states.size()) {
			end = states.get(stateCounter);
			if (end != start) {
				name = start.getName() + ", " + end.getName();
				Transition transition = transitionMappingToStartAndEnd.get(name);
				if (transition != null) {
					if (!start.isInitial() || (start.isInitial() && initialHasInGoingTransition)) {
						transition.removeAllTerminals();
					}
					transition.addTerminal(epsilon);
					existingTransitionFound = true;
				}
			}
			stateCounter++;
		}
	}

	private void createInputSequenceForTransitions(int sequenceCount) {
		List<Transition> transitions = transitionMappingToStartAndEnd.values().stream().collect(Collectors.toList());
		for (int i = 0; i < sequenceCount; i++) {
			if (!transitions.isEmpty()) {
				boolean wasReplaced = replaceSingleTerminalWithSequence();
				if (!wasReplaced) {
					Collections.shuffle(states);
					State start = states.get(0);
					State end = getRandomStateOfAllStatesWithoutState(start);
					Terminal inputSequence = generateRandomTerminalSequence();
					String name = start.getName() + ", " + end.getName();
					Transition transitionForStates = transitionMappingToStartAndEnd.computeIfAbsent(name,
							key -> new Transition(inputSequence, start, end, false));
					transitionForStates.addTerminal(inputSequence);
				}
			} else {
				Collections.shuffle(states);
				State start = states.get(0);
				State end = getRandomStateOfAllStatesWithoutState(start);
				Terminal inputSequence = generateRandomTerminalSequence();
				String name = start.getName() + ", " + end.getName();
				Transition transitionForStates = transitionMappingToStartAndEnd.computeIfAbsent(name,
						key -> new Transition(inputSequence, start, end, false));
				transitionForStates.addTerminal(inputSequence);
			}
		}
	}

	private Terminal generateRandomTerminalSequence() {
		int numberOfPossibleTerminals = rand.nextInt(2) + 2;
		int counterTerminals = 0;
		char[] sequence = new char[numberOfPossibleTerminals];
		while (counterTerminals != numberOfPossibleTerminals) {
			Terminal randomTerminal = getRandomTerminal(false);
			if (randomTerminal.getType() == TerminalType.SINGLE) {
				sequence[counterTerminals] = randomTerminal.getTerminal()[0];
				counterTerminals++;
			}
		}
		return new Terminal(TerminalType.SEQUENCE, sequence);
	}

	private Terminal getRandomTerminal(boolean withEpsilon) {
		List<Terminal> terminalList = new ArrayList<>(terminals);
		if (!withEpsilon) {
			terminalList.remove(new Terminal(TerminalType.EPSILON, Terminal.epsilon));
		}
		return GenerationHelper.getRandomTerminal(terminalList, rand);
	}

	private boolean replaceSingleTerminalWithSequence() {
		boolean exsistingTerminalReplaced = false;
		List<Transition> transitions = transitionMappingToStartAndEnd.values().stream().collect(Collectors.toList());
		Transition transition;
		int transitionCounter = 0;
		while (!exsistingTerminalReplaced && transitionCounter < transitions.size()) {
			transition = transitions.get(transitionCounter);
			if (!transition.getEndState().equals(transition.getStartState())) {
				List<Terminal> terminals = new ArrayList<>(transition.getTerminals());
				boolean terminalWithTypeSingle = false;
				Terminal singleTerminal;
				int terminalCounter = 0;
				while (!terminalWithTypeSingle && terminalCounter < terminals.size()) {
					singleTerminal = terminals.get(terminalCounter);
					if (singleTerminal.getType() == TerminalType.SINGLE) {
						Terminal randomTerminalSequence = generateRandomTerminalSequence();
						transition.removeTerminal(singleTerminal);
						transition.addTerminal(randomTerminalSequence);
						terminalWithTypeSingle = true;
						exsistingTerminalReplaced = true;
					}
					terminalCounter++;
				}
			}
			transitionCounter++;
		}
		return exsistingTerminalReplaced;
	}

	private State getRandomStateOfAllStatesWithoutState(State notIncluded) {
		List<State> possibleStates = new ArrayList<>(states);
		possibleStates.remove(notIncluded);
		Collections.shuffle(possibleStates);
		return possibleStates.get(0);
	}

	private void addAcceptingAttributeToStates(int numberOfAcceptingStates, boolean hasEpsilonTransitions,
			boolean endStateOfEpsilonIsAccepting, List<Transition> transitions) {
		List<State> possibleStates = new ArrayList<>(states);
		int acceptingStates = rand.nextInt(numberOfAcceptingStates + 1);
		if (acceptingStates == 0) {
			acceptingStates++;
		}
		if (hasEpsilonTransitions && endStateOfEpsilonIsAccepting) {
			List<Transition> epsilonTransitions = transitions.stream().filter(t -> t.hasEpsilon())
					.collect(Collectors.toList());
			State endState = epsilonTransitions.get(0).getEndState();
			State startState = epsilonTransitions.get(0).getStartState();
			endState.setAccepting(true);
			possibleStates.remove(endState);
			possibleStates.remove(startState);
			if (acceptingStates == states.size()) {
				acceptingStates -= 2;
			} else {
				acceptingStates--;
			}
		}
		Collections.shuffle(possibleStates);
		for (int i = 0; i < acceptingStates; i++) {
			possibleStates.get(i).setAccepting(true);
		}
	}

	public Automaton getAutomaton() {
		return automaton;
	}
}
