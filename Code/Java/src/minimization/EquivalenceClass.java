package minimization;

import java.util.ArrayList;
import java.util.List;

import commons.State;

public class EquivalenceClass {
	private List<State> states = null;
	private State substituteState = null;

	public EquivalenceClass(List<State> states) {
		if (!states.isEmpty()) {
			this.states = states;
			substituteState = states.get(0);
		}
	}

	public EquivalenceClass(List<State> states, State substituteState) {
		this.states = states;
		this.substituteState = substituteState;
	}

	public EquivalenceClass(EquivalenceClass ec) {
		this.states = new ArrayList<>(ec.getStates());
		this.substituteState = ec.getSubstitute();
	}

	public List<State> getStates() {
		return states;
	}

	public State getSubstitute() {
		return substituteState;
	}

	public boolean containsState(State s) {
		return states.contains(s);
	}

	public void addStateIfNotExistent(State state) {
		if (states == null || states.isEmpty()) {
			states = new ArrayList<>();
			substituteState = state;
		}
		if (!states.contains(state))
			states.add(state);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("{");
		for (State s : states) {
			sb.append(s.toString() + ", ");
		}
		sb.deleteCharAt(sb.length() - 1);
		sb.deleteCharAt(sb.length() - 1);
		sb.append("}");
		return sb.toString();
	}
}
