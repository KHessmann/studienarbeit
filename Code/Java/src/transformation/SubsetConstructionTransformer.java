package transformation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import commons.Automaton;
import commons.State;
import commons.Terminal;
import commons.Transition;
import commons.TransitionTable;
import generation.AutomatonMatrix;
import helper.EmptyStateHelper;

public class SubsetConstructionTransformer {

	private Automaton automaton;
	private int effort;
	private State emptyState;

	public SubsetConstructionTransformer(Automaton automaton, boolean isAlmostDea) {
		if (isAlmostDea) {
			this.automaton = EmptyStateHelper.addEmptyStateToAutomaton(automaton);
			this.effort = automaton.getStates().size();
		} else {
			this.effort = 0;
			this.automaton = transform(automaton);
		}
	}

	public Automaton transform(Automaton a) {
		AutomatonMatrix am = a.getAutomatonMatrix();
		if (am.isAlmostDea()) {
			this.automaton = EmptyStateHelper.addEmptyStateToAutomaton(a);
			this.effort = a.getStates().size();
			return automaton;
		} else if (am.getEpsilonCount() > 0 || am.getSequencesCount() > 0) {
			throw new IllegalArgumentException(
					"the automaton can not contain epsilon transitions or sequences at this point!");
		} else {
			return transformNEA(a);
		}
	}

	private Automaton transformNEA(Automaton nea) {
		emptyState = null;
		State initial = nea.getInitial();
		if (initial == null) {
			throw new IllegalArgumentException("the initial state can't be null at this point!");
		} else {
			effort = 0;
			TransitionTable transitionTable = nea.getTransitionTable();
			int neaCount = getNumberOfStatesWithNEAProperty(nea, transitionTable);
			int stateCount = nea.getStates().size();
			effort += Math.min(stateCount, stateCount + 1 - neaCount);

			Map<List<State>, Map<Terminal, List<State>>> subsetTable = computeSubsetTable(transitionTable, initial,
					nea.getAlphabet());

			Automaton dea = new Automaton();
			addAlphabetToDEA(dea, nea);
			Map<List<State>, State> newStatesMap = addStatesToDEA(dea, subsetTable);
			addTransitionsToDEA(dea, subsetTable, newStatesMap);
			return dea;
		}
	}

	private int getNumberOfStatesWithNEAProperty(Automaton nea, TransitionTable transitionTable) {
		int neaCount = 0;
		for (State s : nea.getStates()) {
			for (Terminal t : nea.getAlphabet()) {
				if (transitionTable.getEndStatesFromTransitionsForState(s, t).size() > 1) {
					neaCount++;
					break;
				}
			}
		}
		return neaCount;
	}

	private void addAlphabetToDEA(Automaton dea, Automaton nea) {
		for (Terminal t : nea.getAlphabet()) {
			dea.addTerminal(new Terminal(t));
		}
	}

	private void addTransitionsToDEA(Automaton dea, Map<List<State>, Map<Terminal, List<State>>> subsetTable,
			Map<List<State>, State> newStatesMap) {
		Map<String, Transition> transitionMap = new HashMap<>();
		for (List<State> fromState : subsetTable.keySet()) {
			Map<Terminal, List<State>> toMap = subsetTable.get(fromState);
			for (Terminal t : toMap.keySet()) {
				List<State> toState = toMap.get(t);
				if (!toState.isEmpty()) {
					State start = newStatesMap.get(fromState);
					State end = newStatesMap.get(toState);
					String name = start.getName() + ", " + end.getName();
					Transition transitionForStates = transitionMap.computeIfAbsent(name,
							key -> new Transition(t, start, end, false));
					transitionForStates.addTerminal(t);
					dea.addTransition(transitionForStates);
				}
			}
		}
	}

	private Map<List<State>, State> addStatesToDEA(Automaton dea,
			Map<List<State>, Map<Terminal, List<State>>> subsetTable) {
		Map<List<State>, State> newStatesMap = new HashMap<>();
		for (List<State> newStates : subsetTable.keySet()) {
			if (!newStates.isEmpty()) {
				State newState = new State(newStates);
				dea.addState(newState);
				newStatesMap.put(newStates, newState);
			}
		}
		return newStatesMap;
	}

	private Map<List<State>, Map<Terminal, List<State>>> computeSubsetTable(TransitionTable transitionTable,
			State initial, Set<Terminal> alphabet) {
		Map<List<State>, Map<Terminal, List<State>>> subsetTable = new LinkedHashMap<>();

		List<List<State>> queue = new ArrayList<>();
		queue.add(getSortedListOfStates(initial));

		while (!queue.isEmpty()) {
			List<State> entry = queue.get(0);
			Map<Terminal, List<State>> transitionMap = new HashMap<>();
			if (entry.size() == 1 && entry.get(0) != null && entry.get(0).equals(emptyState)) {
				effort++; // add empty state
			}
			for (Terminal t : alphabet) {
				List<State> endStates;
				if (entry.size() == 1 && entry.get(0) != null && entry.get(0).equals(emptyState)) {
					endStates = entry;
				} else {
					endStates = transitionTable.getEndStatesFromTransitionsForStates(entry, t);
					for (State s : entry) {
						// one step for each transition
						effort += transitionTable.getEndStatesFromTransitionsForState(s, t).size();
					}
					if (!endStates.isEmpty())
						effort++;// check if new state exists
					else {
						if (emptyState == null) {
							emptyState = State.createEmptyState();
						}
						endStates.add(emptyState);
					}
					if (!subsetTable.containsKey(endStates) && !queue.contains(endStates)) {
						if (endStates.size() > 1)
							effort++;// check if accepting state is included
						queue.add(endStates);
					}
				}
				transitionMap.put(t, endStates);
			}
			subsetTable.put(entry, transitionMap);
			queue.remove(entry);
		}
		return subsetTable;
	}

	private List<State> getSortedListOfStates(State... states) {
		List<State> result = new ArrayList<>();
		for (State s : states) {
			if (!result.contains(s))
				result.add(s);
		}
		Collections.sort(result);
		return result;
	}

	public Automaton getAutomaton() {
		return automaton;
	}

	public int getEffort() {
		return effort;
	}

	public String getInfo() {
		StringBuilder sb = new StringBuilder();
		sb.append("\nSUBSET CONSTRUCTION ALGORITHM \n");
		sb.append("Effort: " + effort + "\n");
		return sb.toString();
	}

}
