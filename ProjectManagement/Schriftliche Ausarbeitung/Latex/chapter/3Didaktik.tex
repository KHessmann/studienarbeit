\chapter{Vorüberlegungen aus didaktischer Sicht}\label{chap:3Didaktik}
Viele der in Kapitel \ref{chap:2Grundlagen} vorgestellten Inhalte zählen in der theoretischen Informatik zum Allgemeinwissen und werden an den (Hoch-)Schulen bereits früh vermittelt. Neben der theoretischen Betrachtung der Eigenschaften von \acsp{DEA}, \acsp{NEA} und \acsp{eNEA} spielt im praktischen Übungsbetrieb die Umwandlung dieser Automaten eine entscheidende Rolle. Die folgenden Kapitel werden genauer auf die üblichen Vorgehensweisen bei der \hyperref[chap:4Transformation]{Transformation} von eNEAs in NEAs und weiter in DEAs, sowie der \hyperref[chap:5Minimierung]{Minimierung} letzterer eingehen. Mit Blick auf die Aufgabenstellung dieser Arbeit, der Generierung didaktisch wertvoller Übungsaufgaben, soll jedoch zunächst darauf eingegangen werden, was eine didaktisch wertvolle Aufgabe ausmacht und welche Vorgaben sich daraus für die Umsetzung ergeben. 

\noindent Dieses Kapitel behandelt zunächst die allgemeinen \hyperref[sec:31Implementierung]{Interessen der Anwender}, welche bei der Auswahl und Implementierung geeigneter Verfahren berücksichtigt werden sollen.
In Abschnitt \ref{sec:32Aufwand} wird ein Regelwerk für die Bewertung des Aufwands eines Anwenders bei der Bearbeitung einzelner (Teil-)Aufgaben definiert.
Das Kapitel schließt mit der Formulierung einiger Anforderungen, die ein aus Sicht der Autoren didaktisch geeigneter \hyperref[sec:33Automat]{Automat} zu jedem Zeitpunkt erfüllen sollte.

\section{Berücksichtigung der Interessen der Anwender während der Implementierung}\label{sec:31Implementierung}
Ziel dieses Abschnittes ist es, ein Regelwerk aus didaktischer Sicht zu entwickeln, welches während der Auswahl und Implementierung diverser Algorithmen als Orientierung dienen soll. Dazu stellt sich zunächst die Frage, welchen Qualitätskriterien didaktisch wertvolle Aufgaben folgen sollen. Dr. Kurt Reusser, Professor für Didaktik an der Universität Zürich, fasst verschiedene Richtlinien für gute Lernaufgaben zusammen, welche als Grundlage für einige der folgenden Regeln dienen\cite{Reusser.2013}. 
\\
\begin{drule}{Die Qualität eines Algorithmus hängt von der Eignung für den Anwender ab}{Implementierung1}
%	Eine gute Aufgabe soll anschaulich sein und dabei \enquote{Neugier und Motivation} wecken \cite[5]{Reusser.2013}. 
	Für jede der zu untersuchenden Problemstellungen gibt es an den (Hoch-)Schulen übliche Lösungsverfahren, die im Übungsbetrieb angewandt werden. Auch wenn sich diese (Hoch-)schulübergreifend kaum unterscheiden, sollen in dieser Arbeit auch andere Lösungsverfahren untersucht und aus didaktischen Gesichtspunkten verglichen werden. Drei mögliche Kriterien dafür sind die Anschaulichkeit des Verfahrens sowohl im Bezug auf die Vorgehens- als auch Funktionsweise und der Aufwand, der mit der Durchführung des Verfahrens verbunden ist. Ein Algorithmus, welcher unkompliziert und schnell durchführbar ist, kann dennoch ungeeignet sein, wenn dem Anwender nicht klar ist, warum die Vorgehensweise zu einem richtigen Ergebnis führt. In dem Fall ist das Kriterium der Anschaulichkeit der Funktionsweise verletzt.
\end{drule}
\\
\begin{drule}{Aufgaben müssen im schrittweise überprüft werden können}{Implementierung2}
	Bei der Bearbeitung von Übungsaufgaben spielt der Umgang mit Fehlern eine entscheidende Rolle. Fehler können als Hinweise gesehen werden, \enquote{um Schwierigkeiten zu erkennen und das Verstehen zu vertiefen}\cite[6]{Reusser.2013}. Erfolgen diese Hinweise zu selten und ohne Bezug auf die unmittelbare Fehlerquelle, kann dies statt den gewünschten Lerneffekt hervorzurufen zu Frustrationen auf Seiten der Anwender führen. Statt das Endergebnis einer Übungsaufgabe als falsch zu markieren, sollten in die Algorithmen Möglichkeiten zur schrittweisen Überprüfung implementiert werden. Damit können Fehlerquellen dem Anwender nach der Einführung einer passenden Benutzeroberfläche direkt angezeigt werden.
\end{drule}
\\
\begin{drule}{Durch die Implementierung des Algorithmus dürfen keine Verständnisfehler entstehen}{Implementierung3}
	Gute Übungsaufgaben \enquote{trainieren und festigen Fertigkeiten und Strategien} und \enquote{laden zu tiefem Verstehen und Problemlösen ein} \cite[5]{Reusser.2013}. 
	Der Anwender befindet sich zum Zeitpunkt der Bearbeitung der Aufgaben zumeist noch im Lernprozess. Dabei zieht er aus dem Feedback zu den von ihm eingereichten Ergebnissen Rückschlüsse auf die Funktionsweise der Verfahren.
	Einschränkungen der Verfahren durch neue Regeln, um etwa die Überprüfung der Ergebnisse zu erleichtern, sollten daher vermieden werden. Dazu gehören beispielsweise Vorgaben zur Bearbeitungsreihenfolge, obwohl diese im Verfahren nicht vorgesehen sind.
	Durch solche Vorgaben würden eben nicht die \enquote{Strategien} der üblichen Verfahren trainiert und vermittelt, sondern die in den Übungsaufgaben abgewandelte Version. 
\end{drule}
\\
\begin{drule}{Der Aufwand einer Aufgabe muss an die Bedürfnisse der Anwender angepasst werden können}{Implementierung4}
	Nach Regel \ref{rule:Implementierung1} soll bei der Auswahl eines Verfahrens der mit der Durchführung verbundene Aufwand mit berücksichtigt werden. Dies bezieht sich darauf, dass einige Verfahren grundsätzlich bei der Durchführung durch den Menschen mit mehr Aufwand verbunden sind als andere. Zusätzlich dazu spielen für den Aufwand jedoch auch verfahrens\-unabhängige Parameter wie Struktur, Anzahl Zustände und Übergänge des betrachteten Automaten eine Rolle. Gute Übungsaufgaben sollen adaptiv an das Leistungsniveau der Anwender angepasst werden können\cite[5]{Reusser.2013}. Ein Anwender benötigt zu Beginn des Übungsbetriebs Aufgaben mit einem anderen Niveau als zu einem späteren Zeitpunkt. Je nach Übungssituation und Lernfortschritt soll es also möglich sein, den Bearbeitungsaufwand eines Verfahrens anzupassen, um auf die individuellen Vorkenntnisse des Anwenders eingehen zu können. Dies kann beispielsweise durch Steuerung der oben genannten Parameter erreicht werden und wird in Kapitel \ref{chap:7Generierung} genauer erläutert.
\end{drule}

\section{Analyse des Arbeitsaufwands der Anwender}\label{sec:32Aufwand}
%1. Motivation:
%was heißt didaktisch wertvoll in dem kontext?
In Abschnitt \ref{sec:31Implementierung} wird die Relevanz des Arbeitsaufwands bei der Erstellung und Bewertung geeigneter Übungsaufgaben erwähnt. Dieser Aufwand wird vom Anwender im Normalfall durch ein individuelles Gefühl bestimmt. Je nachdem, wie lange man für eine Aufgabe benötigt, sei es, weil sie viele einzelne Arbeitsschritte oder viel Zeit zum Nachdenken erfordert, wird sie als mehr oder weniger aufwändig empfunden. 
%Dabei ist Aufwand nicht mit Komplexität gleichzusetzen. 
Diesem individuellen Empfinden soll in dieser Arbeit ein fester Zahlenwert zugeordnet werden können. Dafür soll im Folgenden ein allgemeines Regelwerk eingeführt werden, welches Arbeitsvorgänge in Einzelhandlungen unterteilt und diesen einen Wert zuweist. Als kleinste Einheit wird dafür ein (Arbeits-)\definitionCallout{Schritt} festgelegt. 

%Jedes Verfahren lässt sich auf eine Menge von Operationen reduzieren
\noindent Es ist anzumerken, dass dieses Regelwerk dem individuellen Empfinden nicht vollständig gerecht werden kann. Vielmehr ist es das Ziel, Richtwerte vorzugeben, mit deren Hilfe verschiedene Verfahren nach einem einheitlichen System bewertet und verglichen werden können.
% Mit diesem Regelwerk können die im Folgenden behandelten Verfahren in Vergleich zueinander gesetzt und Übungsaufgaben in Abhängigkeit von der Lernsituation erstellt werden. 
\newpassage
\begin{drule}{Suchoperation}{Aufwand1} 
	Eine Suchoperation kann in einer Tabelle, einem Zustand, einem Graphen oder einer Liste durchgeführt werden. Ziel der Suchoperation kann es sein, ein Element auf eine bestimmte Eigenschaft zu prüfen oder eine Einheit innerhalb des Elements ausfindig zu machen.
	\newline  \hspace*{\fill} \arrowCallout{1 Schritt}%	
\end{drule}
\begin{drule}{Identifikation unterscheidbarer Zustandspaare}{Aufwand2} 
	Zustände können akzeptierend und nicht akzeptierend sein. Anhand dieser Eigenschaft lassen sich unterscheidbare Paare bilden.
	\newline	\hspace*{\fill} \arrowCallout{1 Schritt pro unterscheidbarem Paar}%
\end{drule}
\begin{drule}{Übergangsanalyse}{Aufwand3} 
	Eine Übergangsanalyse ermittelt, wohin ein Übergang ausgehend von einem Zustand $q$ für eine Eingabe $a$ führt, also den Wert $x = \delta(q,a)$.
	\newline	\hspace*{\fill} \arrowCallout{1 Schritt}%
\end{drule}
\begin{drule}{Erstellen eines Übergangs oder Zustands}{Aufwand4}
	Erstellen eines Übergangs inklusive neuen Zustands, falls dies erforderlich ist. 
	\newline	\hspace*{\fill} \arrowCallout{1 Schritt}%
\end{drule}
\begin{drule}{Äquivalenzklassen zusammenfügen oder aufteilen}{Aufwand5} 
	Der Aufwand, der sich aus dem Zusammenfügen von $n$ Äquivalenzklassen ergibt, ist abhängig von der resultierenden Anzahl $k$. Wenn alle Äquivalenzklassen zu einer zusammengefügt werden ($k=1$) oder es zu keiner Veränderung kommt ($k=n$), ist der Aufwand minimal und wird auf 1 geschätzt. Für das Aufteilen einer Äquivalenzklasse mit $n$ Zuständen in $k$ neue Klassen wird der Aufwand analog berechnet. 
	\newline	\hspace*{\fill} \arrowCallout{min(k, n+1-k) Schritte}%
\end{drule}
\begin{drule}{Zeichenoperation}{Aufwand6} 
	Das Zeichnen von Tabellen, Listen und ganzen Graphen wird nicht in die Aufwandsberechnung für die Verfahren mit einberechnet. Dieser Aufwand ist stark von der Gestaltung der Benutzeroberfläche abhängig, sollte jedoch nicht vernachlässigt werden. 
	\newline	\hspace*{\fill} \arrowCallout{0 Schritte}%
\end{drule}






\section{Eigenschaften eines geeigneten Automaten}\label{sec:33Automat}
Abschnitte \ref{sec:31Implementierung} und \ref{sec:32Aufwand} legen fest, wie eine aus didaktischer Sicht wertvolle Übungsaufgabe mit Blick auf das Verfahren erstellt und bewertet werden soll. Ein weiterer Aspekt bei der Gestaltung geeigneter Aufgaben ist die Vorgabe einheitlicher Richtlinien für die Konstruktion eines Automaten. Da es in der Literatur unterschiedliche Varianten gültiger Automaten gibt, dient das folgende Regelwerk zur Festlegung, welche Formen in dieser Arbeit verwendet werden sollen. 
\newline
\begin{drule}{Es gibt genau einen Startzustand}{Automat1}
	Diese Regel ist schon durch Definition \ref{def:231automat} des Endlichen Automaten bedingt. Da es in der Literatur vereinzelt Beispiele gibt, in denen ein Automat mehrere Startzustände besitzt, wird diese Einschränkung noch einmal explizit festgehalten.  	
\end{drule}
\newline
\begin{drule}{Alle Zustände müssen vom Startzustand aus erreichbar sein}{Automat2}
	Weder bei der Generierung, noch durch die Transformation in einen \ac{DEA} oder die Minimierung darf ein Zustand entstehen, welcher nicht vom Startzustand aus erreichbar ist. 
\end{drule}
\newline
\begin{drule}{Jedes Eingabesymbol des Alphabets wird verwendet}{Automat3}
	Diese Einschränkung ergibt sich nicht zwangsläufig aus der Definition des Endlichen Automaten.
\end{drule}
\newline
\begin{drule}{Mindestens ein Zustand ist akzeptierend}{Automat4}
	Rein formal könnte die Menge $F$ an akzeptierenden Zuständen eines Endlichen Automaten auch leer sein. Die zugehörige reguläre Sprache würde in diesem Fall kein Wort (nicht einmal das leere) akzeptieren. Dieser Fall ist für die Übungsaufgaben nicht interessant und soll daher ausgeschlossen werden.
\end{drule}
\newline
\begin{drule}{Zustände eines Automaten ändern ihre Benennung während der Transformation oder Minimierung nicht}{Automat5}
	Eine Umbenennung von Zuständen kann insbesondere bei der Minimierung, aber auch bei der in Abschnitt \ref{sec:43Subset} vorgestellten Potenzmengenkonstruktion, sinnvoll erscheinen. Um keine zusätzlichen Verständnisprobleme zu erzeugen, soll dies jedoch vermieden werden. Dies widerspricht Regel \ref{rule:Implementierung3}, nach der keine künstlichen Einschränkungen die Verfahren verfälschen sollen. In Kapitel \ref{chap:6Testen} werden einige Algorithmen für die Prüfung von Automaten auf Gleichheit unabhängig von der Zustandsbenennung untersucht.
\end{drule}











