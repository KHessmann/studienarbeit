package transformation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import commons.Automaton;
import commons.State;
import commons.Terminal;
import commons.TerminalType;
import commons.Transition;
import generation.AutomatonMatrix;
import helper.TableStringBuilder;

public class RemoveEpsilonTransformer {

	private Automaton automatonWithEpsilon;
	private Automaton automaton;
	private int effort;

	private Map<State, HashMap<Terminal, List<State>>> tableWithEpsilon = new HashMap<>();
	private Map<State, HashMap<Terminal, List<State>>> transitionTable = new HashMap<>();

	public RemoveEpsilonTransformer(Automaton automaton, boolean isAlmostDea) {
		automatonWithEpsilon = automaton;
		tableWithEpsilon = automaton.getTransitionTable().getTransitionTable();
		this.effort = 1;
		if (isAlmostDea) {
			this.automaton = automaton;
		} else {
			this.automaton = transform(automaton);
		}
	}

	public Automaton transform(Automaton automaton) {
		AutomatonMatrix automatonMatrix = automaton.getAutomatonMatrix();
		if (automatonMatrix.getSequencesCount() != 0) {
			throw new IllegalArgumentException("the automaton should have no input sequences at this point");
		} else if (automatonMatrix.getEpsilonCount() == 0) {
			return automaton;
		} else {
			Automaton newAutomaton = new Automaton(automaton);
			transitionTable = newAutomaton.getTransitionTable().getTransitionTable();
			return transformEpsilon(newAutomaton);
		}
	}

	private Automaton transformEpsilon(Automaton newAutomaton) {
		createNewEpsilonTransitions(newAutomaton);
		List<Transition> transitionsWithEpsilon = newAutomaton.getTransitions().stream()
				.filter(transition -> hasEpsilonTerminal(transition)).collect(Collectors.toList());

		for (Transition transition : transitionsWithEpsilon) {
			effort += 4;
			removeEpsilonAndCreateTransitions(newAutomaton, transition);
		}
		newAutomaton.removeTerminalType(TerminalType.EPSILON);
		return newAutomaton;
	}

	private void createNewEpsilonTransitions(Automaton automaton) {
		Terminal epsilon = new Terminal(TerminalType.EPSILON);
		Set<State> newEpsilon = new HashSet<>();
		for (State start : transitionTable.keySet()) {
			newEpsilon.add(start);
			List<State> exsistingEpsilon = new ArrayList<>(transitionTable.get(start).get(epsilon));
			addEpsilonTransitionEndStatesRecursive(epsilon, newEpsilon, exsistingEpsilon);
			exsistingEpsilon.add(start);
			newEpsilon.removeAll(exsistingEpsilon);
			for (State end : newEpsilon) {
				Transition transition = new Transition(new Terminal(TerminalType.EPSILON), start, end, true);
				automaton.addTransition(transition);
			}
			newEpsilon = new HashSet<>();
		}
	}

	private void addEpsilonTransitionEndStatesRecursive(Terminal epsilon, Set<State> newEpsilon,
			List<State> exsistingEpsilon) {
		if (!exsistingEpsilon.isEmpty() && !newEpsilon.containsAll(transitionTable.keySet())
				&& !newEpsilon.containsAll(exsistingEpsilon))
			for (State state : exsistingEpsilon) {
				if (newEpsilon.add(state)) {
					List<State> newStates = new ArrayList<>(transitionTable.get(state).get(epsilon));
					addEpsilonTransitionEndStatesRecursive(epsilon, newEpsilon, newStates);
				}
			}
	}

	private void removeEpsilonAndCreateTransitions(Automaton automaton, Transition transition) {
		State startState = transition.getStartState();
		State endState = transition.getEndState();
		List<Terminal> terminals = transition.getTerminals().stream()
				.filter(terminal -> terminal.getType() == TerminalType.EPSILON).collect(Collectors.toList());
		for (Terminal terminal : terminals) {
			List<Transition> transitionFromEpsilonEndState = automaton.getTransitions().stream()
					.filter(t -> endState == t.getStartState()).collect(Collectors.toList());
			automaton.addAllTransitions(addNewTransitionsToState(true, transitionFromEpsilonEndState, startState));
			effort += transitionFromEpsilonEndState.size();

			List<Transition> transitionToEpsilonStartState = automaton.getTransitions().stream()
					.filter(t -> startState == t.getEndState()).collect(Collectors.toList());
			automaton.addAllTransitions(addNewTransitionsToState(false, transitionToEpsilonStartState, endState));
			effort += transitionToEpsilonStartState.size();

			transition.removeTerminal(terminal);
			if (transition.getTerminals().isEmpty()) {
				automaton.removeTransition(transition);
			}
			if (endState.isAccepting()) {
				startState.setAccepting(true);
				automaton.addAccepting(startState);
			}
		}
	}

	private boolean hasEpsilonTerminal(Transition transition) {
		List<Terminal> terminals = transition.getTerminals().stream()
				.filter(terminal -> terminal.getType() == TerminalType.EPSILON).collect(Collectors.toList());
		return !terminals.isEmpty();
	}

	private List<Transition> addNewTransitionsToState(boolean isStart, List<Transition> transitions, State state) {
		List<Transition> result = new ArrayList<>();
		State start;
		State end;
		for (Transition transition : transitions) {
			if (!transition.hasEpsilon()) {
				if (isStart) {
					start = state;
					end = transition.getEndState();
				} else {
					start = transition.getStartState();
					end = state;
				}
				createTransitionIfNotExsists(result, start, end, transition);
			}
		}
		return result;
	}

	private void createTransitionIfNotExsists(List<Transition> result, State start, State end, Transition transition) {
		List<Terminal> terminals = new ArrayList<>(transition.getTerminals());
		Transition t = new Transition(terminals.get(0), start, end, false);
		t.removeAllTerminals();
		for (Terminal terminal : terminals) {
			List<State> exsistingTransitions = transitionTable.get(start).get(terminal);
			if (!exsistingTransitions.contains(end)) {
				t.addTerminal(terminal);
				exsistingTransitions.add(end);
			}
		}
		if (!t.getTerminals().isEmpty()) {
			result.add(t);
		}
	}

	public Automaton getAutomaton() {
		return automaton;
	}

	public int getEffort() {
		return effort;
	}

	public String getEpsilonClosureAndOldTransitionstableCombined() {
		if (transitionTable.isEmpty()) {
			return automaton.getTransitionTable().toString();
		}

		int rowCount = tableWithEpsilon.size() + 1;
		Set<Terminal> alphabetWithEpsilon = automatonWithEpsilon.getAlphabet();
		Set<Terminal> alphabet = automaton.getAlphabet();
		int colCount = alphabetWithEpsilon.size() + alphabet.size() + 1;
		String[][] table = new String[rowCount][colCount];
		Map<State, State> oldToNew = new HashMap<>();
		for (State old : tableWithEpsilon.keySet()) {
			for (State newState : transitionTable.keySet()) {
				if (old.getName().equals(newState.getName())) {
					oldToNew.put(old, newState);
				}
			}
		}
		int i = 0;
		int j = 0;
		table[i][j] = "States";
		for (Terminal t : alphabetWithEpsilon) {
			j++;
			table[i][j] = "\u03B4(" + t.toString() + ")";
		}
		for (Terminal t : alphabet) {
			j++;
			table[i][j] = "\u03B4'(" + t.toString() + ")";
		}
		for (State from : tableWithEpsilon.keySet()) {
			State fromNew = oldToNew.get(from);
			i++;
			j = 0;
			StringBuilder sb = new StringBuilder();
			sb.append(from.toString());
			table[i][j] = sb.toString();
			HashMap<Terminal, List<State>> terminalMap = tableWithEpsilon.get(from);
			for (Terminal t : alphabetWithEpsilon) {
				j++;
				List<State> endStates = terminalMap.get(t);
				Collections.sort(endStates);

				String stateEntry = "";
				if (endStates.size() > 1) {
					StringBuilder sb2 = new StringBuilder();
					sb2.append(State.cummulatedStartChar);
					for (State s : endStates) {
						sb2.append(s.toString());
						sb2.append(State.delimiter);
					}
					sb2.deleteCharAt(sb2.length() - 1);
					sb2.append(State.cummulatedEndChar);
					stateEntry = sb2.toString();
				} else if (endStates.size() == 1) {
					stateEntry = terminalMap.get(t).get(0).toString();
				}
				table[i][j] = stateEntry;
			}
			HashMap<Terminal, List<State>> terminalToStates = transitionTable.get(fromNew);
			for (Terminal t : alphabet) {
				j++;
				List<State> endStates = terminalToStates.get(t);
				Collections.sort(endStates);
				String stateEntry = "";
				if (endStates.size() > 1) {
					StringBuilder sb2 = new StringBuilder();
					sb2.append(State.cummulatedStartChar);
					for (State s : endStates) {
						sb2.append(s.toString());
						sb2.append(State.delimiter);
					}
					sb2.deleteCharAt(sb2.length() - 1);
					sb2.append(State.cummulatedEndChar);
					stateEntry = sb2.toString();
				} else if (endStates.size() == 1) {
					stateEntry = terminalMap.get(t).get(0).toString();
				}
				table[i][j] = stateEntry;
			}
		}
		return TableStringBuilder.getTableString(table);
	}

	public String getInfo() {
		StringBuilder sb = new StringBuilder();
		sb.append("\nREMOVE EPSILON \n");
		sb.append("Epsilon Closure: \n");
		sb.append(getEpsilonClosureAndOldTransitionstableCombined());
		sb.append("Effort: " + effort + "\n");
		return sb.toString();
	}
}
