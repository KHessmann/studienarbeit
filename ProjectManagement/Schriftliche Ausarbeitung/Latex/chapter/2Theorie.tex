\chapter{Theoretische Grundlagen}\label{chap:2Grundlagen}
In diesem Kapitel werden die für diese Arbeit relevanten theoretischen Grundlagen genauer erläutert. 
Dabei wird zunächst auf die \nameref{sec:21Graphentheorie} eingegangen, darauf folgend auf die \hyperref[sec:22ReguläreSprachen]{Regulären Sprachen} als Teilgebiet der formalen Sprachen. Abschließend wird in Abschnitt \ref{sec:23EA} in die Automatentheorie mit Fokus auf Deterministische und Nichtdeterministische Endliche Automaten eingeführt. 
\section{Graphentheorie}\label{sec:21Graphentheorie}

In der theoretischen Informatik wird ein Graph als Paar bezeichnet. Dieses Paar $G=(V,E)$ besteht aus einer nicht leeren Menge von Objekten, den \definitionCallout{Knoten} (auch \definitionCallout{Ecken} genannt), und einer weiteren nicht leeren Menge von Elementen, den Kanten. Eine Kante ist eine aus zwei Elementen bestehende Teilmenge der Knoten, welche miteinander verbunden sind \cite[2]{Diestel.2017}. 
Ein solcher Graph wird im Folgenden mit Kreisen als Knoten und Linien zwischen je zwei Knoten als Kanten dargestellt (siehe \reffig{graph:21UngerichteterGraph}). Die Knotenanzahl eines Graphen wird als \definitionCallout{Ordnung} bezeichnet \cite[13]{Krumke.2009}.\\

\input{tikz/21UndirectedGraph.tex}

\subsubsection{Der Digraph}
\noindent Eine Kante kann auch eine Richtung aufweisen. Dies wird als \definitionCallout{gerichtete Kante} bezeichnet und durch einen Pfeil am Ende der Kante graphisch dargestellt. Bei der Traversierung des Graphen kann eine solche Kante nur in der entsprechenden Richtung durchlaufen werden \cite[194]{Deo.1974}. Ein Graph ohne gerichtete Kanten wird auch als ungerichteter Graph bezeichnet \cite[28]{Diestel.2017}. Das Knotenpaar $(v_1, v_2)\subseteq V$ einer gerichteten Kante $e \in E$ ist eine geordnete Menge und der Knoten $v_1$ wird als Ausgangsknoten der Kante bezeichnet. Der Knoten $v_2$ ist der Endknoten von $e$ \cite[195]{Deo.1974}. Die Menge der Kanten eines gerichteten Graphen wird als $E\subseteq V\times V$ definiert. Ein Graph mit gerichteten Kanten wird auch als \definitionCallout{Digraph} bezeichnet \cite[32]{Brauer.1984}.

\noindent Zu jedem Knoten kann eine Anzahl eingehender und ausgehender Kanten bestimmt werden. Die Anzahl der eingehenden Kanten wird \definitionCallout{Eingangsgrad} genannt und die der ausgehenden \definitionCallout{Ausgangsgrad}. Der Eingangsgrad eines Knotens $v_{i}$ kann geschrieben werden als $d^{+}(v_{i})$ und der Ausgangsgrad als $d^{-}(v_{i})$. Dabei gilt für den Graphen, dass die Summe des Eingangsgrads aller Knoten der Summe des Ausgangsgrads aller Knoten entspricht \cite[195]{Deo.1974}. 
\begin{flalign*}
	&&& \hspace{25mm}\sum_{i=1}^n d^{+}(v_i) = \sum_{i=1}^n d^{-}(v_i) && \text{\cite[195]{Deo.1974}}
\end{flalign*} 

\begin{exmp}{21Grad} 
	Für den Grad der einzelnen Knoten des Graphen in \reffig{graph:21Digraph} gilt dann:
	\begin{flalign*}
		&& d^{+}(A) = 1 && d^{-}(A) = 3 &&\\
		&& d^{+}(B) = 2 && d^{-}(B) = 1 &&\\
		&& d^{+}(C) = 2 && d^{-}(C) = 1 &&\\
		&& d^{+}(D) = 1 && d^{-}(D) = 1 &&
	\end{flalign*}
\end{exmp}

\noindent Eine Sequenz von Knoten und Kanten, welche aus Knoten besteht, die jeweils durch eine Kante miteinander verbunden sind, wird als \definitionCallout{Weg} bezeichnet. Dabei darf jede Kante nur einmal vorkommen, ein Knoten jedoch mehrmals. Die Kante, welche zwei Knoten miteinander verbindet, steht in der Sequenz zwischen diesen beiden Knoten. Dabei ist darauf zu achten, dass der Ausgangsknoten vor der Kante und der Endknoten hinter der Kante steht. Der Weg geht dabei vom ersten Knoten der Sequenz bis zum letzten Knoten. \cite[201]{Deo.1974} Wenn der beginnende Knoten und der Knoten am Ende der Sequenz identisch sind, handelt es sich um einen \definitionCallout{geschlossen Weg} und sonst um einen offenen Weg \cite[20]{Deo.1974}. 

\noindent Mit Hilfe der Definition eines Wegs kann nun ein \definitionCallout{zusammenhängender Digraph} beschrieben werden. Dabei wird zwischen schwach und stark zusammenhängenden Graphen unterschieden. Die Voraussetzung für einen stark zusammenhängenden Digraphen ist, dass ein Weg von jedem Knoten zu jedem anderen Knoten des Graphen existiert. Für einen schwach zusammenhängenden Digraphen muss  der entsprechende ungerichtete Graph zusammenhängend sein \cite[202]{Deo.1974}.

\noindent Innerhalb eines Digraphen kann es zu zwei verschiedenen Strukturen kommen, welche im Verlauf der Arbeit relevant sind. Die erste Struktur ist eine Kante, welche den gleichen Knoten als Start- und Endknoten besitzt. Diese Struktur wird als \definitionCallout{Schleife} bezeichnet und ist in Abbildung \ref{graph:21Digraph} und  \ref{graph:21bewerteterDigraph} violett hervorgehoben \cite[2]{Diestel.2017}.
Die zweite Struktur entspricht einem \definitionCallout{Zyklus}, welcher als geschlossener Weg mit einer zusätzlichen Einschränkung definiert wird. Bei diesem geschlossen Weg dürfen abgesehen vom Anfangs- und Endknoten keine weiteren Knoten mehrfach vorkommen \cite[20]{Deo.1974}. In Abbildung \ref{graph:21Digraph} und \ref{graph:21bewerteterDigraph} ist jeweils der Zyklus zwischen den Knoten $(B, C, D) \in V$ blau markiert.

\input{tikz/21Digraph+MitGewichten.tex}

\noindent Im Vergleich zu dem Digraphen in \reffig{graph:21Digraph} werden den Kanten des Digraphen auf der rechten Seite in \reffig{graph:21bewerteterDigraph} zusätzlich Beschriftungen zugeordnet. Diese Beschriftungen werden \definitionCallout{Gewichte} oder auch Bewertung genannt.\\

\begin{definition}{Bewerteter Digraph}{21bewerteterDigraph}
	Ein \definitionCallout{bewerteter Digraph} ist ein Tripel $G=(V,X,E)$ für welches gilt:
	\begin{itemize}
		\item $V$ ist die Menge der Knoten
		\item $X$ ist die Menge der Gewichte
		\item $E\subseteq V\times X\times V$  ist die Menge der bewerteten gerichteten Kanten \cite[33]{Brauer.1984}
	\end{itemize}
\end{definition}

\subsubsection{Die Adjazenzmatrix}

\noindent Eine weitere Darstellung eines Graphen ist die \definitionCallout{Adjazenzmatrix}. Dabei handelt es sich um eine $ n\times n$ - Matrix $A(G)$, wobei $n \in V$ gilt. Jeder Eintrag $a_{ij}$ der Adjazenzmatrix gibt an, ob das Knotenpaar eine Kante bildet, also $(ij) \in E$ gilt. Falls eine Kante mit den beiden Knoten vorhanden ist, wird an der Stelle $a_{ij}$ eine $1$ eingetragen, sonst eine $0$ \cite[17]{Krumke.2009}. Für den Digraphen in \reffig{graph:21Digraph} ist beispielhaft in 
\reffig{fig:21Adjazenzmatrix} die entsprechende Adjazenzmatrix abgebildet.
\input{images/21Adjazenzmatrix.tex}

\subsubsection{Isomorphie von Graphen}
Bei dem Vergleich von Automaten kann geprüft werden, ob zwei Automaten isomorph sind. In der Graphentheorie werden zwei Graphen als isomorph bezeichnet, wenn sie die gleichen Eigenschaften besitzen \cite[14]{Deo.1974}. Das bedeutet, dass die Graphen die gleiche Anzahl an Knoten und Kanten besitzen müssen. Zudem muss für jeden Knoten $v_{i}$ des einen Graphen ein entsprechender Knoten $w_{i}$ im anderen Graphen existieren, für den Eingangs-und Ausgangsgrad übereinstimmen. Die Bezeichnung der Knoten ist dabei nicht von Bedeutung \cite[15]{Deo.1974}.

\input{tikz/21Isomorphie.tex}

\begin{exmp}{21Isomorphie} 
	Der in \reffig{graph:21Isomorphie} dargestellte Graph ist in allen drei Abbildungen isomorph zu dem Digraphen A in \ref{graph:21Digraph}, da alle die gleiche Anzahl an Knoten und Kanten besitzen und für jeden Knoten in jedem Graphen ein anderer Knoten existiert, der den gleichen Eingangs- und Ausgangsgrad besitzt. 
	Digraph B' unterscheidet sich von Digraph B durch die Position des Knoten $G$. Digraph B'' unterscheidet sich von B' durch eine Drehung um $90^\circ$ und von Digraph A nur duch die unterschiedliche Benennung.
\end{exmp}

\section{Reguläre Sprachen}\label{sec:22ReguläreSprachen}
Vor der Einführung des Automaten soll zunächst eine kurze Zusammenfassung der wichtigsten Begriffe der Regulären Sprachen erfolgen.
In der theoretischen Informatik ähnlich wie im allgemeinen Gebrauch bezeichnet eine \definitionCallout{formale Sprache} eine Menge von \definitionCallout{Wörtern $w$}, welche wiederum einer Konkatenation von \definitionCallout{Eingabesymbolen} entsprechen. Diese Eingabesymbole sind in einem \definitionCallout{Alphabet} $\Sigma$ definiert. Für Wörter einer festen Länge $l$ schreibt man auch $w \in \Sigma^l$. $\Sigma^0$ ist das leere Wort, dargestellt durch das Symbol $\epsilon$ und $\Sigma^*$ die Menge an Wörtern beliebiger Länge\cite[54\psq]{Hopfcroft.2011}. 
\begin{definition}{Wort}{22wort}
	Ein \definitionCallout{Wort} (auch Zeichenreihe) ist eine endliche Folge von Eingabesymbolen eines Alphabets. Enthält ein Wort kein Eingabesymbol, so wird es als das \definitionCallout{leere Wort} bezeichnet und durch das Symbol $\epsilon$ dargestellt \cite[54]{Hopfcroft.2011}.
\end{definition}
\begin{definition}{Sprache}{22sprache}
	Eine \definitionCallout{Sprache L} über einem Alphabet $\Sigma$ ist eine Menge von Wörtern $w \in \Sigma^*$ \cite[673]{Herold.2017}.
\end{definition}
\noindent Formale Sprachen werden durch Grammatiken erzeugt, welche durch eine Menge von Regeln (Produktionen) die Wörter definieren, die in der Sprache enthalten sind. 
Die im Folgenden relevanten \definitionCallout{regulären Sprachen} beschreiben einen Teil der formalen Sprachen und werden von so genannten \definitionCallout{Typ-3 Grammatiken} (auch reguläre Grammatiken) erzeugt \cite[40]{RaywardSmith.1995}. Als vereinfachte Notationsform zur Beschreibung regulärer Sprachen dienen reguläre Ausdrücke, deren Regeln im Folgenden als bekannt vorausgesetzt werden.
\\
\begin{exmp}{22regex}
	Die reguläre Sprache $L=\{ab^n|n\geq 0\} = \{a, ab, abb, ...\}$ über dem Alphabet $\Sigma = \{a,b\}$ kann ebenso durch den regulären Ausdruck $r=ab^*$ ausgedrückt werden.
\end{exmp}
\vspace{0.4cm}

\noindent Statt eine regulären Sprache mithilfe der Regeln der entsprechenden Grammatik zu erzeugen, kann dazu auch ein sogenannter endlicher Automat verwendet werden. Ein \definitionCallout{endlicher Automat} ist ein Modell einer zustandsorientierten Maschine, welche Eingabeworte zeichenweise einliest. Das eingelesene Wort ist in der Sprache enthalten, wenn der Automat zuletzt einen akzeptierenden Zustand erreicht\cite[675]{Herold.2017}. Diese endlichen Automaten sollen in Abschnitt \ref{sec:23EA} weiter betrachtet werden.

\section{Der Endliche Automat}\label{sec:23EA}
Wie im vorherigen Abschnitt erklärt, kann mithilfe eines endlichen Automaten eine reguläre Sprache erzeugt werden. In der Automatentheorie unterscheidet man zwischen Deterministischen Endlichen Automaten (\acs{DEA}) und Nichtdeterministischen Endlichen Automaten (\acs{NEA}). In diesem Abschnitt sollen zunächst die Bestandteile und Eigenschaften des \hyperref[ssec:231SpracheDEA]{DEA} definiert und verschiedene in dieser Arbeit genutzte \hyperref[ssec:232Notation]{Notationsformen} eingeführt werden. Im Anschluss wird der \hyperref[ssec:233NEA]{NEA} als Erweiterung des DEA definiert. Der Abschnitt schließt mit einem Einblick in die \hyperref[ssec:234Äquivalenz]{Äquivalenz} endlicher Automaten.

\subsection{Die Sprache des DEA}\label{ssec:231SpracheDEA}
Im vorherigen Abschnitt wurde der endliche Automat als Modell einer zustandsorientierten Maschine bezeichnet. Grundsätzlich bedeutet dies, dass ein \ac{DEA} ebenso wie ein \ac{NEA} aus einer Menge an \definitionCallout{Zuständen $Q$} besteht, zwischen denen je nach Eingabesymbol gewechselt wird. Zu Beginn befindet sich ein Automat stets in seinem \definitionCallout{Startzustand}. Liest der Automat ein Wort ein, so gibt die \definitionCallout{Übergangsfunktion $\delta$} an, in welchen Folgezustand der Automat je nach Eingabesymbol wechseln muss. Die erlaubten Eingabesymbole sind wie auch in Definition \ref{def:22sprache} der Sprache durch ein Alphabet $\Sigma$ definiert. 
Das Wort wird akzeptiert, wenn sich der Automat nach dem Einlesen in einem \definitionCallout{akzeptierenden Zustand} der Menge $F$ befindet \cite[72]{Hopfcroft.2011}.  
\begin{definition}{Endlicher Automat}{231automat}
	Ein \definitionCallout{Endlicher Automat} ist ein 5-Tupel $A = (Q, \Sigma, \delta, q_0, F)$ für welches gilt:
	\begin{itemize}
		\item $Q$ ist eine endliche Zustandsmenge
		\item $\Sigma$ ist das Alphabet mit einer endlichen Menge Eingabesymbole
		\item $\delta : \ Q \times \Sigma \rightarrow Q$ ist die Übergangsfunktion
		\item $q_0 \in Q$ ist der Startzustand 
		\item $F\subseteq Q$ ist die Menge akzeptierender Zustände \cite[52]{Martin.2011}
	\end{itemize}
\end{definition}

\noindent Der Zusammenhang zwischen einem endlichen Automaten und den regulären Sprachen wird in Theorem \ref{thm:231kleene} noch einmal festgehalten. Danach kann für jede beliebige reguläre Sprache ein entsprechender Endlicher Automat definiert werden. Umgekehrt muss die von einem Endlichen Automaten akzeptierte Sprache regulär sein.
\begin{theorem}{Kleene Teil 1 \& 2}{231kleene}
	\theoremCallout{1: }Jede reguläre Sprache über ein beliebiges Alphabet $\Sigma$ wird von einem Endlichen Automaten $A$ akzeptiert.\\
	\noindent \theoremCallout{2: }Für jeden Endlichen Automaten $A = (Q,\Sigma, \delta, q_0, F)$ ist die Sprache $L(A)$ regulär \cite[111,114]{Martin.2011}.
\end{theorem}

\noindent Der Endliche Automat heißt \definitionCallout{deterministisch}\label{def:determinismus} , wenn für die Übergangsfunktion zusätzlich die Einschränkung gilt, dass der Automat für jeden Zustand $q_i \in Q$ und jedes Eingabesymbol $a \in \Sigma$ in genau einen Folgezustand übergehen kann \cite[71]{Hopfcroft.2011}.
\begin{align*}
	\forall q_i \in Q, a\in \Sigma \ \exists! \  q_j \in Q:\  \delta(q_i,a) = q_j.
\end{align*} 

\begin{exmp}{231delta1}
	Für die reguläre Sprache $L=\{ab^n|n\geq 0\}$ kann der \ac{DEA} $A = \{Q, \Sigma, \delta, q_0, F\}$ definiert werden mit $Q = \{q_0,q_1,q_2\}$, $\Sigma =\{a,b\}$ und $F=\{q_1\}$. \\ \noindent Für die Übergangsfunktion gilt mit dem leeren Wort $\epsilon$ (siehe Definition \ref{def:22wort}):
	\begin{align*}
		\delta(q_0, \epsilon) &= q_0,~~~ \delta (q_0, a) = q_1,~~~ \delta (q_0, b) = q_2\\
		 \delta (q_1, a) &= q_2,~~~ \delta (q_1,b) = q_1\\
		 \delta (q_2, a) &= q_2,~~~ \delta (q_2,b) = q_2
	\end{align*}
\end{exmp}

\noindent Während der Übergangsfunktion $\delta$ ein Tupel aus Zustand und Eingabesymbol übergeben wird, ist häufig von Interesse, welcher Zustand zuletzt für ein Wort und damit ggf. eine längere Eingabefolge erreicht wird. Dafür wird die \definitionCallout{erweiterte Übergangsfunktion $\hat{\delta}$} definiert.\\

\begin{exmp}{231delta2} 
	Für die reguläre Sprache $L=\{ab^n|n\geq 0\}$ und den zuvor definierten \ac{DEA} gilt dann beispielsweise für das Wort $w=abb$:
	\begin{align*}
		\hat{\delta} (q_0, w) &= \hat{\delta} (q_0, abb) = \delta(\hat{\delta} (q_0,ab),b)=\delta(\delta(\hat{\delta}(q_0,a),b),b) = \delta(\delta(\delta(\delta(q_0,\epsilon),a),b),b) \\
		&= \delta(\delta(\delta(q_0,a),b),b) =\delta(\delta(q_1,b),b)=\delta(q_1,b)\\
		&=q_1
	\end{align*}
	Da $q_1 \in F$, erreicht der Automat einen akzeptierenden Zustand und das Wort ist wie erwartet Teil der Sprache des definierten \ac{DEA}. 
\end{exmp}
\newpassage
 In Abschnitt \ref{sec:22ReguläreSprachen} wurde angegeben, dass ein endlicher Automat durch Einlesen eines Eingabewortes entscheiden kann, ob dieses Teil der zugehörigen regulären Sprache ist. Mit den in diesem Abschnitt definierten Begriffen kann diese Aussage umformuliert werden. Die Sprache eines DEA ist die Menge an Worten w, die vom Startzustand $q_0$ ausgehend in einen akzeptierenden Zustand $q_i \in F$ führen. 
\begin{flalign*}\label{eq:231spracheDEA}
&&&	\hspace{22mm} L(A) = \{w \ | \ \hat{\delta}(q_0,w) \in F\} & \text{\cite[79]{Hopfcroft.2011}}
\end{flalign*} 
Im Umkehrschluss sind all die Worte, für die $\hat{\delta}(q_0,w)$ nicht in einen akzeptierenden Zustand führt, nicht Teil der regulären Sprache.

\subsection{Notationsformen}\label{ssec:232Notation}
Die bisherige Darstellungsmöglichkeit eines Automaten beschränkt sich auf eine Tupel-Notation, bei welcher es, wie in Beispiel \ref{exmp:231delta1} zu sehen, dem Betrachter bereits bei wenigen Zuständen schwer fällt, den Automaten als Ganzes zu erfassen. Aus diesem Grund werden nun drei alternative Notationsformen vorgestellt, welche in dieser Arbeit genutzt werden. 

\subsubsection*{Die Übergangstabelle}
Die \definitionCallout{Übergangstabelle} sammelt alle Informationen des 5-Tupels in einer Tabelle, wobei die Eingabesymbole des Alphabets in der ersten Zeile, die Zustände in der ersten Spalte aufgelistet sind. Akzeptierende Zustände werden hier mit einem ${}^*$ gekennzeichnet, der Startzustand mit einem $\rightarrow$ \cite[75]{Hopfcroft.2011}. In \reftab{tab:232übergangstabelle} ist eine solche Übergangstabelle für $L_1=\{ab^n|n\geq 0\}$ abgebildet.
Wenn die Übergangstabelle nur zur Darstellung der Übergangsfunktion dient, werden die Informationen zu Start- und akzeptierenden Zuständen weggelassen. 

\subsubsection*{Die Übergangsmatrix}
Eine weitere Darstellungsform für die Übergangsfunktion orientiert sich an den in Abschnitt \ref{sec:21Graphentheorie} beschriebenen Adjazenzmatrizen. Anstatt anzuzeigen, ob eine Kante (hier ein Übergang) existiert oder nicht, gibt jeder Eintrag $a_{ij}$ der \definitionCallout{Übergangsmatrix} die Menge an Eingabesymbolen an, mit welchen aus dem \definitionCallout{Ausgangszustand} $q_i$ in den \definitionCallout{Endzustand} $q_j$ gewechselt werden kann. In \reffig{fig:232Adjazenzmatrix} ist die Übergangsmatrix für $L_1=\{ab^n|n\geq 0\}$ zu sehen.
\input{images/232Übergangstabelle+matrix}

\subsubsection*{Das Übergangsdiagramm}
Die wohl am häufigsten genutzte Darstellungsform eines endlichen Automaten $A = \{Q, \Sigma, \delta, q_0, F\}$ ist das \definitionCallout{Übergangsdiagramm}. Dabei handelt es sich um einen bewerteten Digraph (siehe Abschnitt \ref{sec:21Graphentheorie}) mit folgenden Eigenschaften: 
\begin{itemize}
	\item Jeder Zustand $q_i \in Q$ wird durch einen Knoten dargestellt.
	\item Für jeden Übergang $\delta(q_i,a) = q_j$ mit $a\in\Sigma$ wird eine mit dem Eingabesymbol $a$ beschriftete Kante erstellt. Diese führt aus dem Knoten $q_i$ in den Knoten $q_j$. 
	\item Der Startzustand ist durch einen eingehenden Pfeil gekennzeichnet.
	\item Jeder akzeptierende Zustand $q_i \in F$ wird mit einer doppelten Kreislinie gezeichnet \cite[74]{Hopfcroft.2011}. 
\end{itemize}

\begin{exmp}{232übergangsdiagramm}
	In \reffig{fig:232Übergangsdiagramm} ist als abschließende Ergänzung zu den bisherigen Notationsformen ein Übergangsdiagramm für $L_1=\{ab^n|n\geq 0\}$ dargestellt. Durch die graphische Darstellungsform lässt sich mehr noch als zuvor nachvollziehen, dass der Automat nur mit Folgen wie ${a, ab, abb, abbb,...}$ im akzeptierenden Zustand $q_1$ hält. ${b, bb, aba}$ beispielsweise führen stets zu dem nicht akzeptierenden Zustand $q_2$ und sind daher nicht Teil der Sprache. Selbiges gilt auch für das leere Wort $\epsilon$, für welches der Automat im nicht akzeptierenden Startzustand $q_0$ hält. 
\end{exmp}
\input{images/232Übergangsdiagramm}

\subsection{Der Nichtdeterministische Endliche Automat}\label{ssec:233NEA}
Ein endlicher Automat wird als deterministisch bezeichnet, wenn aus jedem Zustand für jedes Eingabesymbol ein eindeutig definierter Zustandswechsel erfolgt. 
Bei einem \definitionCallout{Nichtdeterministischen Endlichen Automaten} ist dies nicht der Fall. 

\begin{exmp}{233NEA1}
	In \reffig{fig:233NEA1} ist ein möglicher NEA zu sehen, welcher die reguläre Sprache $L_1=\{ab^n|n\geq 0\}$akzeptiert. Die Vorgaben zum Determinismus sind hier gleich mehrfach verletzt. Auffällig ist zunächst, dass die Übergangsfunktion, welche den Zustandswechsel von $q_0$ für das Eingabesymbol $a$ definiert, kein eindeutiges Ergebnis liefert.
	\begin{align*}
		\delta(q_0,a) = \{q_1,q_2\}	
	\end{align*} 
	Zudem ist die Übergangsfunktion für mehrere Eingabepaare wie etwa $\delta(q_0,b)$ undefiniert. Dennoch akzeptiert der NEA dieselbe Sprache wie der in Beispiel \ref{exmp:231delta1} definierte DEA.
\end{exmp}
\input{images/233NEA1}

\subsubsection*{Die Sprache des NEA}
Ein NEA unterscheidet sich von einem DEA allein durch den Typ des Rückgabewerts der Übergangsfunktion $\delta$. 
\begin{flalign*}
	&&&  \hspace{22mm} \delta: Q \times \Sigma \rightarrow 2^Q && \text{\cite[67]{RaywardSmith.1995}}
\end{flalign*}
Bei einem NEA wird ein Element der Potenzmenge von $Q$ zurückgegeben, was einem, mehreren oder auch keinem Zustand entsprechen kann. Bei einem DEA wird genau ein Zustand zurückgegeben\cite[85]{Hopfcroft.2011}. Die Sprache eines NEA ist damit die Menge an Worten $w$, die vom Startzustand $q_0$ ausgehend in  einen akzeptierenden Zustand $q_i \in F$ führen. Dabei hat es keine Auswirkungen, wenn der Automat mit demselben Eingabewort durch andere Übergänge in keinem Zustand oder einem nicht akzeptierenden endet.
\begin{flalign*}
&&&	\hspace{22mm} L(A) = \{w \ | \ \hat{\delta}(q_0,w) \cap F \neq \emptyset\}&& \text{\cite[87]{Hopfcroft.2011}}
\end{flalign*} 
\noindent Im Vergleich zur Sprache des DEA sind all die Worte nicht Teil der regulären Sprache des NEA, für die kein Pfad der Zustandsänderungen in einem akzeptierenden Zustand endet.

\subsubsection*{Der Epsilon-NEA}
Ein Endlicher Automat, wie er bisher definiert wurde, führt eine Zustandsänderung ausschließlich durch die Eingabe eines Symbols $a \in \Sigma$ durch. Ein spontaner Wechsel eines Automaten von einem Zustand $q_i$ in einen anderen Zustand $q_j$, ohne ein Eingabesymbol eingelesen zu haben, ist somit nicht möglich. Diese Einschränkung soll mit der Einführung des \definitionCallout{\ac{eNEA}} beseitigt werden. Eine spontane Zustandsänderung kann als Übergang durch \enquote{Eingabe} der leeren Zeichenreihe $\epsilon$ betrachtet werden. In \reffig{fig:233eNEA1} ist ein Automat mit einem solchen $\epsilon$-Übergang dargestellt.

\begin{exmp}{233eNEA1}
	$q_0$ kann zunächst mit $\delta(q_0,a)$ nur in den nicht akzeptierenden Zustand $q_1$ wechseln. Das Wort $a$ wird dennoch durch den Automaten akzeptiert, da ein spontaner Zustandswechsel durch den $\epsilon$-Übergang $\delta(q_1, \epsilon)$ in den Zustand $q_2$ möglich ist. Damit führt ein Einlesen des Worts ohne Konkatenation mit weiteren Eingabesymbolen aus $\Sigma$ zu einem akzeptierenden Zustand und $a$ ist Teil der Sprache des Automaten. 	
\end{exmp}
\input{images/233ENEA1.tex}

\noindent Formal unterscheidet sich der eNEA allein durch die Definition der Übergangsfunktion vom NEA. Die Definitionsvorschrift für $\delta$ lautet
\begin{flalign*}
	&&& \hspace{22mm} \delta: Q \times (\Sigma \cup \{\epsilon\}) \rightarrow 2^Q && \text{\cite[85]{RaywardSmith.1995}}
\end{flalign*}
\noindent Eine valide Eingabe ist damit neben den Elementen des Alphabets  auch das leere Wort $\epsilon$, welches selbst nicht als Eingabesymbol in $\Sigma$ enthalten ist.

\subsection{Äquivalenz endlicher Automaten}\label{ssec:234Äquivalenz}
In diesem Abschnitt wurden für ein und dieselbe reguläre Sprache $L$ bereits mehrere unterschiedliche Automaten definiert. Man hätte noch unendlich viele weitere \acp{DEA}, \acp{NEA} und \acp{eNEA} definieren können, die alle dieselbe Sprache akzeptieren. Ein weiterer solcher DEA ist beispielsweise in \reffig{fig:234DEA1} zu sehen.
\input{images/234DEA1.tex}

\noindent Durch das Hinzufügen weiterer Zustände und Übergänge fällt es dem Betrachter zunehmend schwerer, den Automaten als Ganzes zu erfassen. Dies gilt insbesondere dann, wenn die zugehörige reguläre Sprache komplexer ist als bisher beschrieben. Ziel ist es daher, einen möglichst kleinen DEA zu finden, der als \enquote{Stellvertreter} für alle anderen Endlichen Automaten fungiert, welche dieselbe reguläre Sprache $L$ akzeptieren. Ein solcher DEA wird \definitionCallout{minimaler DEA} genannt. 

\noindent Um einen minimalen DEA zu erhalten wird für einen Automaten $A$ der regulären Sprache $L$ eine Äquivalenzrelation $R_A$ definiert, wobei zwei Worte $w_1,w_2 \in \Sigma^*$ derselben Äquivalenzklasse $[x]$ angehören, wenn sie ausgehend vom Startzustand $q_0$ in denselben Zustand führen: 
\begin{flalign*}
	&&& \hspace{22mm}	w_1R_Aw_2 ~ \Leftrightarrow ~ \hat{\delta}(q_0,w_1) = \hat{\delta}(q_0,w_2) && \text{\cite[94]{RaywardSmith.1995}}
\end{flalign*}  
\noindent Somit kann jedem Zustand $q_i \in Q$ eine Äquivalenzklasse aus $R_A$ zugeordnet werden.
\begin{exmp}{234äquivalenzklassen}
	In \reffig{fig:234DEA1} ergeben sich folgende Äquivalenzklassen nach den entsprechenden Zuständen: 
	\begin{align*}
		[q_0] &= \{\epsilon\}\\
		[q_1] &= \{a\}\\
		[q_2] &= \{ab, abb, abbb,...\}\\
		[q_3] &= \{b,aa,bb,aaa,aab,aba,baa,bab,abba,...\}
	\end{align*}
\end{exmp}

\noindent Zwei Zustände $q_i, q_j$ werden zusätzlich \definitionCallout{nicht unterscheidbar} genannt, wenn ausgehend von diesen Zuständen alle möglichen Folgen an Eingabesymbolen gleichermaßen entweder in akzeptierende oder nicht akzeptierende Zustände führen. 
\begin{flalign*}
	&&& \hspace{22mm} \forall w \in \Sigma^*: \hat{\delta}(q_i,w) \in F \Leftrightarrow \hat{\delta}(q_j, w) \in F && \text{\cite[95]{RaywardSmith.1995}}
\end{flalign*} 
\noindent Daraus folgt mit Eingabe des leeren Worts $\epsilon$ sofort, dass zwei Zustände in jedem Fall unterscheidbar sind, wenn genau einer von beiden akzeptierend ist.\\

\begin{exmp}{234unterscheidbar}
	In \reffig{fig:234DEA1} sind zur Untersuchung auf Unterscheidbarkeit nur die Zustandspaare $q_0, q_3$ (nicht akzeptierend) und $q_1,q_2$ (akzeptierend) zu betrachten.
	\begin{align*}
		%		\intertext{$q_0$ und $q_3$ sind  unterscheidbar:}
		\hat{\delta}(q_0,a) = q_1 \text{\textcolor{red}{\textbf{$\in$}}} F  \text{~~~~und~~~~} &{\hat{\delta}(q_3,a) = q_3 \text{\textcolor{red}{\textbf{$\notin$}}} F}\\
		~~~~\Rightarrow ~~~~ &\text{$q_0$ und $q_3$ sind unterscheidbar} \\
		\\
		\hat{\delta}(q_1,a) = q_3 \text{\textcolor{darkgreen}{\textbf{$\notin$}}} F \text{~~~~und~~~~} &{\hat{\delta}(q_2,a) = q_3 \text{\textcolor{darkgreen}{\textbf{$\notin$}}} F}\\
		\hat{\delta}(q_1,b) = q_2 \text{\textcolor{darkgreen}{\textbf{$\in$}}} F  \text{~~~~und~~~~}&{\hat{\delta}(q_2,b) = q_2 \text{\textcolor{darkgreen}{\textbf{$\in$}}} F}\\
		\hat{\delta}(q_1,ba) = q_3 \text{\textcolor{darkgreen}{\textbf{$\notin$}}} F \text{~~~~und~~~~} &{\hat{\delta}(q_2,ba) = q_3 \text{\textcolor{darkgreen}{\textbf{$\notin$}}} F}\\
		...~~~~\Rightarrow ~~~~ &\text{$q_1$ und $q_2$ sind nicht unterscheidbar} 
	\end{align*}
\end{exmp}

\noindent Ein minimaler DEA besitzt keine in diesem Sinne nicht unterscheidbaren Zustände mehr und die zu ihm zugehörige Äquivalenzrelation besitzt damit die kleinstmögliche Anzahl an Äquivalenzklassen \cite[201\psq]{Hopfcroft.2011}. \\

\begin{exmp}{234mindea}
	Der bereits in \reffig{fig:232Übergangsdiagramm} abgebildete Automat entspricht mit drei Zuständen dem zu der Sprache $L_1=\{ab^n|n\geq 0\}$ zugehörigen minimalen DEA.
\end{exmp}