package generation;

import java.util.ArrayList;
import java.util.List;

import commons.Automaton;
import commons.State;
import commons.Terminal;
import commons.TerminalType;

public class AutomatonGenerator {

	public Automaton generateAutomatonWithBitstreams() {
		return generateAutomatonWithBitstreams(GenerationDefaultSetting.NUMBER_OF_STATES_SEQUENCE,
				GenerationDefaultSetting.PROBABILITY_TRANSITION_EXISTS_TRANSFORMATION,
				GenerationDefaultSetting.MINIMUM_EPSILON, GenerationDefaultSetting.MAXIMUM_EPSILON,
				GenerationDefaultSetting.SEQUENCE_COUNT, GenerationDefaultSetting.MINIMUM_TRANSITIONS_SEQUENCE,
				GenerationDefaultSetting.MAXIMUM_TRANSITIONS_SEQUENCE);
	}

	public Automaton generateAutomatonWithBitstreamsOnlyWithEpsilon() {
		return generateAutomatonWithBitstreams(GenerationDefaultSetting.NUMBER_OF_STATES_EPSILON,
				GenerationDefaultSetting.PROBABILITY_TRANSITION_EXISTS_TRANSFORMATION,
				GenerationDefaultSetting.MINIMUM_EPSILON, GenerationDefaultSetting.MAXIMUM_EPSILON, 0,
				GenerationDefaultSetting.MINIMUM_TRANSITIONS_EPSILON,
				GenerationDefaultSetting.MAXIMUM_TRANSITIONS_EPSILON);
	}

	public Automaton generateAutomatonWithBitstreamsOnlyWithEpsilon(int minEpsilon, int maxEpsilon) {
		return generateAutomatonWithBitstreams(GenerationDefaultSetting.NUMBER_OF_STATES_EPSILON,
				GenerationDefaultSetting.PROBABILITY_TRANSITION_EXISTS_TRANSFORMATION, minEpsilon, maxEpsilon, 0,
				GenerationDefaultSetting.MINIMUM_TRANSITIONS_EPSILON,
				GenerationDefaultSetting.MAXIMUM_TRANSITIONS_EPSILON);
	}

	public Automaton generateAutomatonWithBitstreamsOnlyWithEpsilon(int numberOfStates,
			double probabilityTransitionExists) {
		return generateAutomatonWithBitstreams(numberOfStates, probabilityTransitionExists,
				GenerationDefaultSetting.MAXIMUM_EPSILON, GenerationDefaultSetting.MAXIMUM_EPSILON, 0,
				GenerationDefaultSetting.MINIMUM_TRANSITIONS_EPSILON,
				GenerationDefaultSetting.MAXIMUM_TRANSITIONS_EPSILON);
	}

	public Automaton generateAutomatonWithBitstreamsOnlyWithEpsilon(int numberOfStates,
			double probabilityTransitionExists, int minEpsilon, int maxEpsilon) {
		return generateAutomatonWithBitstreams(numberOfStates, probabilityTransitionExists, minEpsilon, maxEpsilon, 0,
				GenerationDefaultSetting.MAXIMUM_TRANSITIONS_EPSILON,
				GenerationDefaultSetting.MINIMUM_TRANSITIONS_EPSILON);
	}

	public Automaton generateAutomatonWithBitstreamsOnlyWithSequence(int sequenceCount) {
		return generateAutomatonWithBitstreams(GenerationDefaultSetting.NUMBER_OF_STATES_SEQUENCE,
				GenerationDefaultSetting.PROBABILITY_TRANSITION_EXISTS_SEQUENCE, 0, 0, sequenceCount,
				GenerationDefaultSetting.MAXIMUM_TRANSITIONS_SEQUENCE,
				GenerationDefaultSetting.MINIMUM_TRANSITIONS_SEQUENCE);
	}

	public Automaton generateAutomatonWithBitstreamsOnlyWithSequence(int numberOfStates,
			double probabilityTransitionExists, int sequenceCount) {
		return generateAutomatonWithBitstreams(numberOfStates, probabilityTransitionExists, 0, 0, sequenceCount,
				GenerationDefaultSetting.MAXIMUM_TRANSITIONS_SEQUENCE,
				GenerationDefaultSetting.MINIMUM_TRANSITIONS_SEQUENCE);
	}

	public Automaton generateAutomatonWithBitstreams(int minEpsilon, int maxEpsilon, int inputSequenceCount) {
		List<State> states = createStates(GenerationDefaultSetting.NUMBER_OF_STATES_BITSTREAM);
		List<Terminal> terminals = createAlphabet();
		BitstreamGeneration generation = new BitstreamGeneration(states, terminals,
				GenerationDefaultSetting.PROBABILITY_TRANSITION_EXISTS_BITSTREAM,
				GenerationDefaultSetting.PROBABILITY_EPSILON_TRANSITION, inputSequenceCount,
				GenerationDefaultSetting.MAXIMUM_OF_ACCEPTING_STATES,
				GenerationDefaultSetting.END_STATE_OF_EPSILON_IS_ACCEPTING,
				GenerationDefaultSetting.MAXIMUM_TRANSITIONS, GenerationDefaultSetting.MINIMUM_TRANSITIONS, maxEpsilon,
				minEpsilon, GenerationDefaultSetting.USE_MIN_MAX_VALUES);
		return generation.getAutomaton();
	}

	public Automaton generateAutomatonWithBitstreams(int numberOfStates, double probabilityTransitionExists,
			int minEpsilon, int maxEpsilon, int inputSequenceCount, int minNeaTransitions, int maxNeaTransitions) {
		List<State> states = createStates(numberOfStates);
		List<Terminal> terminals = createAlphabet();
		BitstreamGeneration generation = new BitstreamGeneration(states, terminals, probabilityTransitionExists,
				GenerationDefaultSetting.PROBABILITY_EPSILON_TRANSITION, inputSequenceCount,
				GenerationDefaultSetting.MAXIMUM_OF_ACCEPTING_STATES,
				GenerationDefaultSetting.END_STATE_OF_EPSILON_IS_ACCEPTING, maxNeaTransitions, minNeaTransitions,
				maxEpsilon, minEpsilon, GenerationDefaultSetting.USE_MIN_MAX_VALUES);
		return generation.getAutomaton();
	}

	public Automaton generateAutomatonWithMatrix() {
		List<State> states = createStates(GenerationDefaultSetting.NUMBER_OF_STATES_MATRIX);
		List<Terminal> terminals = createAlphabet();
		MatrixGeneration generation = new MatrixGeneration(states, terminals,
				GenerationDefaultSetting.MAX_NUMBER_OF_TRANSITIONS,
				GenerationDefaultSetting.USE_MAX_NUMBER_OF_TRANSITIONS,
				GenerationDefaultSetting.NUMBER_OF_ACCEPTING_STATES);
		return generation.getAutomaton();
	}

	private List<State> createStates(int numberOfStates) {
		List<State> states = new ArrayList<>();
		for (int i = 0; i < numberOfStates; i++) {
			states.add(new State("q" + i, false, false));
		}
		return states;
	}

	private List<Terminal> createAlphabet() {
		List<Terminal> terminals = new ArrayList<>();
		for (int i = 0; i < GenerationDefaultSetting.NUMBER_OF_TERMINALS; i++) {
			char c = (char) (97 + i);
			Terminal terminal = new Terminal(TerminalType.SINGLE, c);
			terminals.add(terminal);
		}
		return terminals;
	}
}
