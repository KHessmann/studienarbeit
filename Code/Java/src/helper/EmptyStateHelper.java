package helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import commons.Automaton;
import commons.State;
import commons.Terminal;
import commons.TerminalType;
import commons.Transition;

public class EmptyStateHelper {

	public static Automaton addEmptyStateToAutomaton(Automaton oldAutomaton) {
		Automaton automaton = new Automaton(oldAutomaton);
		State emptyState = State.createEmptyState();
		Map<String, Transition> transitionMap = new HashMap<>();
		Map<Terminal, List<State>> deadStates = new HashMap<>();
		Set<Terminal> singleTerminals = automaton.getAlphabet().stream()
				.filter(terminal -> terminal.getType() == TerminalType.SINGLE).collect(Collectors.toSet());
		for (Terminal t : singleTerminals) {
			deadStates.put(t, new ArrayList<State>(automaton.getStates()));
		}
		for (Transition transition : automaton.getTransitions()) {
			for (Terminal terminal : transition.getTerminals()) {
				if (terminal.getType() == TerminalType.SINGLE) {
					deadStates.get(terminal).remove(transition.getStartState());
				}
			}
		}
		for (Terminal t : deadStates.keySet()) {
			for (State start : deadStates.get(t)) {
				String name = start.getName() + ", " + emptyState.getName();
				Transition transitionForStates = transitionMap.computeIfAbsent(name,
						key -> new Transition(t, start, emptyState, false));
				transitionForStates.addTerminal(t);
			}
		}
		if (!transitionMap.values().isEmpty()) {
			automaton.addState(emptyState);
			automaton.addAllTransitions(new ArrayList<>(transitionMap.values()));
			Transition transition = new Transition(singleTerminals, emptyState, emptyState, false);
			automaton.addTransition(transition);
		}
		return automaton;
	}
}
