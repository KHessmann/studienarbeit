package helper;

import java.util.HashMap;
import java.util.Map;

import commons.Automaton;
import commons.State;
import commons.Terminal;
import commons.TerminalType;
import commons.Transition;

public class AutomatonStringBuildHelper {

	public static Automaton buildAutomatonWithString(String input) {
		Automaton a = new Automaton();
		String[] split = input.split("#states|#initial");
		if (split.length != 3) {
			throw new IllegalArgumentException("String should include keywords #states and #initial");
		}
		setStates(split[1], a);
		input = split[2];

		String[] split2 = input.split("#accepting");
		if (split2.length != 2) {
			throw new IllegalArgumentException("String should include keyword #accepting");
		}
		setInitialProperty(split2[0], a);
		input = split2[1];

		String[] split3 = input.split("#alphabet");
		if (split3.length != 2) {
			throw new IllegalArgumentException("String should include keyword #alphabet");
		}
		setAcceptingProperty(split3[0], a);
		input = split3[1];

		String[] split4 = input.split("#transitions");
		if (split4.length != 2) {
			throw new IllegalArgumentException("String should include keyword #transitions");
		}
		setAlphabet(split4[0], a);
		setTransitions(split4[1], a);

		return a;
	}

	private static void setTransitions(String string, Automaton a) {
		Map<String, Transition> transitionMap = new HashMap<>();
		string = removeBlanks(string);
		for (String str : string.split(";")) {
			State start = null;
			State end = null;
			Terminal terminal = null;
			boolean isEpsilon = false;
			String[] split = str.split(":");
			String[] split2 = split[1].split(">");
			if (split.length != 2 || split2.length != 2)
				throw new IllegalArgumentException("Transition " + str + " doesn't follow the form s1:t>s2!");
			for (State state : a.getStates()) {
				if (state.getName().equals(split[0])) {
					start = state;
				}
				if (state.getName().equals(split2[1])) {
					end = state;
				}
				if (end != null && start != null) {
					break;
				}
			}
			for (Terminal t : a.getAlphabet()) {
				if (t.toString().equals(split2[0])) {
					terminal = t;
					if (t.getType().equals(TerminalType.EPSILON))
						isEpsilon = true;
				}
			}
			if (terminal != null && start != null && end != null) {
				boolean eps = isEpsilon;
				Terminal t1 = terminal;
				State s = start;
				State e = end;
				String name = start.getName() + ", " + end.getName();
				Transition transitionForStates = transitionMap.computeIfAbsent(name,
						key -> new Transition(t1, s, e, eps));
				transitionForStates.addTerminal(t1);
				a.addTransition(transitionForStates);

			}
		}
	}

	private static void setAlphabet(String string, Automaton a) {
		string = removeBlanks(string);
		for (String s : string.split(";")) {
			char[] chars = s.toCharArray();
			if (chars.length == 0)
				continue;
			if (chars.length > 1)
				a.addTerminal(new Terminal(TerminalType.SEQUENCE, chars));
			else if (chars[0] == (Terminal.epsilon))
				a.addTerminal(new Terminal(TerminalType.EPSILON, chars[0]));
			else
				a.addTerminal(new Terminal(TerminalType.SINGLE, chars[0]));
		}

	}

	private static void setAcceptingProperty(String string, Automaton a) {
		string = removeBlanks(string);
		for (String s : string.split(";")) {
			for (State state : a.getStates()) {
				if (state.getName().equals(s)) {
					state.setAccepting(true);
					a.addAccepting(state);
				}
			}
		}
	}

	private static void setInitialProperty(String string, Automaton a) {
		string = removeBlanks(string);
		for (String s : string.split(";")) {
			for (State state : a.getStates()) {
				if (state.getName().equals(s)) {
					state.setInitial(true);
					a.setInitial(state);
				}
			}
		}
	}

	private static void setStates(String string, Automaton a) {
		string = removeBlanks(string);
		for (String s : string.split(";")) {
			a.addState(new State(s, false, false));
		}
	}

	private static String removeBlanks(String input) {
		return input.replaceAll("\\n|\\r|\\s|\\b", "");
	}

}
